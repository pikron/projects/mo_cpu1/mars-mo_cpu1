/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  system_def.h - definition of MARS system peripheral address map
                 and conditional components inclusion
 
  Copyright (C) 2001-2003 by Pavel Pisa - originator 
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#ifndef _SYSTEM_DEF_H_
#define _SYSTEM_DEF_H_

#include <stdint.h>

#define VER_CODE(major,minor,patch) (major*0x10000+minor*0x100+patch)
/* Software version */
#define SW_VER_ID	"MARS8"
#define SW_VER_MAJOR	1
#define SW_VER_MINOR	0
#define SW_VER_PATCH	0
#define SW_VER_CODE	VER_CODE(SW_VER_MAJOR,SW_VER_MINOR,SW_VER_PATCH)
/* Hardware version */
#define HW_VER_ID	"MO_CPU1"
#define HW_VER_MAJOR	0
#define HW_VER_MINOR	2
//#define HW_VER_MINOR	1
#define HW_VER_PATCH	0
#define HW_VER_CODE	VER_CODE(HW_VER_MAJOR,HW_VER_MINOR,HW_VER_PATCH)
/* Version of mechanical  */
#define MECH_VER_ID	"MARS8"
#define MECH_VER_MAJOR  1
#define MECH_VER_MINOR  2
#define MECH_VER_PATCH  0
#define MECH_VER_CODE	VER_CODE(MECH_VER_MAJOR,MECH_VER_MINOR,MECH_VER_PATCH)

#ifndef STRINGIFY
#define STRINGIFY(x) STRINGIFY1(x)
#define STRINGIFY1(x) #x
#endif

#define CPU_REF_HZ  4000000	/* reference clock frequency      */
#define CPU_SYS_HZ 21000000	/* default system clock frequency */

int cpu_ref_hz;			/* actual external XTAL reference */
int cpu_sys_hz;			/* actual system clock frequency  */

#define ISR_SIM_IARB   0xf

#define ISR_QSM_IARB   0xe
#define ISR_QSM_ILSCI  5
#define ISR_QSM_INTV   0x60	/* 2x .. SCI, SPI */

/* PWM and sampling frequency */
#define WITH_SFI_SEL		/* enable selection of sampling frequency */
#define ISR_CTM4_IARB  0xd
#define ISR_CTM4_IL    3
#define ISR_CTM4_INTV  0x80	/* 64x .. 13 used */
#define ISR_CTM4_SFI_INTV (0x80+2)  /* MCSM2 */
#define ISR_CTM4_SFI_IL   4
#define CTM4_SFI_HZ    1000	/* MCSM2 sampling frequency */
unsigned ctm4_sfi_hz;		/* actual sampling frequency */
volatile unsigned msec_time;	/* system time in msec units */
#define CTM4_PWM_PDIR0	(volatile uint8_t * const)(0xf87000)
#define CTM4_PWM_PDIR1	(volatile uint8_t * const)(0xf87001)
/* BigBot specific */
#define BIGB_PWR_CTRL	(volatile uint8_t * const)(0xf87002)
#define   BIGB_PWR_CTR_WDG1	0x01	/* Togle to enable PWM */
#define   BIGB_PWR_CTR_WDG2	0x02	/* Togle to enable PWM in Limits */
#define   BIGB_PWR_CTR_MAINS	0x10	/* Togle to enable mains */
#define   BIGB_PWR_CTR_BREAK	0x20	/* Break release */
volatile char bigb_pwr_ctr_shad;
#define BIGB_REF_SW	(volatile uint8_t * const)(0xf87004)
#define   BIGB_REF_SW_EMLIM	0x40	/* Emergency limit switch - L */
#define   BIGB_REF_SW_PWRSEN	0x80	/* Power sense  - H */
int mo_psys_mode;	/* mode of psys hardware */
#define PSYS_MODE_BIGB      1	/* BigBot mode, power control LEDs etc */
#define PSYS_MODE_BIGB_MARK 2	/* BigBot reference marks sources for A-D */
#define PSYS_MODE_BIGB_EXTC 4	/* Allow external control mode */
short mo_psys_pwr_flg;	/* flags of requests for power system */
#define PSYS_PWR_ON    1	/* enables output bridges */
#define PSYS_PWR_LIM   2	/* enables bridges even for limit condition */
#define PSYS_PWR_BREAK 4	/* controls breaking of axis 4 (D) */
short mo_psys_pwr_mains;/* main power suply on request */
short mo_extmod_fl;     /* running in external control mode */


/* TouCAN subsystem configuration */
#define ISR_CAN_IARB  0xc
#define ISR_CAN_IL    6
#define ISR_CAN_INTV  0xa0	/* 32x .. 19 used */

/* US Digital IRC inputs */
#define USD_IRC_ADDR  ((volatile uint8_t * const)(0xf88000))
#define USD_IRC_GATE  ((volatile uint8_t * const)(0xf88020))
#define USD_IRC_MARK0 ((volatile uint8_t * const)(0xf88028))
#define USD_IRC_MARK1 ((volatile uint8_t * const)(0xf88030))

/* queued ACD module */
#define ISR_QADC_IARB	0xb

/* IIC interface PCD8584 */
#define PCD_8584_PORT0 (volatile uint8_t * const)(0xf10000)
#define PCD_8584_PORT1 (volatile uint8_t * const)(0xf10001)
#define PCD_8584_CSOR  SIM_CSOR4

/* Keyboard KL41 */
#define KL41_LCD_INST  (volatile uint8_t * const)(0xf89000)
#define KL41_LCD_STAT  (volatile uint8_t * const)(0xf89001)
#define KL41_LCD_WDATA (volatile uint8_t * const)(0xf89002)
#define KL41_LCD_RDATA (volatile uint8_t * const)(0xf89003)
#define KL41_LED_WR    (volatile uint8_t * const)(0xf89001)
#define KL41_KBD_WR    (volatile uint8_t * const)(0xf89003)
#define KL41_KBD_RD    (volatile uint8_t * const)(0xf89004)

#if 1
/* SGM Small graphics LCD module 240x64 */
#define SGM_LCD_DATA  (volatile uint8_t * const)(0xf89000)
#define SGM_LCD_CMD   (volatile uint8_t * const)(0xf89001)
#define SGM_LCD_STAT  (volatile uint8_t * const)(0xf89001)
/* Keyboard on MO_KBD1 */
#define SGM_KBDI      (volatile uint8_t * const)(0xf89002)
#define SGM_KBDO      (volatile uint8_t * const)(0xf89002)
#endif

#if 1
/* USB module with PDIUSBD12 */
#define ISR_USB_INTV  0x1a  /* int.vector 26(0x1a) (addr. 0x68)*/
#define PDIUSB_COMMAND_ADDR (volatile uint8_t * const)(0xf8A001)
#define PDIUSB_READ_DATA_ADDR (volatile uint8_t * const)(0xf8A000)
#define PDIUSB_WRITE_DATA_ADDR (volatile uint8_t * const)(0xf8A000)
#define PDIUSB_TEST_IRQ()      (!(*SIM_PORTF0 & 4))
#endif

/* Enable RS232 GDB debugging */
#define WITH_GDBSTUB
#define WITHOUT_GDBSTUB_HOOKS
#define GDBSTUB_DATA_SECT __attribute((section (".gdbdata")))
/* requires WITH_GDBSTUB and WITHOUT_GDBSTUB_HOOKS */
/* enables to download and test application uder FLASH serial stub */
#undef GDBSTUB_RAM_TARGET

/* RS232 defaults */
#define WITH_RS232_ECHO_OFF
#define WITH_RS232_BAUDRATE 19200

/* Enable digital IO */
#define WITH_DIO	/* enable digital IO */
#define WITH_TPU_TRIG	/* TPU 0 and 1 trigger source */
#define WITH_TRIG_DENOISE /* enable triggers denoising */
//#define WITH_BRAKE_CTRL   /* enable CRS brake control */
//#define WITH_ANALOG_AXIS  /* enable CRS analog gripper support (ADC10) */
#define DIO_CMP_COUNT  12
#define DIO_TRIG_COUNT 12

/* Extended comparators and other features */
#define WITH_CMPQUE	/* compile support for ordered comparators queues */

#endif /* _SYSTEM_DEF_H_ */
