/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  pcd_8584.h - Philips PCD8584 I2C controller definitions
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _PCD8584_H_
#define _PCD8584_H_

#ifndef uchar
  #define uchar unsigned char
#endif

/* PCD8584 Status register S1 */
#define PCD8584_S1_BB	   1
#define PCD8584_S1_LAB	   2
#define	PCD8584_S1_AAS	   4
#define	PCD8584_S1_LRB	   8
#define	PCD8584_S1_BER	   0x10
#define	PCD8584_S1_STS	   0x20
#define	PCD8584_S1_STO	   0x40
/*      PCD8584_S1_PIN     same as below */

/* Control register definitions */
#define PCD8584_S1_ACK	0x01
#define PCD8584_S1_STO	0x02
#define PCD8584_S1_STA	0x04
#define PCD8584_S1_ENI	0x08
#define PCD8584_S1_ES2	0x10
#define PCD8584_S1_ES1	0x20
#define PCD8584_S1_ESO	0x40
#define PCD8584_S1_PIN	0x80

#define	PCD8584_S1_START    0xc5
#define	PCD8584_S1_RESTART  0x45
#define	PCD8584_S1_STOP	    0xc3
#define PCD8584_S1_IDLE     0xc1
/* PCD8584 Clock div register S2 */
#define PCD8584_S2_BUSSPD   0x03
#define PCD8584_S2_XTALDIV  0x3c
#define PCD8584_S2_BUSSPD90 0x00  /*  90 kHz */
#define PCD8584_S2_BUSSPD45  0x01  /*  45 kHz */
#define PCD8584_S2_BUSSPD11 0x02  /*  11 kHz */
#define PCD8584_S2_BUSSPD1  0x03  /* 1.5 kHz */

/* States of IIC adapter */
#define	PCD8584_OK		0
#define PCD8584_NO_ADAPTER_ERROR 1
#define PCD8584_INTERRUPTED	2
#define PCD8584_CLOCK_ERROR	3
#define	PCD8584_MISPLACED_COND	4
#define	PCD8584_BUS_BUSY	5
#define	PCD8584_NO_ACK		6
#define	PCD8584_TIMEOUT		7
#define	PCD8584_SLVTRN		8
#define	PCD8584_SLVRCV		9
#define PCD8584_STOPCON		10
#define	PCD8584_UNKNOW_MSG	11
#define PCD8584_INPUT_BUFF_FULL 12
#define PCD8584_SLAVE_DISCONNECTED 13
#define	PCD8584_ADDRESSED_AS_SLV 14
#define PCD8584_ERROR		15
#define	PCD8584_NO_ERR_NUMBER	16
#define	PCD8584_NO_MSG		17
#define PCD8584_SIZE_BIGGER	18

#endif
