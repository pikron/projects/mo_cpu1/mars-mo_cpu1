/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  pcd_8584.c - Philips PCD8584 I2C controller support routines 
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <cpu_def.h>
#include <sim.h>
#include <string.h>
#include <stdio.h>
#include <utils.h>

#include <pcd_8584.h>

#define PORT0 PCD_8584_PORT0
#define PORT1 PCD_8584_PORT1

void 
pcd_outb(volatile void *port, unsigned char b)
{
  writeb(b,port);
}

unsigned char
pcd_inb(volatile void *port)
{
  return readb(port);
}

void
pcd_wait(int time)
{
  int i;
  for(i=0;i<time;i++){
    __memory_barrier();
  }
}

/*--------------------------------------------------------*/

static unsigned pcd_iic_clock_s2;
static unsigned pcd_iic_slave_addr;


int pcd_iic_init(unsigned slave_addr,int spdeed )
{
  
  pcd_iic_clock_s2=
        __val2mfld(PCD8584_S2_BUSSPD,0)|  /* bus 90 kHz */
	__val2mfld(PCD8584_S2_XTALDIV,4); /* XTAL=4MHz*/

  /* select slower chipselect with 10 waitstates */
  *PCD_8584_CSOR|=SIM_WaitStates_12;
  *PCD_8584_CSOR&=SIM_WaitStates_12 | ~SIM_External;
  
  pcd_wait(100);
  pcd_outb(PORT1,PCD8584_S1_PIN); /* select S0 register */
  pcd_wait(100);
  pcd_outb(PORT0,slave_addr);
  pcd_wait(10);
  if (slave_addr!=pcd_inb(PORT0))
    return PCD8584_NO_ADAPTER_ERROR;
  pcd_iic_slave_addr=slave_addr;

  /* Set Clock Register S2 */
  pcd_outb(PORT1,PCD8584_S1_PIN|PCD8584_S1_ES1); /* select S2 register */
  pcd_wait(10);
  pcd_outb(PORT0,pcd_iic_clock_s2);
  pcd_wait(10);
  if (pcd_iic_clock_s2!=(pcd_inb(PORT0)&0x1f))
    return PCD8584_NO_ADAPTER_ERROR;

  /* Set None Interrupt Vector S3 */
  pcd_outb(PORT1,PCD8584_S1_ES1);/* select S3 register */
  pcd_wait(10);
  pcd_outb(PORT0,0);
  pcd_wait(10);
  if (0!=pcd_inb(PORT0))
    return PCD8584_NO_ADAPTER_ERROR;
  /* delay(1); */
  pcd_outb(PORT1,PCD8584_S1_IDLE); /* Enable serial interface */
  pcd_wait(10);
  return PCD8584_OK;
}

#if 0

void out(unsigned int port,unsigned char value)
{
  asm {  mov dx,port
  	 mov al,value
         out dx,al
  	 shr dx,1
         shl dx,1
         add dx,4
         out dx,al
      }
}

BYTE TestPCadapter(unsigned int portA0)
{
/*
 Function Return:NO_ADAPTER_ERROR
 		 OK
*/

 time_t t;
 unsigned int	portA1=portA0+1,
                i;

 printf("\n -INFO: Test off the IIC Adapter - BasePort 0x%x -- 0x%x",portA0,portA1);
 outportb(portA1,0x80);   //software reset function
 delay(1);

//Testing of own address registr
 printf("\n  Own Address Register Test ");
 outportb(portA1,0);  //select S0 register

 for(i=10; i<0x80; i++)
  {
   if ((i&2)&&(i&8)&&(i&1)) printf(".");

   outportb(portA0,i);

   if (i!=inportb(portA0))
    {
     printf(" BAD");
     return(NO_ADAPTER_ERROR);
    }
  }

 printf(" OK");


//Testing of interrupt vector register S3
 printf("\n  Interrupt Vector Register Test ");
 outportb(portA1,16);  //select S3 register
 delay(1);
 for(i=0; i<0xff; i++)
  {
   if ((i&2)&&(i&8)&&(i&1)&&(i&4)) printf(".");

   outportb(portA0,i);

   if (i!=inportb(portA0))
    {
     printf(" BAD");
     return(NO_ADAPTER_ERROR);
    }
  }
 printf(" OK");

//Testing of clock register S2
 printf("\n  Clock Register Test ");
 outportb(portA1,32);  //select S2 register

 for(i=0; i<32; i++)
  {
   if ((i&2)&&(i&1)) printf(".");
   outportb(portA0,i);
   if (i!=inportb(portA0))
    {
     printf(" BAD");
     return(NO_ADAPTER_ERROR);
    }
  }
 printf(" OK");
 outportb(portA1,0x80);  //Reset the circuit
 delay(1);
 return(OK);
}

//--------------------------------------------------------

BYTE TestPCadapterQuiet(unsigned int portA0)
{
 /* The same function as the TestPCadapter without
    printing messages
 */

 unsigned int	portA1=portA0+1,
                i;

 outportb(portA1,0x80);   //software reset function
 delay(1);

//Testing of own address registr
 outportb(portA1,0);  //select S0 register

 for(i=0; i<0x80; i++)
  {
   outportb(portA0,i);
   if (i!=inportb(portA0))
    {
     return(NO_ADAPTER_ERROR);
    }
  }
//Testing of interrupt vector register S3
 outportb(portA1,16);  //select S3 register

 for(i=0; i<0xff; i++)
  {
   outportb(portA0,i);
   if (i!=inportb(portA0))
    {
     return(NO_ADAPTER_ERROR);
    }
  }
//Test of clock register S2
 outportb(portA1,32);  //select S2 register

 for(i=0; i<32; i++)
  {
   outportb(portA0,i);
   if (i!=inportb(portA0))
    {
     return(NO_ADAPTER_ERROR);
    }
  }
 outportb(portA1,0x80);  //Reset circuit
 delay(1);
 return(OK);
}

//-----------------------------------------------------------

void BityVBytu(int C)
{
 printf (" %x     %x %x %x %x %x %x %x %x    %d \n",C,
 (C&0x80)&&1,(C&0x40)&&1,(C&0x20)&&1,(C&0x10)&&1,
 (C&0x08)&&1,(C&0x04)&&1,(C&0x02)&&1,(C&0x01)&&1,C);
}

//-----------------------------------------------------------

BYTE InitAdapter(unsigned int portA0,unsigned int ownSlaveAddress,
 		unsigned int clockFrequency )
{
/*
 Function Return:NO_ADAPTER_ERROR
 		 OK
*/
 unsigned int	portA1 = portA0+1,
                clockRegister;

 outportb(portA1, 0x80); //Set Off Circuit
 delay(1);
 switch (clockFrequency)
  {
   case 90:	clockRegister = 20;  //6MHz prescaler
                break;
   case 45:     clockRegister = 21;
                break;
   case 11:	clockRegister = 22;
                break;
   case 1:    clockRegister = 23;
                break;
   default:	return(CLOCK_ERROR);
  }

//Set Own Address S0
 outportb(portA1,0);  //select S0 register

 outportb(portA0,ownSlaveAddress);
 if (ownSlaveAddress!=inportb(portA0))
   {
    return(NO_ADAPTER_ERROR);
   }

//Set Clock Register S2
 outportb(portA1,32);  //select S2 register

 outportb(portA0,clockRegister);
 if (clockRegister!=inportb(portA0))
   {
    return(NO_ADAPTER_ERROR);
   }

//Set None Interrupt Vector S3
 outportb(portA1,16);  //select S3 register
 delay(1);
 outportb(portA0, 0);
 if (0!=inportb(portA0))
   {
    return(NO_ADAPTER_ERROR);
   }
 delay(1);
 outportb(portA1, ENABLE);  //Enable serial interface
 delay(1);
 return(OK);
}

//-----------------------------------------------------------

BYTE MStart (unsigned int portA0, unsigned int slaveRcvAddress)
{
/*
Function return: MISPLACED_COND
                 BUS_BUSY
                 INTERRUPTED
                 NO_ACK
                 TIMEOUT
                 OK
*/
 unsigned int	portA1 = portA0+1;
 volatile BYTE	dummy;
 clock_t start;
 int	S1;

 while(kbhit()) getch();

 //testing IIC bus becomes free
 S1 = inportb(portA1);
 if (BER & S1)
 {
  return( MISPLACED_COND );
 }
 if (!(BB & S1)) return( BUS_BUSY );
 outportb(portA0, slaveRcvAddress);
 dummy=1;  //spozdeni kvuli obvodu

 //sending Start condition
 outportb(portA1, START);
 delay(1);
 start = clock()/CLK_TCK;
 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
   if (kbhit())
    {
     return( INTERRUPTED );
    }
   if (((clock()/CLK_TCK) - start) > 2)
    {
     return(TIMEOUT);
    }
  } while (PIN & S1);
 if (LRB & S1)
  {
   return( NO_ACK );
  }
 return(OK);
}

//-----------------------------------------------------------
BYTE DiscardSlvAddr (unsigned int portA0)
{
/*
Function return: OK
*/

 volatile BYTE	dummy;

 dummy = inportb(portA0);
 return(OK);
}

//-----------------------------------------------------------
BYTE MReStart (unsigned int portA0, unsigned int slaveRcvAddress)
{
/*
Function return: MISPLACED_COND
                 INTERRUPTED
                 NO_ACK
                 OK
*/
 unsigned int	portA1 = portA0+1;
 volatile BYTE	dummy;
 BYTE	S1;

 while(kbhit()) getch();
 S1 = inportb(portA1);
 if (BER & S1)
 {
  return( MISPLACED_COND );
 }
 outportb(portA1, RESTART);     //send Start condition
 dummy=1;  //spozdeni kvuli obvodu
 outportb(portA0, slaveRcvAddress);
 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
   if (kbhit())
    {
     return( INTERRUPTED );
    }
  } while (PIN & S1);
 if (LRB & S1)
  {
   return( NO_ACK );
  }
 return(OK);
}

//-----------------------------------------------------------

BYTE MTrnByte(unsigned int portA0, BYTE shield)
{
/*
Function return: MISPLACED_COND
                 NO_ACK
                 OK
*/
 unsigned int	portA1 = portA0+1;
 BYTE	S1;

 outportb(portA0, shield);
 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
/* Slave by nemel zakazovat preruseni, ale nastavovat
   NOT ACK. V opacnem pripade zde zacnu cyklit */
  } while (PIN & S1);
 if (LRB & S1)
  {
   return( NO_ACK );
  }
 return(OK);
}

//-----------------------------------------------------------

BYTE MRcvByte(unsigned int portA0, BYTE *shield)
{
/*
Function return: MISPLACED_COND
                 NO_ACK
                 OK
*/
 unsigned int	portA1 = portA0+1;
 BYTE	S1;

 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
  } while (PIN & S1);
 if (LRB & S1)
  {
   return( NO_ACK );
  }
 *shield = inportb(portA0);
 return(OK);
}

//-----------------------------------------------------------

BYTE MRcvEndRcv(unsigned int portA0, BYTE *shield)
{
/*
Function return: MISPLACED_COND
                 NO_ACK
                 OK
*/
 unsigned int	portA1 = portA0+1;
 BYTE	S1;

 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
  } while (PIN & S1);
 if (LRB & S1)
  {
   return( NO_ACK );
  }


 outportb(portA1,0x40); /* 0xc0 ?? !!!!!!!! */
 *shield = inportb(portA0);

 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
  } while (PIN & S1);

 shield++; /* !!!!!!!!!!!!!!! */

 outportb(portA1,0xc3);
 *shield = inportb(portA0);
 return(OK);
}

//-----------------------------------------------------------

void MStop (unsigned int portA0)
{
/*
Function return: NONE
*/
 unsigned int	portA1 = portA0+1;

 outportb(portA1, STOP);
}

//-----------------------------------------------------------

BYTE SlvMode(unsigned int portA0)
{
/*
Function return: MISPLACED_COND
                 SLVRCV
                 SLVTRN

*/

 unsigned int	portA1 = portA0+1;
 BYTE	S1,
 	inAddr;

 //Check whether "Addressed as Slave"
 do
  {
   S1 = inportb(portA1);
   if (S1 & BER) return( MISPLACED_COND );
  } while (!(S1 & AAS));

 //Check that Own Address has arrived
 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
  } while (PIN & S1);

 inAddr = inportb(portA0);
 if (inAddr & 1)
  {
   return (SLVTRN);
  }
  else
  {
   return (SLVRCV);
  }
}

//-----------------------------------------------------------

BYTE SlvRcv(unsigned int portA0, BYTE *shield)
{
/*
Function return: MISPLACED_COND
                 OK
                 STOPCON
*/
 unsigned int	portA1 = portA0+1;
 BYTE	S1;
 volatile int dummy;

 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
  } while (PIN & S1);
 if (STS & S1)
  {
   *shield = inportb(portA0);
   return(STOPCON);
  }
  else
  {
   *shield = inportb(portA0);
   dummy=1;
   return(OK);
  }
}

//-----------------------------------------------------------

BYTE IsAddressedAsSlave(unsigned int portA0)
{
/*
Function return: TRUE
		 FALSE
*/

 unsigned int	portA1 = portA0+1;
 BYTE	S1;

 //Check is "Addressed as Slave"?
 S1 = inportb(portA1);
 if (S1 & BER) return( MISPLACED_COND );
 if (S1 & AAS)
  {
   return (TRUE);
  }
  else
  {
   return (FALSE);
  }
}


//-----------------------------------------------------------

BYTE WhichMode(unsigned int portA0)
{
/*
Function return: MISPLACED_COND
                 SLVRCV
                 SLVTRN

*/

 unsigned int	portA1 = portA0+1;
 BYTE	S1,
 	inAddr;

 //Check that Own Address has arrived
 do
  {
   S1 = inportb(portA1);
   if (BER & S1) return( MISPLACED_COND );
  } while (PIN & S1);

 inAddr = inportb(portA0);
 if (inAddr & 1)
  {
   return (SLVTRN);
  }
  else
  {
   return (SLVRCV);
  }
}

#endif
