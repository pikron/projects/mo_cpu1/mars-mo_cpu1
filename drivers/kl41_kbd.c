/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  (COLAMI)

  kl41_kbd.c - periodically scanned keyboard MicroWindows driver

  Copyright holders and project originators
    (C) 2001-2004 by Pavel Pisa pisa@cmp.felk.cvut.cz
    (C) 2002-2004 by PiKRON Ltd. http://www.pikron.com

 The COLAMI components can be used and copied under next licenses
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - LGPL - Lesser GNU Public License
   - and other licenses added by project originators
 Code can be modified and re-distributed under any combination
 of the above listed licenses. If contributor does not agree with
 some of the licenses, he can delete appropriate line.
 Warning, if you delete all lines, you are not allowed to
 distribute code or build project.
 *******************************************************************/

#include <string.h>
//#include <mwtypes.h>
//#include "device.h"

typedef void kbd_parser_fnc_t( void *ptr, unsigned short keycode, unsigned long mods);

//#define WITH_RTEMS
#undef  DEBUG

#ifndef WITH_RTEMS

#include <types.h>
#include <cpu_def.h>
#include <system_def.h>

#define KEY_TIMER msec_time
#define KEY_PUSH_T	20
#define KEY_RELEASE_T	10
#define KEY_REPFIRST_T	800
#define KEY_REPNEXT_T	300
typedef long kl41_kbd_interval_t;

#define KBD_DR _reg_PTD_DR
#define KBD_SSR _reg_PTD_SSR
#define KBD_DDIR _reg_PTD_DDIR
#define KBD_PUEN _reg_PTD_PUEN

typedef unsigned long kbdisr_lock_level_t;
#define kbdisr_lock   save_and_cli
#define	kbdisr_unlock restore_flags

#else /*WITH_RTEMS*/

#include <rtems.h>
#include <rtems/io.h>
#include <rtems/libio.h>
#include <rtems/error.h>
#include <rtems/mw_uid.h>
#include <stdio.h>
#include <irq.h>
#include <inttypes.h>
#include <system_def.h>
#include <drvsupport.h>

#define KEY_TIMER  ({rtems_interval ticks=0; \
	  rtems_clock_get(RTEMS_CLOCK_GET_TICKS_SINCE_BOOT,&ticks); \
	  ticks; \
	})
/*to obtain tick resolution use RTEMS_CLOCK_GET_TICKS_PER_SECOND*/
/*rtems/config.h: rtems_configuration_get_microseconds_per_tick()*/
#define KEY_PUSH_T	20
#define KEY_RELEASE_T	10
#define KEY_REPFIRST_T	700
#define KEY_REPNEXT_T	200
typedef rtems_interval kl41_kbd_interval_t;

#define KBD_DR      MC9328MXL_GPIOD_DR
#define KBD_SSR     MC9328MXL_GPIOD_SSR
#define KBD_DDIR    MC9328MXL_GPIOD_DDIR
#define KBD_PUEN    MC9328MXL_GPIOD_PUEN
#define KBD_IMR     MC9328MXL_GPIOD_IMR
#define KBD_ISR     MC9328MXL_GPIOD_ISR
#define KBD_ICR1    MC9328MXL_GPIOD_ICR1
#define KBD_ICR2    MC9328MXL_GPIOD_ICR2
#define KBD_IRQ_NUM BSP_INT_GPIO_PORTD

#define KBD_ISR_EVENT RTEMS_EVENT_26

typedef rtems_interrupt_level kbdisr_lock_level_t;
#define kbdisr_lock   rtems_interrupt_disable
#define	kbdisr_unlock rtems_interrupt_enable

#endif /*WITH_RTEMS*/


/*
 * MX_KBD matrix is 5 inputs x 3 outputs.
 * Outputs KL41_KBD_WR, bit 0 to 2
 * Inputs KL41_KBD_RD, bit 0 to 4
 */

#define KBD_SCAN_CNT  3
#define KBD_SCAN_BIT0 0
#define KBD_RET_CNT   5
#define KBD_RET_BIT0  0

#define KBD_SCAN_MASK (((1<<KBD_SCAN_CNT)-1)<<KBD_SCAN_BIT0)
#define KBD_RET_MASK  (((1<<KBD_RET_CNT)-1)<<KBD_RET_BIT0)

#define MWKEY int
#define MWKEYMOD int

/*
 *-----------------------------------------------------------
 */


static int kl41_scan_aux=0;

static inline
unsigned char kl41_kbd_onerow(unsigned char scan)
{
	//kbdisr_lock_level_t level;
	unsigned int scan_mask=KBD_SCAN_MASK;
	unsigned int scan_val=(~scan<<KBD_SCAN_BIT0) & scan_mask;
	unsigned int ret;
	int delay=1;

	//kbdisr_lock(level);

	*KL41_KBD_WR=scan_val|kl41_scan_aux;
	//kbdisr_unlock(level);

	while(delay--)
		ret=*KL41_KBD_RD;
	ret=*KL41_KBD_RD;

	return (~ret>>KBD_RET_BIT0)&((1<<KBD_RET_CNT)-1);
}

static inline
void kl41_kbd_setio(void)
{
}


typedef struct {
	MWKEY bc;
	MWKEY sc;
} scan2mwkey_t;

typedef struct {
	int scan;
	int flag;
	MWKEYMOD is_mod;
	MWKEYMOD set_mod;
	MWKEYMOD xor_mod;
} scan2mwmod_t;

#define MWKMOD_SGM_SC		0x8000
#define MWKMOD_SGM_RELEASE	0x0080


const scan2mwkey_t kl41_kbd_scan2mwkey_basic[]={
	[0x00]={0,0},
	[0x09]={'0',0},
	[0x0b]={'1',0},
	[0x06]={'2',0},
	[0x01]={'3',0},
	[0x0c]={'4',0},
	[0x07]={'5',0},
	[0x02]={'6',0},
	[0x0d]={'7',0},
	[0x08]={'8',0},
	[0x03]={'9',0},
	[0x0e]={'*',0},
	[0x04]={'#',0},
	[0x0a]={0x1b /*MWKEY_ESCAPE*/,0},
	[0x0f]={0x0d /*MWKEY_ENTER*/,0},

};

const scan2mwmod_t kl41_kbd_scan2mwmod_basic[]={
	/*{0x28,0,0,MWKMOD_LSHIFT|MWKMOD_SGM_SC,0},*/
	{0,0,0,0}
};

scan2mwkey_t *kl41_kbd_scan2mwkey_tab=(scan2mwkey_t*)kl41_kbd_scan2mwkey_basic;
scan2mwmod_t *kl41_kbd_scan2mwmod_tab=(scan2mwmod_t*)kl41_kbd_scan2mwmod_basic;

/* State of keyboard matrix and key press reporting */

static unsigned char key_down_arr[KBD_SCAN_CNT];
static unsigned char key_chng_arr[KBD_SCAN_CNT];
static unsigned char key_hit;

static MWKEYMOD key_mod;

static int key_last_changed;

/* Internal state for repeat processing */

static short key_use_timer;
static short key_state;
static kl41_kbd_interval_t key_time;

#define KEY_STATE_IDLE     0
#define KEY_STATE_PUSH     1
#define KEY_STATE_RELEASE  2
#define KEY_STATE_REPEAT   4
#define KEY_STATE_NOISE    8
#define KEY_STATE_BUSY     (KEY_STATE_PUSH|KEY_STATE_RELEASE)


/**
 * kl41_kbd_scan - Scan keyboard matrix and report requests for state change
 *
 * Scans keyboard matrix connected row by row by calling function
 * kl41_kbd_onerow(). Number of scanned output lines is defined
 * by %KBD_SCAN_CNT. Checks read keyboard state against @key_down_arr
 * and updates @key_change_arr array. The @key_down_arr state is
 * left unchanged. It is changed later by kl41_kbd_down() function.
 * Returns 0, if no keyboard activity is found. Returns 1
 * if at least one key is pressed. Returns 2 or 3 in case
 * of detected change.
 */
int
kl41_kbd_scan()
{
	int i, ret=0;
	unsigned char mask, val, chng;
	for(i=0,mask=1;i<KBD_SCAN_CNT;i++,mask<<=1) {
		val=kl41_kbd_onerow(mask);
		chng=val^key_down_arr[i];
		key_chng_arr[i]=chng;
		if(val) ret|=1;
		if(chng) ret|=2;
	}
	/* kl41_kbd_onerow(~0); */
	return ret;
}


/**
 * kl41_kbd_scan2mod - Propagate keyboard matrix changes between modifiers
 * @scan_code:		Scan code of last detected key change
 *
 * Functions check keyboard matrix state in @key_down_arr.
 * It updates @key_mod according to @key_down_arr and
 * modifiers transformations table @kl41_kbd_scan2mwmod_tab.
 */
void
kl41_kbd_scan2mod(int scan_code)
{
	unsigned char val, chng;
	int s;
	scan2mwmod_t *mt=kl41_kbd_scan2mwmod_tab;

	for(;(s=mt->scan);mt++) {
		chng=(s==scan_code);
		s--;
		val=key_down_arr[s/KBD_RET_CNT]&(1<<(s%KBD_RET_CNT));
		if(val) {
			key_mod|=mt->set_mod;
			if(chng){
				key_mod^=mt->xor_mod;
			}
		} else {
			key_mod&=~mt->set_mod;
		}
	}
}

/**
 * kl41_kbd_down - Detects changed key scancode and applies changes to matrix state
 *
 * Functions check @key_chng_arr and process changes.
 * It updates its internal state @key_state, does
 * noise cancellation and repeat timing, then updates
 * @key_down_arr, stores detected scancode to @key_last_changed
 * and calls modifiers processing kl41_kbd_scan2mod().
 * Return value is zero if no change is detected.
 * In other case evaluated scancode is returned.
 * Variable @key_hit signals by value 1 pressed key, by value
 * 2 key release.
 */
int
kl41_kbd_down()
{
	int i, j=0;
	unsigned char val;

        if(!(key_state&KEY_STATE_BUSY)){
		for(i=0;i<KBD_SCAN_CNT;i++) {
			if(!(val=key_chng_arr[i])) continue;
			for(j=0;!(val&1);j++) val>>=1;
			key_last_changed=i*KBD_RET_CNT+j+1;
			if(key_down_arr[i]&(1<<j)){
				key_time=KEY_TIMER+KEY_PUSH_T;
				key_state=KEY_STATE_RELEASE;
			}else{
				key_time=KEY_TIMER+KEY_RELEASE_T;
				key_state=KEY_STATE_PUSH;
			}
			break;
		}
		if(key_state==KEY_STATE_IDLE)
			return 0;
	} else {
		if(!key_last_changed){
			key_state=KEY_STATE_IDLE;
	  		return 0;
		}
		i=(key_last_changed-1)/KBD_RET_CNT;
		j=(key_last_changed-1)%KBD_RET_CNT;
		if(!(key_chng_arr[i]&(1<<j))){
			/* Noise detected */
			if(!(key_state&KEY_STATE_NOISE)){
			        key_time=KEY_TIMER+KEY_RELEASE_T;
			        key_state|=KEY_STATE_NOISE;
			}
		}
	}

	if(!key_use_timer){
		if(KEY_TIMER) key_use_timer=1;
		if(key_state&KEY_STATE_REPEAT) return 0;
	}else{
		if((long)(KEY_TIMER-key_time)<0) return 0;
	}

	if(key_state==KEY_STATE_PUSH) {
		key_down_arr[i]|=1<<j;
		kl41_kbd_scan2mod(key_last_changed);
		key_state=KEY_STATE_REPEAT;
		key_time=KEY_TIMER+KEY_REPFIRST_T;
		key_hit=1;
		return key_last_changed;
	} else if(key_state==KEY_STATE_REPEAT) {
		key_time=KEY_TIMER+KEY_REPNEXT_T;
		key_hit=1;
		return key_last_changed;
	} else if(key_state==KEY_STATE_RELEASE) {
		key_down_arr[i]&=~(1<<j);
		kl41_kbd_scan2mod(key_last_changed);
	        key_state=KEY_STATE_IDLE;
		key_hit=2;
		return key_last_changed;
	}
	key_state=KEY_STATE_IDLE;
	return 0;
}

/**
 * kl41_kbd_scan2mwkey - Converts scancode to MWKEY keyboard values
 * @scan:	Detected scancode
 *
 * Computes MWKEY value for detected scancode.
 * Uses @kl41_kbd_scan2mwkey_tab transformation table
 * and @key_mod modifiers information.
 */
MWKEY kl41_kbd_scan2mwkey(int scan)
{
	if((key_mod&MWKMOD_SGM_SC)&&kl41_kbd_scan2mwkey_tab[scan].sc)
		return kl41_kbd_scan2mwkey_tab[scan].sc;
	return kl41_kbd_scan2mwkey_tab[scan].bc;
}

int
kl41_kbd_read_scan_code(void)
{
	int scanval;
	if(!key_hit) {
		if(kl41_kbd_scan()){
			kl41_kbd_down();
		}
	}
	if(!key_hit)
		return -1;

	scanval = key_last_changed;
	key_hit=0;
	return scanval;
}
