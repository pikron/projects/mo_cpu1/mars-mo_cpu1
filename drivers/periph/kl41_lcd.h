/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  kl41_lcd.h - suppot functions for character LCD display driver
               provided on KL41 keyboard
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

/***** low level routines and variables *****/

short kl41_lcd_init_ok;

/* Wait for display ready state */
void kl41_lcd_nbusy(void);

/* Write command into LCD controller */
void kl41_lcd_wrcmd(short cmd);

/* write data (character) onto display */
void kl41_lcd_wrchr(short chr);

/* write zero terminated string */
void kl41_lcd_wrstr(char *s);

/* initialize display */
int kl41_lcd_init(void);




