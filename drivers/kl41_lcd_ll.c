/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  kl41_lcd_ll.c - suppot functions for character LCD display driver
                  provided on KL41 keyboard
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

/* Character display on KL41 keyboard */

#include <types.h>
#include <cpu_def.h>
#include <periph/chmod_lcd.h>
#include <periph/kl41_lcd.h>
#include <system_def.h>

short kl41_lcd_init_ok=0;

void kl41_lcd_wait()
{
  long i;
  for(i=0;i<30000;i++)
    __memory_barrier();
}

void kl41_lcd_nbusy()
{ int i;
  /* for(i=0;i<100;i++)
    __memory_barrier(); */
  i=10000;
  while(*KL41_LCD_STAT&CHMOD_LCD_BF)
    if(!i--) {kl41_lcd_init_ok=-1; break;};
}

void kl41_lcd_wrcmd(short cmd)
{ 
  if(!kl41_lcd_init_ok) return;
  kl41_lcd_nbusy();
  *KL41_LCD_INST=cmd;
}

void kl41_lcd_wrchr(short chr)
{
  if(!kl41_lcd_init_ok) return;
  kl41_lcd_nbusy();
  *KL41_LCD_WDATA=chr;
}

void kl41_lcd_wrstr(char *s)
{
  if(!kl41_lcd_init_ok) return;
  while(*s)
  {
    kl41_lcd_nbusy();
    *KL41_LCD_WDATA=*(s++);
  }
}

int kl41_lcd_init()
{
  kl41_lcd_init_ok=0;
  *KL41_LCD_INST=CHMOD_LCD_MOD;
  kl41_lcd_wait();
  *KL41_LCD_INST=CHMOD_LCD_MOD;
  kl41_lcd_wait();
  *KL41_LCD_INST=CHMOD_LCD_CLR;
  kl41_lcd_wait();
  if(*KL41_LCD_STAT!=0) return -1;
  kl41_lcd_wait();
  *KL41_LCD_WDATA=0x55;
  kl41_lcd_wait();
  *KL41_LCD_WDATA=0xAA;
  kl41_lcd_wait();
  *KL41_LCD_INST=CHMOD_LCD_HOME;
  kl41_lcd_wait();
  if(*KL41_LCD_RDATA!=0x55) return -2;
  kl41_lcd_wait();
  if(*KL41_LCD_RDATA!=0xAA) return -3;
  kl41_lcd_init_ok=1;
  kl41_lcd_wrcmd(CHMOD_LCD_CLR);
  kl41_lcd_wrcmd(CHMOD_LCD_NROL);
  kl41_lcd_wrcmd(CHMOD_LCD_DON|CHMOD_LCD_CON);
  kl41_lcd_wrcmd(CHMOD_LCD_NSH);
  kl41_lcd_wrcmd(CHMOD_LCD_CLR);
  if(kl41_lcd_init_ok!=1)
    { kl41_lcd_init_ok=0; return -4;}
  return 0;
}


