#ifndef _CAN_HW_H_
#define _CTM_HW_H_


#include <cpu_def.h>


/* CAN_CRB (CAN Control Register Block) base address of the TouCAN
   control registers */
#if defined(SIM_MM_VAL)&&(SIM_MM_VAL == 0)
#define CAN_CRB 0x7ff080
#else
#undef SIM_MM_VAL
#define SIM_MM_VAL 1
#define CAN_CRB 0xfff080
#endif


#define CAN_MCR (volatile uint16_t * const)(0x00 + CAN_CRB)
                                /* TouCAN Module Configuration Register */
#define    CAN_STOP    0x8000	/*    Low-Power Stop Mode Enable */
#define    CAN_FRZ     0x4000	/*    FREEZE Assertion Response */
#define    CAN_HALT    0x1000	/*    Halt TouCAN S-Clock */
#define    CAN_NOTRDY  0x0800	/*    TouCAN Not Ready */
#define    CAN_WAKEMSK 0x0400	/*    Wakeup Interrupt Mask */
#define    CAN_SOFTRST 0x0200	/*    Soft Reset */
#define    CAN_FRZACK  0x0100	/*    TouCAN Disable */
#define    CAN_SUPV    0x0080	/*    Supervisor/User Data Space */
#define    CAN_SELFWAKE 0x0040	/*    Self Wake Enable */
#define    CAN_APS     0x0020	/*    Auto Power Save */
#define    CAN_STOPACK 0x0010	/*    Stop Acknowledge */
#define    CAN_IARB    0x000f   /*    Interrupt Arbitration ID */

#define CAN_TCR (volatile uint16_t * const)(0x02 + CAN_CRB)
				/* TouCAN Test Configuration Register */

#define CAN_ICR (volatile uint16_t * const)(0x04 + CAN_CRB)
				/* TouCAN Interrupt Configuration Register */
#define    CAN_ILCAN   0x0700	/*    Interrupt Request Level */
#define    CAN_IVBA    0x00e0	/*    Interrupt Vector Base Address */

#define CAN_CTRL0 (volatile uint8_t * const)(0x06 + CAN_CRB)
				/* Control Register 0 */
#define    CAN_BOFFMSK 0x80	/*    Bus Off Interrupt Mask */
#define    CAN_ERRMSK  0x40	/*    Error Interrupt Mask */
#define    CAN_RXMODE  0x0c	/*    Receive Pin Configuration Control */
#define    CAN_TXMODE  0x03	/*    Transmit Pin Configuration Control */

#define CAN_CTRL1 (volatile uint8_t * const)(0x07 + CAN_CRB)
				/* Control Register 1 */
#define    CAN_SAMP    0x80	/*    Sampling Mode */
#define    CAN_LOOP    0x40	/*    TouCAN Loop Back */
#define    CAN_TSYNC   0x20	/*    Timer Synchronize Mode */
#define    CAN_LBUF    0x10	/*    Lowest Buffer Transmitted First */
#define    CAN_PROPSEG 0x07	/*    Propagation Segment Time */

#define CAN_PRESDIV (volatile uint8_t * const)(0x08 + CAN_CRB)
				/* Prescaler Divide Register */

#define CAN_CTRL2 (volatile uint8_t * const)(0x09 + CAN_CRB)
				/* Control Register 2 */
#define    CAN_RJW     0xc0	/*    Resynchronization Jump Width */
#define    CAN_PSEG1   0x38	/*    Phase Buffer Segment 1 */
#define    CAN_PSEG2   0x07	/*    Phase Buffer Segment 2 */

#define CAN_TIMER (volatile uint16_t * const)(0x0a + CAN_CRB)
				/* Free Running Timer Register */

#define CAN_RXGMSK   (volatile uint32_t * const)(0x10 + CAN_CRB)
#define CAN_RXGMSKHI (volatile uint16_t * const)(0x10 + CAN_CRB)
				/* Receive Global Mask Register High */
#define CAN_RXGMSKLO (volatile uint16_t * const)(0x12 + CAN_CRB)
				/* Receive Global Mask Register Low */

#define CAN_RX14MSK   (volatile uint32_t * const)(0x14 + CAN_CRB)
#define CAN_RX14MSKHI (volatile uint16_t * const)(0x14 + CAN_CRB)
				/* Receive Buffer 14 Mask Register High */
#define CAN_RX14MSKLO (volatile uint16_t * const)(0x16 + CAN_CRB)
				/* Receive Buffer 14 Mask Register Low */

#define CAN_RX15MSK   (volatile uint32_t * const)(0x18 + CAN_CRB)
#define CAN_RX15MSKHI (volatile uint16_t * const)(0x18 + CAN_CRB)
				/* Receive Buffer 15 Mask Register High */
#define CAN_RX15MSKLO (volatile uint16_t * const)(0x1A + CAN_CRB)
				/* Receive Buffer 15 Mask Register Low */

#define CAN_ESTAT (volatile uint16_t * const)(0x20 + CAN_CRB)
				/* Error and Status Register */
#define    CAN_BITERR  0xc000	/*    Transmit Bit Error  */
#define    CAN_ACKERR  0x2000	/*    Acknowledge Error */
#define    CAN_CRCERR  0x1000	/*    Cyclic Redundancy Check Error */
#define    CAN_FORMERR 0x0800	/*    Message Format Error */
#define    CAN_STUFFERR 0x0400	/*    Bit Stuff Error */
#define    CAN_TXWARN  0x0200	/*    Transmit Error Status Flag */
#define    CAN_RXWARN  0x0100	/*    Receiver Error Status Flag */
#define    CAN_IDLE    0x0080	/*    Idle Status */
#define    CAN_TXRX    0x0040	/*    Transmit/Receive Status */
#define    CAN_FCS     0x0030	/*    Fault Confinement State */
#define    CAN_BOFFINT 0x0004	/*    Bus Off Interrupt */
#define    CAN_ERRINT  0x0002	/*    Error Interrupt */
#define    CAN_WAKEINT 0x0001	/*    Wake Interrupt */

#define CAN_IMASK (volatile uint16_t * const)(0x22 + CAN_CRB)
				/* Interrupt Mask Register for Buffers */

#define CAN_IFLAG (volatile uint16_t * const)(0x24 + CAN_CRB)
				/* Interrupt Flag Register for Buffers */

#define CAN_RXECTR (volatile uint8_t * const)(0x26 + CAN_CRB)
				/* Receive Error Counter */

#define CAN_TXECTR (volatile uint8_t * const)(0x27 + CAN_CRB)
				/* Transmit Error Counter */

#define CAN_BUFF  ((volatile can_msg_buf * const)(0x80 + CAN_CRB))
				/* CAN Communication Buffers */

#define    CAN_BSC_TSAMP 0xff00	/*    High byte of the free running timer */
#define    CAN_BSC_CODE  0x00f0	/*    Code of buffer state */
#define    CAN_BSC_LEN   0x000f	/*    Number of data bytes of message */

/* Buffer states for receive operation */
#define    CAN_BST_DISABLE  0x0	/*    Message buffer is not active */
#define    CAN_BST_EMPTY    0x4	/*    Message buffer is active and empty */
#define    CAN_BST_FULL     0x2	/*    Message buffer is full */
#define    CAN_BST_OVERRUN  0x6	/*    Next message replaced previous received one */
#define    CAN_BST_BUSY     0x1	/*    Data are transferred into buffer */
/* Buffer states for transmit operation */
#define    CAN_BST_NREADY   0x8	/*    Message buffer not ready for transmit */
#define    CAN_BST_SEND     0xc	/*    Data frame to be transmitted once, unconditionally */
#define    CAN_BST_RREPLY   0xa	/*    Data frame to be transmitted only as a response to a remote frame */
#define    CAN_BST_SENDARR  0xe	/*    Send once, than becomes remote reply */

typedef struct /* structure to access a can msg buffer */
{
  uint16_t control;
  union {
    struct {
      uint16_t id;
      uint16_t time;
    } std;
    struct {
      uint32_t id;
    } ext;
    uint32_t hwid;
  } id;
  uint8_t  data[8];
  uint16_t reserved;
} can_msg_buf;

#define CAN_HWID2IDE(hwid) ({ uint32_t id=hwid; \
			     ((id&0x7fffe)>>1)|((id&0xffe00000)>>3); })

#define CAN_IDE2HWID(idin) ({ uint32_t id=idin; \
			     ((id&0x3ffff)<<1)|((id&0x1ffc0000)<<3); })

#define CAN_HWID2ID_STD(hwid) ( (uint32_t)hwid>>(5+16) )

#define CAN_ID_FLG2HWID(idin,ext,rtr) ( \
	    ext?CAN_IDE2HWID(idin)|CAN_HWID_IDE|CAN_HWID_SRR|(rtr?CAN_HWID_RTR:0): \
                ((uint32_t)idin<<5|(rtr?CAN_STDID_RTR:0))<<16 \
	  )

#define CAN_HWID_IDE 0x080000	/* 1 for extended ID format */

#define CAN_HWID_SRR 0x100000	/* substitute RTR for extended format */

#define CAN_HWID_RTR 0x000001	/* RTR for extended format */

#define CAN_STDID_RTR  0x0010	/* RTR for standard id format format */

#endif /* _CAN_HW_H_ */
