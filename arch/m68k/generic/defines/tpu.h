#ifndef _TPU_H_
#define _TPU_H_


#include <cpu_def.h>


/* TPU_CRB (TPU Control Register Block) base address of the TPU
   control registers */
#if defined(SIM_MM_VAL)&&(SIM_MM_VAL == 0)
#define TPU_CRB    0x7ffe00
#define TRAM_CRB   0x7ffb00
#else
#undef SIM_MM_VAL
#define SIM_MM_VAL 1
#define TPU_CRB    0xfffe00
#define TRAM_CRB   0xfffb00
#endif
   
   
#define TPU_TPUMCR (volatile uint16_t * const)(0x00 + TPU_CRB)
                                /* Module Configuration Register (S) */
#define    TPU_STOP  0x8000	/*    Low-Power Stop Mode Enable */
#define    TPU_TCR1P 0x6000	/*    Timer Count Register 1 Prescaler Control */
#define    TPU_TCR2P 0x1800	/*    Timer Count Register 2 Prescaler Control */
#define    TPU_EMU   0x0400	/*    Emulation Control */
#define    TPU_T2CG  0x0200	/*    TCR2 Clock/Gate Control */
#define    TPU_STF   0x0100	/*    Stop Flag */
#define    TPU_SUPV  0x0080	/*    Supervisor/Unrestricted */
#define    TPU_PSCK  0x0040	/*    Prescaler Clock */
#define    TPU_IARB  0x000f	/*    Interrupt Arbitration ID */

#define TPU_TCR (volatile uint16_t * const)(0x02 + TPU_CRB)
                                /* Test Configuration Register (S) */

#define TPU_DSCR (volatile uint16_t * const)(0x04 + TPU_CRB)
                                /* Development Support Control Register (S) */
#define    TPU_HOT4  0x8000	/*    Hang on T4 */
#define    TPU_BLC   0x0400	/*    Branch Latch Control */
#define    TPU_CLKS  0x0200	/*    Stop Clocks (to TCRs) */
#define    TPU_FRZ   0x0180	/*    FREEZE Assertion Response */
#define    TPU_CCL   0x0040	/*    Channel Conditions Latch */
#define    TPU_BP    0x0020	/*    Break  mPC == mPC breakpoint register */
#define    TPU_BC    0x0010	/*    Break if CHAN start or set */
#define    TPU_BH    0x0008	/*    Break if host service latch set */
#define    TPU_BL    0x0004	/*    Break if link service latch set */
#define    TPU_BM    0x0002	/*    Break if MRL set at beginning*/
#define    TPU_BT    0x0001	/*    Break if TDL set at beginnibg */

#define TPU_DSSR (volatile uint16_t * const)(0x06 + TPU_CRB)
                                /* Development Support Status Register (S) */
#define    TPU_BKPT  0x0080	/*    Breakpoint Asserted Flag */
#define    TPU_PCBK  0x0040	/*    mPC Breakpoint Flag */
#define    TPU_CHBK  0x0020	/*    Channel Register Breakpoint Flag */
#define    TPU_SRBK  0x0010	/*    Service Request Breakpoint Flag */
#define    TPU_TPUF  0x0008	/*    TPU FREEZE Flag */

#define TPU_TICR (volatile uint16_t * const)(0x08 + TPU_CRB)
                                /* TPU Interrupt Configuration Register (S) */
#define    TPU_CIRL  0x0700	/*    Channel Interrupt Request Level */
#define    TPU_CIBV  0x00f0	/*    Bits [7:4] of Channel Interrupt Base Vector */

#define TPU_CIER (volatile uint16_t * const)(0x0A + TPU_CRB)
                                /* Channel Interrupt Enable Register (S) */

#define TPU_CFSR0 ((volatile uint16_t * const)(0x0C + TPU_CRB))
                                /* Channel Function Selection Register 0 (S) */

#define TPU_CFSR1 ((volatile uint16_t * const)(0x0E + TPU_CRB))
                                /* Channel Function Selection Register 1 (S) */

#define TPU_CFSR2 ((volatile uint16_t * const)(0x10 + TPU_CRB))
                                /* Channel Function Selection Register 2 (S) */

#define TPU_CFSR3 ((volatile uint16_t * const)(0x12 + TPU_CRB))
                                /* Channel Function Selection Register 3 (S) */

#define TPU_HSQR0 ((volatile uint16_t * const)(0x14 + TPU_CRB))
                                /* Host Sequence Register 0 (S/U) */

#define TPU_HSQR1 ((volatile uint16_t * const)(0x16 + TPU_CRB))
                                /* Host Sequence Register 1 (S/U) */

#define TPU_HSRR0 ((volatile uint16_t * const)(0x18 + TPU_CRB))
                                /* Host Service Request Register 0 (S/U) */

#define TPU_HSRR1 ((volatile uint16_t * const)(0x1A + TPU_CRB))
                                /* Host Service Request Register 1 (S/U) */

#define TPU_CPR0 ((volatile uint16_t * const)(0x1C + TPU_CRB))
                                /* Channel Priority Register 0 (S) */

#define TPU_CPR1 ((volatile uint16_t * const)(0x1E + TPU_CRB))
                                /* Channel Priority Register 1 (S) */

#define TPU_CISR (volatile uint16_t * const)(0x20 + TPU_CRB)
                                /* Channel Interrupt Status Register (S) */

#define TPU_LR (volatile uint16_t * const)(0x22 + TPU_CRB)
                                /* Link Register (S) */

#define TPU_SGLR (volatile uint16_t * const)(0x24 + TPU_CRB)
                                /* Service Grant Latch Register (S) */

#define TPU_DCNR (volatile uint16_t * const)(0x26 + TPU_CRB)
                                /* Decoded Channel Number Register (S) */

#define TPU_PARAM ((volatile uint16_t * const)(0x100 + TPU_CRB)) /* 256 entries */
                                /* TPU Parameter RAM (S/U) */


/*** TPU RAM module ***/

#define TRAM_CR (volatile uint16_t * const)(0x00 + TRAM_CRB)
                                /* Module Configuration Register (S) */
#define    TRAM_STOP  0x8000	/*    Low-Power Stop Mode Enable */
#define    TRAM_RASP  0x0080	/*    TPURAM Array Space */

#define TRAM_TST (volatile uint16_t * const)(0x02 + TRAM_CRB)
                                /* TPURAM Test Register (S) */

#define TRAM_BAR (volatile uint16_t * const)(0x04 + TRAM_CRB)
                                /* TPURAM Base Address and Status Register (S) */
#define    TRAM_ADDR  0xfff0	/*    TPURAM Array Base Address */
#define    TRAM_RAMDS 0x0001	/*    RAM Array Disable */

#endif /* _TPU_H_ */
