/*
 *-------------------------------------------------------------------
 *
 *   SIM -- System Integration Module
 *
 * The system integration module (SIM) is used on many Motorola 16-
 * and 32-bit MCUs for the following functions:
 *
 *  () System configuration and protection. Bus and software watchdog
 *  monitors are provided in addition to periodic interrupt generators.
 *
 *  () Clock signal generation for other intermodule bus (IMB) members
 *  and external devices.
 *
 *  () The generation of chip-select signals that simplify external 
 *  circuitry interface.
 *
 *  () Data ports that are available for general purpose input and
 *  output.
 *
 *  () A system test block that is intended only for factory tests.
 *
 * For more information, refer to Motorola's "Modular Microcontroller
 * Family System Integration Module Reference Manual" (Motorola document
 * SIMRM/AD). 
 *
 * This file has been created by John S. Gwynne for support of 
 * Motorola's 68332 MCU in the efi332 project.
 * 
 * Redistribution and use in source and binary forms are permitted
 * provided that the following conditions are met:
 * 1. Redistribution of source code and documentation must retain
 *    the above authorship, this list of conditions and the
 *    following disclaimer.
 * 2. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * This software is provided "AS IS" without warranty of any kind,
 * either expressed or implied, including, but not limited to, the
 * implied warranties of merchantability, title and fitness for a
 * particular purpose.
 *
 *------------------------------------------------------------------
 *
 *  sim.h,v 1.4 1996/06/03 15:49:22 joel Exp
 */

#ifndef _SIM_H_
#define _SIM_H_


#ifndef __ASSEMBLER__
#include <stdint.h>
#endif

/* SAM-- shift and mask */
#undef  SAM
#define SAM(a,b,c) ((a << b) & c)

/*
 *  These macros make this file usable from assembly.
 */

#ifdef __ASSEMBLER__
#define SIM_VOLATILE_U16_POINTER
#define SIM_VOLATILE_U8_POINTER
#else
#define SIM_VOLATILE_U16_POINTER (volatile uint16_t * const)
#define SIM_VOLATILE_U8_POINTER  (volatile uint8_t  * const)
#endif

/* SIM_CRB (SIM Control Register Block) base address of the SIM
   control registers */
/* not included in ram_init.h */
#if defined(SIM_MM_VAL)&&(SIM_MM_VAL == 0)
#define SIM_CRB 0x7ffa00
#else
#undef SIM_MM_VAL
#define SIM_MM_VAL 1
#define SIM_CRB 0xfffa00
#endif 
/* end not included in ram_init.h */



#define SIM_SIMCR SIM_VOLATILE_U16_POINTER(0x00 + SIM_CRB)
				/* Module Configuration Register */
#define    SIM_EXOFF 0x8000	/*    External Clock Off */
#define    SIM_FRZSW 0x4000	/*    Freeze Software Enable */
#define    SIM_FRZBM 0x2000	/*    Freeze Bus Monitor Enable */
#define    SIM_SLVEN 0x0800	/*    Factory Test Model Enabled (ro)*/
#define    SIM_SHEN  0x0300	/*    Show Cycle Enable */
#define    SIM_SUPV  0x0080	/*    Supervisor/Unrestricted Data Space */
#define    SIM_MM    0x0040 	/*    Module Mapping */
#define    SIM_IARB  0x000f	/*    Interrupt Arbitration Field */



#define SIM_SIMTR SIM_VOLATILE_U16_POINTER(0x02 + SIM_CRB)
				/* SIM Test Register */
/* Used only for factor testing */



#define SIM_SYNCR SIM_VOLATILE_U16_POINTER(0x04 + SIM_CRB)
				/* Clock Synthesizer Control Register */
#define    SIM_W     0x8000		/*    Frequency Control (VCO) */
#define    SIM_X     0x4000		/*    Frequency Control Bit (Prescale) */
#define    SIM_Y     0x3f00		/*    Frequency Control Counter */
#define    SIM_EDIV  0x0080		/*    ECLK Divide Rate */
#define    SIM_SLIMP 0x0010		/*    Limp Mode Status */
#define	   SIM_SLOCK 0x0008		/*    Synthesizer Lock */
#define    SIM_RSTEN 0x0004		/*    Reset Enable */
#define    SIM_STSIM 0x0002		/*    Stop Mode SIM Clock */
#define    SIM_STEXT 0x0001		/*    Stop Mode External Clock */



#define SIM_RSR SIM_VOLATILE_U8_POINTER(0x07 + SIM_CRB)
				/* Reset Status Register */
#define    SIM_EXT   0x0080		/*    External Reset */
#define    SIM_POW   0x0040		/*    Power-On Reset */
#define    SIM_SW    0x0020		/*    Software Watchdog Reset */
#define    SIM_DBF   0x0010		/*    Double Bus Fault Reset */
#define    SIM_LOC   0x0004		/*    Loss of Clock Reset */
#define    SIM_SYS   0x0002		/*    System Reset */
#define    SIM_TST   0x0001		/*    Test Submodule Reset */



#define SIM_SIMTRE SIM_VOLATILE_U16_POINTER(0x08 + SIM_CRB)
				/* System Integration Test Register */
/* Used only for factor testing */



#define SIM_PORTE0 SIM_VOLATILE_U8_POINTER(0x11 + SIM_CRB)
#define SIM_PORTE1 SIM_VOLATILE_U8_POINTER(0x13 + SIM_CRB)
				/* Port E Data Register */
#define SIM_DDRE SIM_VOLATILE_U8_POINTER(0x15 + SIM_CRB)
				/* Port E Data Direction Register */
#define SIM_PEPAR SIM_VOLATILE_U8_POINTER(0x17 + SIM_CRB)
				/* Port E Pin Assignment Register */
/* Any bit cleared (zero) defines the corresponding pin to be an I/O
   pin. Any bit set defines the corresponding pin to be a bus control
   signal. */



#define SIM_PORTF0 SIM_VOLATILE_U8_POINTER(0x19 + SIM_CRB)
#define SIM_PORTF1 SIM_VOLATILE_U8_POINTER(0x1b + SIM_CRB)
				/* Port F Data Register */
#define SIM_DDRF SIM_VOLATILE_U8_POINTER(0x1d + SIM_CRB)
				/* Port E Data Direction Register */
#define SIM_PFPAR SIM_VOLATILE_U8_POINTER(0x1f + SIM_CRB)
/* Any bit cleared (zero) defines the corresponding pin to be an I/O
   pin. Any bit set defines the corresponding pin to be a bus control
   signal. */



#define SIM_SYPCR SIM_VOLATILE_U8_POINTER(0x21 + SIM_CRB)
/* !!! can write to only once after reset !!! */
				/* System Protection Control Register */
#define    SIM_SWE   0x80		/*    Software Watch Enable */
#define    SIM_SWP   0x40		/*    Software Watchdog Prescale */
#define    SIM_SWT   0x30		/*    Software Watchdog Timing */
#define    SIM_HME   0x08		/*    Halt Monitor Enable */
#define    SIM_BME   0x04		/*    Bus Monitor External Enable */
#define    SIM_BMT   0x03		/*    Bus Monitor Timing */



#define SIM_PICR SIM_VOLATILE_U16_POINTER(0x22 + SIM_CRB)
				/* Periodic Interrupt Control Reg. */
#define    SIM_PIRQL 0x0700		/*    Periodic Interrupt Request Level */
#define    SIM_PIV   0x00ff		/*    Periodic Interrupt Level */



#define SIM_PITR SIM_VOLATILE_U16_POINTER(0x24 + SIM_CRB)
				/* Periodic Interrupt Timer Register */
#define    SIM_PTP   0x0100		/*    Periodic Timer Prescaler Control */
#define    SIM_PITM  0x00ff		/*    Periodic Interrupt Timing Modulus */



#define SIM_SWSR SIM_VOLATILE_U8_POINTER(0x27 + SIM_CRB)
				/* Software Service Register */
/* write 0x55 then 0xaa to service the software watchdog */



#define SIM_TSTMSRA SIM_VOLATILE_U16_POINTER(0x30 + SIM_CRB)
				/* Test Module Master Shift A */
#define SIM_TSTMSRB SIM_VOLATILE_U16_POINTER(0x32 + SIM_CRB)
				/* Test Module Master Shift A */
#define SIM_TSTSC SIM_VOLATILE_U16_POINTER(0x34 + SIM_CRB)
				/* Test Module Shift Count */
#define SIM_TSTRC SIM_VOLATILE_U16_POINTER(0x36 + SIM_CRB)
				/* Test Module Repetition Counter */
#define SIM_CREG SIM_VOLATILE_U16_POINTER(0x38 + SIM_CRB)
				/* Test Module Control */
#define SIM_DREG SIM_VOLATILE_U16_POINTER(0x3a + SIM_CRB)
				/* Test Module Distributed */
/* Used only for factor testing */



#define SIM_PORTC SIM_VOLATILE_U8_POINTER(0x41 + SIM_CRB)
				/* Port C Data */



#define SIM_CSPAR0 SIM_VOLATILE_U16_POINTER(0x44 + SIM_CRB)
				/* Chip Select Pin Assignment
				   Resgister 0 */
/* CSPAR0 contains seven two-bit fields that determine the functions
   of corresponding chip-select pins. CSPAR0[15:14] are not
   used. These bits always read zero; write have no effect. CSPAR0 bit
   1 always reads one; writes to CSPAR0 bit 1 have no effect. */
#define SIM_CSPAR1 SIM_VOLATILE_U16_POINTER(0x46 + SIM_CRB)
				/* Chip Select Pin Assignment 
				   Register 1 */
/* CSPAR1 contains five two-bit fields that determine the finctions of
   corresponding chip-select pins. CSPAR1[15:10] are not used. These
   bits always read zero; writes have no effect. */
/*
 *
 *                      Bit Field  |  Description
 *                     ------------+---------------
 *                         00      | Discrete Output
 *                         01      | Alternate Function
 *                         10      | Chip Select (8-bit port)
 *                         11      | Chip Select (16-bit port)
 */
#define SIM_DisOut 0x0
#define SIM_AltFun 0x1
#define SIM_CS8bit 0x2
#define SIM_CS16bit 0x3
/*
 *
 * CSPARx Field        |Chip Select Signal  |  Alternate Signal  |  Discrete Output
 *---------------------+--------------------+--------------------+---------------*/
#define SIM_CS_5    12 /*     !CS5          |         FC2        |       PC2     */
#define SIM_CS_4    10 /*     !CS4          |         FC1        |       PC1     */
#define SIM_CS_3     8 /*     !CS3          |         FC0        |       PC0     */
#define SIM_CS_2     6 /*     !CS2          |       !BGACK       |               */
#define SIM_CS_1     4 /*     !CS1          |         !BG        |               */
#define SIM_CS_0     2 /*     !CS0          |         !BR        |               */
#define SIM_CSBOOT   0 /*     !CSBOOT       |                    |               */
/*                     |                    |                    |               */
#define SIM_CS_10    8 /*     !CS10         |       ADDR23       |      ECLK     */
#define SIM_CS_9     6 /*     !CS9          |       ADDR22       |       PC6     */
#define SIM_CS_8     4 /*     !CS8          |       ADDR21       |       PC5     */
#define SIM_CS_7     2 /*     !CS7          |       ADDR20       |       PC4     */
#define SIM_CS_6     0 /*     !CS6          |       ADDR19       |       PC3     */

#define SIM_BS_2K 0x0
#define SIM_BS_8K 0x1
#define SIM_BS_16K 0x2
#define SIM_BS_64K 0x3
#define SIM_BS_128K 0x4
#define SIM_BS_256K 0x5
#define SIM_BS_512K 0x6
#define SIM_BS_1M 0x7

#define SIM_CS_ADDR(addr) (((addr)>>8)&0xfff8)

#define SIM_CSBARBT SIM_VOLATILE_U16_POINTER(0x48 + SIM_CRB)
#define SIM_CSBAR0 SIM_VOLATILE_U16_POINTER(0x4c + SIM_CRB)
#define SIM_CSBAR1 SIM_VOLATILE_U16_POINTER(0x50 + SIM_CRB)
#define SIM_CSBAR2 SIM_VOLATILE_U16_POINTER(0x54 + SIM_CRB)
#define SIM_CSBAR3 SIM_VOLATILE_U16_POINTER(0x58 + SIM_CRB)
#define SIM_CSBAR4 SIM_VOLATILE_U16_POINTER(0x5c + SIM_CRB)
#define SIM_CSBAR5 SIM_VOLATILE_U16_POINTER(0x60 + SIM_CRB)
#define SIM_CSBAR6 SIM_VOLATILE_U16_POINTER(0x64 + SIM_CRB)
#define SIM_CSBAR7 SIM_VOLATILE_U16_POINTER(0x68 + SIM_CRB)
#define SIM_CSBAR8 SIM_VOLATILE_U16_POINTER(0x6c + SIM_CRB)
#define SIM_CSBAR9 SIM_VOLATILE_U16_POINTER(0x70 + SIM_CRB)
#define SIM_CSBAR10 SIM_VOLATILE_U16_POINTER(0x74 + SIM_CRB)

#define SIM_MODE 0x8000
#define SIM_Disable 0
#define SIM_LowerByte 0x2000
#define SIM_UpperByte 0x4000
#define SIM_BothBytes 0x6000
#define SIM_ReadOnly 0x0800
#define SIM_WriteOnly 0x1000
#define SIM_ReadWrite 0x1800
#define SIM_SyncAS 0x0
#define SIM_SyncDS 0x0400

#define SIM_WaitStates_0 (0x0 << 6)
#define SIM_WaitStates_1 (0x1 << 6)
#define SIM_WaitStates_2 (0x2 << 6)
#define SIM_WaitStates_3 (0x3 << 6)
#define SIM_WaitStates_4 (0x4 << 6)
#define SIM_WaitStates_5 (0x5 << 6)
#define SIM_WaitStates_6 (0x6 << 6)
#define SIM_WaitStates_7 (0x7 << 6)
#define SIM_WaitStates_8 (0x8 << 6)
#define SIM_WaitStates_9 (0x9 << 6)
#define SIM_WaitStates_10 (0xa << 6)
#define SIM_WaitStates_11 (0xb << 6)
#define SIM_WaitStates_12 (0xc << 6)
#define SIM_WaitStates_13 (0xd << 6)
#define SIM_FastTerm (0xe << 6)
#define SIM_External (0xf << 6)

#define SIM_CPUSpace (0x0 << 4)
#define SIM_UserSpace (0x1 << 4)
#define SIM_SupSpace (0x2 << 4)
#define SIM_UserSupSpace (0x3 << 4)

#define SIM_IPLevel_any 0x0
#define SIM_IPLevel_1 0x2
#define SIM_IPLevel_2 0x4
#define SIM_IPLevel_3 0x6
#define SIM_IPLevel_4 0x8
#define SIM_IPLevel_5 0xa
#define SIM_IPLevel_6 0xc
#define SIM_IPLevel_7 0xe

#define SIM_AVEC 1

#define SIM_CSORBT SIM_VOLATILE_U16_POINTER(0x4a + SIM_CRB)
#define SIM_CSOR0 SIM_VOLATILE_U16_POINTER(0x4e + SIM_CRB)
#define SIM_CSOR1 SIM_VOLATILE_U16_POINTER(0x52 + SIM_CRB)
#define SIM_CSOR2 SIM_VOLATILE_U16_POINTER(0x56 + SIM_CRB)
#define SIM_CSOR3 SIM_VOLATILE_U16_POINTER(0x5a + SIM_CRB)
#define SIM_CSOR4 SIM_VOLATILE_U16_POINTER(0x5e + SIM_CRB)
#define SIM_CSOR5 SIM_VOLATILE_U16_POINTER(0x62 + SIM_CRB)
#define SIM_CSOR6 SIM_VOLATILE_U16_POINTER(0x66 + SIM_CRB)
#define SIM_CSOR7 SIM_VOLATILE_U16_POINTER(0x6a + SIM_CRB)
#define SIM_CSOR8 SIM_VOLATILE_U16_POINTER(0x6e + SIM_CRB)
#define SIM_CSOR9 SIM_VOLATILE_U16_POINTER(0x72 + SIM_CRB)
#define SIM_CSOR10 SIM_VOLATILE_U16_POINTER(0x76 + SIM_CRB)

#endif /* _SIM_h_ */
