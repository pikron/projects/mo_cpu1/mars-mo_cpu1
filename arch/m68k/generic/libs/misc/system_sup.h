/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_sup.h - system miscellaneous support routines
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _SYSTEM_SUP_H
#define _SYSTEM_SUP_H

extern void breakpoint();
void gdb_clear_data_seg ();
int gdb_without_hooks;
void gdb_set_debug_traps (int without_hooks);
unsigned trap_pc_addr;
int trap_occ_show();
int sim_init_pll(int req_sys_hz);
int sim_init();

#endif /* _SYSTEM_SUP_H */
