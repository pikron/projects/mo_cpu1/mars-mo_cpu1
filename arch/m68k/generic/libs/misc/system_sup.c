/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  system_sup.c - system miscellaneous support routines
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <qsm.h>
#include <periph/qsm_rs232.h>
#include <qadc.h>
#include <string.h>
#include <stdio.h>
#include <system_sup.h>

/* jumps to target pc with defined sp and regs d0-d7 and a0-a6 */
extern void jump_pc_sp_regs(long pc, long sp, long regs[15]);

__asm__ (
  ".even\n\t"
  "jump_pc_sp_regs:\n\t"
  "addql  #4,%sp	/* requested pc */\n\t"
  "movel  %sp@+,%d0	/* requested pc */\n\t"
  "movel  %sp@+,%a0	/* requested sp */\n\t"
  "movel  %sp@+,%a1	/* requested regs */\n\t"
  "moveml %a1@(48),%a4-%a6\n\t"
  "movel  %d0,%a0@-\n\t"
  "movel  %a0,%sp@-\n\t"
  "moveml %a1@,%d0-%d7/%a0-%a3\n\t"
  "movel  %sp@+,%a7\n\t"
  "rts\n"
);

void irq_redirect2vector(int vectno,struct pt_regs *regs)
{
  long *vbr;
  __get_vbr(vbr);
  jump_pc_sp_regs(vbr[vectno],(long)regs+4*15,(long*)regs);  
}

void set_exception_handler(int vectno, void *fnc)
{
  long *vbr;
  __get_vbr(vbr);
  vbr[vectno]=(long)fnc;
}

void exception_hook_nop(int vectno)
{
  return;
}

#ifdef WITH_GDBSTUB

/* GDB stub support routines */

typedef void (*ExceptionHook)(int);   /* pointer to function with int parm */
typedef void (*ExceptionHandler)(int vectno, void *fnc); 

#ifdef WITHOUT_GDBSTUB_HOOKS
int gdb_without_hooks=1;
#else  /* WITHOUT_GDBSTUB_HOOKS */
int gdb_without_hooks=0;
#endif /* WITHOUT_GDBSTUB_HOOKS */
int qsm_used4gdbonly GDBSTUB_DATA_SECT=0;
ExceptionHandler _exceptionHandler GDBSTUB_DATA_SECT=set_exception_handler;
ExceptionHook _exceptionHook GDBSTUB_DATA_SECT=exception_hook_nop;

#endif /* WITH_GDBSTUB */

void _exit(int code)
{ char *p;
  if(!code) p="\r\nProgram exited normaly\r\n";
  else p="\r\nProgram exited with error\r\n";
  qsm_rs232_sendstr(p);
  while(1);
}

extern void irq_entry(void);

__asm__ (
  ".even\n\t"
  "irq_entry:\n\t"
  "moveml %d0-%d7/%a0-%a6 ,%sp@-\n\t"
  "movel  %a7,%a6\n\t"			/* *sp=d0-d7,a0-a6,sr.w,pc.l,vec */
  "movew  66(%a6),%d7\n\t"		/* (8+7)*4+6 */
  "andl   #0x3fc,%d7\n\t"
  "movel  irq_array(%d7),%a5\n\t"
  "tstl   %a5\n\t"
  "beqb   4f\n\t"
  "lsrl   #2,%d7\n\t"

  "\n1:\n\t"
  "movel  %a6,%sp@-\n\t"		/* frame with registers */
  "movel  8(%a5),%sp@-\n\t"		/* interrupt info */
  "movel  %d7,%sp@-\n\t"		/* vector number */
  "movel  (%a5),%a4\n\t"		/* addres of interrupt routine */
  "jsrl   (%a4)\n\t"			/* call it now */
  "addl   #12,%sp\n\t"
  "movel  16(%a5),%a5\n\t"
  "tstl   %a5\n\t"
  "bneb   1b\n\t"
  
  "\n3:\n"
  "moveml %sp@+,%d0-%d7/%a0-%a6\n\t"
  "rte\n"
  "\n4:\n\t"
  "jmp    3b\n"
);

extern void trap_entry(void);

__asm__ (
  ".even\n\t"
  "trap_entry:\n\t"
  "moveml %d0-%d7/%a0-%a6 ,%sp@-\n\t"
  "movel  %a7,%a6\n\t"			/* *sp=d0-d7,a0-a6,sr.w,pc.l,vec */
  "movew  66(%a6),%d7\n\t"		/* (8+7)*4+6 */
  "andl   #0x3fc,%d7\n\t"
  "lsrl   #2,%d7\n\t"
  "movel  %a6,%sp@-\n\t"
  "movel  %d7,%sp@-\n\t"
  "jsrl   do_trap\n\t"
  "addl   #8,%sp\n\t"
  "moveml %sp@+,%d0-%d7/%a0-%a6\n\t"
  "rte\n"
);


unsigned trap_pc_addr=0;

typedef struct trap_occ_info{
  unsigned short sr;
  unsigned long  pc;
  unsigned format :  4; /* frame format specifier */
  unsigned vector : 12; /* vector offset */
  unsigned short info[8];
  unsigned long  app_sp;
}trap_occ_info_t;

trap_occ_info_t trap_occ_info={0,0,0,0};

void do_trap_break(int vectno,struct pt_regs *regs)
{
  __memory_barrier();
}

void do_trap(int vectno,struct pt_regs *regs)
{
  int fr_size;
  trap_pc_addr=regs->pc;
  /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
  /* switch off all power outputs in case of critical error */
  /* disable PWM outputs */
  *QADC_PORTQ|=0x8000;
  /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
  if(regs->format==0) fr_size=4*2;
  else if(regs->format==2) fr_size=6*2;
  else fr_size=12*2;
  memcpy(&(trap_occ_info.sr),&(regs->sr),fr_size);
  if(regs->sr&0x2000) /* trap from supervisor mode */
    trap_occ_info.app_sp=(long)&regs->sr+fr_size;
  else /* trap from user mode */
    __get_usp(trap_occ_info.app_sp);
  do_trap_break(vectno,regs);
}

int add_irq_handler(int vectno,irq_handler_t *handler)
{
  void **vbr;
  unsigned flags;
  if(!vectno) vectno=handler->vectno;
  if((vectno>=NR_IRQS)||(vectno<0)) return -1;
  handler->vectno=vectno;
  handler->next=0;
  save_and_cli(flags);
  if(handler->flags & IRQH_ON_LIST){
    vectno=-1;
  }else{
    atomic_set_mask(IRQH_ON_LIST,&handler->flags);
    __get_vbr(vbr);
    vbr[vectno]=irq_entry;
    if(irq_array[vectno])
      handler->next=irq_array[vectno];
    irq_array[vectno]=handler;
  }
  restore_flags(flags);
  return vectno;
}

int test_irq_handler(int vectno,const irq_handler_t *handler)
{
  int vectstop;
  irq_handler_t *h;
  if((vectno>=NR_IRQS)||(vectno<0)) return -1;
  vectstop=vectno?vectno:NR_IRQS-1;
  for(;vectno<=vectstop;vectno++){
    h=irq_array[vectno];
    for(;h;h=h->next){
      if(h==handler)
        return vectno;
    }
  }
  return 0;
}

int del_irq_handler(int vectno,irq_handler_t *handler)
{
  unsigned flags;
  irq_handler_t *h,**ph;
  if(!vectno) vectno=handler->vectno;
  if((vectno>=NR_IRQS)||(vectno<0)) return -1;
  save_and_cli(flags);
  ph=&irq_array[vectno];
  while(1){
    if(!(h=*ph)){
      restore_flags(flags);
      return -1;
    }
    if(h==handler){
      *ph=h->next;
      break;
    }
    ph=&(h->next);
  }
  atomic_clear_mask(IRQH_ON_LIST,&handler->flags);
  restore_flags(flags);
  handler->next=0;
  return vectno;
}

int sim_init_pll(int req_sys_hz)
{
  int q;
  uint16_t w,x,r;
  /* evaluate pll quotient multiplied by 32 */
  q=((req_sys_hz<<5)+(cpu_ref_hz>>1))/cpu_ref_hz;
  if(q&1) x=0; else {x=SIM_X;q>>=1;}
  if(q&3) w=0; else {w=SIM_W;q>>=2;}
  if(q>64) return -1;
  cpu_sys_hz=cpu_ref_hz*q;
  if(!x) cpu_sys_hz>>=1;
  if(!w) cpu_sys_hz>>=2;
  cpu_sys_hz>>=2;
  r=*SIM_SYNCR;
  r&=~(SIM_W|SIM_X|SIM_Y);
  r|=x|w|((q-1)<<8);
  *SIM_SYNCR=r;
  return 0;
}

int sim_init()
{
  cpu_ref_hz=CPU_REF_HZ;	/* reference clock frequency */
  sim_init_pll(CPU_SYS_HZ);	/* setup default system clock */

 #ifdef WITH_GDBSTUB
  if(!gdb_without_hooks) {
    _exceptionHandler=set_exception_handler;
    _exceptionHook=exception_hook_nop;
  }
 #endif /* WITH_GDBSTUB */
  {
    void **vbr;
    int i;
    __get_vbr(vbr);
    for(i=0;i<NR_IRQS;i++) 
      irq_array[i]=0;
   #ifndef GDBSTUB_RAM_TARGET
    for(i=0;i<NR_IRQS;i++) 
      vbr[i]=trap_entry;
   #endif /* GDBSTUB_RAM_TARGET */
  }
  return 0;
}

typedef struct vect_names_des{
  unsigned char vectnum;
  unsigned char range;
  char *name;
}vect_names_des_t;

const vect_names_des_t vect_names_table[]={
  {0,1,"INISP"},
  {1,1,"INIPC"},
  {2,1,"BUSERR"},
  {3,1,"ADRERR"},
  {4,1,"ILLEGALINST"},
  {5,1,"ZERODIV"},
  {6,1,"CHK"},
  {7,1,"TRAPINST"},
  {8,1,"PRIVVIOLATION"},
  {9,1,"TRACE"},
  {10,1,"EMU1010"},
  {11,1,"EMU1111"},
  {12,1,"HWBREAK"},
  {13,1,"COPROC"},
  {14,1,"FORMERR0"},
  {15,1,"FORMERR2"},
  {24,1,"SPURINT"},
  {25,1,"AVLEV1"},
  {26,1,"AVLEV2"},
  {27,1,"AVLEV3"},
  {28,1,"AVLEV4"},
  {29,1,"AVLEV5"},
  {30,1,"AVLEV6"},
  {31,1,"AVLEV7"},
  {32,1,"TRAP0"},
  {33,1,"TRAP1"},
  {34,1,"TRAP2"},
  {35,1,"TRAP3"},
  {36,1,"TRAP4"},
  {37,1,"TRAP5"},
  {38,1,"TRAP6"},
  {39,1,"TRAP7"},
  {40,1,"TRAP8"},
  {41,1,"TRAP9"},
  {42,1,"TRAP10"},
  {43,1,"TRAP11"},
  {44,1,"TRAP12"},
  {45,1,"TRAP13"},
  {46,1,"TRAP14"},
  {47,1,"TRAP15"}
};

int trap_occ_show()
{
  int vectnum, format;
  char s[40];
  if(!trap_occ_info.pc) return 0;
  vectnum=trap_occ_info.vector>>2;
  format=trap_occ_info.format;
  sprintf(s,"\n\rTRAP! %d %s at pc=0x%06lX\n\r",vectnum,
	  vectnum<48?vect_names_table[vectnum].name:"",
	  trap_occ_info.pc);
  qsm_rs232_sendstr(s);
  sprintf(s,"    sr=0x%04X format=%d\n\r",trap_occ_info.sr,format);
  qsm_rs232_sendstr(s);
  if(format==2){
    sprintf(s,"    faulted_pc=0x%06lX\n\r",*(unsigned long*)(trap_occ_info.info+0));
    qsm_rs232_sendstr(s);
  }else if(format==12){
    sprintf(s,"    faulted_adr=0x%06lX\n\r",*(unsigned long*)(trap_occ_info.info+0));
    qsm_rs232_sendstr(s);
  }
  return 1;
}
