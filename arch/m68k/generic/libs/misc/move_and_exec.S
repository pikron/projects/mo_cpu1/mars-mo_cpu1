/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  move_and_exec.S - routine to start new application

  Copyright (C) 2001-2017 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

.global move_and_exec_start
.global move_and_exec_end
.global move_and_exec

/*
void move_and_exec(void *dst, void *src, size_t size, void *entry)

implementation: copy byte by byte is inefficient but has
                no alignment limitations and size counting
                is easy
*/
move_and_exec_start:
move_and_exec:
	movel	%sp@(4),%a0	/* dst */
	movel	%sp@(8),%a1	/* src */
	movel	%sp@(12),%d0	/* size */
	movel	%sp@(16),%a2	/* entry point */
	cmpl	%a1,%a0		/* dst - src ? */
	jeq	5f		/* no move required */
	jle	4f		/* forward move to lower address */
	movel	%a1,%a3
	addl	%d0,%a3
	cmpl	%a3,%a0
	jge	4f		/* forward move to higher address */
	subl	#1,%d0		/* overlap, move in reverse direction */
	addl	%d0,%a0
1:	moveb	%a3@-,%a0@-	/* move from high address first */
	dbra	%d0,1b
	clrw	%d0
	subl	#1,%d0		/* dbra executes on lower 16 bits only */
	jcc	1b
	jra	5f
3:	moveb	%a1@+,%a0@+	/* regular forward move/copy */
4:	dbra	%d0,3b
	clrw	%d0
	subl	#1,%d0		/* dbra executes on lower 16 bits only */
	jcc	3b
5:	jmp	%a2@		/* jump to new code entry point */
move_and_exec_end:
