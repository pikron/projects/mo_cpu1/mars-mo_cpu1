#ifndef _QSM_RS232_H_
#define _QSM_RS232_H_

#define QSM_RS232_MODEA (QSM_ILT|QSM_TE|QSM_RE)

int qsm_rs232_setmode(int baud, int mode, int flowc);

int qsm_rs232_sendch(int c);
int qsm_rs232_recch();
int qsm_rs232_sendstr(const char *s);
int qsm_rs232_send_finished(void);
int qsm_break2gdb;
int qsm_used4gdbonly;
void qsm_used4gdbdirect(void);

#endif /* _QSM_RS232_H_ */
