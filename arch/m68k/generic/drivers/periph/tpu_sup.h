/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  tpu_sup.h - timing processor unit (TPU) access support routines
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _TPU_SUP_H_
#define _TPU_SUP_H_

#if !defined(TPU_SUP_IMPLEMENT_FNC) && !defined(TPU_SUP_INLINE_FNC)
  #define TPU_SUP_INLINE_FNC static inline
#endif

static inline volatile uint16_t * tpu_param_addr(int chan,int par)
{
  return TPU_PARAM+8*chan+par;
}

#if !defined(TPU_SUP_IMPLEMENT_FNC) && !defined(TPU_SUP_INLINE_FNC)

/* Channel Interrupt Enable (S) */
void tpu_set_ier(chan,val);
/* Channel Interrupt Status Register (S) */
int tpu_test_cisr(int chan);
void tpu_clear_cisr(int chan);
/* Channel Function Selection (S) */
void tpu_set_cfsr(int chan,int val);
/* Host Sequence (S/U) */
void tpu_set_hsqr(int chan,int val);
/* Host Service Request (S/U) */
void tpu_set_hsrr(int chan,int val);
/* Channel Priority (S) */
void tpu_set_cpr(int chan,int val);

#else /* TPU_SUP_NOINLINE_FNC */

#ifndef TPU_SUP_INLINE_FNC
  #define TPU_SUP_INLINE_FNC
#endif

TPU_SUP_INLINE_FNC
void tpu_set_ier(chan,val)
{
  if(val)
    atomic_set_mask_w(1<<chan,TPU_CIER);  
  else
    atomic_clear_mask_w(1<<chan,TPU_CIER);  
}

TPU_SUP_INLINE_FNC
int tpu_test_cisr(int chan)
{
  return *TPU_CISR&(1<<chan);
}

TPU_SUP_INLINE_FNC
void tpu_clear_cisr(int chan)
{
  *TPU_CISR=~(1<<chan);
}

TPU_SUP_INLINE_FNC
void tpu_set_cfsr(int chan,int val)
{
  atomic_clear_mask_w(0xf<<((chan&3)<<2),TPU_CFSR0+3-(chan>>2));
  atomic_set_mask_w((val&0xf)<<((chan&3)<<2),TPU_CFSR0+3-(chan>>2));  
}

TPU_SUP_INLINE_FNC
void tpu_set_hsqr(int chan,int val)
{
  atomic_clear_mask_w(~(val&3)<<((chan&7)<<1),TPU_HSQR0+1-(chan>>3));
  atomic_set_mask_w((val&3)<<((chan&7)<<1),TPU_HSQR0+1-(chan>>3));
}

TPU_SUP_INLINE_FNC
void tpu_set_hsrr(int chan,int val)
{
  atomic_clear_mask_w(3<<((chan&7)<<1),TPU_HSRR0+1-(chan>>3));
  atomic_set_mask_w((val&3)<<((chan&7)<<1),TPU_HSRR0+1-(chan>>3));  
}

TPU_SUP_INLINE_FNC
void tpu_set_cpr(int chan,int val)
{
  atomic_clear_mask_w(3<<((chan&7)<<1),TPU_CPR0+1-(chan>>3));
  atomic_set_mask_w((val&3)<<((chan&7)<<1),TPU_CPR0+1-(chan>>3));  
}

#endif /* TPU_SUP_NOINLINE_FNC */

#endif /* _TPU_SUP_H_ */
