/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  qsm_rs232.c - 683xx QSM module RS-232 communication implementation
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <stdint.h>
#include <system_def.h>
#include <sim.h>
#include <qsm.h>
#include <periph/qsm_rs232.h>

#define QSM_CHAR_XON  0x11
#define QSM_CHAR_XOFF 0x13

#define QSM_RS232_RTS_PEM (1<<0)
#define QSM_RS232_CTS_PEM (1<<1)

#define qsm_rs232_rts_true() \
	atomic_set_mask_b(QSM_RS232_RTS_PEM,SIM_PORTE0)
#define qsm_rs232_rts_false()  \
	atomic_clear_mask_b(QSM_RS232_RTS_PEM,SIM_PORTE0)
#define qsm_rs232_cts()       (!(*SIM_PORTE0&QSM_RS232_CTS_PEM))

int qsm_rs232_baud=9600;
int qsm_rs232_mode=QSM_RS232_MODEA;
int qsm_rs232_flowc=1;
#define QSM_RSFLC_HW  0x1
#define QSM_RSFLC_XON 0x2

typedef volatile struct{
  uint8_t *buf_beg;
  uint8_t *buf_end;
  uint8_t *ip;
  uint8_t *op;
} qsm_que_t;

#define QSM_RS232_BUF_LEN    (80*8)
#define QSM_RS232_BUF_FULLTG (84)

qsm_que_t qsm_rs232_que_in;	/* input qureue */
uint8_t qsm_rs232_buf_in[QSM_RS232_BUF_LEN];

qsm_que_t qsm_rs232_que_out;	/* output queue */
uint8_t qsm_rs232_buf_out[QSM_RS232_BUF_LEN];

short qsm_rs232_flags;
#define QSM_RSFL_ROR   0x1
#define QSM_RSFL_RFE   0x2
#define QSM_RSFL_RPE   0x4
#define QSM_RSFL_SXOFF 0x10
#define QSM_RSFL_SXON  0x20
#define QSM_RSFL_TFCDI 0x40
#define QSM_RSFL_RFCDI 0x80
#define QSM_RSFL_TIP   0x100
#define QSM_RSFL_TWCTS 0x200

int qsm_rs232_irq_cnt;


/* put character c into queue, if full return -1 */
int qsm_que_put(qsm_que_t *q, int c)
{
  uint8_t *p;
  p=q->ip;
  *(p++)=c;
  if(p==q->buf_end) p=q->buf_beg;
  if(p==q->op) return -1;
  q->ip=p;
  return c;
}

/* get character from queue, if empty return -1 */
int qsm_que_get(qsm_que_t *q)
{
  uint8_t *p;
  int  c;
  p=q->op;
  if(p==q->ip) return -1;
  c=*(p++);
  if(p==q->buf_end) p=q->buf_beg;
  q->op=p;
  return c;
}

int qsm_que_freecnt(qsm_que_t *q)
{
  uint8_t *ip=q->ip,*op=q->op;
  return op-ip-1+(op<=ip?q->buf_end-q->buf_beg:0);
}

int qsm_que_incnt(qsm_que_t *q)
{
  uint8_t *ip=q->ip,*op=q->op;
  return ip-op+(ip<op?q->buf_end-q->buf_beg:0);
}

#ifdef WITH_GDBSTUB
void putDebugChar(int ch)      /* write a single character      */
{
  while(!(*QSM_SCSR&QSM_TDRE)) ;
  *QSM_SCDR=ch;
}

int getDebugChar(void)         /* read and return a single char */
{
  while(!(*QSM_SCSR&QSM_RDRF)) ;
  return *QSM_SCDR;
}

extern void qsm_rs232_gdb_isr();

__asm__ (
  ".even\n\t"
  "qsm_rs232_gdb_isr:\n\t"
  /* emulate vector 31 SIGINT - unix signal number 2 */
  "movew #0x7c,%sp@(6)\n\t"
  "subl  #4,%sp\n\t"
  "movel %a0,%sp@-\n\t"
  "movec %vbr,%a0\n\t"
  /* redirect to the vector 31 */
  "movel %a0@(0x7c),%a0\n\t"
  "movel %a0,%sp@(4)\n\t"
  "movel %sp@+,%a0\n\t"
  "rts\n\t"
);

void qsm_used4gdbdirect(void)
{
  void **vbr;
  /* only receive interrupt is used */
  atomic_clear_mask_w(QSM_TIE|QSM_ILIE,QSM_SCCR1);
  __get_vbr(vbr);
  vbr[ISR_QSM_INTV]=qsm_rs232_gdb_isr;
}

#endif /* WITH_GDBSTUB */


void qsm_rs232_isr(int intno, void *dev_id, struct pt_regs *regs)
{
  union{
    uint32_t srdr;
    struct{
      uint16_t sr;
      uint16_t dr;
    } s;
  } scsrdr;
  short val;
  
  qsm_rs232_irq_cnt++;
  scsrdr.srdr=*(volatile uint32_t*)QSM_SCSR;
  if(scsrdr.s.sr&QSM_RDRF){
    /* character received */
    do{
     #ifdef WITH_GDBSTUB
      if((qsm_break2gdb&&(scsrdr.s.dr==0x3))||qsm_used4gdbonly){
        /* ctrl+c received or GDB-only set */
        /* initiate exception 31 equal to */
	/* SIGINT - unix signal number 2 */;	
	regs->vector=4*31; /* emulated SIGINT vector */
        irq_redirect2vector(31,regs);
        break;
      }
     #endif /* WITH_GDBSTUB */
      if(scsrdr.s.sr&(QSM_OR|QSM_FE|QSM_PE)){
        if(scsrdr.s.sr&QSM_OR)qsm_rs232_flags|=QSM_RSFL_ROR;
        if(scsrdr.s.sr&QSM_FE)qsm_rs232_flags|=QSM_RSFL_RFE;      
        if(scsrdr.s.sr&QSM_PE)qsm_rs232_flags|=QSM_RSFL_RPE;
        break;
      }
      if(qsm_rs232_flowc&QSM_RSFLC_XON){
        if(scsrdr.s.dr==QSM_CHAR_XON){
          qsm_rs232_flags&=~QSM_RSFL_TFCDI;
          break;
        }else if(scsrdr.s.dr==QSM_CHAR_XOFF){
          qsm_rs232_flags|=QSM_RSFL_TFCDI;
          break;
        }
      }
      if(qsm_que_put(&qsm_rs232_que_in,scsrdr.s.dr)<0)
        qsm_rs232_flags|=QSM_RSFL_ROR;
      if((!(qsm_rs232_flags&QSM_RSFL_RFCDI))&&
         (qsm_que_freecnt(&qsm_rs232_que_in)<=QSM_RS232_BUF_FULLTG)){
        qsm_rs232_flags|=QSM_RSFL_RFCDI;
        if(qsm_rs232_flowc&QSM_RSFLC_XON){
          qsm_rs232_flags&=~QSM_RSFL_SXON;
          qsm_rs232_flags|=QSM_RSFL_SXOFF;
        }
        if(qsm_rs232_flowc&QSM_RSFLC_HW)  qsm_rs232_rts_false();
      }
    }while(0);
  }
  
  if(scsrdr.s.sr&QSM_TDRE){
    atomic_clear_mask_w(QSM_TIE,QSM_SCCR1);
   #ifdef WITH_GDBSTUB
    if(qsm_used4gdbonly) {
      return;
    }
   #endif /* WITH_GDBSTUB*/
    qsm_rs232_flags&=~QSM_RSFL_TIP;
    if(!(qsm_rs232_flowc&QSM_RSFLC_HW)||qsm_rs232_cts()){
      qsm_rs232_flowc&=~QSM_RSFL_TWCTS;
      if(qsm_rs232_flags&(QSM_RSFL_SXON|QSM_RSFL_SXOFF)){
        val=qsm_rs232_flags&QSM_RSFL_SXON?QSM_CHAR_XON:QSM_CHAR_XOFF;
        qsm_rs232_flags&=~(QSM_RSFL_SXON|QSM_RSFL_SXOFF);
        qsm_rs232_flags|=QSM_RSFL_TIP;
        *QSM_SCDR=val;
        atomic_set_mask_w(QSM_TIE,QSM_SCCR1);
      }else{
        if(!(qsm_rs232_flags&QSM_RSFL_TFCDI)){
          if((val=qsm_que_get(&qsm_rs232_que_out))>=0){
            qsm_rs232_flags|=QSM_RSFL_TIP;
            *QSM_SCDR=val;
            atomic_set_mask_w(QSM_TIE,QSM_SCCR1);
          }
        }
      }
    }else
      qsm_rs232_flowc|=QSM_RSFL_TWCTS;
  }
}

int qsm_rs232_sendch(int c)
{
  if(qsm_que_put(&qsm_rs232_que_out,c)<0){ 
    if((qsm_rs232_flags&QSM_RSFL_TWCTS)&&qsm_rs232_cts())
      atomic_set_mask_w(QSM_TIE,QSM_SCCR1);
    c=-1;
  }
  if(qsm_rs232_flags&QSM_RSFL_TIP) return c;
  if(qsm_rs232_flags&QSM_RSFL_TFCDI) return c;
  if((qsm_rs232_flowc&QSM_RSFLC_HW)&&!qsm_rs232_cts()){
    __memory_barrier();
    qsm_rs232_flags|=QSM_RSFL_TWCTS;
    __memory_barrier();
    return c;
  }
  atomic_set_mask_w(QSM_TIE,QSM_SCCR1);
  return c;
}

int qsm_rs232_recch()
{
  int val;
  if((qsm_rs232_flags&QSM_RSFL_TWCTS)&&qsm_rs232_cts())
    atomic_set_mask_w(QSM_TIE,QSM_SCCR1);
  val=qsm_que_get(&qsm_rs232_que_in);
  if(qsm_rs232_flags&QSM_RSFL_RFCDI)
  {
    if(qsm_que_freecnt(&qsm_rs232_que_in)>=QSM_RS232_BUF_FULLTG+8)
    {
      if(qsm_rs232_flowc&QSM_RSFLC_HW)
        qsm_rs232_rts_true();
      __memory_barrier();
      qsm_rs232_flags&=~QSM_RSFL_RFCDI;
      __memory_barrier();
    }
  }
  return val;
}

int qsm_rs232_sendstr(const char *s)
{
  int cnt=0;
  while(*s)
  {
    if(qsm_rs232_sendch((unsigned char)(*(s++)))<0) break;
    cnt++;
  }
  return cnt;
}

int qsm_rs232_send_finished(void)
{
  return qsm_que_incnt(&qsm_rs232_que_out)?0:1;
}

/*  
  unsigned flags;
  save_and_cli(flags);

  restore_flags(flags);
*/

irq_handler_t qsm_rs232_handler={
  handler: qsm_rs232_isr,
  flags:   0,
  dev_id:  0,
  devname: "qsm_rs232",
  next:    0
};


int qsm_rs232_setmode(int baud, int mode, int flowc)
{
  if(baud==-1) baud=qsm_rs232_baud;
  if(mode==-1) mode=qsm_rs232_mode;
  if(flowc==-1) flowc=qsm_rs232_flowc;
  
  atomic_clear_mask_w(QSM_RIE|QSM_TIE|QSM_TCIE|QSM_ILIE,QSM_SCCR1);
  __memory_barrier();
  qsm_used4gdbonly=0;
  qsm_rs232_baud=baud;
  baud*=32;
  *QSM_SCCR0=(cpu_sys_hz+baud/2)/baud;	/* SCBR */
  qsm_rs232_mode=mode;
  *QSM_SCCR1=mode;
  qsm_rs232_flowc=flowc;
  qsm_rs232_flags=0;

  *QSM_PORTQS|=0x80;
  *QSM_DDRQS|=0x80;
  *QSM_PQSPAR|=0x80;
  
  qsm_rs232_que_in.buf_beg=qsm_rs232_buf_in;
  qsm_rs232_que_in.buf_end=qsm_rs232_que_in.buf_beg+sizeof(qsm_rs232_buf_in);
  qsm_rs232_que_in.ip=qsm_rs232_que_in.buf_beg;
  qsm_rs232_que_in.op=qsm_rs232_que_in.buf_beg;

  qsm_rs232_que_out.buf_beg=qsm_rs232_buf_out;
  qsm_rs232_que_out.buf_end=qsm_rs232_que_out.buf_beg+sizeof(qsm_rs232_buf_out);
  qsm_rs232_que_out.ip=qsm_rs232_que_out.buf_beg;
  qsm_rs232_que_out.op=qsm_rs232_que_out.buf_beg;

  if(qsm_rs232_flowc&QSM_RSFLC_HW){
   *SIM_DDRE|=QSM_RS232_RTS_PEM;   
   *SIM_DDRE&=~QSM_RS232_CTS_PEM;
   *SIM_PEPAR&=~(QSM_RS232_CTS_PEM|QSM_RS232_RTS_PEM);
   qsm_rs232_rts_true();
  }

  __memory_barrier();
  *QSM_CR&=~(QSM_STOP|(QSM_IARB^ISR_QSM_IARB));
  *QSM_CR|=ISR_QSM_IARB;
  *QSM_QILR&=~QSM_ILSCI;
  *QSM_QIVR=ISR_QSM_INTV;
  *QSM_QILR|=ISR_QSM_ILSCI;
  qsm_rs232_irq_cnt=0;
  if(test_irq_handler(ISR_QSM_INTV,&qsm_rs232_handler)==0)
    add_irq_handler(ISR_QSM_INTV,&qsm_rs232_handler);

  atomic_set_mask_w(QSM_RIE,QSM_SCCR1);
  return 1;
}

