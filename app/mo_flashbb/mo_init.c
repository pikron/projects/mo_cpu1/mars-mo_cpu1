/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  mo_init.c - MO_CPU1 board hardware setup
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <types.h>
#include <sim.h>
#include <cpu_def.h>
#include <system_def.h>
#include <cpu_def.h>
#include <sim.h>
#include <tpu.h>
#include <flashprog.h>
#include <mo_flashbb.h>

#define v2mf __val2mfld
#define mf2v __mfld2val

typedef struct fpb_sim_cs_info {
  uint16_t csbar;
  uint16_t csor;
} fpb_sim_cs_info_t;

typedef struct fpb_sim_info {
  uint16_t cspar0;
  uint16_t cspar1;
  uint16_t csbarbt;
  uint16_t csorbt;
  fpb_sim_cs_info_t cs[11];

} fpb_sim_info_t;


extern void __ram_end;


uint32_t const __flashbb_default_hwcfg[]={
  /* sim config */
  FPB_HM_WRS(SIM_SIMCR,	SIM_FRZSW|SIM_SUPV|SIM_MM|SIM_IARB),
  /* disable watchdog and enable bus monitor */
  FPB_HM_WRB(SIM_SYPCR,	SIM_BME|v2mf(SIM_BMT,2)),
  /* port E assignment */
  FPB_HM_WRB(SIM_PEPAR,	0xf4),
  /* port F assignment */
  FPB_HM_WRB(SIM_PFPAR,	0x00),
  /* 21 MHz system clock for 4 MHz external reference */
  FPB_HM_WRS(SIM_SYNCR,	v2mf(SIM_W,1)|v2mf(SIM_X,1)|v2mf(SIM_Y,20)|
  		v2mf(SIM_EDIV,0)|v2mf(SIM_SLIMP,0)),
  /* setup STANBY RAM at 0xffd000 */
  FPB_HM_WRS(0xfffb40,	0x8000),	/* RAMMCR ... STOP */
  FPB_HM_WRL(0xfffb44,	0xffd000),	/* RAMBAH RAMBAL */
  FPB_HM_WRS(0xfffb40,	0x0000),	/* RAMMCR ... ENABLE */
  /* setup TPU RAM at 0xffe000 */
  FPB_HM_WRS(TRAM_CR,	TRAM_STOP),	/* STOP */
  FPB_HM_WRS(TRAM_BAR,	0xffe000>>8),	/* base address */
  FPB_HM_WRS(TRAM_CR,	0),		/* ENABLE */

  /* chipselect configuration and flash relocation */
  /* BOOT ROM 0x800000 1MB  RW UL */
  /* FPB_HM_WRS(SIM_CSBARBT, SIM_CS_ADDR(0x800000)|SIM_BS_1M), */
  FPB_HM_FSREL(0x800000,0xffd000,(long)SIM_CSBARBT|FPB_HM_CHECK_FL,
        SIM_CS_ADDR(0x800000)|SIM_BS_1M),
  FPB_HM_WRS(SIM_CSORBT,
  	SIM_BothBytes|SIM_ReadWrite|SIM_SyncAS|SIM_WaitStates_0|SIM_UserSupSpace),
  
  /* CS4,CS7,CS8,CS9  8-bit rest 16-bit and A19 */
  FPB_HM_WRS(SIM_CSPAR0,
  	SIM_CS16bit<<SIM_CSBOOT|SIM_CS16bit<<SIM_CS_0|SIM_CS16bit<<SIM_CS_1|
	SIM_CS16bit<<SIM_CS_2|SIM_CS16bit<<SIM_CS_3|SIM_CS8bit<<SIM_CS_4|
	SIM_CS16bit<<SIM_CS_5),
  FPB_HM_WRS(SIM_CSPAR1,
  	SIM_AltFun<<SIM_CS_6|SIM_CS8bit<<SIM_CS_7|SIM_CS8bit<<SIM_CS_8|
	SIM_CS8bit<<SIM_CS_9|SIM_CS16bit<<SIM_CS_10),

  /* CS0 to CS10 initialization */
  FPB_HM_WRM(SIM_CSBAR0,11*4,
      /* CS0  ROM 0x900000 1MB  RW UL */
	(SIM_CS_ADDR(0x900000)|SIM_BS_1M)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncAS|SIM_WaitStates_0|SIM_UserSupSpace,
      /* CS1  unused */
	(0)<<16|
	 0,
      /* CS2  RAM 0x000000 1MB   RW UL - Main RAM first 1MB */
	(SIM_CS_ADDR(0x000000)|SIM_BS_1M)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncAS|SIM_WaitStates_0|SIM_UserSupSpace,
      /* CS3  RAM 0x100000 1MB   RW UL - Main RAM second 1MB */
	(SIM_CS_ADDR(0x100000)|SIM_BS_1M)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncAS|SIM_WaitStates_0|SIM_UserSupSpace,
      /* CS4 PER 0xf00000 512kB  RW UL - CMOS RAM, RTC, other devices */
	(SIM_CS_ADDR(0xf00000)|SIM_BS_512K)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncDS|SIM_WaitStates_2|SIM_UserSupSpace,
      /* CS5  unused */
	(0)<<16|
	 0,
      /* CS6  unused */
	(0)<<16|
	 0,
      /* CS7  PER 0xf87000  2k   RW UL - MO_PWR */
	(SIM_CS_ADDR(0xf87000)|SIM_BS_2K)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncDS|SIM_WaitStates_1|SIM_UserSupSpace,
      /* CS8  PER 0xf88000  2k   RO UL - IRC */
	(SIM_CS_ADDR(0xf88000)|SIM_BS_2K)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncDS|SIM_WaitStates_1|SIM_UserSupSpace,
      /* CS9  PER 0xf89000  2k   WR UL - KBD*/
	(SIM_CS_ADDR(0xf89000)|SIM_BS_2K)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncDS|SIM_WaitStates_3|SIM_UserSupSpace,
      /* CS10 PER 0xf8A000 2k   WR UL - USB */
	(SIM_CS_ADDR(0xf8a000)|SIM_BS_2K)<<16|
	 SIM_BothBytes|SIM_ReadWrite|SIM_SyncDS|SIM_WaitStates_3|SIM_UserSupSpace
  ),
  FPB_HM_SP(0x0ffffc),
  FPB_HM_END(),
  FPB_HM_SP(&__ram_end-8)
};

