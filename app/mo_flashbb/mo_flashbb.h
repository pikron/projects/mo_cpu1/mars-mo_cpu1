/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  mo_flashbb.h - hardware configuration script definitions
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

/* ID of hardware configuration script in flash parameter block */
#define FPB_ID_HWCFG	99

/* Commands possible in FPB_ID_HWCFG block */
#define FPB_HWC_SHIFT	26
#define FPB_HWC_MASK	(0x3f<<FPB_HWC_SHIFT)
/* end of HWCFG script  Cx xx */
#define FPB_HWC_END	(0<<FPB_HWC_SHIFT)
/* jump absolute	CA AA */
#define FPB_HWC_JMP	(1<<FPB_HWC_SHIFT)
/* set sp		CA AA */
#define FPB_HWC_SP	(2<<FPB_HWC_SHIFT)
/* self reloc offset	CA AA */
#define FPB_HWC_RELO	(3<<FPB_HWC_SHIFT)
/* write byte		CA AA xx xD */
#define FPB_HWC_WRB	(4<<FPB_HWC_SHIFT)
/* write short		CA AA xx DD */
#define FPB_HWC_WRS	(5<<FPB_HWC_SHIFT)
/* write long		CA AA DD DD */
#define FPB_HWC_WRL	(6<<FPB_HWC_SHIFT)
/* write multiple	CA AA lx CC DD DD ...  */
#define FPB_HWC_WRM	(7<<FPB_HWC_SHIFT)
/* clear multiple	CA AA CC CC */
#define FPB_HWC_CLRM	(8<<FPB_HWC_SHIFT)
/* flash self reloc     offs hlpmem   reg  val  
			CA AA MM MM lR RR DD DD */
#define FPB_HWC_FSREL	(9<<FPB_HWC_SHIFT)

/* Macro shell for configuration script commands */
#define FPB_HM_END()  (FPB_HWC_END)
#define FPB_HM_JMP(addr) \
	(FPB_HWC_JMP+(long)(addr))
#define FPB_HM_SP(addr) \
	(FPB_HWC_SP+(long)(addr))
#define FPB_HM_RELO(addr) \
	(FPB_HWC_RELO+(long)(addr))
#define FPB_HM_WRB(addr,data) \
	(FPB_HWC_WRB+(long)(addr)),\
	(long)(data)
#define FPB_HM_WRS(addr,data) \
	(FPB_HWC_WRS+(long)(addr)),\
	(long)(data)
#define FPB_HM_WRL(addr,data) \
	(FPB_HWC_WRL+(long)(addr)),\
	(long)(data)
#define FPB_HM_WRM(addr,count,data...) \
	(FPB_HWC_WRM+(long)(addr)),\
	(long)(count),\
	data
#define FPB_HM_CLRM(addr,count) \
	(FPB_HWC_CLRM+(long)(addr)),\
	(long)(count)
#define FPB_HM_FSREL(reloffs,helpmem,regaddr,regdata) \
	(FPB_HWC_FSREL+(long)(reloffs)),\
	(long)(helpmem),\
	(long)(regaddr),\
	(long)(regdata)

/* long transfer for FPB_HM_FSREL register and FPB_HM_WRM, else short */
#define FPB_HM_LONG_TRANS_FL 0x80000000
/* check for already set target register in FPB_HM_FSREL */
#define FPB_HM_CHECK_FL 0x40000000
