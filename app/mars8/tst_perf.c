#include <types.h>
#include <system_def.h>
#include <sim.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <ctm4.h>

int perftest(char *s)
{
  uint16_t spent_time;
  unsigned flags=0;  
  
  printf("Performance testing\n");

  save_and_cli(flags);
  spent_time=*CTM4_FCSM12CNT;
  __asm__ __volatile__(
    "	move	#0,%%a0\n"
    "	move	#0,%%a1\n"
    "	move	#1000,%%d0\n"
    "1:	nop\n"
    "	dbra	%%d0,1b\n"
    :::"cc","a0","a1","d0","d1"
  );
  spent_time=*CTM4_FCSM12CNT-spent_time;
  restore_flags(flags);
  printf("1000x nop               ... %6d\n",(int)spent_time);

  save_and_cli(flags);
  spent_time=*CTM4_FCSM12CNT;
  __asm__ __volatile__(
    "	move	#0,%%a0\n"
    "	move	#0,%%a1\n"
    "	move	#1000,%%d0\n"
    "1:	addw	%%a0@+,%%d1\n"
    "	dbra	%%d0,1b\n"
    :::"cc","a0","a1","d0","d1"
  );
  spent_time=*CTM4_FCSM12CNT-spent_time;
  restore_flags(flags);
  printf("1000x addw %%a0@+,%%d1    ... %6d\n",(int)spent_time);

  save_and_cli(flags);
  spent_time=*CTM4_FCSM12CNT;
  __asm__ __volatile__(
    "	move	#0,%%a0\n"
    "	move	#0,%%a1\n"
    "	move	#1000,%%d0\n"
    "1:	movew	%%a0@+,%%a1@+\n"
    "	dbra	%%d0,1b\n"
    :::"cc","a0","a1","d0","d1"
  );
  spent_time=*CTM4_FCSM12CNT-spent_time;
  restore_flags(flags);
  printf("1000x movew %%a0@+,%%a1@+ ... %6d\n",(int)spent_time);

  save_and_cli(flags);
  spent_time=*CTM4_FCSM12CNT;
  __asm__ __volatile__(
    "	move	#0,%%a0\n"
    "	move	#0,%%a1\n"
    "	move	#1000,%%d0\n"
    "1:	movel	%%a0@+,%%a1@+\n"
    "	dbra	%%d0,1b\n"
    :::"cc","a0","a1","d0","d1"
  );
  spent_time=*CTM4_FCSM12CNT-spent_time;
  restore_flags(flags);
  printf("1000x movel %%a0@+,%%a1@+ ... %6d\n",(int)spent_time);

  save_and_cli(flags);
  spent_time=*CTM4_FCSM12CNT;
  __asm__ __volatile__(
    "	move	#0,%%a0\n"
    "	move	#0,%%a1\n"
    "	move	#125,%%d0\n"
    "1:	movel	%%a0@+,%%a1@+\n"
    "	movel	%%a0@+,%%a1@+\n"
    "	movel	%%a0@+,%%a1@+\n"
    "	movel	%%a0@+,%%a1@+\n"
    "	movel	%%a0@+,%%a1@+\n"
    "	movel	%%a0@+,%%a1@+\n"
    "	movel	%%a0@+,%%a1@+\n"
    "	movel	%%a0@+,%%a1@+\n"
    "	dbra	%%d0,1b\n"
    :::"cc","a0","a1","d0","d1"
  );
  spent_time=*CTM4_FCSM12CNT-spent_time;
  restore_flags(flags);
  printf("4KB data transfer       ... %6d\n",(int)spent_time);

  return 0;
}
