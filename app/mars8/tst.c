/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  tst.c - standard application module
 
  Copyright (C) 2001-2009 by Pavel Pisa - originator 
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2009 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <sim.h>
#include <qsm.h>
#include <periph/qsm_rs232.h>
#include <ctm4.h>
#include <tpu.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include <mo_dio.h>
#include <qadc.h>
#include <periph/chmod_lcd.h>
#include <periph/kl41_lcd.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>
#include <load_zlib_binary.h>
#include <flashprog.h>
#include <usd_7266.h>
#include <utils.h>
#include <system_sup.h>
#include <cmd_proc.h>
#include <move_and_exec.h>
#include "app_local_config.h"

#ifdef WITH_DIO
  extern cmd_des_t *cmd_dio_tab[1];
#endif

#ifdef WITH_CMPQUE
  #include <pxmc_cmpque.h>
  extern cmd_des_t *cmd_cmpque_tab[1];
#endif

#ifdef WITH_ANALOG_AXIS
  extern cmd_des_t *cmd_analogaxis_tab[1];
#endif

#ifdef CONFIG_USB_BASE
 #ifdef CONFIG_USB_PDIUSB
  #define WITH_USB
 #endif /*CONFIG_USB_PDIUSB*/
#endif /*CONFIG_USB_BASE*/

#ifdef WITH_USB
#include <usb/usb.h>
#include <usb/pdi.h>
#include <usb/pdiusb.h>
#include <usb/usbdebug.h>
cmd_des_t const *cmd_usb_testing[];
#endif /*WITH_USB*/

long cmd_time_mask=0xffff;
long cmd_coordmv_sqn_mask=0xffff;
long cmd_coordmv_par_bits=24;

/*
 *-----------------------------------------------------------
 */

#define WITH_RTC64613
//define WITH_RTC7242

#ifdef WITH_RTC7242

volatile char *rtc7242_addr=(char*)0xf08000;
char date[20]="2000-01-06 16:41:00";


extern uint8_t __rtc7242_read_byte_sb;
extern uint8_t __rtc7242_read_byte_se;
uint8_t *__rtc7242_read_byte_sm=(uint8_t*)0xFFD000;

__asm__ (
	".text\n"
	".even\n"
	"__rtc7242_read_byte_sb:\n"
	"	nop\n"
	"	moveb	%a0@,%d0\n"
	"	nop\n"
	"	rts\n"
	"__rtc7242_write_byte_sb:\n"
	"	nop\n"
	"	moveb	%d0,%a0@\n"
	"	nop\n"
	"	rts\n"
	"__rtc7242_read_byte_se:\n"
	);


#define __rtc7242_read_byte_si() \
	{ memmove(__rtc7242_read_byte_sm,&__rtc7242_read_byte_sb, \
	  &__rtc7242_read_byte_se-&__rtc7242_read_byte_sb); \
	}

#define __rtc7242_read_byte_sc(addr) \
	({ uint8_t val; \
	  __asm__ __volatile__ ( \
	     "	movel	%1,%%a0\n" \
	     "	movel	__rtc7242_read_byte_sm,%%a1\n" \
	     "	jsr	%%a1@\n" \
	     "	moveb	%%d0,%0\n" \
	    :"=d"(val):"r"(addr):"d0","a0","a1","cc" \
	  ); val; \
	})

#define __rtc7242_write_byte_sc(addr,valin) \
	({ uint8_t val=valin; \
	  __asm__ __volatile__ ( \
	     "	movel	%1,%%a0\n" \
	     "	moveb	%0,%%d0\n" \
	     "	movel	__rtc7242_read_byte_sm,%%a1\n" \
	     "	jsr	%%a1@(__rtc7242_write_byte_sb-__rtc7242_read_byte_sb)\n" \
	    ::"d"(val),"r"(addr):"d0","a0","a1","cc" \
	  ); val; \
	})

void get_date()
{
  __rtc7242_read_byte_si();
  __rtc7242_write_byte_sc(rtc7242_addr+0xd,3);
  while(__rtc7242_read_byte_sc(rtc7242_addr+0xd)&2)
  {
    __rtc7242_write_byte_sc(rtc7242_addr+0xd,2);
    __rtc7242_write_byte_sc(rtc7242_addr+0xd,3);
  }
  
  date[0]='2';
  date[1]='0';
  date[2]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0xb)&0xf);
  date[3]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0xa)&0xf);
  date[4]='-';
  date[5]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x9)&0xf);
  date[6]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x8)&0xf);
  date[7]='-';
  date[8]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x7)&0xf);
  date[9]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x6)&0xf);
  date[10]=' ';
  date[11]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x5)&0x3);
  date[12]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x4)&0xf);
  date[13]=':';
  date[14]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x3)&0xf);
  date[15]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x2)&0xf);
  date[16]=':';
  date[17]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x1)&0xf);
  date[18]='0'+(__rtc7242_read_byte_sc(rtc7242_addr+0x0)&0xf);
  date[19]=0;
  __rtc7242_write_byte_sc(rtc7242_addr+0xd,2);
}

void set_date()
{
  date[4]=0x34; /* value written into mcs F, select 24h mode */
  __rtc7242_read_byte_si();
  __rtc7242_write_byte_sc(rtc7242_addr+0xf,date[7]);
  __rtc7242_write_byte_sc(rtc7242_addr+0xb,date[2]);
  __rtc7242_write_byte_sc(rtc7242_addr+0xa,date[3]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x9,date[5]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x8,date[6]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x7,date[8]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x6,date[9]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x5,date[11]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x4,date[12]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x3,date[14]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x2,date[15]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x1,date[17]);
  __rtc7242_write_byte_sc(rtc7242_addr+0x0,date[18]);
  __rtc7242_write_byte_sc(rtc7242_addr+0xf,date[4]);
}


#endif /* WITH_RTC7242 */

#ifdef WITH_RTC64613
volatile char *rtc64613_addr=(char*)0xf08000;
char date[20]="2000-01-06 16:41:00";

void get_date()
{
  int lpcnt;
  uint8_t b;
  uint8_t creg_a;  
  creg_a=rtc64613_addr[1];
  lpcnt=5;
  do{
    rtc64613_addr[1]=creg_a&~0x80;
    date[0]='2';
    date[1]='0';
    b=rtc64613_addr[7];	/* year */
    date[2]='0'+((b>>4)&0xf);
    date[3]='0'+(b&0xf);
    date[4]='-';
    b=rtc64613_addr[6];	/* month */
    date[5]='0'+((b>>4)&0x1);
    date[6]='0'+(b&0xf);
    date[7]='-';
    b=rtc64613_addr[5];	/* day */
    date[8]='0'+((b>>4)&0x3);
    date[9]='0'+(b&0xf);
    date[10]=' ';
    b=rtc64613_addr[3];	/* hour */
    date[11]='0'+((b>>4)&0x3);
    date[12]='0'+(b&0xf);
    date[13]=':';
    b=rtc64613_addr[2];	/* minute */
    date[14]='0'+((b>>4)&0x7);
    date[15]='0'+(b&0xf);
    date[16]=':';
    b=rtc64613_addr[1];	/* second */
    date[17]='0'+((b>>4)&0x7);
    date[18]='0'+(b&0xf);
    date[19]=0;
  }while(((creg_a=rtc64613_addr[1])&0x80)&&lpcnt--);
}

void set_date()
{
  uint8_t b;

  rtc64613_addr[15]=0;
  
  b=(date[2]-'0')<<4;
  b|=date[3]-'0';
  rtc64613_addr[7]=b;	/* year */
  b=(date[5]-'0')<<4;
  b|=date[6]-'0';
  rtc64613_addr[6]=b;	/* month */
  b=(date[8]-'0')<<4;
  b|=date[9]-'0';
  rtc64613_addr[5]=b;	/* day */
  b=(date[11]-'0')<<4;
  b|=date[12]-'0';
  rtc64613_addr[3]=b;	/* hour */
  b=(date[14]-'0')<<4;
  b|=date[15]-'0';
  rtc64613_addr[2]=b;	/* minute */
  b=(date[17]-'0')<<4;
  b|=date[18]-'0';
  rtc64613_addr[1]=b;	/* second */

  rtc64613_addr[15]=1;
}

#endif /* WITH_RTC64613 */

/*
 *-----------------------------------------------------------
 */

extern int ctm4_pwm_and_sfi_init();
extern void ctm4_pwm_set(short num, short val);
extern unsigned ctm4_sfi_spmax;
extern volatile pxmc_state_t mcs0;
extern int usd_irc_init_all(void);
extern int pxmc_fast_call(pxmc_state_t *mcs, int fnc(pxmc_state_t *mcs));
extern const typeof(USD_IRC_ADDR) usd_irc_addr_tab[];
extern int usd_irc_get_mark(pxmc_state_t *mcs);
extern int can_hw_init(int baud, int mode);
extern int can_hw_test(char *s);
#ifdef WITH_SFI_SEL
int ctm4_sfi_sel(unsigned new_sfi_hz);
#endif /* WITH_SFI_SEL */

/* QADC system */

short qadc_bq2;
short qadc_ch_num;

int qadc_init_16ch()
{
  int i;
  int loc;
  *QADC_QACR1=0;
  *QADC_QACR2=0;

  qadc_bq2=10;
  *QADC_MCR=ISR_QADC_IARB;
  *QADC_INT=0;
  /* *QADC_DDRQA=0x00; */

  /* 1MHz QCLK (16+5) for 21MHz fsys, no mux */ 
  *QADC_QACR0=__val2mfld(QADC_MUX,0)|__val2mfld(QADC_PSH,15)|
              __val2mfld(QADC_PSA,0)|__val2mfld(QADC_PSL,4);

  loc=qadc_bq2;
  /* conversion of ADC0(PQB0) to ADC15(PQA7), Vrl and Vrh */
  for(i=0;i<18;i++,loc++){
    QADC_CCW[loc]=__val2mfld(QADC_P,0)|
                  __val2mfld(QADC_BYP,0)|
                  __val2mfld(QADC_IST,1)|
                  __val2mfld(QADC_CHAN,i<4?i:i+44);
  }
  QADC_CCW[loc]=__val2mfld(QADC_CHAN,63);
  
  /* queue 2 software triggered continuous-scan mode */
  *QADC_QACR2=__val2mfld(QADC_MQ2,17)|QADC_RES|
              __val2mfld(QADC_BQ2,qadc_bq2);

  qadc_ch_num=18;

  return 0;
}

int qadc_read_ch(unsigned adc_chan)
{
  if(adc_chan>=qadc_ch_num)
    return -1;
  return QADC_RJURR[qadc_bq2+adc_chan];
}

/* Basic SPI mode settings */

int qspi_ffree_loc;	/* first free QSPI loaction */

int qspi_mode_init()
{
  *QSM_PORTQS=0xfb;	/* SCK inactive is L, rest H */
  *QSM_DDRQS= 0xfe;	/* MISO is input, rest output */
  *QSM_PQSPAR=0xff;	/* all pins have QSPI function */

  /* master, variable length 16 bits, 500 kBd for fsys 21 MHZ */
  *QSM_SPCR0=QSM_MSTR|__val2mfld(QSM_BITS,16)|
             __val2mfld(QSM_SPBR,21);

  /* delay first clock 1us after PCSx, delay after transfer 1.5us */
  *QSM_SPCR1=__val2mfld(QSM_DSCKL,21)|__val2mfld(QSM_DTL,1);

  /* interrupt disabled, after finish wrap to NEWQP */
  *QSM_SPCR2=QSM_SPIFIE*0|QSM_WREN*1|QSM_WRTO*1|
             __val2mfld(QSM_ENDQP,0)|__val2mfld(QSM_NEWQP,0);

  *QSM_SPCR3=QSM_HMIE*0|QSM_HALT*0;
  *QSM_SPSR=0;
  
  qspi_ffree_loc=0;	/* no transfer location is allocated */
  
  return 0;
}


/* Current sensing ADC on SPI - TLC0838 */

short tlcadc_qspi_loc;

int tlcadc_init()
{
  int i;
  int loc;

  atomic_clear_mask_w(QSM_SPE,QSM_SPCR1);
  loc=qspi_ffree_loc;
  tlcadc_qspi_loc=loc;
  for(i=7;i>=0;i--,loc++){
    /* SPCR0 defined bit count, SPCR1 defined delay after transfer
       and delay after PCS, TLC0838 connected to PCS0 */
    QSM_QSPICR[loc]=QSM_CONT*0|QSM_BITSE*1|QSM_DT*1|QSM_DSCK*1|
                    __val2mfld(QSM_PCS,~1);

    /* TLC0838 command */
    /* StartBit SGL/DIFF SEL1 SEL0 */
    QSM_QSPITR[loc]=(1<<15)|(1<<14)|((i&1)<<13)|((i>>1)<<11);
  }

  qspi_ffree_loc=loc;
  *QSM_SPCR2=(*QSM_SPCR2&~QSM_ENDQP)|__val2mfld(QSM_ENDQP,loc-1);
  atomic_set_mask_w(QSM_SPE,QSM_SPCR1);
  return 0;
}

int tlcadc_read(int i)
{
  return (QSM_QSPIRR[tlcadc_qspi_loc+i]>>2)&0xff;
  //return QSM_QSPIRR[tlcadc_qspi_loc+i];
}

/* Inicialization of digital io subsystem */

#ifdef WITH_DIO

short mo_dio_qspi_loc;

int mo_dio_init(void)
{
  short loc=qspi_ffree_loc;
  mo_dio_qspi_loc=loc;
  
  /* SPCR0 defined bit count, SPCR1 defined delay after transfer
     and delay after PCS, DIO connected to PCS3 */
  QSM_QSPICR[loc]=QSM_CONT*0|QSM_BITSE*1|QSM_DT*1|QSM_DSCK*1|
                  __val2mfld(QSM_PCS,~8);
  QSM_QSPITR[loc]=0;

  dio_init16(&dio_hw_des[0], (uint16_t*)&QSM_QSPIRR[loc], \
                             (uint16_t*)&QSM_QSPITR[loc]);
  loc++;

  /* SPCR0 defined bit count, SPCR1 defined delay after transfer
     and delay after PCS, DIO connected to PCS3 */
  QSM_QSPICR[loc]=QSM_CONT*0|QSM_BITSE*1|QSM_DT*1|QSM_DSCK*1|
                  __val2mfld(QSM_PCS,~4);
  QSM_QSPITR[loc]=0;

  dio_init16(&dio_hw_des[1], (uint16_t*)&QSM_QSPIRR[loc], \
                             (uint16_t*)&QSM_QSPITR[loc]);
  loc++;
  
  qspi_ffree_loc=loc;

  *QSM_SPCR2=(*QSM_SPCR2&~QSM_ENDQP)|__val2mfld(QSM_ENDQP,loc-1);
  atomic_set_mask_w(QSM_SPE,QSM_SPCR1);

  dio_init_markrd(&dio_hw_des[2]);
  return 0;
}
#endif

/* Led indicators and power control panel on SPI */

/* bit 2  .. ERR	*/
/* bit 3  .. BSY	*/
/* bit 4  .. S2		*/
/* bit 5  .. S1		*/
/* bit 8  .. mot A	*/
/* ...			*/
/* bit 15 .. mot H	*/

#define QSPI_LED_MSK_ARM     0x0001
#define QSPI_LED_MSK_STOP    0x0002
#define QSPI_LED_MSK_ERR     0x0004
#define QSPI_LED_MSK_BSY     0x0008
#define QSPI_LED_MSK_S2      0x0010
#define QSPI_LED_MSK_S1      0x0020
#define QSPI_LED_MSK_A       0x0100

/* Power status and switches */

#define QSPI_LED_PRESENT_L   0x0010
#define QSPI_LED_SWITCH_ARM  0x0100
#define QSPI_LED_SWITCH_STOP 0x0200
#define QSPI_LED_PWR_SENSE   0x0400
#define QSPI_LED_PRESENT_H   0x1000

short qspi_led_qspi_loc;

int qspi_led_init()
{
  int loc;
  int pcs_mask;

  if(mo_psys_mode&PSYS_MODE_BIGB){
    pcs_mask=~4; /* LED indicators at PCS1 non */
  }else{
    pcs_mask=~2; /* LED indicators at PCS2 non */
  }

  atomic_clear_mask_w(QSM_SPE,QSM_SPCR1);
  loc=qspi_ffree_loc;
  qspi_led_qspi_loc=loc;
  /* SPCR0 defined bit count, SPCR1 defined delay after transfer
     and delay after PCS, TLC0838 connected to PCS1 */
  QSM_QSPICR[loc]=QSM_CONT*0|QSM_BITSE*1|QSM_DT*1|QSM_DSCK*1|
                  __val2mfld(QSM_PCS,pcs_mask);
  QSM_QSPITR[loc]=0x1234;
  loc++;

  qspi_ffree_loc=loc;
  *QSM_SPCR2=(*QSM_SPCR2&~QSM_ENDQP)|__val2mfld(QSM_ENDQP,loc-1);
  atomic_set_mask_w(QSM_SPE,QSM_SPCR1);
  return 0;
}

void qspi_led_set_val(int val)
{
  QSM_QSPITR[qspi_led_qspi_loc]=val;
}

int qspi_led_rdswitch()
{
  return QSM_QSPIRR[qspi_led_qspi_loc];
}

unsigned short statchk_counter=0;
unsigned short statchk_switches_last=0;
unsigned short statchk_switches=0;
unsigned short statchk_switches_change=0;
unsigned short statchk_power_stop=0;
unsigned short statchk_power_off=0;

void status_check_and_led()
{
  short flg;
  int   i;
  unsigned short chan;
  unsigned long msk;
  unsigned long err_bits;
  unsigned long bsy_bits;
  unsigned new_led_state;
  unsigned short switches;
  unsigned short switches_off;
  pxmc_state_t *mcs;
  bsy_bits=err_bits=0;
  for(chan=0,msk=1;chan<pxmc_main_list.pxml_cnt;chan++,msk<<=1){
    mcs=pxmc_main_list.pxml_arr[chan];
    if(mcs){
      flg=mcs->pxms_flg;
      if(!(flg&PXMS_ERR_m)){
        if(flg&PXMS_BSY_m) bsy_bits|=msk;
      } else err_bits|=msk;
    }
  }  
  
  new_led_state=(bsy_bits*QSPI_LED_MSK_A)|
                (statchk_counter&2?(err_bits*QSPI_LED_MSK_A):0);
		
  if(trap_pc_addr){
    if(statchk_counter&2) new_led_state|=QSPI_LED_MSK_ERR;
  }else{
    if(err_bits) new_led_state|=QSPI_LED_MSK_ERR;
  }
  if(bsy_bits) new_led_state|=QSPI_LED_MSK_BSY;
  if(pxmc_coordmv_state.mcs_flg&MCS_INPR_m)
	       new_led_state|=QSPI_LED_MSK_S1;

  if(mo_psys_mode&PSYS_MODE_BIGB){
    statchk_power_off=(*BIGB_REF_SW)&BIGB_REF_SW_PWRSEN?0:1;
  };

  switches=qspi_led_rdswitch();
  if((switches&QSPI_LED_PRESENT_H)&&
     (~switches&QSPI_LED_PRESENT_L)){
    statchk_switches_change=(statchk_switches^switches)&
			    (statchk_switches_last^switches);
    statchk_switches_last=switches;
    statchk_switches^=statchk_switches_change;
    
    if(!(mo_psys_mode&PSYS_MODE_BIGB)){
      statchk_power_off=switches&QSPI_LED_PWR_SENSE?0:1;
    }
    
    switches=(statchk_switches_change&statchk_switches);
    switches_off=(statchk_switches_change&~statchk_switches);
    
    if(switches&QSPI_LED_SWITCH_ARM){
      statchk_power_stop=0;
    }
    if((switches&QSPI_LED_SWITCH_STOP)||
       (switches_off&QSPI_LED_PWR_SENSE)){
      for(i=0;i<pxmc_main_list.pxml_cnt;i++){
	mcs=pxmc_main_list.pxml_arr[i];
	pxmc_set_const_out(mcs,0);
      }
      statchk_power_stop=1;
      if(~switches&QSPI_LED_SWITCH_STOP)
        statchk_power_stop=2;
    }
			    
  }

  if(!statchk_power_off)
    new_led_state|=QSPI_LED_MSK_ARM;
  if(statchk_power_stop)
    if((statchk_counter&2)||(statchk_power_stop==1))
    new_led_state|=QSPI_LED_MSK_STOP;

  qspi_led_set_val(new_led_state);
  
  statchk_counter++;
}

/*
 *-----------------------------------------------------------
 */

/* TPU tests */

int check_tstme(int flg)
{
  uint16_t val1,val2,val3,old;
  uint16_t cnt=0;
  old=*SIM_CREG;
  *SIM_CREG=0x201;
  val1=*SIM_CREG;
  do{
    val2=*SIM_CREG;
  }while((cnt=++cnt&0x1f)&&!(val2&1));
  if(flg&1){
    printf("Test TSTME pin state\n");
    printf("  CREG=0x%04X\n",old);
    printf("  CREG=0x%04X\n",val1);
    printf("  CREG=0x%04X after %d loops\n",val2,cnt);
  }
  if(!(flg&2))
  {
    *SIM_CREG=0x200;
    val3=*SIM_CREG;
    if(flg&1)printf("  CREG=0x%04X\n",val1);
  }
  return cnt;
}

/*
 *-----------------------------------------------------------
 */

/* selection of debug messages */

#define	DBGPF_AXES	0x00ff	/* mask for all axes */
#define	DBGPF_PER_TIME	0x0100	/* periodic time print */
#define	DBGPF_PER_CUR	0x0200	/* periodic motor current */
#define	DBGPF_PER_ADC	0x0400	/* periodic adc info */
#define	DBGPF_PER_POS	0x0800	/* periodic position info */
#define	DBGPF_CMD_PROC	0x1000	/* command proccessing */
#define	DBGPF_QSPI_LED	0x2000	/* leds on QSPI */
unsigned dbg_prt_flg=0;

typedef struct dbg_prt_des{
  unsigned mask;
  char *name;
}dbg_prt_des_t;

dbg_prt_des_t dbg_prt_des[]={
  {~0,"all"},
  {1,"A"},{2,"B"},{4,"C"},{8,"D"},
  {16,"E"},{32,"F"},{64,"G"},{128,"H"},
  {DBGPF_PER_TIME,"time"},
  {DBGPF_PER_CUR, "cur"},
  {DBGPF_PER_ADC, "adc"},
  {DBGPF_PER_POS, "pos"},
  {DBGPF_CMD_PROC,"cmd"},
  {DBGPF_QSPI_LED,"led"},
  {0,NULL}
};

pxmc_state_t *cmd_opchar_getreg(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{ 
  unsigned chan;
  pxmc_state_t *mcs;
  chan=*param[1]-'A';
  if(chan>=pxmc_main_list.pxml_cnt) return NULL;
  mcs=pxmc_main_list.pxml_arr[chan];
  if(!mcs) return NULL;
  return mcs;
}


int cmd_do_reg_go(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  val=atol(param[3])<<mcs->pxms_subdiv;
  val=pxmc_go(mcs,val,(int)des->info[0],(int)des->info[1]);
  if(val<0)
    return val;
  return 0;
}


int cmd_do_pwm(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  val=atoi(param[3]);
  pxmc_set_const_out(mcs,val);
  return 0;
}


int cmd_do_reg_hh(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  val=pxmc_hh(mcs);
  if(val<0)
    return val;
  return 0;
}


int cmd_do_reg_spd(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  val=atol(param[3]);
  val=pxmc_spd(mcs,val,0);
  if(val<0)
    return val;
  return 0;
}


int cmd_do_reg_spdt(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  long val,timeout;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  p=param[3];
  if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
  if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
  if(si_long(&p,&timeout,0)<0) return -CMDERR_BADPAR;
  si_skspace(&p);
  if(*p) return -CMDERR_GARBAG;
  val=pxmc_spd(mcs,val,timeout);
  if(val<0)
    return val;
  return 0;
}


int cmd_do_clr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_set_const_out(mcs,0);
  /* Set actual position to zero */
  pxmc_axis_set_pos(mcs,0);
  mcs->pxms_ep=0;
  return 0;
}


int cmd_do_stop(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_stop(mcs,0);
  return 0;
}


int cmd_do_release(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  pxmc_set_const_out(mcs,0);
  return 0;
}


int cmd_do_setap(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  long val;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  val=atol(param[3])<<mcs->pxms_subdiv;
  /* Set new actual position */
  if(pxmc_axis_set_pos(mcs,val)<0) return -1;
  return 0;
}


int cmd_do_reg_rw_pos(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  long *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(long*)((int)des->info[0]+(char*)mcs);
  if(opchar==':'){
    val=atoi(param[3]);
    *ptr=val<<mcs->pxms_subdiv;
  }else{
    return cmd_opchar_replong(cmd_io, param, (*ptr)>>mcs->pxms_subdiv, 0, 0);
  }
  return 0; 
}


int cmd_do_reg_short_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  short *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(short*)((int)des->info[0]+(char*)mcs);
  if(opchar==':'){
    val=atoi(param[3]);
    *ptr=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0; 
}


int cmd_do_reg_long_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  long *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(long*)((int)des->info[0]+(char*)mcs);
  if(opchar==':'){
    val=atol(param[3]);
    *ptr=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0; 
}


int cmd_do_reg_type(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  /* should set controller structure */
  return 0;
}

#ifdef PXMC_WITH_PHASE_TABLE
int cmd_do_regptmod_short_val(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  short *ptr;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  ptr=(short*)((long)des->info[0]+(char*)mcs);
  if(opchar==':'){
    if(mcs->pxms_flg&PXMS_BSY_m) return -CMDERR_BSYREG;
    pxmc_set_const_out(mcs,0);
    val=atoi(param[3]);
    if((val<((long)des->info[1])) || (val>((long)des->info[2])))
      return -CMDERR_BADPAR;
    *ptr=val;
    /*val=pxmc_axis_mode(mcs,0);
    if(val<0)
      return val;*/
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0;
}
#endif /*PXMC_WITH_PHASE_TABLE*/

int cmd_do_reg_dbgset(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  if(opchar==':'){
    val=atoi(param[3]);
    pxmc_dbgset(mcs,NULL,val);
  }else{
    cmd_io_write(cmd_io,param[0],param[2]-param[0]);
    cmd_io_putc(cmd_io,'=');
    cmd_io_putc(cmd_io,mcs->pxms_flg&PXMS_DBG_m?'1':'0');
  }
  return 0; 
}


int cmd_do_reg_dbgpre(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_dbg_hist_t *hist;
  long count, val;
  int i;
  int opchar;
  char *ps;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  ps=param[3];
  if(si_long(&ps,&count,0)<0) return -CMDERR_BADPAR;
  if(!count||(count>0x10000)) return -CMDERR_BADPAR;
  pxmc_dbg_histfree(NULL);
  if((hist=pxmc_dbg_histalloc(count+2))==NULL) return -CMDERR_NOMEM;
  for(i=0;i<count;i++){
    ps=cmd_rs232_rdline(cmd_io,1);
    if(si_long(&ps,&val,0)<0) return -CMDERR_BADPAR;
    hist->buff[i]=val;
  }
  pxmc_dbg_hist=hist;
  return 0;
}


int cmd_do_reg_dbghis(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_dbg_hist_t *hist;
  long count, val;
  int i, opchar;
  char *ps;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  ps=param[3];
  if(si_long(&ps,&count,0)<0) return -CMDERR_BADPAR;
  if(!count||(count>0x10000)) return -CMDERR_BADPAR;
  hist=pxmc_dbg_hist;
  for(i=0;i<count;i++){
    if(hist&&(&hist->buff[i]<hist->end))
      val=hist->buff[i];
    else
      val=0;
    printf("%ld\r\n",val);
  }
  return 0;
}


int cmd_do_reg_dbggnr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    if(mcs&&(mcs->pxms_flg&PXMS_DBG_m)){
      if(pxmc_dbg_gnr(mcs)<0){
	for(i=0;i<pxmc_main_list.pxml_cnt;i++){
	  mcs=pxmc_main_list.pxml_arr[i];
	  if(mcs&&(mcs->pxms_flg&PXMS_DBG_m))
              pxmc_stop(pxmc_main_list.pxml_arr[i],0);
        }
        return -CMDERR_BADREG;
      }
    }
  }
  pxmc_dbg_hist->ptr=pxmc_dbg_hist->buff;  
  return 0; 
}


#ifdef WITH_SFI_SEL
int cmd_do_regsfrq(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    val=atoi(param[3]);
    return ctm4_sfi_sel(val);
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)ctm4_sfi_hz, 0, 0);
  }
  return 0; 
}
#endif /* WITH_SFI_SEL */


int cmd_do_axst_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  if(!param[1])
  {
    for(i=0;i<pxmc_main_list.pxml_cnt;i++){
      mcs=pxmc_main_list.pxml_arr[i];
      if(mcs){
        printf("mcs%d flg=0x%04x ap=%8d rp=%8d as=%4d rs=%4d ene=%6d\n",
                i,mcs->pxms_flg,
                (int)(mcs->pxms_ap>>mcs->pxms_subdiv),
                (int)(mcs->pxms_rp>>mcs->pxms_subdiv),
                (int)(mcs->pxms_as>>mcs->pxms_subdiv),
                (int)(mcs->pxms_rs>>mcs->pxms_subdiv),
                (int)(mcs->pxms_ene)
              );
      }
    }
  }else{
    i=atoi(param[1]);
    if(i>=pxmc_main_list.pxml_cnt)
      return -10;
    mcs=pxmc_main_list.pxml_arr[i];
    if(!mcs) return -11;
    printf("mcs%d flg=0x%04x ap=%8d rp=%8d as=%4d rs=%4d ene=%6d\n",
            i,mcs->pxms_flg,
            (int)(mcs->pxms_ap>>mcs->pxms_subdiv),
            (int)(mcs->pxms_rp>>mcs->pxms_subdiv),
            (int)(mcs->pxms_as>>mcs->pxms_subdiv),
            (int)(mcs->pxms_rs>>mcs->pxms_subdiv),
            (int)(mcs->pxms_ene)
          );
  
    printf("     p=%4d i=%4d d=%4d me=%4d foi=%5d fod=%5d erc=%5d\n",
            (int)(mcs->pxms_p),(int)(mcs->pxms_i),(int)(mcs->pxms_d),
            (int)(mcs->pxms_me),(int)(mcs->pxms_foi),(int)(mcs->pxms_fod),
            (int)(mcs->pxms_erc)
          );
    printf("     md=%4d ms=%4d ma=%4d tmp=%6d\n",
            (int)(mcs->pxms_md>>mcs->pxms_subdiv),
            (int)(mcs->pxms_ms),
            (int)(mcs->pxms_ma),
            (int)(mcs->pxms_tmp)
          );
  }  
  return 0;
}


int cmd_do_callreg(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  if(!param[1]) return -2;
  i=atoi(param[1]);
  if(i>=pxmc_main_list.pxml_cnt)
    return -10;
  mcs=pxmc_main_list.pxml_arr[i];
  if(!mcs->pxms_do_con) return -11;
  pxmc_fast_call(mcs,mcs->pxms_do_con);
  return 0;
}


int cmd_do_cer_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    if(mcs->pxms_flg&PXMS_ERR_m){
      pxmc_set_const_out(mcs,0);
      pxmc_clear_flags(mcs,PXMS_ERR_m|PXMS_ENR_m|PXMS_ENG_m|PXMS_BSY_m);
    }
  }
  statchk_power_stop=0;
  return 0;
}


int cmd_do_clr_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    pxmc_set_const_out(mcs,0);
    pxmc_clear_flags(mcs,PXMS_ERR_m|PXMS_ENR_m|PXMS_BSY_m);
    /* Set actual position to zero */
    pxmc_axis_set_pos(mcs,0);
    mcs->pxms_ep=0;
  }
  statchk_power_stop=0;
  return 0;
}


int cmd_do_stop_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    pxmc_stop(pxmc_main_list.pxml_arr[i],0);
  }
  return 0;
}


int cmd_do_release_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    pxmc_set_const_out(mcs,0);
  }
  return 0;
}


int cmd_do_status_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int val;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar=='?'){
    val=0;
    for(i=0;i<pxmc_main_list.pxml_cnt;i++){
      mcs=pxmc_main_list.pxml_arr[i];
      if(mcs)
        val|=mcs->pxms_flg;
    }
    if(pxmc_coordmv_checkst(&pxmc_coordmv_state)>=2)
      val|=PXMS_CQF_m;	/* coordinator coomand queue full */
    if(trap_pc_addr)
      val|=0x1000000;
    if(statchk_power_stop)
      val|=0x20000;
    if(statchk_power_off)
      val|=0x10000;
    return cmd_opchar_replong(cmd_io, param, val, 0, 0);
  }
  return 0; 
}

int cmd_do_status_bsybits(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int val;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar=='?'){
    val=0;
    for(i=0;i<pxmc_main_list.pxml_cnt;i++){
      mcs=pxmc_main_list.pxml_arr[i];
      if(mcs)
        val|=(mcs->pxms_flg&PXMS_BSY_b)?1<<i:0;
    }
    return cmd_opchar_replong(cmd_io, param, val, 0, 0);
  }
  return 0; 
}


/* controller helper variables */
short ready_all_rs232=0;
unsigned long ready_rpt_rs232=0;/* report ready for axis */
#define MAX_READY_BIT (sizeof(ready_rpt_rs232)*8-1)
int idlerel_time=600;

int cmd_do_r_one(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned chan;
  unsigned long val;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  chan=*param[1]-'A';
  if(chan>=pxmc_main_list.pxml_cnt) return -CMDERR_BADREG;
  if(!pxmc_main_list.pxml_arr[chan]) return -CMDERR_BADREG;
  if(chan>=MAX_READY_BIT) chan=MAX_READY_BIT;
  val=1<<chan;
  ready_rpt_rs232|=val;
  return 0;
}

int cmd_do_r_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  ready_all_rs232=1;
  return 0;
}

int cmd_check_r(cmd_io_t *cmd_io)
{
  unsigned short chan;
  unsigned long msk;
  volatile pxmc_state_t *mcs;
  short errcnt;
  short bsycnt;
  short flg;
  if(!ready_all_rs232&&!ready_rpt_rs232)
    return 0;
  errcnt=bsycnt=0;
  for(chan=0,msk=1;chan<pxmc_main_list.pxml_cnt;chan++,msk<<=1){
    mcs=pxmc_main_list.pxml_arr[chan];
    if(mcs){
      flg=mcs->pxms_flg;
      if(!(flg&PXMS_ERR_m)){
        if(flg&PXMS_BSY_m){bsycnt++;continue;}
        if(!(ready_rpt_rs232&msk)) continue;
        cmd_io_write(&cmd_io_rs232,"R",1);
      }else{
        errcnt++;
        if(!(ready_rpt_rs232&msk)) continue;
        cmd_io_write(&cmd_io_rs232,"FAIL",4);
      }
      ready_rpt_rs232&=~msk;
      cmd_io_putc(cmd_io,chan+'A');
      cmd_io_putc(cmd_io,'!');
      return 1;
    }
  }
  if(ready_all_rs232){
    if(errcnt){
      cmd_io_write(&cmd_io_rs232,"FAIL!",5);
      ready_all_rs232=0;
      return 1;
    }else{
      if(!bsycnt){
        cmd_io_write(&cmd_io_rs232,"R!",2);
        ready_all_rs232=0;
	return 1;
      }
    }
  }
  return 0;
}


int errstop_mode=1;

int cmd_check_errstop()
{
  unsigned short flg;
  unsigned short chan;
  pxmc_state_t *mcs;
  flg=0;
  for(chan=0;chan<pxmc_main_list.pxml_cnt;chan++){
    mcs=pxmc_main_list.pxml_arr[chan];
    if(mcs){
      flg|=mcs->pxms_flg;
    }
  }
  if((flg&PXMS_ENG_m)&&(flg&PXMS_ERR_m)){
    for(chan=0;chan<pxmc_main_list.pxml_cnt;chan++){
      mcs=pxmc_main_list.pxml_arr[chan];
      if(mcs&&(mcs->pxms_flg&PXMS_ENG_m)){
        pxmc_stop(mcs,0);
      }
    }
  }
  return 0;
}


int cmd_do_callgen(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  int i;
  if(!param[1]) return -2;
  i=atoi(param[1]);
  if(i>=pxmc_main_list.pxml_cnt)
    return -10;
  mcs=pxmc_main_list.pxml_arr[i];
  if(!mcs->pxms_do_gen) return -11;
  pxmc_fast_call(mcs,mcs->pxms_do_gen);
  return 0;
}


int cmd_do_usd_test(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_state_t *mcs;
  typeof(USD_IRC_ADDR) pu;
  int ret=0;
  int i, j;
  long x, y;
  for(i=0;i<pxmc_main_list.pxml_cnt;i++){
    mcs=pxmc_main_list.pxml_arr[i];
    pxmc_set_const_out(mcs,0);
    pxmc_clear_flags(mcs,PXMS_ERR_m|PXMS_ENI_m|PXMS_BSY_m);
    pu=usd_irc_addr_tab[mcs->pxms_out_info];
    /* disable A & B */
    pu[1]=USD_7266_IOR|
          USD_7266_IOR_DISAB|USD_7266_IOR_RCNTR|USD_7266_IOR_CMPBO;
    for(j=0;j<100;j++)
    {
      x=j*0x123456;
      /* */
      pu[1]=USD_7266_RLD|USD_7266_RLD_BP;
      pu[0]=x; pu[0]=x>>8; pu[0]=x>>16;
      pu[1]=USD_7266_RLD|USD_7266_RLD_PR2C;
      pu[1]=USD_7266_RLD|USD_7266_RLD_BP;
      pu[1]=USD_7266_RLD|USD_7266_RLD_C2OL;
      y=pu[0]; y|=(unsigned long)pu[0]<<8;
      y|=(unsigned long)pu[0]<<16;
      if((y^x)&0xffffff)
        if(ret++<10)
          printf("USD7266 #%i error: 0x%lx != 0x%lx\n",i,y,x);
    }
    pu[1]=USD_7266_RLD|USD_7266_RLD_RC;    
    /* enable A & B */
    pu[1]=USD_7266_IOR|
       USD_7266_IOR_ENAB|USD_7266_IOR_RCNTR|USD_7266_IOR_CMPBO;     
    pxmc_set_flags(mcs,PXMS_ENI_m);
  }
  if(!ret) printf("USD7266 OK\n");
  return 0;
}


int cmd_do_switches(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned val,*pval;
  long l;
  dbg_prt_des_t *pdes,*pd;
  char *ps;
  char str[20];
  char pm_flag;
  pval=(unsigned*)(des->info[0]);
  pdes=(dbg_prt_des_t*)(des->info[1]);
  ps=(des->mode&CDESM_OPCHR)?param[3]:param[1];
  val=*pval;
  if(*ps=='?'){
    while(pdes->name){
      printf(" %s",pdes->name);
      pdes++;
    }
    printf("\n");
  }
  while(*ps){
    si_skspace(&ps);
    if(!*ps) break;
    pm_flag=0;
    if(*ps=='+'){ps++;}
    else if(*ps=='-'){pm_flag=1;ps++;}
    else val=0;
    if(isdigit((uint8_t)*ps)){if(si_long(&ps,&l,0)<0) return -1;}
    else{
      si_alnumn(&ps,str,20);
      pd=pdes;
      do{
        if(!pd->name) return -1;
	if(!strcmp(pd->name,str)){
	  l=pd->mask; break;
	}
	pd++;
      }while(1);
    }
    if(pm_flag) val&=~l;
    else val|=l;
  }
  *pval=val;
  return 0;
}


int cmd_do_ioecho(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    val=*param[3];
    if(val=='0')
      cmd_io->priv.ed_line.in->flg&=~FL_ELB_ECHO;
    else if(val=='1')
      cmd_io->priv.ed_line.in->flg|=FL_ELB_ECHO;
    else return -CMDERR_BADPAR;
  }else{
    cmd_io_write(cmd_io,param[0],param[2]-param[0]);
    cmd_io_putc(cmd_io,'=');
    cmd_io_putc(cmd_io,cmd_io->priv.ed_line.in->flg&FL_ELB_ECHO?'1':'0');
  }
  return 0;
}


int cmd_do_rs232baud(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  val=atoi(param[3]);
  if(val<=2400) return -CMDERR_BADPAR;
  qsm_rs232_setmode(val,-1,-1);
  return 0;
}


const char software_ver[]="MARS8 v"STRINGIFY(SW_VER_MAJOR)"."STRINGIFY(SW_VER_MINOR)
           " build Pi "__DATE__" "__TIME__;

int cmd_do_ver(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!='?') return -CMDERR_OPCHAR;
  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  cmd_io_write(cmd_io,software_ver,strlen(software_ver));
  return 0;
}


int cmd_do_reboot(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  /* disable interrupts */
  __cli();
  /* disable PWM outputs */
  *QADC_PORTQ|=0x8000;
  __asm__ (
	"reset\n\t"
	"movl	#0x800000,%a0\n\t"
	"movl	4(%a0),%a1\n\t"
	"addl	%a0,%a1\n\t"
	"movl	0(%a0),%a7\n\t"
	"jmp	%a1@\n\t"
  );
  
  return 0;
}


int cmd_do_spmax(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  printf("spmax=%8d\n",ctm4_sfi_spmax);
  ctm4_sfi_spmax=0;
  return 0;
}


int cmd_do_date(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  /* for(i=0;param[i];i++) printf("param %d = \"%s\"\n",i,param[i]); */
  if(param[1]){
    strncpy(date,param[1],19);
    set_date();
  }
  get_date();
  printf("actual date is %s\n",date);
  return 0;
}


extern int mo_math_tst(char *s);

int cmd_do_math(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  mo_math_tst(param[1]);
  return 0;
}


/*** coordinated movement ***/

extern int pxmc_coordmv_tst(char *s);

int cmd_do_coordmv_tst(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_tst(param[1]);
  return 0;
}


int cmd_do_coordmv(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state=&pxmc_coordmv_state;
  int con_cnt=mcs_state->mcs_con_cnt;
  int i;
  long final[con_cnt], val;
  long mintim=0;
  char *s;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  s=param[3];
  if((long)(des->info[1])&1){	/* coordinated movement with time */
    if(si_long(&s,&mintim,10)<0) return -CMDERR_BADPAR;
    if(mintim<0) return -CMDERR_BADPAR;
    if(si_fndsep(&s,",")<=0) return -CMDERR_BADSEP;
  }
  if(!con_cnt) return -CMDERR_BADCFG;
  i=0;
  while(1){
    if(si_long(&s,&val,10)<0) return -CMDERR_BADPAR;
    mcs=pxmc_coordmv_indx2mcs(mcs_state,i);
    if(!mcs) return -CMDERR_BADREG;
    final[i]=val<<mcs->pxms_subdiv;
    if(++i>=con_cnt) break;
    if(si_fndsep(&s,",")<=0) return -CMDERR_BADSEP;
  }
  si_skspace(&s); 
  if(*s) return -CMDERR_GARBAG;
  
  if((long)(des->info[1])&2){
    if(pxmc_coordmv_relmv(mcs_state,con_cnt,final,mintim)<0)
      return -1;
  }else{
    if(pxmc_coordmv_absmv(mcs_state,con_cnt,final,mintim)<0)
      return -1;
  }
  return 0;
}


int cmd_do_coordspline(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state=&pxmc_coordmv_state;
  int con_cnt=mcs_state->mcs_con_cnt;
  long spline_param[con_cnt*PXMC_SPLINE_ORDER_MAX];
  int i;
  int o;
  int a;
  long val;
  long mintim=0;
  long order;
  char *s;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  s=param[3];
  if((long)(des->info[1])&1){	/* coordinated movement with time */
    if(si_long(&s,&mintim,10)<0) return -CMDERR_BADPAR;
    if(mintim<0) return -CMDERR_BADPAR;
    if(si_fndsep(&s,",")<=0) return -CMDERR_BADSEP;
  }
  if(!con_cnt) return -CMDERR_BADCFG;

  if(si_long(&s,&order,10)<0) return -CMDERR_BADPAR;
  if(mintim<0) return -CMDERR_BADPAR;
  if(si_fndsep(&s,",")<=0) return -CMDERR_BADSEP;

  if((order<=0)||(order>PXMC_SPLINE_ORDER_MAX))
    return -CMDERR_BADPAR;

  i=0; o=0; a=0;
  while(1){
    if(si_long(&s,&val,10)<0) return -CMDERR_BADPAR;
    mcs=pxmc_coordmv_indx2mcs(mcs_state,a);
    if(!mcs) return -CMDERR_BADREG;
    spline_param[i]=val<<mcs->pxms_subdiv;
    i++;
    if(++o>=order){
      o=0;
      if(++a>=con_cnt) break;
    }
    if(si_fndsep(&s,",")<=0) return -CMDERR_BADSEP;
  }
  si_skspace(&s);
  if(*s) return -CMDERR_GARBAG;

  if(pxmc_coordmv_spline(mcs_state,i,spline_param,order,mintim)<0)
    return -1;
  return 0;
}


int cmd_do_coordgrp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state=&pxmc_coordmv_state;
  pxmc_state_list_t *reg_list=mcs_state->mcs_con_list;
  int reg_max=reg_list->pxml_cnt;
  int ret;
  unsigned chan;
  int con_indx[reg_max];
  int con_cnt=0;
  char *s;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  s=param[3];
  do{
    si_skspace(&s); 
    if(!*s) break;
    chan=*(s++)-'A';
    if(chan>=reg_max) return -CMDERR_BADREG;
    con_indx[con_cnt]=chan;
    con_cnt++;
    if((ret=si_fndsep(&s,","))<0) return -CMDERR_BADSEP;
    if(ret==0) break;
  }while(1);

  return  pxmc_coordmv_grp(mcs_state,con_cnt,con_indx);
}


int cmd_do_coordgetpos(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state=&pxmc_coordmv_state;
  int con_cnt=mcs_state->mcs_con_cnt;
  pxmc_coordmv_seg_t *mcseg;
  unsigned long msec_time_a;
  unsigned long msec_time_b = 0;
  int is_iddle;
  unsigned long seg_sqn_a;
  unsigned long seg_sqn_b = 0;
  unsigned long seg_par_a;
  unsigned long seg_par_b = 0;
  long val_array_a[con_cnt*2];
  long val_array_b[con_cnt*2];
  long *p;
  int try_cnt = 0;
  int query_mode = (int)des->info[1];
  char str[20];

  if(*param[2]!='?') return -CMDERR_OPCHAR;

  do {
    __memory_barrier();
    mcseg = mcs_state->mcs_seg_inpr;
    msec_time_a = msec_time;
    __memory_barrier();
    if (mcseg!=NULL) {
      seg_sqn_a = mcseg->mcseg_sqn;
      seg_par_a = mcs_state->mcs_par;
      is_iddle = 0;
    } else {
      seg_sqn_a = mcs_state->mcs_next_sqn;
      seg_par_a = 0;
      is_iddle = 1;
    }
    if (query_mode & 1)
      seg_par_a = mcs_state->mcs_next_sqn;
    if (try_cnt & 1)
      p = val_array_a;
    else
      p = val_array_b;
    pxmc_coordmv_foreach_get_pos(mcs_state, p, con_cnt,
                                  (unsigned int)des->info[0], 1);
    if (query_mode & 2)
      pxmc_coordmv_foreach_get_long(mcs_state, p + con_cnt, con_cnt,
                                    pxmc_state_offs(pxms_as));
    if (try_cnt++) {
      if ((msec_time_b == msec_time_a) &&
          (seg_sqn_b == seg_sqn_a) &&
          (seg_par_b == seg_par_a)) {
        long *pa = val_array_a;
        long *pb = val_array_b;
        int cnt = con_cnt;
        if (query_mode & 2)
          cnt *= 2;
        while(cnt && (*(pa++) == *(pb++))) {
          cnt--;
        }
        if (!cnt)
          break;
      }
    }

    msec_time_b = msec_time_a;
    seg_sqn_b = seg_sqn_a;
    seg_par_b = seg_par_a;
  } while(try_cnt<3);

  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  i2str(str, msec_time_a & cmd_time_mask, 0, 0);
  cmd_io_write(cmd_io, str, strlen(str));
  cmd_io_putc(cmd_io,',');
  i2str(str, seg_sqn_a & cmd_coordmv_sqn_mask, 0, 0);
  cmd_io_write(cmd_io, str, strlen(str));
  cmd_io_putc(cmd_io,',');
  if (query_mode & 1) {
    seg_par_a &= cmd_coordmv_sqn_mask;
    i2str(str, seg_par_a, 0, 0);
  } else {
    seg_par_a >>= 31 - cmd_coordmv_par_bits;
    if (!is_iddle)
      i2str(str, seg_par_a, 0, 0);
    else
      i2str(str, -1, 0, 0);
  }
  cmd_io_write(cmd_io, str, strlen(str));
  {
    int cnt = con_cnt;
    if (query_mode & 2)
      cnt *= 2;
    while (cnt--) {
      cmd_io_putc(cmd_io,',');
      i2str(str, *(p++), 0, 0);
      cmd_io_write(cmd_io, str, strlen(str));
    }
  }

  return 0;
}

int cmd_do_regpwrflg(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    val=atol(param[3]);
    mo_psys_pwr_flg=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, mo_psys_pwr_flg, 0, 0);
  }
  return 0; 
}


int cmd_do_regpwron(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    val=atol(param[3]);
    mo_psys_pwr_mains=(val==1);
  }else{
    return cmd_opchar_replong(cmd_io, param, mo_psys_pwr_mains, 0, 0);
  }
  return 0; 
}


int cmd_do_syscpuhz(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int new_sys_hz;
  if(param[1]&&*param[1]){
    new_sys_hz=atoi(param[1]);
    if((new_sys_hz<1000000)||(new_sys_hz>40000000))
      new_sys_hz=CPU_SYS_HZ;
    sim_init_pll(new_sys_hz);
    qsm_rs232_setmode(-1,-1,-1);
    can_hw_init(-1,-1);
  }
  printf("System clock is %d HZ\n",cpu_sys_hz);
  return 0;
}

/*** Analog inputs reading ***/

int cmd_do_adc(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  int opchar;
  unsigned adc_chan;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar=='?'){
    if(cmd_num_suffix(cmd_io, des, param, &adc_chan)<0)
      return -CMDERR_BADSUF;
    if(adc_chan>=18)
      return -CMDERR_BADDIO;
    val=qadc_read_ch(adc_chan);
    return cmd_opchar_replong(cmd_io, param, val, 0, 0);
  }else {
    return -CMDERR_OPCHAR;
  }
  return 0; 
}


extern int perftest(char *s);

int cmd_do_perftest(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  perftest(param[1]);
  return 0;
}

extern void dhry_main(void);
int cmd_do_dhrytest(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  dhry_main();
  return 0;
}

int cmd_do_cantest(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  can_hw_test(param[1]);
  return 0;
}


int cmd_do_check_tstme(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  check_tstme(1);
  return 0;
}


int cmd_do_errorshow(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int ret;
  ret=trap_occ_show();
  if(!ret)
    cmd_io_write(cmd_io,"no error",8);
  return 0;
}

/* Select default flash algorithms to AMD29F400 */
/*flash_alg_info_t const *flash_alg_info=&amd_29f400_x16;*/

/* Select default flash algorithms to AMD29F800 */
flash_alg_info_t const *flash_alg_info=&amd_29f800_x16;

int cmd_do_flashid(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  uint32_t addr;
  uint16_t checkid[2];
  int ret;

  if(sscanf(param[1],"%"SCNi32 ,&addr)!=1){
    printf("bad addres format\n");
    return 0;
  }
  ret=flash_check_id(flash_alg_info,(void*)addr,checkid);
  printf("flash_check_id returned %d, manid 0x%X devid 0x%X\n",
  		ret,checkid[0],checkid[1]);
  return 0;
}


int cmd_do_flashprog(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  uint32_t addr;
  uint32_t val;
  int ret;

  if(sscanf(param[1],"%"SCNi32" %"SCNi32,&addr,&val)!=2){
    printf("bad addres or data format\n");
    return 0;
  }
  ret=flash_prog(flash_alg_info,(void*)addr,val);
  printf("flash_prog returned %d\n",ret);
  return 0;
}


int cmd_do_flasherase(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  uint32_t addr;
  uint32_t range;
  int ret;

  if(sscanf(param[1],"%"SCNi32" %"SCNi32,&addr,&range)!=2){
    printf("bad addres or range format\n");
    return 0;
  }
  ret=flash_erase(flash_alg_info,(void*)addr,(uint16_t)range);
  printf("flash_erase returned %d\n",ret);
  return 0;
}


int cmd_do_flashtest(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  uint32_t addr;
  int ret;
  uint8_t *p;
  uint16_t *pw;
  uint16_t val;

  if(sscanf(param[1],"%"SCNi32,&addr)!=1){
    addr=0x800000;
  }
  printf("Checking flash id at addr 0x%08"PRIx32" ... ",addr);
  ret=flash_check_id(flash_alg_info,(void*)addr,NULL);
  printf("%s\n",ret<0?"Error":"OK"); if(ret<0) return 0;
  printf("Flash erase ... ");
  fflush(stdout);
  ret=flash_erase(flash_alg_info,(void*)addr,0);
  printf("%s\n",ret<0?"Error":"OK"); if(ret<0) return 0;
  ret=0;
  for(p=(uint8_t*)addr;p<=(uint8_t*)addr+flash_alg_info->addr_mask;p++)
    if(*p!=0xff){
      if(ret<100)printf("Erase not blank at 0x%08lx data 0x%02x\n",
      			  (long)p,(int)*p);
      ret++;
    }
  if(ret) return 0;
  printf("Flash pattern program ");
  fflush(stdout);
  val=0;
  for(pw=(uint16_t*)addr;(uint8_t*)pw<=(uint8_t*)addr+flash_alg_info->addr_mask;pw++){
    ret=flash_prog(flash_alg_info,pw,val);
    val++;
    if(ret<0) break;
    if(!((long)pw&0x3fff)){
      printf("."); val++;
      fflush(stdout);
    }
  }
  if(ret<0){
    printf("\nProgram error at 0x%08lx code %d\n",(long)pw,ret);
    return 0;
  }
  printf(" OK\n");
  printf("Flash pattern verify ... ");
  fflush(stdout);
  val=0; ret=0;
  for(pw=(uint16_t*)addr;(uint8_t*)pw<=(uint8_t*)addr+flash_alg_info->addr_mask;pw++){
    if((*pw!=val)&&(ret<100)){
      printf("\nProgram verify error at 0x%08lx diff %d",(long)pw,*pw^val);
      ret++;
    }
    val++;
    if(!((long)pw&0x3fff)) val++;
  }
  if(ret) {printf(" Error\n");return 0;}
  printf(" OK\n");
  printf("All tests returned OK\n");
  return 0;
}

int cmd_do_gdbbreak(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int break_fl;
  if(sscanf(param[1],"%i",&break_fl)!=1){
    break_fl=2;
  }

  /* Decide if Ctrl+C is redirected to GDB stub */
  qsm_break2gdb=break_fl?1:0;
  /* Decide if RS232 dedicated for GDB debugging only */
  qsm_used4gdbonly=break_fl>=3;
  /* Prepare for load of new application */
  if(break_fl>=4) {
    *CTM4_MCSM2SIC&=~CTM4_MCSM_IL; /* Stop interrupts */
    *QSM_QILR&=~QSM_ILQSPI;
    *TPU_TICR&=~TPU_CIRL;
    /* interrupts of all other used modules should be stopped here */
    qsm_used4gdbdirect();
  }
  gdb_set_debug_traps(gdb_without_hooks);
  if(break_fl>=2)
    breakpoint();
  return 0;
}

int cmd_do_rungzipapp_progress_fnc(void *context, size_t bytes, char *msg)
{

  if (msg != NULL)
    printf("zlib message \"%s\" at pos %ld\n\r", msg, (long)bytes);

  return 0;
}

int cmd_do_rungzipapp_read_char(void *read_char_arg)
{

  cmd_io_t* cmd_io = (cmd_io_t*)read_char_arg;

  return cmd_io_getc(cmd_io);
}

int cmd_do_rungzipapp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  uint32_t max_size = 512 * 1024;
  uint32_t start_addr = 0x00004400;
  void *app_dst;
  void *app_src;
  size_t app_size;
  void *app_entry;
  void *buff;
  cmd_io_t* io_raw = cmd_io;
  void (*move_and_exec_fnc)(void *dst, void *src, size_t size, void *entry);

  sscanf(param[1],"%"SCNi32" %"SCNi32,&max_size,&start_addr);

  move_and_exec_fnc = move_and_exec;

  if (max_size == 0) {
    max_size = 0x100000 - 1024;
    buff = (void*)0x100000;
  } else {
    buff = malloc(max_size + &move_and_exec_end - &move_and_exec_start);
    if (buff < 0) {
      printf("No enoug memory to allocate %"PRIi32" bytes\n", max_size);
      return -CMDERR_NOMEM;
    }
  }

  if(cmd_io->priv.ed_line.io_stack != NULL)
    io_raw=cmd_io->priv.ed_line.io_stack;

  res = load_zlib_binary(&app_size, cmd_do_rungzipapp_read_char,
           io_raw, buff, max_size, 0,
           cmd_do_rungzipapp_progress_fnc, NULL);

  if (res != 0) {
    printf("Load of copressed image failed, code %d\n", res);
    return -1;
  }

  app_src = buff;
  app_dst = (void*)start_addr;
  app_entry = (void*)start_addr;

  if (((void*)&move_and_exec_start < (void*)app_dst + app_size) &&
      ((void*)&move_and_exec_end > (void*)app_dst)) {
     move_and_exec_fnc = (void*)(((uintptr_t)buff + app_size + 0x1f) & ~0xf);
     printf("Relocating move and exec code to %p\n", move_and_exec_fnc);
     memcpy(move_and_exec_fnc, &move_and_exec_start,
            &move_and_exec_end - &move_and_exec_start);
  }

  printf("Ready to proceed jump to application\n");
  printf("app_dst=%p, app_src=%p, app_size=%ld, app_entry=%p\n",
         app_dst, app_src, (long)app_size, app_entry);

  {
    int i;
    uint8_t *p;
    p = app_src;
    for (i = 0; i < 16; i++)
      printf("%02x%c", *(p++), (i & 0xf) == 0xf? '\n': ' ');
    p = app_src + app_size - 16;
    for (i = 0; i < 16; i++)
      printf("%02x%c", *(p++), (i & 0xf) == 0xf? '\n': ' ');
  }

  while (!qsm_rs232_send_finished()) {
    /* not ideal but keeps RTS/CTS handshaking process */
    qsm_rs232_recch();
  }

  __cli();
  *CTM4_MCSM2SIC&=~CTM4_MCSM_IL; /* Stop interrupts */
  *QSM_QILR&=~QSM_ILQSPI;
  *TPU_TICR&=~TPU_CIRL;

  move_and_exec_fnc(app_dst, app_src, app_size, app_entry);

  return 0;
}

/* motor executable commands */
cmd_des_t const cmd_des_go={0, CDESM_OPCHR|CDESM_RW,
			"G?","go to target position",cmd_do_reg_go,
			{0,(char*)PXMC_GO_ABS}};
cmd_des_t const cmd_des_gorel={0, CDESM_OPCHR|CDESM_RW,
			"GR?","go relative",cmd_do_reg_go,
			{0,(char*)PXMC_GO_REL}};
cmd_des_t const cmd_des_pwm={0, CDESM_OPCHR|CDESM_RW,
			"PWM?","direct axis PWM output",cmd_do_pwm,{}};
cmd_des_t const cmd_des_hh={0, CDESM_OPCHR,"HH?","hard home request for axis",cmd_do_reg_hh,{}};
cmd_des_t const cmd_des_spd={0, CDESM_OPCHR,"SPD?","speed request for axis",cmd_do_reg_spd,{}};
cmd_des_t const cmd_des_spdt={0, CDESM_OPCHR,"SPDT?","speed request with timeout",cmd_do_reg_spdt,{}};
cmd_des_t const cmd_des_clr={0, CDESM_OPCHR,
			"CLEAR?","releases clear position of axis",cmd_do_clr,{}};
cmd_des_t const cmd_des_stop={0, CDESM_OPCHR,
			"STOP?","stop motion of requested axis",cmd_do_stop,{}};
cmd_des_t const cmd_des_release={0, CDESM_OPCHR,
			"RELEASE?","releases axis closed loop control",cmd_do_release,{}};
cmd_des_t const cmd_des_setap={0, CDESM_OPCHR,
			"SETAP?","set new actual position",cmd_do_setap,{}};
/* motors and controllers variables */
cmd_des_t const cmd_des_ap={0, CDESM_OPCHR|CDESM_RD,
			"AP?","actual position",cmd_do_reg_rw_pos,
			{(char*)pxmc_state_offs(pxms_ap),
			 0}};
cmd_des_t const cmd_des_st={0, CDESM_OPCHR|CDESM_RD,
			"ST?","axis status bits encoded in number",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_flg),
			 0}};
cmd_des_t const cmd_des_r_one={0, CDESM_OPCHR,
			"R?","send R?! or FAIL?! at axis finish",cmd_do_r_one,{}};
cmd_des_t const cmd_des_regp={0, CDESM_OPCHR|CDESM_RW,
			"REGP?","controller proportional gain",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_p),
			 0}};
cmd_des_t const cmd_des_regi={0, CDESM_OPCHR|CDESM_RW,
			"REGI?","controller integral gain",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_i),
			 0}};
cmd_des_t const cmd_des_regd={0, CDESM_OPCHR|CDESM_RW,
			"REGD?","controller derivative gain",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_d),
			 0}};
cmd_des_t const cmd_des_regs1={0, CDESM_OPCHR|CDESM_RW,
			"REGS1?","controller S1",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_s1),
			 0}};
cmd_des_t const cmd_des_regs2={0, CDESM_OPCHR|CDESM_RW,
			"REGS2?","controller S2",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_s2),
			 0}};
cmd_des_t const cmd_des_regmd={0, CDESM_OPCHR|CDESM_RW,
			"REGMD?","maximal allowed position error",cmd_do_reg_rw_pos,
			{(char*)pxmc_state_offs(pxms_md),
			 0}};
cmd_des_t const cmd_des_regms={0, CDESM_OPCHR|CDESM_RW,
			"REGMS?","maximal speed",cmd_do_reg_long_val,
			{(char*)pxmc_state_offs(pxms_ms),
			 0}};
cmd_des_t const cmd_des_regacc={0, CDESM_OPCHR|CDESM_RW,
			"REGACC?","maximal acceleration",cmd_do_reg_long_val,
			{(char*)pxmc_state_offs(pxms_ma),
			 0}};
cmd_des_t const cmd_des_regme={0, CDESM_OPCHR|CDESM_RW,
			"REGME?","maximal PWM energy or voltage for axis",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_me),
			 0}};
cmd_des_t const cmd_des_regcfg={0, CDESM_OPCHR|CDESM_RW,
			"REGCFG?","hard home and profile configuration",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_cfg),
			 0}};
#ifdef PXMC_WITH_PHASE_TABLE
cmd_des_t const cmd_des_ptirc={0, CDESM_OPCHR|CDESM_RW,
			"REGPTIRC?","number of irc pulses per phase table",cmd_do_regptmod_short_val,
			{(char*)pxmc_state_offs(pxms_ptirc),
			 (char*)4,(char*)10000}};
cmd_des_t const cmd_des_ptper={0, CDESM_OPCHR|CDESM_RW,
			"REGPTPER?","number of elmag. revolutions per phase table",cmd_do_regptmod_short_val,
			{(char*)pxmc_state_offs(pxms_ptper),
			 (char*)1,(char*)100}};
cmd_des_t const cmd_des_ptshift={0, CDESM_OPCHR|CDESM_RW,
			"REGPTSHIFT?","shift (in irc) of generated phase curves",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_ptshift),
			 0}};
cmd_des_t const cmd_des_ptvang={0, CDESM_OPCHR|CDESM_RW,
			"REGPTVANG?","angle (in irc) between rotor and stator mag. fld.",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_ptvang),
			 0}};
cmd_des_t const cmd_des_pwm1cor={0, CDESM_OPCHR|CDESM_RW,
			"REGPWM1COR?","PWM1 correction",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_pwm1cor),
			 0}};
cmd_des_t const cmd_des_pwm2cor={0, CDESM_OPCHR|CDESM_RW,
			"REGPWM2COR?","PWM2 correction",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_pwm2cor),
			 0}};
cmd_des_t const cmd_des_pwm3cor={0, CDESM_OPCHR|CDESM_RW,
			"REGPWM3COR?","PWM3 correction",cmd_do_reg_short_val,
			{(char*)pxmc_state_offs(pxms_pwm2cor),
			 0}};
#endif /*PXMC_WITH_PHASE_TABLE*/
/* axes debugging and tuning */
cmd_des_t const cmd_des_reg_type={0, CDESM_OPCHR|CDESM_RW,
			"REGTYPE?","unused",cmd_do_reg_type,{}};
cmd_des_t const cmd_des_reg_dbgset={0, CDESM_OPCHR|CDESM_RW,
			"REGDBG?","sets debug flag",cmd_do_reg_dbgset,{}};
cmd_des_t const cmd_des_reg_dbgpre={0, CDESM_OPCHR|CDESM_WR,
			"REGDBGPRE","store reference course",cmd_do_reg_dbgpre,{}};
cmd_des_t const cmd_des_reg_dbghis={0, CDESM_OPCHR|CDESM_WR,
			"REGDBGHIS","read history course",cmd_do_reg_dbghis,{}};
cmd_des_t const cmd_des_reg_dbggnr={0, CDESM_OPCHR|CDESM_WR,
			"REGDBGGNR","controller response to HIST course",cmd_do_reg_dbggnr,{}};
/* global motor commands and variables */
cmd_des_t const cmd_des_cer_all={0, CDESM_OPCHR,"PURGE","clear error flags and release failded axes",
			cmd_do_cer_all,{}};
cmd_des_t const cmd_des_clr_all={0, CDESM_OPCHR,
			"CLEAR",0,cmd_do_clr_all,{}};
cmd_des_t const cmd_des_stop_all={0, CDESM_OPCHR,
			"STOP","stop all motions",cmd_do_stop_all,{}};
cmd_des_t const cmd_des_release_all={0, CDESM_OPCHR,
			"RELEASE","releases all axes closed loop control",cmd_do_release_all,{}};
cmd_des_t const cmd_des_status_all={0, CDESM_OPCHR|CDESM_RD,
			"ST","system status bits encoded in number",
			cmd_do_status_all,{}};
cmd_des_t const cmd_des_status_bsybits={0, CDESM_OPCHR|CDESM_RD,
			"STBSYBITS","busy bits of all axes in one number",
			cmd_do_status_bsybits,{}};
cmd_des_t const cmd_des_stamp={0, CDESM_OPCHR,
			"STAMP","host communication stamp",
			cmd_do_stamp,{}};
cmd_des_t const cmd_des_r_all={0, CDESM_OPCHR,
			"R","send R! or FAIL! at finish",cmd_do_r_all,{}};
cmd_des_t const cmd_des_errstop={0, CDESM_OPCHR|CDESM_RW,
			"ERRSTOP","stop all axes at error",cmd_do_rw_int,
			{(char*)&errstop_mode,
			 0}};
cmd_des_t const cmd_des_idlerel={0, CDESM_OPCHR|CDESM_RW,
			"IDLEREL","release controllers after ? seconds",cmd_do_rw_int,
			{(char*)&idlerel_time,
			 0}};
cmd_des_t const cmd_des_reggoflg={0, CDESM_OPCHR|CDESM_RW,
			"REGGOFLG","global modifications of G? execution",cmd_do_rw_int,
			{(char*)&pxmc_go_flg,
			 0}};
cmd_des_t const cmd_des_coordmv={0, CDESM_OPCHR|CDESM_WR,
			"COORDMV","initiate coordinated movement to point f1,f2,...",cmd_do_coordmv,
			{0,0}};
cmd_des_t const cmd_des_coordmvt={0, CDESM_OPCHR|CDESM_WR,
			"COORDMVT","coord. movement with time to point mintime,f1,f2,...",cmd_do_coordmv,
			{0,(char*)1}};
cmd_des_t const cmd_des_coordrelmvt={0, CDESM_OPCHR|CDESM_WR,
			"COORDRELMVT","coord. relative movement with time to point mintime,f1,f2,...",
			cmd_do_coordmv,
			{0,(char*)3}};
cmd_des_t const cmd_des_coordsplinet={0, CDESM_OPCHR|CDESM_WR,
			"COORDSPLINET","coord. spline movement with time to point mintime,order,a11,a12,...,a21,..",
			cmd_do_coordspline,
			{0,(char*)1}};
cmd_des_t const cmd_des_coordgrp={0, CDESM_OPCHR|CDESM_WR,
			"COORDGRP","group axes for COORDMV, for ex. C,D,F",cmd_do_coordgrp,
			{0,0}};
cmd_des_t const cmd_des_coorddiscont={0, CDESM_OPCHR|CDESM_RW,
			"COORDDISCONT","max relative discontinuity between segs",cmd_do_rw_long,
			{(char*)&pxmc_coordmv_state.mcs_disca,
			 0}};
cmd_des_t const cmd_des_coordgetap={0, CDESM_OPCHR|CDESM_RD,
			"COORDAP","report actual position, format msec_time,seg_sqn,seg_par,p1,p2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_ap),0}};
cmd_des_t const cmd_des_coordgetaps={0, CDESM_OPCHR|CDESM_RD,
			"COORDAPS","report actual position and speed, format msec_time,seg_sqn,seg_par,p1,p2,...,s1,s2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_ap),(void*)2}};
cmd_des_t const cmd_des_coordgetrp={0, CDESM_OPCHR|CDESM_RD,
			"COORDRP","report requestes position, format msec_time,seg_sqn,seg_par,p1,p2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_rp),0}};
cmd_des_t const cmd_des_coordgetep={0, CDESM_OPCHR|CDESM_RD,
			"COORDEP","report planed end position, format msec_time,next_sqn,seg_par,p1,p2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_ep),(void*)1}};
cmd_des_t const cmd_des_time_mask={0, CDESM_OPCHR|CDESM_RW,
			"CMDTIMEMASK","mask to reduce reported time",cmd_do_rw_long,
			{(char*)&cmd_time_mask,
			 0}};
cmd_des_t const cmd_des_coordmv_sqn_mask={0, CDESM_OPCHR|CDESM_RW,
			"COORDSQNMASK","mask to reduce reported time",cmd_do_rw_long,
			{(char*)&cmd_coordmv_sqn_mask,
			 0}};
cmd_des_t const cmd_des_coordmv_par_bits={0, CDESM_OPCHR|CDESM_RW,
			"COORDPARBITS","bits reported as parametr unit",cmd_do_rw_long,
			{(char*)&cmd_coordmv_par_bits,
			 0}};
cmd_des_t const cmd_des_regpwrflg={0, CDESM_OPCHR|CDESM_RW,
			"REGPWRFLG","power control flags",cmd_do_regpwrflg,
			{(char*)&mo_psys_pwr_flg,
			 0}};
cmd_des_t const cmd_des_regpwron={0, CDESM_OPCHR|CDESM_RW,
			"REGPWRON","power control flags",cmd_do_regpwron,
			{(char*)&mo_psys_pwr_flg,
			 0}};
#ifdef WITH_SFI_SEL
cmd_des_t const cmd_des_regsfrq={0, CDESM_OPCHR|CDESM_RW,
			"REGSFRQ","set new sampling frequency",cmd_do_regsfrq,{}};
#endif /* WITH_SFI_SEL */

cmd_des_t const cmd_des_adc={0, CDESM_OPCHR|CDESM_RD,
			"ADC#*","read AD converter value",cmd_do_adc,{}};

/* motors low level debug */
cmd_des_t const cmd_des_axst_all={0, 0,"axst","controlers status",cmd_do_axst_all,{}};
cmd_des_t const cmd_des_callreg={0, 0,"cr",0,cmd_do_callreg,{}};
cmd_des_t const cmd_des_callgen={0, 0,"cg",0,cmd_do_callgen,{}};
cmd_des_t const cmd_des_usd_test={0, 0,"usdtest",0,cmd_do_usd_test,{}};
/* communication flags and options */
/*cmd_des_t const cmd_des_echo={0, CDESM_OPCHR|CDESM_RW,
			"ECHO","enable echoing of received character",cmd_do_rw_bitflag,
			{(char*)&cmd_io_rs232.priv.ed_line.in->flg,
			 (char*)FL_ELB_ECHO}};*/
cmd_des_t const cmd_des_echo={0, CDESM_OPCHR|CDESM_RW,
			"ECHO","enable echoing of received character",cmd_do_ioecho,{}};
cmd_des_t const cmd_des_rs232baud={0, CDESM_OPCHR|CDESM_WR,
			"RS232BAUD","communication baud rate",cmd_do_rs232baud,{}};
cmd_des_t const cmd_des_ver={0, CDESM_OPCHR|CDESM_RD,
			"VER","software version",cmd_do_ver,{}};
/* system debug and options */
cmd_des_t const cmd_des_reboot={0, CDESM_OPCHR|CDESM_RD,
			"REBOOT","reset and reboot",cmd_do_reboot,{}};
cmd_des_t const cmd_des_dprint={0, 0,"dprint","enable debug messages to print, use + - to add remove, list types ?",
			cmd_do_switches,
			{(char*)&dbg_prt_flg,(char*)dbg_prt_des}};
cmd_des_t const cmd_des_spmax={0, 0,"spmax",0,cmd_do_spmax,{}};
cmd_des_t const cmd_des_cmd1={0, CDESM_OPCHR,"cmd1",0,NULL,{(char*)100,(char*)333}};
cmd_des_t const cmd_des_date={0, 0,"date",0,cmd_do_date,{}};
cmd_des_t const cmd_des_math={0, 0,"math",0,cmd_do_math,{}};
cmd_des_t const cmd_des_coordmv_tst={0, 0,"coordtst",0,cmd_do_coordmv_tst,{}};
cmd_des_t const cmd_des_syscpuhz={0, 0,"syscpuhz",0,cmd_do_syscpuhz,{}};
cmd_des_t const cmd_des_perftest={0, 0,"perftest",0,cmd_do_perftest,{}};
cmd_des_t const cmd_des_dhrytest={0, 0,"dhry",0,cmd_do_dhrytest,{}};
cmd_des_t const cmd_des_cantest={0, 0,"can","CAN subcommands, try can ?",cmd_do_cantest,{}};
cmd_des_t const cmd_des_check_tstme={0, 0,"checktstme","pin TSTME ?",cmd_do_check_tstme,{}};
cmd_des_t const cmd_des_errorshow={0, 0,"errorshow","show captured errors",cmd_do_errorshow,{}};
cmd_des_t const cmd_des_flashid={0, 0,"flashid","check flash id at addr",cmd_do_flashid,{}};
cmd_des_t const cmd_des_flashprog={0, 0,"flashprog","flash prog value at addr",cmd_do_flashprog,{}};
cmd_des_t const cmd_des_flasherase={0, 0,"flasherase","check flash at addr erase",cmd_do_flasherase,{}};
cmd_des_t const cmd_des_flashtest={0, 0,"flashtest","test flash",cmd_do_flashtest,{}};
cmd_des_t const cmd_des_gdbbreak={0, 0,"gdbbreak","break and wait on RS232 for GDB",cmd_do_gdbbreak,{}};
cmd_des_t const cmd_des_rungzipapp={0, 0,"rungzipapp","load gzipped application and run it",cmd_do_rungzipapp,{}};

cmd_des_t const cmd_des_help={0, 0,"help","prints help for commands",
			cmd_do_help,{(char*)&cmd_rs232}};

cmd_des_t const *cmd_rs232_default[]={
  &cmd_des_go,
  &cmd_des_gorel,
  &cmd_des_pwm,
  &cmd_des_hh,
  &cmd_des_spd,
  &cmd_des_spdt,
  &cmd_des_clr,
  &cmd_des_stop,
  &cmd_des_release,
  &cmd_des_setap,
  &cmd_des_ap,
  &cmd_des_st,
  &cmd_des_r_one,
  &cmd_des_regp,
  &cmd_des_regi,
  &cmd_des_regd,
  &cmd_des_regs1,
  &cmd_des_regs2,
  &cmd_des_regmd,
  &cmd_des_regms,
  &cmd_des_regacc,
  &cmd_des_regme,
  &cmd_des_regcfg,
 #ifdef PXMC_WITH_PHASE_TABLE
  &cmd_des_ptirc,
  &cmd_des_ptper,
  &cmd_des_ptshift,
  &cmd_des_ptvang,
  &cmd_des_pwm1cor,
  &cmd_des_pwm2cor,
  &cmd_des_pwm3cor,
 #endif /*PXMC_WITH_PHASE_TABLE*/
  &cmd_des_reg_type,
  &cmd_des_reg_dbgset,
  &cmd_des_reg_dbgpre,
  &cmd_des_reg_dbghis,
  &cmd_des_reg_dbggnr,
  &cmd_des_cer_all,
  &cmd_des_clr_all,
  &cmd_des_stop_all,
  &cmd_des_release_all,
  &cmd_des_status_all,
  &cmd_des_status_bsybits,
  &cmd_des_stamp,
  &cmd_des_r_all,
  &cmd_des_errstop,
  &cmd_des_idlerel,
  &cmd_des_reggoflg,
  &cmd_des_coordmv,
  &cmd_des_coordmvt,
  &cmd_des_coordrelmvt,
  &cmd_des_coordsplinet,
  &cmd_des_coordgrp,
  &cmd_des_coorddiscont,
  &cmd_des_coordgetap,
  &cmd_des_coordgetaps,
  &cmd_des_coordgetrp,
  &cmd_des_coordgetep,
  &cmd_des_time_mask,
  &cmd_des_coordmv_sqn_mask,
  &cmd_des_coordmv_par_bits,
  &cmd_des_regpwrflg,
  &cmd_des_regpwron,
#ifdef WITH_SFI_SEL
  &cmd_des_regsfrq,
#endif /* WITH_SFI_SEL */
  &cmd_des_adc,
 #ifdef WITH_DIO
  CMD_DES_INCLUDE_SUBLIST(cmd_dio_tab),
 #endif
 #ifdef WITH_CMPQUE
  CMD_DES_INCLUDE_SUBLIST(cmd_cmpque_tab),
 #endif
 #ifdef WITH_ANALOG_AXIS
  CMD_DES_INCLUDE_SUBLIST(cmd_analogaxis_tab),
 #endif
  &cmd_des_axst_all,
  &cmd_des_callreg,
  &cmd_des_callgen,
  &cmd_des_usd_test,
  &cmd_des_echo,
  &cmd_des_rs232baud,
  &cmd_des_ver,
  &cmd_des_reboot,
  &cmd_des_dprint,
  &cmd_des_spmax,
  &cmd_des_cmd1,
  &cmd_des_date,
  &cmd_des_math,
  &cmd_des_coordmv_tst,
  &cmd_des_syscpuhz,
  &cmd_des_perftest,
  &cmd_des_dhrytest,
  &cmd_des_cantest,
  &cmd_des_check_tstme,
  &cmd_des_errorshow,
  &cmd_des_flashid,
  &cmd_des_flashprog,
  &cmd_des_flasherase,
  &cmd_des_flashtest,
  &cmd_des_gdbbreak,
  &cmd_des_rungzipapp,
 #ifdef WITH_USB
  CMD_DES_INCLUDE_SUBLIST(cmd_usb_testing),
 #endif
  &cmd_des_help,
  NULL
};

cmd_des_t const **cmd_rs232=cmd_rs232_default;


int cmd_rs232_enable_rec=1;
int cmd_rs232_generate=0;
unsigned long cmd_rs232_last_msec;
unsigned long cmd_rs232_idle_msec;
unsigned long last_status_check_msec;

int cmd_rs232_processor_init()
{
  int val;  
  char s[30];

  qsm_rs232_sendstr("# ");
  qsm_rs232_sendstr(software_ver);
  if(mo_psys_mode&PSYS_MODE_BIGB)
    qsm_rs232_sendstr(" - BigBot mode");
  if(mo_extmod_fl)
    qsm_rs232_sendstr(" - External mode");
  qsm_rs232_sendstr("\r\n");

  /* Test TSTME pin state */
  if(check_tstme(0))
    qsm_rs232_sendstr("# test mode enabled\r\n");
  else
    qsm_rs232_sendstr("# test mode disabled\r\n");

  /* kl41 init */
  *KL41_LED_WR=0x7f;
  val=kl41_lcd_init();
  kl41_lcd_wrstr("Ziji");
  i2str(s,val,4,0);
  qsm_rs232_sendstr("# kl41_lcd_init : ");
  qsm_rs232_sendstr(s);
  qsm_rs232_sendstr("\r\n");
  
  /* RTC */
  /* set_date(); */
  get_date();
  last_status_check_msec=msec_time;
  cmd_rs232_last_msec=msec_time;
  cmd_rs232_idle_msec=msec_time;
  qsm_rs232_sendstr("# date : ");
  qsm_rs232_sendstr(date);
  qsm_rs232_sendstr("\r\n");

  cmd_io_rs232.priv.ed_line.in->inbuf=0;
  cmd_io_rs232.priv.ed_line.out->inbuf=0;

 #ifdef WITH_RS232_ECHO_OFF
  cmd_io_rs232.priv.ed_line.in->flg&=~FL_ELB_ECHO;
 #else /* WITH_RS232_ECHO_OFF */
  cmd_io_rs232.priv.ed_line.in->flg|=FL_ELB_ECHO;
 #endif /* WITH_RS232_ECHO_OFF */
  return 0;
}

int cmd_rs232_processor_run()
{
  int val;  
  static int cnt=0;
  int i;
  unsigned new_msec;
  char s[30];
  pxmc_state_t *mcs;
  cmd_io_t* cmd_io;
   
  /* check for errstop */
  if(errstop_mode)
  {
    static short old_check_time;
    short new_check_time;
    new_check_time=msec_time;
    if(new_check_time!=old_check_time){
      old_check_time=new_check_time;
      cmd_check_errstop();
    }
  }
  
  /* status checking and LED update */
  if((msec_time-last_status_check_msec)>250){
    last_status_check_msec+=250;
    status_check_and_led();   
  }

  /* coordinator bottom handling */
  if(pxmc_coordmv_state.mcs_flg&MCS_BOTT_m){
    pxmc_coordmv_bottom(&pxmc_coordmv_state);
  }
  
  cmd_io=&cmd_io_rs232;

  /* receive characters from serial line only when is not out pending */
  if(!cmd_rs232_line_out(cmd_io)&&cmd_rs232_enable_rec){
    if(cmd_rs232_line_in(cmd_io)>0){
      if(dbg_prt_flg&DBGPF_CMD_PROC)
        printf("ed_line_buf : %s\r\n",cmd_io->priv.ed_line.in->buf);
      if(cmd_rs232){
	val=proc_cmd_line(cmd_io, cmd_rs232, cmd_io->priv.ed_line.in->buf);
      }else{
	val=-CMDERR_BADCMD;
      }
      if(dbg_prt_flg&DBGPF_CMD_PROC)
        printf("proc_cmd_line returned %d\r\n",val);
      if(cmd_io->priv.ed_line.out->inbuf){
	cmd_io_putc(cmd_io,'\r');
	cmd_io_putc(cmd_io,'\n');
      }else if(val<0){
	char s[20];
	cmd_io_write(&cmd_io_rs232,"ERROR ",6);
	i2str(s,-val,0,0);
	cmd_io_write(cmd_io,s,strlen(s));
	cmd_io_putc(cmd_io,'\r');
	cmd_io_putc(cmd_io,'\n');
      }
      cmd_rs232_idle_msec=msec_time+idlerel_time*1000l;
    }
  }
  
  /* Check for ready condition */
  if(!cmd_io->priv.ed_line.out->inbuf){
    cmd_check_r(cmd_io);
    if(cmd_io->priv.ed_line.out->inbuf){
      cmd_io_putc(cmd_io,'\r');
      cmd_io_putc(cmd_io,'\n');
      cmd_rs232_line_out(cmd_io);
    }
  }

 #ifdef WITH_DIO
  /* Check for trigger occurences */
  if(!cmd_io->priv.ed_line.out->inbuf&&dio_trig_occured){
    do_dio_trig_send(cmd_io);
    if(cmd_io->priv.ed_line.out->inbuf){
      cmd_io_putc(cmd_io,'\r');
      cmd_io_putc(cmd_io,'\n');
      cmd_rs232_line_out(cmd_io);
    }
  }

  /* Check for comparator events */
  if(!cmd_io->priv.ed_line.out->inbuf&&dio_cmp_occured){
    do_dio_cmp_send(cmd_io);
    if(cmd_io->priv.ed_line.out->inbuf){
      cmd_io_putc(cmd_io,'\r');
      cmd_io_putc(cmd_io,'\n');
      cmd_rs232_line_out(cmd_io);
    }
  }
 #endif /* WITH_DIO */

 #ifdef WITH_CMPQUE
  if(!cmd_io->priv.ed_line.out->inbuf&&pxmc_cmpque_bottom_needed) {
    pxmc_cmpque_bottom();
    if(cmd_io->priv.ed_line.out->inbuf){
      cmd_io_putc(cmd_io,'\r');
      cmd_io_putc(cmd_io,'\n');
      cmd_rs232_line_out(cmd_io);
    }
  }
 #endif

  /* idle state checking */
  if(idlerel_time&&((cmd_rs232_idle_msec-msec_time)<=0)){
    val=0;
    for(i=0;i<pxmc_main_list.pxml_cnt;i++){
      mcs=pxmc_main_list.pxml_arr[i];
      if(mcs){
        val|=mcs->pxms_flg;
      }
    }
    if(!(val&(PXMS_BSY_m|PXMS_ENG_m))){
      for(i=0;i<pxmc_main_list.pxml_cnt;i++){
        mcs=pxmc_main_list.pxml_arr[i];
        if(mcs) pxmc_set_const_out(mcs,0);
      }
    }
    cmd_rs232_idle_msec=msec_time+idlerel_time*(1000l/2);
  }

  if(cmd_rs232_generate)
  {
    val='0'+cnt++;
    if(cnt>9) cnt=0;
    while(qsm_rs232_sendch(val)<0);
  }
  new_msec=msec_time;
  if(new_msec-cmd_rs232_last_msec>2000)
  {
    cmd_rs232_last_msec+=2000;
    if(!cmd_io_rs232.priv.ed_line.in->lastch){

      if(dbg_prt_flg&DBGPF_PER_TIME){
        get_date();
        qsm_rs232_sendstr("date! ");
        qsm_rs232_sendstr(date);
        qsm_rs232_sendstr(" spmax");
        i2str(s,ctm4_sfi_spmax,6,0);
        qsm_rs232_sendstr(s);
        qsm_rs232_sendstr(" msec");
        i2str(s,msec_time,8,3);
        qsm_rs232_sendstr(s);
        qsm_rs232_sendstr("\r\n");
      }
      
      if(dbg_prt_flg&DBGPF_PER_POS){
        char reg_ascii_id[4];
	short reg_mask;
	reg_ascii_id[1]='!';
	reg_ascii_id[2]=0;
	
        for(i=0,reg_mask=1;i<pxmc_main_list.pxml_cnt;
	    i++,reg_mask<<=1){
	  if(dbg_prt_flg&reg_mask&DBGPF_AXES){
	    reg_ascii_id[0]='A'+i;
            qsm_rs232_sendstr(reg_ascii_id);
            mcs=pxmc_main_list.pxml_arr[i];
            i2str(s,mcs->pxms_ap>>mcs->pxms_subdiv,8,0);
            qsm_rs232_sendstr(s);
            qsm_rs232_sendstr(mcs->pxms_flg&PXMS_ERR_m?" E":
	    		      mcs->pxms_flg&PXMS_BSY_m?" B":" -");
            val=usd_irc_get_mark(mcs);
            qsm_rs232_sendstr(val&0x78?" S":" -");
            qsm_rs232_sendstr(val&(0x78<<8)?"I":"-");
	    if(1){		/* for USD index check */
	      typeof(USD_IRC_ADDR) pu;
    	      pu=usd_irc_addr_tab[mcs->pxms_out_info];
	      /*i2str(s,pu[1],3,0);*/
	      val=pu[1];
	      qsm_rs232_sendstr(val&USD_7266_FLAG_IDX?" idx":" ---");
	      qsm_rs232_sendstr(val&USD_7266_FLAG_S?" s":" -");
	      qsm_rs232_sendstr(val&USD_7266_FLAG_CPT?" cpt":" ---");
	    }
            qsm_rs232_sendstr("\r\n");
	  }
	}
      }
      
      if(dbg_prt_flg&DBGPF_PER_CUR){ /* ADC on SPI values */
	qsm_rs232_sendstr("cur!");
	for(i=0;i<8;i++){
	  qsm_rs232_sendstr(" ");
	  i2str(s,tlcadc_read(i),5,0);
	  qsm_rs232_sendstr(s);
	}
	qsm_rs232_sendstr("\r\n");
      }
      if(dbg_prt_flg&DBGPF_PER_ADC){ /* QADC values */
	qsm_rs232_sendstr("adc0!");
	for(i=0;i<18;i++){
	  if(i==8)qsm_rs232_sendstr("\r\nadc8!");
	  qsm_rs232_sendstr(" ");
	  i2str(s,qadc_read_ch(i),5,0);
	  qsm_rs232_sendstr(s);
	}
	qsm_rs232_sendstr("\r\n");
      }
      if(dbg_prt_flg&DBGPF_QSPI_LED){ /* SQPI LEDs and SWITCHes */
        sprintf(s,"led! send 0x%04X, rec 0x%04X\n\r",
		QSM_QSPITR[qspi_led_qspi_loc],
		QSM_QSPIRR[qspi_led_qspi_loc]);
	qsm_rs232_sendstr(s);
      }      
    }
    if(kl41_lcd_init_ok){
      *KL41_LED_WR=(msec_time/2000)&0x7f;
      kl41_lcd_wrstr(".");
    }
  }
  if(kl41_lcd_init_ok){
    int scancode=0;
    scancode=kl41_kbd_read_scan_code();
    if(scancode>=0){
      char s[30];
      kl41_lcd_wrcmd(CHMOD_LCD_CLR);
      sprintf(s,"Scan:%02x",scancode);
      kl41_lcd_wrstr(s);
    }
  }
  return 0;
}

/*
 *-----------------------------------------------------------
 */
#ifdef WITH_USB

int cmd_usb_enable_rec=1;
int cmd_usb_processor_run(void);
int cmd_usb_processor_init(void);
int usb_ready2work=0;

usb_device_t usb_pdi_device;

int usb_device_init(void)
{
  usb_pdi_device.id = 1;
  usb_pdi_device.cntep = 0;
  
  #ifndef USB_PDI_DIRECT_FNC
  usb_pdi_device.init = usb_pdi_init;
  #endif /*USB_PDI_DIRECT_FNC*/
  usb_debug_set_level(0);
  usb_init(&usb_pdi_device);
  if(1) {
    usb_connect(&usb_pdi_device);
    usb_ready2work=1;
  }
// usb_flags.bits.terminal_mode=1;
// usb_pdi_device.vendor_fnc=usb_native_loader;

  return 0;
}

int cmd_do_usbcon(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  usb_connect(&usb_pdi_device);
  usb_ready2work=1;
  return 0;
}

int cmd_do_usbdiscon(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  usb_ready2work=0;
  usb_disconnect(&usb_pdi_device);
  return 0;
}

int cmd_do_usbchipid(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int chip_id=pdiGetChipID();
  printf("PDIUSB chip id is %04x\n",chip_id);
  return 0;
}

int cmd_do_usbtest1(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{

  unsigned char bugacc=0, b;
  int i, bugcnt=0;
  
  for(i=0; i<0x100; i++) {

    pdiSetDMA( i);
    b=pdiGetDMA();
    if(b != (unsigned char)i) {
      bugacc |= b ^ i;
      bugcnt++;
      if(bugcnt <= 10) {
        printf("PDIUSB readback failure, %02x -> %02x\n",i,b);
      }
    }
  }

  if(!bugcnt)
    printf("PDIUSB check 1 OK\n");
  else {
    printf("PDIUSB check 1 failed %d times, bus error mask 0x%02x\n",bugcnt,bugacc);
  }

  pdiSetDMA( PDI_DMA_EP4_INT | PDI_DMA_EP5_INT);

  return 0;
}

int cmd_do_usbm(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long cmd;
  long val;
  char *s;
  
  s=param[1];
  
  if(si_long(&s,&cmd,16)<0) return -1;
  
  *PDIUSB_COMMAND_ADDR=cmd;
  
  printf("USB cmd %02x", (int)cmd);

  for(si_skspace(&s);*s;si_skspace(&s)) {
    if(si_long(&s,&val,16)>=0) {
      printf(" %02x", (int)val);
      *PDIUSB_WRITE_DATA_ADDR=val;
    }else{
      if(*s != '?') break;
      s++;
      val=*PDIUSB_READ_DATA_ADDR;
      printf(" %02x", (int)val);
    }
  }
  if(*s>=' ') printf("USB syntax error\n");
  else printf("\nUSB status %02x\n",*PDIUSB_COMMAND_ADDR);

  /*
  cmd_io_write(cmd_io,param[0],param[1]-param[0]);
  cmd_io_putc(cmd_io,' ');
  i2str(str,val,len,form);
  cmd_io_write(cmd_io,str,strlen(str));
  */
  
  return 0;
}


int cmd_do_usbvlev(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  char *s;
  
  s=param[1];
  
  if(si_long(&s,&val,16)<0) return -1;

  usb_debug_set_level(val);
  
  return 0;
}

int cmd_usb_processor_run()
{
  return 0;
}

int cmd_usb_processor_init()
{
  return 0;
}

cmd_des_t const cmd_des_usbcon={0, 0,"usbcon","USB connect to bus",
			cmd_do_usbcon,{}};

cmd_des_t const cmd_des_usbdiscon={0, 0,"usbdiscon","USB disconnect from bus",
			cmd_do_usbdiscon,{}};

cmd_des_t const cmd_des_usbchipid={0, 0,"usbchipid","print USB chip ID",
			cmd_do_usbchipid,{}};

cmd_des_t const cmd_des_usbtest1={0, 0,"usbtest1","test presence of USB chip",
			cmd_do_usbtest1,{}};

cmd_des_t const cmd_des_usbm={0, 0,"usbm","direct access to USB chip",
			cmd_do_usbm,{}};

cmd_des_t const cmd_des_usbvlev={0, 0,"usbvlev","USB verbosity level",
			cmd_do_usbvlev,{}};

cmd_des_t const *cmd_usb_testing[]={
  &cmd_des_usbcon,
  &cmd_des_usbdiscon,
  &cmd_des_usbchipid,
  &cmd_des_usbtest1,
  &cmd_des_usbm,
  &cmd_des_usbvlev,
  NULL
};


#endif /*WITH_USB*/


/*
 *-----------------------------------------------------------
 */

short mo_extmod_fl=0;

void mo_switch_to_extmod(void)
{
  /* Set ADC15(PQA7) = 1 to disable all control outputs */
  *QADC_PORTQ|=0x8000;

  /* DASM4 initialize */
  *CTM4_DASM3SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_IPWM;

  /* DASM4 initialize */
  *CTM4_DASM4SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_IPWM;

  /* DASM9 initialize */
  *CTM4_DASM9SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_IPWM;

  /* DASM10 initialize */
  *CTM4_DASM10SIC=(CTM4_DASM_BSL*1)|CTM4_DASM_EDPOL|CTM4_DASM_IPWM;

  /* Set ADC13(PQA5) = 1 to enable external control of PWM */
  *QADC_PORTQ|=0x2000;
  *QADC_DDRQA|=0x2000;
}

int mo_test_extmod_sw(void)
{
  int testcnt=1000;
  int i;
  int testact=0;

  for(i=0;i<testcnt;i++)
    if(*SIM_PORTF0 & (1<<2))
      testact++;

  return (testact*2>testcnt)?1:0;
}
/*
 *-----------------------------------------------------------
 */

void __main(void) {;};

int main()
{ unsigned flags=0;
  /* initialize PLL clock generator, cpu_ref_hz and cpu_sys_hz */
  sim_init();
 #ifndef GDBSTUB_RAM_TARGET
  /* clear GDB stub data area */
  gdb_clear_data_seg();
 #else /* GDBSTUB_RAM_TARGET */
  qsm_break2gdb=0;
 #endif /* GDBSTUB_RAM_TARGET */
  /* initialize RS232 communication interface */
  qsm_rs232_setmode(WITH_RS232_BAUDRATE,QSM_RS232_MODEA,1);
  /* initialize fast timer and CTM4 PWM generators */
  msec_time=0;
  /* mode of hardware specific parts */
 #ifdef CONFIG_MARS8_PSYS_MODE
  mo_psys_mode=CONFIG_MARS8_PSYS_MODE;
 #else /*CONFIG_MARS8_PSYS_MODE*/
  mo_psys_mode= 0*PSYS_MODE_BIGB |
                0*PSYS_MODE_BIGB_MARK |
		0*PSYS_MODE_BIGB_EXTC;
 #endif /*CONFIG_MARS8_PSYS_MODE*/
  mo_psys_pwr_mains=0;
  mo_psys_pwr_flg=0;
  ctm4_pwm_and_sfi_init();
  /* initialze USD7266 IRC counters */
  usd_irc_init_all();
  /* start 16 chanel QADC conversion */
  qadc_init_16ch();
  /* initialize SPI subsystem */
  qspi_mode_init();
  tlcadc_init();

  if(mo_psys_mode&PSYS_MODE_BIGB_EXTC){
    if(mo_test_extmod_sw()){
      mo_switch_to_extmod();
      mo_extmod_fl=1;
    }
  }

  /* initialize pxmc_main_list */
  pxmc_initialize();
  
  qspi_led_init(); /* LED indicators at PCS2 non */

#ifdef WITH_DIO
  /* initialize digital IO subsystem */
  mo_dio_init();
#endif

  /* initialize CAN subsystem */
  can_hw_init(-1,-1);
  
  /* *(int*)0xa00000=0; */

  /* force start of serial debugger and stop */
  if(0){
    qsm_break2gdb=1;
    gdb_set_debug_traps(gdb_without_hooks);
   #if 1
    breakpoint();
   #else
    /* check serial remote stub */
    __asm__ (
      "   movel %a6,%sp@-\n"
      "   movel #0x12345600,%d0\n"
      "   movel #0x23456711,%d1\n"
      "   movel #0x34567822,%d2\n"
      "   movel #0x45678933,%d3\n"
      "   movel #0x56789044,%d4\n"
      "   movel #0x67890155,%d5\n"
      "   movel #0x78901266,%d6\n"
      "   movel #0x89012377,%d7\n"
      "   movel #0x90123400,%a0\n"
      "   movel #0x01234511,%a1\n"
      "   movel #0x12345622,%a2\n"
      "   movel #0x23456733,%a3\n"
      "   movel #0x34567844,%a4\n"
      "   movel #0x45678955,%a5\n"
      "   movel #0x56789066,%a6\n"
      "   movew #0x20a5,%sr\n"
      "   /*trap  #1*/\n"	/* this one to check compiled break */
      "1: /*bra   1b*/\n"		/* this one to check ctrl+c */
      "   movel %sp@+,%a6\n"
    );
   #endif
  }

  save_flags(flags);
  flags&=~0x0700;
  restore_flags(flags);

 #ifdef WITH_USB
  usb_device_init();
  cmd_usb_processor_init();
 #endif /*WITH_USB*/
  cmd_rs232_processor_init();
  do{
    cmd_rs232_processor_run();
   #ifdef WITH_USB
    cmd_usb_processor_run();
    if(usb_ready2work) {
      if(PDIUSB_TEST_IRQ()) usb_check_events(&usb_pdi_device);
      usb_control_response(&usb_pdi_device);
    }
   #endif /*WITH_USB*/
  }while(1);
  return 0;
}

