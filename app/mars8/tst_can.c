/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  tst_can.c - 68376 TouCAN module low level hardware support
              routines and test code
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <can_hw.h>
#include <utils.h>

int can_hw_baud=1000000;
int can_hw_mode=0;

int can_hw_init(int baud,int mode)
{
  int i;
  int dummy;
  int tq_per_bit;
  int presdiv,propseg,pseg1,pseg2;
  int proptim;

  if(baud==-1) baud=can_hw_baud;
  if(mode==-1) mode=can_hw_mode;
  
  /* reset and halt TouCAN module */
  *CAN_MCR=CAN_HALT|CAN_SOFTRST;
  *CAN_MCR=CAN_HALT|__val2mfld(CAN_IARB,ISR_CAN_IARB)|CAN_WAKEMSK*0;
  
  /* stop activity of all buffers */
  for(i=0;i<16;i++)
    CAN_BUFF[i].control=__val2mfld(CAN_BSC_CODE,CAN_BST_DISABLE);
  
  proptim=5;	/* propagation time [100ns] */
  tq_per_bit=(cpu_sys_hz+baud/2)/baud;
  presdiv=tq_per_bit/9;
  if(presdiv>256) presdiv=256;
  while(tq_per_bit%presdiv){
    if(presdiv<=0) return -1;
    presdiv--;
    if(tq_per_bit/presdiv>33) return -1;
  };
  tq_per_bit/=presdiv;
  /* convert propagation time from [100ns] to time quanta */
  i=10000*presdiv;
  proptim=(proptim*(cpu_sys_hz/1000)+i/2)/i;
  if(presdiv==1) { 
    i=tq_per_bit-3; pseg2=2;	/* minimum for bit processing */
  }else{
    i=tq_per_bit-2; pseg2=1;
  }
  propseg=proptim>8?8:proptim;
  if(propseg>i-1) propseg=i-1;
  i-=propseg;
  pseg2+=(i-2)/2;
  if(pseg2>16) pseg2=16;
  pseg1=tq_per_bit-1-propseg-pseg2;
  if(pseg1>8){
    i=pseg1-8; pseg1=8;
    propseg+=i; if(propseg>8){
      pseg2+=propseg-8; propseg=8;
    }
  } 
  
  can_hw_mode=mode;
  can_hw_baud=cpu_sys_hz/presdiv/tq_per_bit;
  
  if(mode&0x10000)
    printf("# CAN baud %d, pdiv %d, tqpb %d, p %d s1 %d s2 %d proptim %d\r\n",
 	    can_hw_baud,presdiv,tq_per_bit,propseg,pseg1,pseg2,proptim);

  /* S-clock is equal to system clock / presdiv */
  *CAN_PRESDIV=presdiv-1;

  /* TouCAN hardware basic settings */
  *CAN_CTRL0=CAN_BOFFMSK*0|CAN_ERRMSK*0|CAN_RXMODE*0|CAN_TXMODE*0;

  /* TouCAN bit timming = 1+(PROPSEG+1)+(PSEG1+1)+(PSEG2+1)*Sclk*/
  /*	synchronisation	= 1*Sclock				*/
  /*	propagation	= <1,8>*Sclk  = (PROPSEG+1)*Sclk	*/
  /*			> 2 *driver+bus+line+comparator delay	*/
  /*	phase 1		= <1,8>*Sclk  = (PSEG1+1)*Sclk		*/
  /*	phase 2		= <1,16>*Sclk = (PSEG2+1)*Sclk		*/
  /*			>= 3 for PRESDIV=0 else >=2		*/
  /* Other settings */
  /*	adjust jump width (RJW+1)*Sclk				*/
  
  *CAN_CTRL1=CAN_SAMP*1|CAN_LOOP*0|CAN_TSYNC*0|CAN_LBUF*1|
	     __val2mfld(CAN_PROPSEG,propseg-1);

  *CAN_CTRL2=__val2mfld(CAN_RJW,2)|__val2mfld(CAN_PSEG1,pseg1-1)|
	     __val2mfld(CAN_PSEG2,pseg2-1);

  /* disable and configure interrupts */
  /* TouCAN interrupts : 16 one for each buffer, bus off, error and wake */
  *CAN_IMASK=0;
  *CAN_ICR=__val2mfld(CAN_ILCAN,ISR_CAN_IL)|(CAN_IVBA&ISR_CAN_INTV)|
  	   (*CAN_ICR&~(CAN_ILCAN|CAN_IVBA));

  /* enable CAN communication */
  *CAN_MCR&=~CAN_HALT;

  /* release buffer lock */
  dummy=*CAN_TIMER;

  return 0;
}

/* set receive mask for defined buffer */
/* buffers 0 to 13 have common mask for TouCAN */
int can_hw_set_buf_mask(int bnr, long mask, int ext)
{
  if(bnr>15) return -1;
  mask=CAN_ID_FLG2HWID(mask,ext,0);
  if(bnr==15) *CAN_RX15MSK=mask;
  else if(bnr==14) *CAN_RX14MSK=mask;
  else *CAN_RXGMSK=mask;
  return 1;
}

const char *can_st_names[]={
  "DIS ",
  "BUSY",
  "FULL",
  "?3? ",
  "EMPT",
  "?5? ",
  "OVER",
  "?7? ",
  "NRDY",
  "?9? ",
  "RREP",
  "?b? ",
  "SEND",
  "?d? ",
  "SARR",
  "?f? "
};

void can_hw_print()
{
  int bnr;
  int ctrl;
  int st,len;
  int i,id,ext,rtr;
  int dummy;
  uint32_t hwid;

  printf("CAN buffers at time 0x%04x rxerr=%d txerr=%d\n",
  	   *CAN_TIMER,*CAN_RXECTR,*CAN_TXECTR);

  for(bnr=0;bnr<16;bnr++)
  {
    ctrl=CAN_BUFF[bnr].control;
    st=__mfld2val(CAN_BSC_CODE,ctrl);
    if(st==CAN_BST_DISABLE) continue;
    len=__mfld2val(CAN_BSC_LEN,ctrl);
    hwid=CAN_BUFF[bnr].id.hwid;
    ext=hwid&CAN_HWID_IDE?1:0;
    id=ext?CAN_HWID2IDE(hwid):CAN_HWID2ID_STD(hwid);
    rtr=ext?(hwid&CAN_HWID_RTR?1:0):(hwid&CAN_HWID_SRR?1:0);

    printf("buf %2d st=%s len=%d id%c=0x%08x %c",
		bnr,can_st_names[st],len,ext?'e':'_',id,rtr?'R':'_');
    for(i=0;i<len;i++) printf(" 0x%02x",ctrl=CAN_BUFF[bnr].data[i]);
    printf("\n");
    fflush(NULL);
  }

  printf("masks 0-13: 0x%08x 14: 0x%08x 15: 0x%08x\n", 
         CAN_HWID2IDE(*CAN_RXGMSK),CAN_HWID2IDE(*CAN_RX14MSK),
	 CAN_HWID2IDE(*CAN_RX15MSK));

  st=*CAN_ESTAT;
  printf("status");
  if(st&CAN_BITERR) printf(" BITERR%d",__mfld2val(CAN_BITERR,st));
  if(st&CAN_ACKERR) printf(" ACKERR");
  if(st&CAN_CRCERR) printf(" CRCERR");
  if(st&CAN_FORMERR) printf(" FORMERR");
  if(st&CAN_STUFFERR) printf(" STUFFERR");
  if(st&CAN_TXWARN) printf(" TXWARN");
  if(st&CAN_RXWARN) printf(" RXWARN");
  if(st&CAN_IDLE) printf(" IDLE");
  else if(st&CAN_TXRX) printf(" TX");
    else printf(" RX");
  if(st&CAN_FCS) printf(" FCS%d",__mfld2val(CAN_FCS,st));
  printf("\n");
  fflush(NULL);

  dummy=*CAN_TIMER;
}

int can_hw_test_1()
{
  int dummy;
  
  can_hw_print();

  /* prepare buffer 1 for message receiption */
  CAN_BUFF[1].control=__val2mfld(CAN_BSC_CODE,CAN_BST_DISABLE);
  if(0){
    CAN_BUFF[1].id.std.id=0x411<<5;	/* filling standard frame ID */
    CAN_BUFF[1].id.std.time=0x0;
  } else {				/* filling extended frame ID */
    CAN_BUFF[1].id.hwid=CAN_IDE2HWID(0x12345678)|CAN_HWID_IDE|CAN_HWID_SRR;
  }					/* mark buffer as empty and receive */
  CAN_BUFF[1].control=__val2mfld(CAN_BSC_CODE,CAN_BST_EMPTY);

  /* prepare message with 2 data bytes for send in buffer 0 */
  CAN_BUFF[0].control=__val2mfld(CAN_BSC_CODE,CAN_BST_DISABLE);
  if(0){
    CAN_BUFF[0].id.std.id=0x411<<5;	/* filling standard frame ID */
    CAN_BUFF[0].id.std.time=0x0;
  } else {				/* filling extended frame ID */
    CAN_BUFF[0].id.hwid=CAN_IDE2HWID(0x12345678)|CAN_HWID_IDE|CAN_HWID_SRR;
  }
  CAN_BUFF[0].data[0]=0x55;		/* prepare data to send */
  CAN_BUFF[0].data[1]=0xaa;
  CAN_BUFF[0].control=__val2mfld(CAN_BSC_CODE,CAN_BST_SEND)|
  		      __val2mfld(CAN_BSC_LEN,2); /* mark buffer to be send */

  dummy=*CAN_TIMER;			/* release buffer lock */
  return 0;
}

#define CAN_TXSTDBNR 12
#define CAN_TXEXTBNR 13
#define CAN_RXSTDBNR 14
#define CAN_RXEXTBNR 15

void can_rxstd_isr(int intno, void *dev_id, struct pt_regs *regs);

irq_handler_t can_rxstd_handler={
  handler: can_rxstd_isr,
  flags:   0,
  dev_id:  0,
  devname: "can_rxstd",
  next:    0
};

long can_rxstd_isr_txid=0;

void can_rxstd_isr(int intno, void *dev_id, struct pt_regs *regs)
{
  int dummy;
  long id;
  int len;
  uint8_t data[8];
  uint32_t hwid;

  atomic_clear_mask_w(1<<CAN_RXSTDBNR,CAN_IFLAG);
  do{
    dummy=CAN_BUFF[CAN_RXSTDBNR].control;
  }while(dummy&__val2mfld(CAN_BSC_CODE,CAN_BST_BUSY));
  len=__mfld2val(CAN_BSC_LEN,dummy);	/* length of received data */
  hwid=CAN_BUFF[CAN_RXSTDBNR].id.hwid;
  /* ID of received message */
  id=(hwid&CAN_HWID_IDE)?CAN_HWID2IDE(hwid):CAN_HWID2ID_STD(hwid);
  /* ext=hwid&CAN_HWID_IDE?1:0; */	/* only STD messages in this buffer */
  memcpy(data,(void*)CAN_BUFF[CAN_RXSTDBNR].data,8);
  /* prepare receive buffer for next message */
  CAN_BUFF[CAN_RXSTDBNR].control=__val2mfld(CAN_BSC_CODE,CAN_BST_EMPTY);

  if(can_rxstd_isr_txid==id)
  { /* received message, which was transmitted last */
    can_rxstd_isr_txid=0;
    return;
  };
  if(__mfld2val(CAN_BSC_CODE,CAN_BUFF[CAN_TXSTDBNR].control)>=CAN_BST_SEND)
  { /* output buffer is not free */
    return;
  }
  CAN_BUFF[CAN_TXSTDBNR].control=__val2mfld(CAN_BSC_CODE,CAN_BST_NREADY);
  CAN_BUFF[CAN_TXSTDBNR].id.hwid=CAN_ID_FLG2HWID(id,id>0x7ff,0);
  memcpy((void*)CAN_BUFF[CAN_TXSTDBNR].data,data,8);
  /* send transmit buffer */
  CAN_BUFF[CAN_TXSTDBNR].control=__val2mfld(CAN_BSC_CODE,CAN_BST_SEND)|
  				 __val2mfld(CAN_BSC_LEN,len);
  can_rxstd_isr_txid=id;
  
  dummy=*CAN_TIMER;			/* release buffer lock */
}

int can_hw_test_mirror(char *s)
{
  int dummy;

  atomic_clear_mask_w(1<<CAN_RXSTDBNR,CAN_IMASK);
  
  if(test_irq_handler(ISR_CAN_INTV+CAN_RXSTDBNR,&can_rxstd_handler)==0)
    add_irq_handler(ISR_CAN_INTV+CAN_RXSTDBNR,&can_rxstd_handler);
  can_hw_set_buf_mask(CAN_RXSTDBNR,0l,0);

  CAN_BUFF[CAN_RXSTDBNR].id.hwid=CAN_IDE2HWID(0)|CAN_HWID_IDE*0;
  CAN_BUFF[CAN_RXSTDBNR].control=__val2mfld(CAN_BSC_CODE,CAN_BST_EMPTY);

  atomic_set_mask_w(1<<CAN_RXSTDBNR,CAN_IMASK);

  dummy=*CAN_TIMER;			/* release buffer lock */
  
  return 0;
}

int can_hw_test(char *s)
{
  char cmd[10];
  int  rtr;
  int  err=0;
  int  i;
  long bnr,l;
  int dummy;

  si_skspace(&s);
  
  if((*s=='?')||!strcmp(s,"help")){  
    printf("possible CAN subcommands\n\
	test		sends buff 0 and empty buff 1\n\
	p		print CAN buffers\n\
	ide<bnr> <id>r	sets ext fr <id> for buff <bnr>\n\
	id<bnr> <id>r	same for std fr, r for remote\n\
	mask<bnr> <msk>	sets mask for buff 0-13, 14 or 15\n\
	empty<bnr>	prep buff for receive\n\
	send<bnr>	send buff\n\
	reply<bnr>	mark buff as reply\n\
	data<bnr> <x>	set data\n\
	baud <bd>	set new baudrate\n\
	reset		reset can subsystem\n\
	dominant	send dominant value\n\
	mirror [inc]	message mirroring\n");
    return 0;
  }
  
  while(si_alphan(&s,cmd,sizeof(cmd)-1))
  {
    if(!strcmp(cmd,"test")) can_hw_test_1();
    else if(!strcmp(cmd,"p")) can_hw_print();
    else if(!strcmp(cmd,"ide")){
      if(si_long(&s,&bnr,0)<0){err=-1;break;};
      if(bnr>15){err=-1;break;};
      if(si_long(&s,&l,0)<0){err=-1;break;};
      if((*s=='r')||(*s=='R')){s++;rtr=1;}
        else rtr=0;
      CAN_BUFF[bnr].id.hwid=CAN_IDE2HWID(l)|CAN_HWID_IDE|CAN_HWID_SRR|
      			      (rtr?CAN_HWID_RTR:0);
    }else if(!strcmp(cmd,"id")){
      if(si_long(&s,&bnr,0)<0){err=-1;break;};
      if(bnr>15){err=-1;break;};
      if(si_long(&s,&l,0)<0){err=-1;break;};
      if((*s=='r')||(*s=='R')){s++;rtr=1;}
        else rtr=0;
      CAN_BUFF[bnr].id.std.id=l<<5|(rtr?CAN_STDID_RTR:0);
      CAN_BUFF[bnr].id.std.time=0;
    }else if(!(i=strcmp(cmd,"mask"))||!strcmp(cmd,"maske")){
      if(si_long(&s,&bnr,0)<0){err=-1;break;};
      if(si_long(&s,&l,0)<0){err=-1;break;};
      if(can_hw_set_buf_mask((int)bnr,l,i/*ext*/)<0)
        {err=-1;break;};
    }else if(!strcmp(cmd,"empty")){
      if(si_long(&s,&bnr,0)<0){err=-1;break;};
      if(bnr>15){err=-1;break;};
      CAN_BUFF[bnr].control=__val2mfld(CAN_BSC_CODE,CAN_BST_EMPTY);
    }else if(!strcmp(cmd,"send")){
      if(si_long(&s,&bnr,0)<0){err=-1;break;};
      if(bnr>15){err=-1;break;};
      CAN_BUFF[bnr].control&=~CAN_BSC_CODE;
      CAN_BUFF[bnr].control|=__val2mfld(CAN_BSC_CODE,CAN_BST_SEND);
    }else if(!strcmp(cmd,"reply")){
      if(si_long(&s,&bnr,0)<0){err=-1;break;};
      if(bnr>15){err=-1;break;};
      CAN_BUFF[bnr].control&=~CAN_BSC_CODE;
      CAN_BUFF[bnr].control|=__val2mfld(CAN_BSC_CODE,CAN_BST_RREPLY);
    }else if(!strcmp(cmd,"data")){
      if(si_long(&s,&bnr,0)<0){err=-1;break;};
      if(bnr>15){err=-1;break;};
      i=0;
      do{
         if(si_long(&s,&l,0)<0){err=-1;break;};
         CAN_BUFF[bnr].data[i++]=l;
         si_skspace(&s);
      }while(isdigit((uint8_t)(*s))&&(i<8));
      CAN_BUFF[bnr].control&=~CAN_BSC_LEN;
      CAN_BUFF[bnr].control|=__val2mfld(CAN_BSC_LEN,i);
    }else if(!strcmp(cmd,"reset")){
      can_hw_init(-1,-1);
    }else if(!strcmp(cmd,"baud")){
      if(si_long(&s,&l,0)<0){err=-1;break;};
      can_hw_mode|=0x10000;
      can_hw_init(l,-1);
    }else if(!strcmp(cmd,"dominant")){
      *CAN_MCR|=CAN_HALT|CAN_SOFTRST;
      *CAN_MCR&=~CAN_SOFTRST;
      *CAN_CTRL0=(*CAN_CTRL0 & ~CAN_TXMODE)|__val2mfld(CAN_TXMODE,1);
      *CAN_MCR&=~CAN_HALT;
    }else if(!strcmp(cmd,"mirror")){
      si_skspace(&s);
      can_hw_test_mirror(s);
      break;
    }
    else {printf("unknown CAN command\n"); break;}
    si_skspace(&s);
  };
  dummy=*CAN_TIMER;			/* release buffer lock */
  if(err) printf("line parse error\n");
  fflush(NULL);
  return 0;
}

/*
   CAN command examples
   
   can id1 0x123 data1 0x10 0x21 0x32 0x43 send1
   can id4 0x000 data4 0x11 0x22 0x33 0x44 send4
   can id4 0x75b2 data4 0x34 0x43 send4

   can id3 0x411 data3 0xaa 0xbb 0xcc reply3
   can id2 0x411r send2
   can id1 0x123 data1 0x11 0x22 0x33 0x44 send1
   can ide1 0x12345678 empty1
   can ide0 0x12345678 data0 0x11 0x22 0x33 0x44 send0
   can p

   can empty14 id14 0 mask14 0
   can empty15 ide15 0 mask15 0

*/
