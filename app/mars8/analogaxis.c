/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  analogaxis.c - support for analog axis control

  Copyright (C) 2001-2009 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2009 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <pxmc.h>
#include <qadc.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <utils.h>
#include <cmd_proc.h>
#include <pxmc_cmds.h>
#include "app_local_config.h"

extern int pxmc_fast_pid(pxmc_state_t *mcs);

int analogaxis_do_imp(struct pxmc_state *mcs)
{
  int adc_chan;
  long abs_pos;

  adc_chan=mcs->pxms_inp_info&0xff;
  abs_pos=qadc_read_ch(adc_chan)<<mcs->pxms_subdiv;

  mcs->pxms_as=abs_pos-mcs->pxms_ap;
  mcs->pxms_ap=abs_pos;

  return 0;
}

int
analogaxis_do_con(struct pxmc_state *mcs)
{
  int ret;
  int cur_chan=mcs->pxms_out_info&0xff;
  short cur_val;
  short cur_lim;
  short cur_corr;
  short ene;
  long foi;
  long l;

  ene=mcs->pxms_ene;

  ret=pxmc_fast_call(mcs, pxmc_fast_pid);

  cur_val=tlcadc_read(cur_chan);
  cur_lim=mcs->pxms_s1;

  if(cur_val>cur_lim){
    cur_corr=cur_val-cur_lim;

    if((ene>0)&&(mcs->pxms_ene>0)) {
      l=(long)mcs->pxms_s2*cur_corr;
      if(l>mcs->pxms_ene)
        l=mcs->pxms_ene;
      mcs->pxms_ene-=l;

      l*=mcs->pxms_p;
      l>>=8;
      if(l>0x8000)
        l=0x8000;
      l*=mcs->pxms_i;
      l>>=8;
      if(l>0x8000)
        l=0x8000;

      foi=mcs->pxms_foi-l;
      if(foi<-0x6000)
        foi=-0x6000;
      mcs->pxms_foi=foi;

    } else if((ene<0)&&(mcs->pxms_ene<0)) {

      l=-(long)mcs->pxms_s2*cur_corr;
      if(l<mcs->pxms_ene)
        l=mcs->pxms_ene;
      mcs->pxms_ene-=l;

      l*=mcs->pxms_p;
      l>>=8;
      if(l<-0x8000)
        l=-0x8000;
      l*=mcs->pxms_i;
      l>>=8;
      if(l<-0x8000)
        l=-0x8000;

      foi=mcs->pxms_foi-l;
      if(foi>0x6000)
        foi=0x6000;
      mcs->pxms_foi=foi;

    }
  }

  return ret;
}

int cmd_do_analogaxis_setup(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;
  pxmc_state_t *mcs;
  char *p;
  long adc_chan, cur_lim;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;
  if(opchar==':'){
    p=param[3];

    if(si_long(&p,&adc_chan,0)<0)
      return -CMDERR_BADPAR;

    if(si_fndsep(&p,",")<0)
      return -CMDERR_BADSEP;
    if(si_long(&p,&cur_lim,0)<0)
      return -CMDERR_BADPAR;

    si_skspace(&p);
    if(*p) return -CMDERR_GARBAG;

    pxmc_set_const_out(mcs,0);

    if(qadc_read_ch(adc_chan)<0)
      return -CMDERR_BADPAR;

    pxmc_clear_flag(mcs,PXMS_ENI_b);

    mcs->pxms_subdiv=8;

    mcs->pxms_do_inp=analogaxis_do_imp;
    mcs->pxms_inp_info=adc_chan&0xff;

    mcs->pxms_do_con=analogaxis_do_con;

    mcs->pxms_s1=cur_lim;

    if(mcs->pxms_s2<100)
      mcs->pxms_s2=400;

    pxmc_set_flag(mcs,PXMS_ENI_b);

  }else{
    return -CMDERR_OPCHAR;
  }
  return 0;
}

cmd_des_t const cmd_des_analogaxis_setup={0, CDESM_OPCHR|CDESM_WR,
			"ANAXSETUP?","setup given axis to the analog mode",
			cmd_do_analogaxis_setup,
			{NULL,
			 0}};

cmd_des_t const *cmd_analogaxis_tab[]={
  &cmd_des_analogaxis_setup,
  NULL
};
