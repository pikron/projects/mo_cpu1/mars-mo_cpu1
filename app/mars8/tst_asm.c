#undef NOFLOAT

#if 0
/* hack for start of main, should use crt0.o instead */
__asm__ /*__volatile__*/(
	".even\n"
	".global start\n"
	"start : \n\t"
	"movl	#0xf0000,%sp\n\t"
	"movl	%sp,%fp\n\t"
	"bsr	main\n\t"
	"movl   %d0,-(%sp)\n\t"
	"bsr	_exit\n\t"
	);
#endif

/***********************************************************/

void f1()
{ char ch;
  /* writestr("\r\nHello word ! Try me !\r\n");
  while((ch=getchar())!=0xd) putchar(ch); */
}


int f2(int a,int b,int c)
{
  int d, e, f; 
  
  __asm__ /*__volatile__*/(
	"movl %3,%%a4\n\t"
	"movl %4,%%d4\n\t"
	"movw %4,%%d4\n\t"

	"movw #56,%%d4\n\t"
	"movw (%%a4),%%d4\n\t"
	"movw (%%a4),%%d4\n\t"
	"movl 20(%%a4),%%d4\n\t"
	"movw 20(%%a4,%%d4*8),%%d4\n\t"
	"movl 20(%%a4,%%d4.w*8),%%d4\n\t"
	"movl 20(%%pc,%%d4.w*8),%%d4\n\t"
	"movw (%%a4)+,%%d4\n\t"
	"movw -(%%a4),%%d4\n\t"
	"movl (%%a4)+,(%%a5)+\n\t"
	"movl 10(%%a4,%%d4*2),-(%%a5)\n\t"
	"movl 20(%%d4),(%%d4*8)\n\t"

	"movml (%%a4)+,%%d4-%%d6/%%a4/%%a5\n\t"

/*	"movl ([40,%%a4],%%d4*8,20),%%d4\n\t" */	/* m68020 */
/*	"movl ([40,%%a4,%%d4.w*8],20),%%d4\n\t" */	/* m68020 */
/*	"movec %%d0,%%isp\n\t"			*/	/* m68020 */
	"movec %%d0,%%usp\n\t"
	"movec %%d0,%%vbr\n\t"
	"movw %%sr,%%d0\n\t"

	"addl %4,%%a4\n\t"
	
	"addl  %%d6,%%d4\n\t"
	"addw  %%d6,%%d4\n\t"
	"addw  %%d6,%%a4\n\t"

	"subxl  -(%%a5),-(%%a4)\n\t"
	"cmpl  (%%a5)+,(%%a4)+\n\t"

	"mulsw %%d6,%%d4\n\t"
	"mulsl %4,%%d4\n\t"
	"mulsl %%d6,%%d5:%%d4\n\t"
	"divul %%d5:%%d6\n\t"
	"divsl %%d5:%%d4,%%d6\n\t"
/*	"tblsl %%d5:%%d4,%%d6\n\t"*/	/* m68332 */
/*	"tblsnl %%d5:%%d4,%%d6\n\t"*/	/* m68332 */
	"cmp2  (%%a5),%%d6\n\t"

	"bclr  #4,%%d1\n\t"
	"bset  #5,%%d2\n\t"
	"bchg  #6,%%d1\n\t"
	"btst  #6,%%d1\n\t"

	:"=r" (d),"=d" (e),"=&a" (f)
	:"g" (a),"%d" (b),"a" (c)
	:"a4","a5","d4","d5","d6","d7","memory","cc");

  return  d;
}


#ifndef NOFLOAT
float f3(float f, double d, int i)
{
  double g;
  
  g=f*1.23456+d*i;
  i=g;
  return g*i+1;
}
#endif

