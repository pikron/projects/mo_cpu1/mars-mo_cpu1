/*******************************************************************
  Motion and Robotic System (MARS) aplication components.
 
  skel-main.c - minimal application skeleton
 
  Copyright (C) 2001-2003 by Pavel Pisa - originator 
                          pisa@cmp.felk.cvut.cz

  This code can be reused for any purposes

 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <sim.h>
#include <qsm.h>
#include <periph/qsm_rs232.h>
#include <tpu.h>
#include <qadc.h>
#include <periph/chmod_lcd.h>
#include <periph/kl41_lcd.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <utils.h>
#include <system_sup.h>
#include "app_local_config.h"

/*
 *-----------------------------------------------------------
 */

int main()
{ unsigned flags=0;
  int counter;
  int res;
  /* initialize PLL clock generator, cpu_ref_hz and cpu_sys_hz */
  sim_init();
  /* initialize RS232 communication interface */
  qsm_rs232_setmode(WITH_RS232_BAUDRATE,QSM_RS232_MODEA,1);
  /* initialize CAN subsystem */
  /*can_hw_init(-1,-1);*/
  
  save_flags(flags);
  flags&=~0x0700;
  restore_flags(flags);

  /* kl41 init */
  *KL41_LED_WR=0x7f;
  res=kl41_lcd_init();
  kl41_lcd_wrstr("Init OK");

  printf("base-skel started\n");

  do{
    char s[30];
    kl41_lcd_wrcmd(CHMOD_LCD_CLR);
    sprintf(s,"Loops:%02x",counter++);
    kl41_lcd_wrstr(s);
  }while(1);
  return 0;
}

