/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  mo_fwupdate.c - flash firmware updater

  Copyright (C) 2001-2017 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. http://www.pikron.com

  This code can be reused for any purposes

 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <sim.h>
#include <qsm.h>
#include <periph/qsm_rs232.h>
#include <tpu.h>
#include <qadc.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <flashprog.h>
#include <utils.h>
#include <system_sup.h>
#include "app_local_config.h"

extern char _binary_mars8_flash_bin_start;
extern char _binary_mars8_flash_bin_end;

#define FLASH_START_ADDR 0x00800000
#define FLASH_END_ADDR   0x00900000
#define KEEP_START_ADDR  0x00800000
#define KEEP_END_ADDR    0x00808000
#define APP_START_ADDR   0x00808000
#define APP_IMAGE_START (&_binary_mars8_flash_bin_start)
#define APP_IMAGE_END   (&_binary_mars8_flash_bin_end)


/* Select default flash algorithms to AMD29F400 */
/*flash_alg_info_t const *flash_alg_info=&amd_29f400_x16;*/

/* Select default flash algorithms to AMD29F800 */
flash_alg_info_t const *flash_alg_info=&amd_29f800_x16;

int fwupdate(void)
{
  int ret;
  unsigned flags=0;
  uint16_t checkid[2];
  void *flash_start = (void *)FLASH_START_ADDR;
  /*void *flash_end = (void *)FLASH_END_ADDR;*/
  void *keep_start = (void *)KEEP_START_ADDR;
  void *keep_end = (void *)KEEP_END_ADDR;
  void *app_start = (void *)APP_START_ADDR;
  void *img_start = (void *)APP_IMAGE_START;
  void *img_end = (void *)APP_IMAGE_END;
  void *keep_buf;
  void *psrc;
  void *pdst;

  if (keep_end - keep_start > 0) {
    keep_buf= malloc(keep_end - keep_start);
    memcpy(keep_buf, keep_start, keep_end - keep_start);
  }

  ret=flash_check_id(flash_alg_info,(void*)flash_start,checkid);
  printf("flash_check_id returned %d, manid 0x%X devid 0x%X\n",
         ret,(unsigned int)checkid[0],(unsigned int)checkid[1]);

  printf("keeping original content %p..%p\n",
         keep_start, keep_end);

  printf("flashing data from %p..%p to %p\n",
         img_start, img_end, app_start);

  while (!qsm_rs232_send_finished()) {
    /* not ideal but keeps RTS/CTS handshaking process */
    qsm_rs232_recch();
  }

  save_and_cli(flags);

  ret=flash_erase(flash_alg_info, flash_start, 0);
  if (ret < 0) {
    ret -= 200;
    goto error;
  }

  if (keep_end - keep_start > 0) {
    psrc = keep_buf;
    pdst = flash_start;
    while (pdst < keep_end) {
      ret=flash_prog(flash_alg_info, pdst, *(uint16_t*)psrc);
      if (ret < 0) {
        ret -= 300;
        goto error;
      }
      pdst += 2;
      psrc += 2;
    }
  }

  psrc = img_start;
  pdst = app_start;
  while (psrc < img_end) {
    ret=flash_prog(flash_alg_info, pdst, *(uint16_t*)psrc);
    if (ret < 0) {
      ret -= 400;
      goto error;
    }
    pdst += 2;
    psrc += 2;
  }

  restore_flags(flags);

  if (memcmp(keep_buf, keep_start, keep_end - keep_start))
    return -101;

  if (memcmp(img_start, app_start, img_end - img_start))
    return -102;

  return 0;

error:
  restore_flags(flags);
  return ret;
}

/*
 *-----------------------------------------------------------
 */

int main()
{ unsigned flags=0;
  int counter;
  int res;
  int trials = 0;
  /* initialize PLL clock generator, cpu_ref_hz and cpu_sys_hz */
  sim_init();
  /* initialize RS232 communication interface */
  qsm_rs232_setmode(WITH_RS232_BAUDRATE,QSM_RS232_MODEA,1);
  /* initialize CAN subsystem */
  /*can_hw_init(-1,-1);*/

  save_flags(flags);
  flags&=~0x0700;
  restore_flags(flags);

  printf("MARS8 firmware updater started\n");

  while (trials++ < 3) {
    res = fwupdate();
    if (res == 0)
      break;
  }

  counter = 0;
  do{
    volatile int delaycnt;
    printf("end reached: result %d after %d trials (report %d)\n",
           res, trials, counter++);
    for(delaycnt = 0; delaycnt < 4 * 1000000; delaycnt++);
  }while(1);
  return 0;
}
