#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "m_types.h"

// #define	INLINE	static inline
#define INLINE
#define DEBUG	0
#define dcflDebugInfo 0x8000
#define dcflDeflate   0x4000
#define IF_DEB if((flg&dcflDebugInfo)&&DEBUG)

#define MALLOC malloc
#define FREE   free

/*==============================================================*/
/* bitstream reading */

/* for reading and writting from/to bitstream */
typedef
 struct {
   uint32_t buf;	/* bit buffer */
     int pb;	/* already readed bits from buf */
   uint16_t *pd;	/* first not readed input data */
   uint16_t *pe;	/* after end of data */
   /* file reading part */
   size_t fbuf_siz; /* buffer size */
   int   fd;	/* file descriptor */
   int   stat;	/* status */
   uint8_t  *fbuf;	/* file buffer */
 } bits_t;

const unsigned sq_bmsk[]=
   {0x0,0x1,0x3,0x7,0xF,0x1F,0x3F,0x7F,0xFF,
    0x1FF,0x3FF,0x7FF,0xFFF,0x1FFF,0x3FFF,0x7FFF,0xFFFF};

void sq_rdfill(bits_t* pbits);

/* read next 16 bits from input */
#define RDN_G16(bits) \
   { \
    (bits).buf>>=16; \
    (bits).pb-=16; \
    if((bits).pd<(bits).pe) \
    { \
     (bits).buf|=((uint32_t)(CF_LE_u16(*((bits).pd++))))<<16; \
    } else { \
      sq_rdfill(&(bits));\
    }; \
   }

/* prepares at least 16 bits for reading */
#define RDN_PR(bits,u) \
   { \
    if((bits).pb>=16) RDN_G16(bits); \
    u=(bits).buf>>(bits).pb; \
   }

/* initializes reading from bitstream stored in memory */
INLINE void sq_rdi(bits_t *pbits,void *pin,unsigned lin)
{
  pbits->pb=32;
  pbits->pd=(uint16_t*)pin;
  pbits->pe=pbits->pd+((lin+1)>>1);
  pbits->fbuf_siz=0;
};

/* reads n<=16 bits from bitstream *pbits */
INLINE unsigned sq_rdn(bits_t *pbits,int n)
{
  unsigned u;
  RDN_PR(*pbits,u);
  pbits->pb+=n;
  u&=sq_bmsk[n];
  return u;
};


/* initializes reading from file bitstream */
int sq_rdinitfd(bits_t *pbits,int fd)
{ 
  pbits->stat=-1;
  pbits->fbuf=NULL;
  if(fd<0) return -1;
  pbits->fbuf_siz=0x4000;
  if((pbits->fbuf=(uint8_t *)malloc(pbits->fbuf_siz))==NULL) return -1;  
  pbits->fd=fd;
  
  pbits->pb=32;
  pbits->pd=(uint16_t*)pbits->fbuf;
  pbits->pe=(uint16_t*)pbits->fbuf;
  pbits->stat=0x10;
  return fd;
};

/* ends reading from file bitstream */
int sq_rddonefd(bits_t *pbits)
{
  if(pbits->stat>0) pbits->stat=0;
  if(pbits->fbuf) free(pbits->fbuf);
  pbits->fbuf=NULL;
  pbits->fbuf_siz=0;
  return pbits->fd;
};

void sq_rdfill(bits_t* pbits)
{ int len, rqlen;
  uint8_t *p;  
  if(pbits->stat<=0) return;	/* already in error state */
  if(!pbits->fbuf_siz) {pbits->stat=-2; return;}; /* no buffer */
  if(pbits->pb>16) { pbits->stat=-4;return;} /* under flow - read after end */
  if(pbits->stat&1) {
    p=pbits->fbuf;
    *(p++)=*(uint8_t*)pbits->pe;	/* residual odd byte */
    rqlen=pbits->fbuf_siz-1;
  } else {
    p=pbits->fbuf;
    rqlen=pbits->fbuf_siz;
  };
  len=read(pbits->fd,p,rqlen);	/* read from file */
  if(len==-1) {pbits->stat=-3; return;};
  if(!len) {pbits->stat|=0x8;*(uint16_t*)p=0;}; /* no more data */
  
  len+=pbits->stat&1;		/* odd byte correction */
  pbits->pd=(uint16_t*)(pbits->fbuf);
  pbits->pe=(uint16_t*)(pbits->fbuf+(len&~1));
  pbits->stat&=~1;pbits->stat|=len&1;
  pbits->buf|=((uint32_t)(CF_LE_u16(*(pbits->pd++))))<<16;
};

char *sq_rdstr(bits_t *pbits)
{ char *s, c;
  int  len, buf;
  len=0;
  buf=64;
  s=(char *)malloc(buf);
  do
  {    
    if(pbits->stat<0) {free(s); return NULL;};
    if(len>=buf) {buf+=64; s=realloc(s,buf);};
    if(!s) return NULL;
    s[len++]=c=sq_rdn(pbits,8);
  } while(c);
  return s;
};

/*==============================================================*/
/* huffman decoding */

#define MAX_SPDA_BITS	10
#define MAX_SPDA_LEN	(1<<MAX_SPDA_BITS)
#define MAX_BITS	16
#define MAX_CODES	0x140
#define OUT_OVER	0x100

typedef
 struct {
  int8_t ln;			/* character lens .. for tokens -0x40 */
  uint8_t ch;			/* character/token code */
 }huf_chln_t;

typedef
 struct {
  unsigned cd_ln[MAX_BITS+1];	/* distribution of bits */
  unsigned cd_ch[MAX_BITS+1];	/* distribution of codes codes */
  int  bn;			/* chln array convert max bn bits codes */
  huf_chln_t chln1[MAX_CODES];	/* for codes with more than bn bits */
  huf_chln_t chln[0];		/* character codes decode array length SPDA_LEN */
 }huf_rd_t;

#define HUF_RD_SIZE(SPDA_LEN) \
	((sizeof(huf_rd_t)+SPDA_LEN*sizeof(huf_chln_t)+3)&~3)

const uint8_t swap_bits_xlat[]=
	  {0x00,0x80,0x40,0xc0,0x20,0xa0,0x60,0xe0,0x10,0x90,
	   0x50,0xd0,0x30,0xb0,0x70,0xf0,0x08,0x88,0x48,0xc8,
	   0x28,0xa8,0x68,0xe8,0x18,0x98,0x58,0xd8,0x38,0xb8,
	   0x78,0xf8,0x04,0x84,0x44,0xc4,0x24,0xa4,0x64,0xe4,
	   0x14,0x94,0x54,0xd4,0x34,0xb4,0x74,0xf4,0x0c,0x8c,
	   0x4c,0xcc,0x2c,0xac,0x6c,0xec,0x1c,0x9c,0x5c,0xdc,
	   0x3c,0xbc,0x7c,0xfc,0x02,0x82,0x42,0xc2,0x22,0xa2,
	   0x62,0xe2,0x12,0x92,0x52,0xd2,0x32,0xb2,0x72,0xf2,
	   0x0a,0x8a,0x4a,0xca,0x2a,0xaa,0x6a,0xea,0x1a,0x9a,
	   0x5a,0xda,0x3a,0xba,0x7a,0xfa,0x06,0x86,0x46,0xc6,
	   0x26,0xa6,0x66,0xe6,0x16,0x96,0x56,0xd6,0x36,0xb6,
	   0x76,0xf6,0x0e,0x8e,0x4e,0xce,0x2e,0xae,0x6e,0xee,
	   0x1e,0x9e,0x5e,0xde,0x3e,0xbe,0x7e,0xfe,0x01,0x81,
	   0x41,0xc1,0x21,0xa1,0x61,0xe1,0x11,0x91,0x51,0xd1,
	   0x31,0xb1,0x71,0xf1,0x09,0x89,0x49,0xc9,0x29,0xa9,
	   0x69,0xe9,0x19,0x99,0x59,0xd9,0x39,0xb9,0x79,0xf9,
	   0x05,0x85,0x45,0xc5,0x25,0xa5,0x65,0xe5,0x15,0x95,
	   0x55,0xd5,0x35,0xb5,0x75,0xf5,0x0d,0x8d,0x4d,0xcd,
	   0x2d,0xad,0x6d,0xed,0x1d,0x9d,0x5d,0xdd,0x3d,0xbd,
	   0x7d,0xfd,0x03,0x83,0x43,0xc3,0x23,0xa3,0x63,0xe3,
	   0x13,0x93,0x53,0xd3,0x33,0xb3,0x73,0xf3,0x0b,0x8b,
	   0x4b,0xcb,0x2b,0xab,0x6b,0xeb,0x1b,0x9b,0x5b,0xdb,
	   0x3b,0xbb,0x7b,0xfb,0x07,0x87,0x47,0xc7,0x27,0xa7,
	   0x67,0xe7,0x17,0x97,0x57,0xd7,0x37,0xb7,0x77,0xf7,
	   0x0f,0x8f,0x4f,0xcf,0x2f,0xaf,0x6f,0xef,0x1f,0x9f,
	   0x5f,0xdf,0x3f,0xbf,0x7f,0xff};

/* swap 16 bits order */
INLINE unsigned swap_bits_order_16(unsigned d)
{ unsigned r;
  #ifdef USE_GNU_ASM_i386
    __asm__ (
	"xlat\n\t"
	"xchgb	%%al,%%ah\n\t"
	"xlat\n\t"
	:"=a"(r):"0"(d),"b"(swap_bits_xlat)); 
  #else
    r=((unsigned)swap_bits_xlat[(uint8_t)d])<<8;
    r|=swap_bits_xlat[(uint8_t)(d>>8)];
  #endif
  return r;
};

/* swap bit order */
INLINE unsigned swap_bits_order(unsigned d,int n)
{ unsigned r=0;
  while(n--) { r<<=1;r|=d&1;d>>=1;};
  return r;
};

/* initializes huffman conversion structure *phuf for m codes,
   *ca code and token bit lengths, ends with 0xFF, 
   bn predicated maximal bit length */
int sq_rdhufi(huf_rd_t *phuf,int m,int bn,uint8_t *ca)
{
 if(bn>MAX_SPDA_BITS) bn=MAX_SPDA_BITS;
 phuf->bn=bn;
 {
  int i;
  unsigned u,us,ut;
  memset(phuf->cd_ln,0,sizeof(phuf->cd_ln));i=0;
  while((u=ca[i++])<=MAX_BITS) phuf->cd_ln[u]++;
  memset(phuf->cd_ch,0,sizeof(phuf->cd_ch));
  phuf->cd_ln[0]=0;us=0;ut=0;
  for(i=1;i<=MAX_BITS;i++)
  {
   u=phuf->cd_ln[i];phuf->cd_ln[i]=ut;
   phuf->cd_ch[i]=us;ut+=u;us+=u;us<<=1;
  };
  /* if suceed, codespace should be full else report error */ 
  if (us&((1<<MAX_BITS)-1)) 
   if(us!=1)return(0); /* exeption, zip 2.0 allows one code one bit long */
 };
 {
  int i,ln,l,ch,sh,cod;
  for(i=0;(l=ln=ca[i])<=MAX_BITS;i++) if(ln)
  {
   sh=(bn-ln);
   cod=(phuf->cd_ch[ln])++;
   cod=swap_bits_order_16(cod)>>(16-ln);
   if(i<m) ch=i; else {ch=i-m+1;ln-=0x40;};
   if (sh>0)
   {
    sh=1<<sh;
    l=1<<l;
    while(sh--)
    {
      phuf->chln[cod].ch=ch;
      phuf->chln[cod].ln=ln;
      cod+=l;
    };
   } else if (sh==0) {
    phuf->chln[cod].ch=ch;
    phuf->chln[cod].ln=ln;
   } else {
    cod&=sq_bmsk[bn];
    phuf->chln[cod].ch=0x00;
    phuf->chln[cod].ln=-0x40;
    cod=(phuf->cd_ln[l])++;
    phuf->chln1[cod].ch=ch;
    phuf->chln1[cod].ln=ln;
   };
  };
  /* if suceed ln should be 0xFF */
 };
 return(1);
};

/* read and huffman decode of characters, stops on tokens or buffer ends */
INLINE unsigned sq_rdh(bits_t *pbits,const huf_rd_t *phuf,uint8_t **pout,uint8_t *pend)
{
 unsigned ch;
 unsigned bmsk=sq_bmsk[phuf->bn];

 while(1)
 {while(1)
  {if(pbits->pb>=16)
    RDN_G16(*pbits);
   if (*pout>=pend) return OUT_OVER;
   ch=(pbits->buf>>pbits->pb)&bmsk;
   if((pbits->pb+=phuf->chln[ch].ln)<0) break;
   *((*pout)++)=phuf->chln[ch].ch;

   if(pbits->pb<16)
   {if (*pout>=pend) return OUT_OVER;
    ch=(pbits->buf>>pbits->pb)&bmsk;
    if((pbits->pb+=phuf->chln[ch].ln)<0) break;
    *((*pout)++)=phuf->chln[ch].ch;

    if(pbits->pb<16)
    {if (*pout>=pend) return OUT_OVER;
     ch=(pbits->buf>>pbits->pb)&bmsk;
     if((pbits->pb+=phuf->chln[ch].ln)<0) break;
     *((*pout)++)=phuf->chln[ch].ch;
    };
   };
  };

  ch=phuf->chln[ch].ch;
  pbits->pb+=0x40; if(ch--) return ch;
  /* code longer than phuf->bn */
  if(pbits->pb>=16) RDN_G16(*pbits);
  ch=swap_bits_order_16((uint16_t)(pbits->buf>>pbits->pb));
  {
   int i;
   i=phuf->bn;
   do
    i++;
   while((phuf->cd_ch[i]<=(ch>>(16-i)))&&(i<MAX_BITS));
   ch=((ch>>(16-i)))-phuf->cd_ch[i]+phuf->cd_ln[i];
  };
  if((pbits->pb+=phuf->chln1[ch].ln)<0) 
  {pbits->pb+=0x40;
   return phuf->chln1[ch].ch-1;
  };
  *((*pout)++)=phuf->chln1[ch].ch;
 };
};

/* read one huffman encoded value */
INLINE unsigned sq_rdh1(bits_t *pbits,const huf_rd_t *phuf)
{unsigned ch;
 if(pbits->pb>=16) RDN_G16(*pbits);
 ch=(pbits->buf>>pbits->pb)&sq_bmsk[phuf->bn];
 if((pbits->pb+=phuf->chln[ch].ln)>=0) return phuf->chln[ch].ch;
 ch=phuf->chln[ch].ch;
 pbits->pb+=0x40; if(ch) return ch+0x100-1;
 ch=swap_bits_order_16((uint16_t)(pbits->buf>>pbits->pb));
 {int i;
  i=phuf->bn;
  do
   i++;
  while((phuf->cd_ch[i]<=(ch>>(16-i)))&&(i<MAX_BITS));
  ch=((ch>>(16-i)))-phuf->cd_ch[i]+phuf->cd_ln[i];
 };
 if((pbits->pb+=phuf->chln1[ch].ln)>=0) return phuf->chln1[ch].ch;
 pbits->pb+=0x40;
 return phuf->chln1[ch].ch+0x100-1;
};

/*==============================================================*/

typedef
 struct {   
   uint8_t *pd;	/* next written or read data */
   uint8_t *pe;	/* after end of data */
   uint8_t *buf;	/* data buffer */
   size_t buf_siz; /* buffer size */
   size_t min_hist; /* minimal history length */
   int  fd;	/* file handle */
   int stat;
 } hbuf_t;

int hbuf_initfd(hbuf_t *phbuf, int fd)
{
  phbuf->stat=-1;
  phbuf->buf=NULL;
  if(fd<0) return -1;
  phbuf->fd=fd;
  phbuf->min_hist=0x8000;
  phbuf->buf_siz=0x10000;
  if((phbuf->buf=(uint8_t*)malloc(phbuf->buf_siz))==NULL) return -1;
  phbuf->pd=phbuf->buf;
  phbuf->pe=phbuf->buf+phbuf->buf_siz;

  phbuf->stat=0x10;
  return fd;
};

int hbuf_donefd(hbuf_t *phbuf)
{
  int len, lenrq;
  if(((lenrq=phbuf->pd-phbuf->buf)>0)&&(phbuf->stat>0)) 
  { len=write(phbuf->fd,phbuf->buf,lenrq);
    if(len!=lenrq) phbuf->stat=-2;
  };
  if(phbuf->stat>0) return phbuf->stat=0;
  if(phbuf->buf!=NULL) free(phbuf->buf);
  return phbuf->fd;
};

void hbuf_put(hbuf_t *phbuf)
{
  int len, lenrq, lendat;
  if(phbuf->stat<0) return;
  lendat=phbuf->pd-phbuf->buf;
  if(lendat<phbuf->min_hist) return;
  lenrq=lendat-phbuf->min_hist;
  len=write(phbuf->fd,phbuf->buf,lenrq);
  if(len!=lenrq) phbuf->stat=-2;
  memcpy(phbuf->buf,phbuf->buf+len,lendat-len);
  phbuf->pd=phbuf->buf+lendat-len;
};

/*==============================================================*/
/* SQ decompression */

/* index conversion table for first bitlen table */
const int code_index_1[]={0x10,0x11,0x12,0x00,0x08,0x07,0x09,0x06,0x0A,0x05,
			  0x0B,0x04,0x0C,0x03,0x0D,0x02,0x0E,0x01,0x0F};

const unsigned sqt_repbas[]={
	0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0D,
	0x0F,0x11,0x13,0x17,0x1B,0x1F,0x23,0x2B,0x33,0x3B,
	0x43,0x53,0x63,0x73,0x83,0xA3,0xC3,0xE3,0x102,0x00,0x00};

const unsigned char sqt_repbln[]={
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,
	0x01,0x01,0x02,0x02,0x02,0x02,0x03,0x03,0x03,0x03,
	0x04,0x04,0x04,0x04,0x05,0x05,0x05,0x05,0x00,0x63,0x63};

const unsigned sqt_offbas[]={
	0x0001,0x0002,0x0003,0x0004,0x0005,0x0007,0x0009,0x000D,
	0x0011,0x0019,0x0021,0x0031,0x0041,0x0061,0x0081,0x00C1,
	0x0101,0x0181,0x0201,0x0301,0x0401,0x0601,0x0801,0x0C01,
	0x1001,0x1801,0x2001,0x3001,0x4001,0x6001,0x0000,0x0000};

const unsigned char sqt_offbln[]={
	0x00,0x00,0x00,0x00,0x01,0x01,0x02,0x02,
	0x03,0x03,0x04,0x04,0x05,0x05,0x06,0x06,
	0x07,0x07,0x08,0x08,0x09,0x09,0x0A,0x0A,
	0x0B,0x0B,0x0C,0x0C,0x0D,0x0D,0x00,0x00};

int sq_dec(bits_t *pbits,hbuf_t *phbuf, int flg)
{ 
  uint8_t *r;
  unsigned u, u1, repoffs, replen;
  int i,bn_max;
  int final_flag;
  int count_1;
  int count_2;
  int count_3;
  int method;
  unsigned mask;
  uint8_t *code_bln;	/* bitlengths of char, tokens and rep codes [0x150] */
  huf_rd_t *huf1,*huf2;	/* tables for huffman decoding */
  char *work_mem;

  if(!flg&dcflDeflate)
  { if((sq_rdn(pbits,8)!='S')||(sq_rdn(pbits,8)!='Q'))
    { IF_DEB printf("Data are not SQ compressed\n");
      return(0);
    };
    u=sq_rdn(pbits,16);
    IF_DEB printf("SQ version %X\n",u);
  };

  /* allocating work memory */
  work_mem=(char*)MALLOC(0x150+HUF_RD_SIZE(MAX_SPDA_LEN)+HUF_RD_SIZE(256));
  if(!work_mem) 
    {IF_DEB printf("sq_dec: Not enought memory\n");return 0;};
  code_bln=work_mem;
  huf1=(huf_rd_t*)(work_mem+0x150);
  huf2=(huf_rd_t*)(work_mem+0x150+HUF_RD_SIZE(MAX_SPDA_LEN));
  do
  { final_flag=sq_rdn(pbits,1); IF_DEB printf("final_flag %d\n",final_flag);
    method=sq_rdn(pbits,2);     IF_DEB printf("method %d\n",method);
    switch(method)
    {
      case 0:
        printf("submethod not tested - raw read\n");
	/* go to byte boundary */
	/* read 16 bits - count of raw bytes */
	sq_rdn(pbits,(8-pbits->pb)&7);
	replen=sq_rdn(pbits,16);
	if (replen+sq_rdn(pbits,16)!=0xFFFF) {FREE(work_mem);return 0;};
	r=(uint8_t*)pbits->pd-(32-pbits->pb)/8;
        if(r+replen>(uint8_t*)pbits->pe) {FREE(work_mem);return 0;};
        if(phbuf->pd+replen>phbuf->pe) hbuf_put(phbuf);
	M_MOVSB(phbuf->pd,r,replen); /* copy/repeat function */
	if((unsigned)r&1) pbits->pb=32;
	else {pbits->pb=32+8;r--;};
	pbits->pd=(typeof(pbits->pd))r;
        break;

      case 1:
        printf("submethod not tested - fixed huffman\n");
        /* 0x90*8 0x70*9 0x18*7 8*8 sqt_repbln sqt_repbas 0x101 0x120h */
        /* 0x1E*5 offset sqt_offbln sqt_offbas 0 0x1Eh */
        bn_max=9;
        count_1=0x120;
        count_2=0x20;
        i=0;
        while(i<0x90)  code_bln[i++]=8;
        while(i<0x100) code_bln[i++]=9;
        while(i<0x118) code_bln[i++]=7;
        while(i<0x120) code_bln[i++]=8;
        while(i<0x140) code_bln[i++]=5;
        goto case_1_cont;
      
      case 2:
        IF_DEB printf("huffman\n"); /* SQ_dec_sub_C */
        count_1=sq_rdn(pbits,5)+0x101;		/* esp+20 */
        IF_DEB printf("count_1 %d\n",count_1);
        if(count_1>0x11E) 
          {printf("huff count_1 too big\n");FREE(work_mem);return(0);};
        count_2=sq_rdn(pbits,5)+1;		/* esp+24 */
        IF_DEB printf("count_2 %d\n",count_2);
        if(count_2>0x1E) 
          {printf(" huff count_2 too big\n");FREE(work_mem);return(0);};
        count_3=sq_rdn(pbits,4)+4;		/* esp+14 */
        IF_DEB printf("count_3 %d\n",count_3);
        bn_max=0;
        for(i=0;i<count_3;i++)
        { u=sq_rdn(pbits,3);
          code_bln[code_index_1[i]]=u;
          if(u>bn_max)bn_max=u;
        };
        while(i<19) code_bln[code_index_1[i++]]=0;code_bln[19]=0xFF;
        IF_DEB
        { printf("code_bln ");
          for(i=0;i<19;i++) printf("%d:%d ",i,code_bln[i]);
          printf("\n");
        };
        i=sq_rdhufi(huf1,19,bn_max,code_bln);
        IF_DEB printf("sq_rdhufi - helper table %d\n",i);
        if(!i)
        { printf("huff error in helper table\n");
          FREE(work_mem);return 0;
        };
        mask=sq_bmsk[huf1->bn]; u1=0; bn_max=0;
        for(i=0;i<count_1+count_2;)
        { RDN_PR(*pbits,u);
          pbits->pb+=huf1->chln[u&mask].ln;
          u=huf1->chln[u&mask].ch;
          switch(u)
          { case 16:		/* 3 to 6 repeats of last */
              u=sq_rdn(pbits,2)+3;
              IF_DEB printf("%dx%d ",u,u1);
              while(u--) code_bln[i++]=u1;
              break;
            case 17:		/* 3 to 10 repeats of 0 */
              u=sq_rdn(pbits,3)+3; u1=0;
              IF_DEB printf("%dxO ",u);
              while(u--) code_bln[i++]=u1;
              break;
            case 18:		/* 11 to 139 repeats of 0 */
              u=sq_rdn(pbits,7)+11; u1=0;
              IF_DEB printf("%dxO ",u);
              while(u--) code_bln[i++]=u1;
              break;
            default:
              IF_DEB printf("%d ",u);
              code_bln[i++]=u;
              u1=u;
              if(u>bn_max) bn_max=u;
          };
        };
	IF_DEB printf("\n");

      case_1_cont:
        /* code_bln+count_1	0x96  count_2 sqt_offbln sqt_offbas */
        code_bln[count_1+count_2]=0xFF;
        i=sq_rdhufi(huf2,0x100,8,code_bln+count_1);
        IF_DEB printf("sq_rdhufi - offset table %d\n",i);        
        if(!i)
        { printf("huff error in offset table\n");
          FREE(work_mem);return 0;
        };
        
        /* code_bln		0x100 count_1 sqt_repbln sqt_repbas */
        code_bln[count_1]=0xFF;
        i=sq_rdhufi(huf1,0x100,bn_max,code_bln);
        IF_DEB printf("sq_rdhufi - char and rep table %d\n",i);
        if(!i)
        { printf("huff error in char and len table\n");
          FREE(work_mem);return 0;
        };
        
        while((u=sq_rdh(pbits,huf1,&(phbuf->pd),phbuf->pe))!=0)
        { if(phbuf->stat<0) break;
          if(u==OUT_OVER){hbuf_put(phbuf);continue;};
          u--;
          replen=sqt_repbas[u]+sq_rdn(pbits,sqt_repbln[u]);
          u=sq_rdh1(pbits,huf2);
          repoffs=sqt_offbas[u]+sq_rdn(pbits,sqt_offbln[u]);
          IF_DEB printf("At %5d repeat %5d from %5d \n",
                         phbuf->pd-phbuf->buf,replen,repoffs);
          if(!repoffs)
          { printf("Bad repoffs !!!\n\n");
            FREE(work_mem);return(0);
          };
          if (phbuf->pd+replen>phbuf->pe) hbuf_put(phbuf);
          if (phbuf->buf+repoffs>phbuf->pd) 
            {repoffs=phbuf->pd-phbuf->buf;printf(" Under !!!");};
          r=phbuf->pd-repoffs; 
          M_MOVSB(phbuf->pd,r,replen); /* copy/repeat function */
        };
      
        IF_DEB printf("Last token %d\n",u);
        if(u)
        { printf("huff BAD last token %x\n",u);
          FREE(work_mem);return 0;
        };
        break;
      
      case 3:
        printf("unknown submethod - 3\n");
        FREE(work_mem);
        return(0);      
    };
  } while(!final_flag);
  FREE(work_mem);
  return(1);
};

/*==============================================================*/
/* main */

#include <fcntl.h>
#include <ctype.h>
#include <sys/stat.h>
#include <time.h>

#ifndef  O_BINARY
#define  O_BINARY 0
#endif

int sq_unzip_main(int argc,char* argv[])
{
 int i, field_msk;
 int fd_in, fd_out;
 bits_t bits, *pbits=&bits;
 hbuf_t hbuf, *phbuf=&hbuf;
 int flg=0;
 long origdate;
 int  origspd, origos;
 unsigned char *c, *p, *p1;
 char *in_file_name="sq.raw";
 char *out_file_name="sq.dat";
 long int in_bytes=0;
 long int out_bytes=0;
 clock_t mark_time,end_time;

 if(argc>=2) in_file_name=argv[1];
 if(argc>=3) out_file_name=argv[2];
 if(argc>=4)
 {
  c=argv[3];
  if(toupper(*c)=='D') {flg|=dcflDebugInfo; c++;};
 };

 if((fd_in=open(in_file_name, O_RDONLY|O_BINARY,S_IREAD))==-1)
 {printf("In file open error\n");return 1;};
 sq_rdinitfd(pbits,fd_in);

 /* byte 0, 1 */
 if(sq_rdn(pbits,16)!=0x8B1F) {printf("Not gzip file\n");return 1;};
 /* byte 2 */
 if(sq_rdn(pbits,8)!=8) {printf("Not deflated gzip file\n");return 1;};
 /* byte 3 */
 field_msk=sq_rdn(pbits,8);
 printf("Gzip flags and fields 0x%X\n",field_msk);
 /* byte 4 .. 7 date */
 origdate=sq_rdn(pbits,16);
 origdate|=(long)sq_rdn(pbits,16)<<16;
 /* byte 8 */ 
 origspd=sq_rdn(pbits,8);
 printf("Gzip spd/comp %d\n",origspd);
 /* byte 9 */
 origos=sq_rdn(pbits,8);
 printf("Gzip os %d\n",origos);
 /* byte 10 and more - text fields */
 if(field_msk&0x04) 
  {printf("Extra:     %s\n",p=sq_rdstr(pbits)); free(p);};
 if(field_msk&0x08) 
  {printf("Orig name: %s\n",p=sq_rdstr(pbits)); free(p);};
 if(field_msk&0x10) 
  {printf("Comment:   %s\n",p=sq_rdstr(pbits)); free(p);};
 
 if((fd_out=open(out_file_name, O_RDWR | O_BINARY | O_CREAT | O_TRUNC, S_IWRITE | S_IREAD))==-1)
 {printf("Out file open error\n");return 1;};
 hbuf_initfd(phbuf,fd_out);

 i=sq_dec(pbits,phbuf,flg|dcflDeflate);
 printf("decomp %d\n",i);

 hbuf_donefd(phbuf);
 sq_rddonefd(pbits);
 close(fd_in);
 close(fd_out);

 return(0);
};


int main(int argc,char* argv[])
{
 return sq_unzip_main(argc,argv);
};
