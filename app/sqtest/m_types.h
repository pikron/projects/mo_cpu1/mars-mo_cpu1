/* machine and compiler dependent parts */

#ifdef __BCPLUSPLUS__
typedef unsigned char	uint8_t;
typedef signed char	int8_t;
typedef unsigned	uint16_t;
typedef unsigned long	uint32_t;
#else
#ifdef __DJGPP__
typedef unsigned char	uint8_t;
typedef signed char	int8_t;
typedef unsigned short	uint16_t;
typedef unsigned 	uint32_t;
#else
#ifdef __mc68000__
#include <cpu_def.h>
#include <types.h>
#else
#include <inttypes.h>
#endif
#endif
#endif

#define CF_LE_u16(v) (v)
#define CT_LE_u16(v) (v)
#define C_ST_u16(p,v) {*(((uint16_t*)p)++)=v;}
#define C_LD_u16(p,v) {v=*(((uint16_t*)p)++);}

typedef long int pos_t;
typedef long int sect_t;

#ifdef __GNUC__ 
 #ifdef __i386__
  #define USE_GNU_ASM_i386
 #endif
#endif

/* high speed compare and move routines */
#ifdef USE_GNU_ASM_i386

/* copy block, overlaping part is replaced by repeat of previous part */
/* pointers and counter are modified to point after block */
#define M_MOVSB(D,S,C) \
__asm__ /*__volatile__*/(\
	"cld\n\t" \
	"rep\n\t" \
	"movsb\n" \
	:"=D" (D),"=S" (S),"=c" (C) \
	:"0" (D),"1" (S),"2" (C) \
	:"memory")

/* compare blocks, overlaping repeat test */
/* pointers and counter are modified to point after block */
/* D and S points to first diff adr */
#define M_FIRSTDIFF(D,S,C) \
__asm__ /*__volatile__*/(\
	"cld\n\t" \
	"rep\n\t" \
	"cmpsb\n\t" \
	"je 1f\n\t" \
	"dec %0\n\t" \
	"dec %1\n\t" \
	"1:\n" \
	:"=D" (D),"=S" (S),"=c" (C) \
	:"0" (D),"1" (S),"2" (C) \
	)

#else

#define M_MOVSB(D,S,C) for(;(C);(C)--) *((uint8_t*)(D)++)=*((uint8_t*)(S)++)
#define M_FIRSTDIFF(D,S,C) for(;(*(uint8_t*)(D)==*(uint8_t*)(S))&&(C);\
                           (uint8_t*)(D)++,(uint8_t*)(S)++,(C)--)

#endif
