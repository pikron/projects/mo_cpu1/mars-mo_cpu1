/*****************************************************/
/***   Module : USB module                         ***/
/***   Author : Roman Bartosinski (C) 28.04.2002   ***/
/***   Modify : 08.08.2002, 16.04.2003             ***/
/*****************************************************/

//#include "hal.h"
#include <stdio.h>
#include <string.h>
#include <system_def.h>
#include <endian.h>
#if __BYTE_ORDER == __BIG_ENDIAN
  #include <byteswap.h>
#endif

//#include <usb/pdiusb.h> /* to by tu pak nemelo bejt vubec ... */
#include <usb/usb.h>
#include <usb/usbdebug.h>

#include <usb/usb_stdreq.h> /* temporary include - standard control request responses */

/* ep0 buffer */
  xdata unsigned char ep0_buffer[ MAX_CONTROL_XFER_DATA_SIZE];


/* usb initialize */
  int usb_init( usb_device_t *udev) {
    int ret = 0;
    usb_debug_print( DEBUG_LEVEL_LOW, ("init_usb\r\n"));
    /* create dynamic fields - endpoints */

    udev->ep_events = 0;
    udev->flags = 0;
    udev->configuration = 0;
    //udev->altinterface = 0;
    udev->ep0.udev = udev;
    udev->ep0.flags = USB_STATE_IDLE;
    udev->ep0.epnum = 0;

//    usb_init_stdreq_fnc( udev);

    if ( usb_udev_is_fnc(udev,init)) {
      ret = usb_udev_init( udev);
    }
    return ret;
  }


// connecting to USB by SoftConnect
  int usb_connect( usb_device_t *udev) {
    int ret = 0;
    usb_debug_print( DEBUG_LEVEL_LOW,("USB:ON\n"));

    udev->ep_events = 0;
    udev->flags = 0;
    udev->configuration = 0;
    //udev->altinterface = 0;
    udev->ep0.flags &= ~USB_STATE_MASK; //state = USB_STATE_IDLE;

    if ( usb_udev_is_fnc(udev,connect)) {
      ret = usb_udev_connect( udev);
    }
    return ret;
  }


  int usb_disconnect( usb_device_t *udev) {
    int ret = 0;
    usb_debug_print( DEBUG_LEVEL_LOW,("USB:OFF\n"));

    udev->flags &= ~USB_FLAG_CONFIGURED;
    udev->configuration = 0;
    udev->ep_events = 0;
    //udev->altinterface = 0;
    udev->ep0.flags &= ~USB_STATE_MASK; //state = USB_STATE_IDLE;

    if ( usb_udev_is_fnc(udev,disconnect)) {
      ret = usb_udev_disconnect( udev);
    }
    return ret;
  }


  void usb_stall( usb_ep_t *ep) {
    usb_debug_print( DEBUG_LEVEL_HIGH, ("USB:STALL %1d\n", ep->epnum));
    if ( usb_udev_is_fnc(ep->udev,stall)) {
      usb_udev_stall( ep);
    }
  }


  int usb_check_events( usb_device_t *udev)
  {
    int ret = 0;
    if ( usb_udev_is_fnc( udev, check_events)) {
      ret = usb_udev_check_events( udev);
    }
    return ret;
  }


// ************************************
// ***  Control transfer functions  ***
// ************************************

  void usb_complete_control_transfer(usb_ep_t *ep0, int status) {
    usb_debug_print( DEBUG_LEVEL_HIGH, ( "CCT:st=%d", status));
   #ifdef USB_WITH_CB_FNC
    if ( ep0->complete_fnc )
      ep0->complete_fnc( ep0, status);
    ep0->next_pkt_fnc = NULL;
    ep0->complete_fnc = NULL;
   #endif /*USB_WITH_CB_FNC*/
    ep0->flags &= ~USB_STATE_MASK; //state = USB_STATE_IDLE;
  }

/* Send any data in the data phase of the control transfer */
  void usb_send_control_data( usb_device_t *udev, unsigned char *pData, unsigned short len) {
    usb_ep_t *ep0 = &(udev->ep0);
    usb_debug_print( DEBUG_LEVEL_HIGH, ( "SCD:ptr=%p,s=%d\n", pData, len));
    ep0->efnc = NULL;
    ep0->ptr = pData;
    ep0->actual = 0;
    if ( ep0->size > len) ep0->size = len;

    /* Schedule TX processing for later execution */
    ep0->flags = (ep0->flags & ~USB_STATE_MASK) | USB_STATE_TRANSMIT;
    udev->flags |= USB_FLAG_EVENT_TX0;
  }

  void usb_set_control_endfnc( usb_device_t *udev, endfnc_t *efnc) { //REENTRANT_SIGN {
    udev->ep0.efnc = efnc;
  }

  void usb_ack_setup( usb_ep_t *ep) {
    usb_udev_ack_setup(ep->udev);
  }


/*************************************************************
 *** Control endpoint0 responses
 *************************************************************/
  int usb_control_response( usb_device_t *udev) {
    int ret = 0;
    usb_ep_t *ep0 = &(udev->ep0);

/* response to interrupt BusReset */
    if ( udev->flags & USB_FLAG_BUS_RESET) {
      udev->flags &= ~(USB_FLAG_BUS_RESET | USB_FLAG_SUSPEND); // usb_flags.bus_reset = 0; usb_flags.configured = 0;
      usb_debug_print( DEBUG_LEVEL_MEDIUM, ( "USBreset\n"));
      ret = 1;
    }
/* response to interrupt Suspend */
    if ( udev->flags & USB_FLAG_SUSPEND) {
      udev->flags &= ~(USB_FLAG_SUSPEND); //usb_flags.suspend = 0;
      usb_debug_print( DEBUG_LEVEL_MEDIUM, ( "USBsuspend\n"));
      ret = 1;
    }


/* response to interrupt SetupPacket execute response to standard device request or vendor request */
    if ( udev->flags & USB_FLAG_SETUP) {
      unsigned char type, req;
      USB_DEVICE_REQUEST *preq = &(udev->request);

      ep0->ptr = NULL;
      ep0->size = 0;
      ep0->actual = 0;
      ep0->efnc = NULL;
      ep0->flags &= ~USB_STATE_MASK; //state = USB_STATE_IDLE;
      udev->flags &= ~USB_FLAG_SETUP;   // usb_flags.setup_packet = 0;
     #ifdef USB_WITH_CB_FNC
      ep0->next_pkt_fnc = NULL;
      ep0->complete_fnc = NULL;
     #endif /*USB_WITH_CB_FNC*/

      if ( usb_udev_read_endpoint(ep0, preq, sizeof( USB_DEVICE_REQUEST))
                                   != sizeof( USB_DEVICE_REQUEST)) {
        usb_udev_stall( ep0);
        return -1;
      }
     #if __BYTE_ORDER == __BIG_ENDIAN
      preq->wValue  = bswap_16( preq->wValue);
      preq->wIndex  = bswap_16( preq->wIndex);
      preq->wLength = bswap_16( preq->wLength);
     #endif
      usb_debug_print( DEBUG_LEVEL_MEDIUM,( "SePa:x%02X,x%02X,x%04X,x%04X,x%04X\n", preq->bmRequestType, preq->bRequest, preq->wValue, preq->wIndex, preq->wLength));

      // acknowledge setup here
      if(usb_udev_is_fnc( udev, ack_control_setup)) {
        usb_udev_ack_control_setup(udev);
      }

      ep0->size = preq->wLength;
      if ((( preq->bmRequestType & USB_DATA_DIR_MASK) == USB_DATA_DIR_FROM_HOST) && preq->wLength) {
        ep0->ptr = ep0_buffer;
        ep0->flags = (ep0->flags & ~USB_STATE_MASK) | USB_STATE_RECEIVE;
      }

      type = preq->bmRequestType & USB_REQUEST_TYPE_MASK;
      req = preq->bRequest & USB_REQUEST_MASK;
      if ( type == USB_STANDARD_REQUEST) {
        int ret = -1;
        usb_debug_print( DEBUG_LEVEL_HIGH, ( "StdReq-%d\n", req));
/*
        if ( (udev->stdreq[ req]) != NULL) {
          ret = udev->stdreq[ req]( udev);
        }
        if( ret < 0)
          udev->ack_setup( udev);
*/
        switch( req) {
          case USB_REQUEST_GET_STATUS:        ret=usb_stdreq_get_status( udev); break;
          case USB_REQUEST_CLEAR_FEATURE:     ret=usb_stdreq_clear_feature( udev); break;
          case USB_REQUEST_SET_FEATURE:       ret=usb_stdreq_set_feature( udev); break;
          case USB_REQUEST_SET_ADDRESS:       ret=usb_stdreq_set_address( udev); break;

          case USB_REQUEST_GET_DESCRIPTOR:    ret=usb_stdreq_get_descriptor( udev); break;
//          case USB_REQUEST_SET_DESCRIPTOR:    break;
          case USB_REQUEST_GET_CONFIGURATION: ret=usb_stdreq_get_configuration( udev); break;
          case USB_REQUEST_SET_CONFIGURATION: ret=usb_stdreq_set_configuration( udev); break;
          case USB_REQUEST_GET_INTERFACE:     ret=usb_stdreq_get_interface( udev); break;
          case USB_REQUEST_SET_INTERFACE:     ret=usb_stdreq_set_interface( udev); break;
//          case USB_REQUEST_SYNC_FRAME:        break;
//          default:                            ret=-1; break;
        }
        if (ret<0)
          usb_udev_stall( ep0);
      } else {
        if ( type == USB_VENDOR_REQUEST) {
//putchar('#');
         #ifdef USB_WITH_CB_FNC
          int ret = -1;
//         if(USBVendorRequestCBFnc != NULL)
//         ret = USBVendorRequestCBFnc(&usb_ep0, &dreq);
          if ( udev->vendor_fnc != NULL)
            ret = udev->vendor_fnc( udev);
          if ( ret < 0)
            usb_udev_stall( ep0);
//         #else /*USB_WITH_CB_FNC*/
//          if ( USBVendorRequest(&dreq) == -1)
//          udev->ack_setup( udev);
         #endif /*USB_WITH_CB_FNC*/
        } else if ( type == USB_CLASS_REQUEST) {
         #ifdef USB_WITH_CB_FNC
          int ret = -1;
//         if(USBClassRequestCBFnc != NULL)
//         ret = USBClassRequestCBFnc(&usb_ep0, &dreq);
          if( udev->class_fnc != NULL)
            ret = udev->class_fnc( udev);
          if( ret < 0)
            usb_udev_stall( ep0);
//         #else /*USB_WITH_CB_FNC*/
//          if ( USBClassRequest(&dreq) == -1)
//          udev->ack_setup( udev);
         #endif /*USB_WITH_CB_FNC*/
        } else
            usb_udev_stall( ep0);
      }
      ret = 1;
    }

/* response to interrupt Ep0RxInt  - receive data */
    if ( udev->flags & USB_FLAG_EVENT_RX0) {
      int i;
      udev->flags &= ~USB_FLAG_EVENT_RX0;
      usb_debug_print( DEBUG_LEVEL_MEDIUM, ( "Ep0Rx\n"));
      if (( ep0->flags & USB_STATE_MASK) == USB_STATE_RECEIVE) {
        usb_debug_print( DEBUG_LEVEL_HIGH, ( "RCV:p=%04lX,s=%d\n", (unsigned long)ep0->ptr, ep0->size));

        i = usb_udev_read_endpoint(ep0, ep0->ptr, ep0->max_packet_size);
        ep0->actual += i;
        ep0->ptr +=i;

       #ifdef USB_WITH_CB_FNC
       if ( ep0->next_pkt_fnc ) {
          if( ep0->next_pkt_fnc( ep0, i, USB_NEXT_PKT_REC) < 0) {
            usb_udev_stall( ep0);
            return -1;
          }
        }
       #endif /*USB_WITH_CB_FNC*/

        if (( i != ep0->max_packet_size) || ( ep0->actual >= ep0->size)) {
          usb_complete_control_transfer( ep0, USB_COMPLETE_OK );
          if ( ep0->efnc) {
            ep0->efnc(ep0);
          }
        }
      } else {
        ep0->flags &= ~USB_STATE_MASK; //state = USB_STATE_IDLE;
      }
    }

/* response to interrupt Ep0TxInt */
    if ( udev->flags & USB_FLAG_EVENT_TX0) {
      short i = ep0->size - ep0->actual;
      udev->flags &= ~USB_FLAG_EVENT_TX0;
//usb_debug_print( DEBUG_LEVEL_LOW, ("0S-%d(%d){%d}\n", ep0->state, ep0->size, ep0->max_packet_size));
      usb_debug_print( DEBUG_LEVEL_MEDIUM, ( "EP0Tx:i=%d\n", i));

      if (( ep0->flags & USB_STATE_MASK) == USB_STATE_TRANSMIT) {

        if(i > ep0->max_packet_size) i = ep0->max_packet_size;

        if ( i > 0 ) {
         #ifdef USB_WITH_CB_FNC
          if ( ep0->next_pkt_fnc) {
            if( ep0->next_pkt_fnc( ep0, i, USB_NEXT_PKT_SEND) < 0) {
              usb_udev_stall( ep0);
              return -1;
            }
          }
         #endif /*USB_WITH_CB_FNC*/
    usb_debug_print( DEBUG_LEVEL_HIGH, ("Wr(%d)\n",i));
          usb_udev_write_endpoint( ep0, ep0->ptr, i);
          ep0->actual += i;
          ep0->ptr +=i;

          if( i != ep0->max_packet_size) {
            /* last packed without full size has been sent, state can change to idle */
            usb_complete_control_transfer( ep0, USB_COMPLETE_OK );
          }
        } else {
           usb_udev_ack_setup( udev);   // Send zero packet at the end ???
          usb_complete_control_transfer( ep0, USB_COMPLETE_OK );
        }
      }
      ret = 1;
    }

    return ret;
  }


