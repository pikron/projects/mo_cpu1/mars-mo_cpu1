/*************************************************/
/***   Module : USB specification              ***/
/***   Author : Roman Bartosinski 29.07.2002   ***/
/***   Modify : 08.08.2002, 14.01.2003         ***/
/*************************************************/

#ifndef _USB_SPECIFICATIONS_AND_DEFINITIONS_MODULE
 #define _USB_SPECIFICATIONS_AND_DEFINITIONS_MODULE

/* #include <inttypes.h> */
#include <types.h>

#ifndef PACKED
  #ifdef  __GNUC__
    #define PACKED __attribute__((packed))
  #else /*__GNUC__*/
    #define PACKED /*nothing*/
  #endif /*__GNUC__*/
#endif

/* this section is from __USB100.H__ and __CHAP9.H__ and define USB constants and structs */

/* *** USB Device Request *** (spec. 9.3) */
  typedef struct _tag_usb_device_request {
    uint8_t  bmRequestType;
    uint8_t  bRequest;
    uint16_t wValue;
    uint16_t wIndex;
    uint16_t wLength;
  } USB_DEVICE_REQUEST;

/****************************************************************************************/
/*** definitions for USB tranfer standard packets described in USB secif. - chapter 9 ***/
/****************************************************************************************/
  #define DEVICE_ADDRESS_MASK   0x7F

/* bmRequestType D7 - Data transfer direction */
  #define USB_DATA_DIR_MASK        (uint8_t)0x80
  #define USB_DATA_DIR_FROM_HOST   (uint8_t)0x00
  #define USB_DATA_DIR_TO_HOST     (uint8_t)0x80
/* bmRequestType D4..D0 - Recipient */
  #define USB_RECIPIENT            (uint8_t)0x1F
  #define USB_RECIPIENT_DEVICE     (uint8_t)0x00
  #define USB_RECIPIENT_INTERFACE  (uint8_t)0x01
  #define USB_RECIPIENT_ENDPOINT   (uint8_t)0x02
  #define USB_RECIPIENT_OTHER      (uint8_t)0x03
/* bmRequestType D6..D5 - Type */
  #define USB_REQUEST_TYPE_MASK    (uint8_t)0x60
  #define USB_STANDARD_REQUEST     (uint8_t)0x00
  #define USB_CLASS_REQUEST        (uint8_t)0x20
  #define USB_VENDOR_REQUEST       (uint8_t)0x40
/* Standard request codes (spec. 9.4) */
  #define USB_REQUEST_MASK         (uint8_t)0x0F
  #define USB_REQUEST_GET_STATUS                  0x00
  #define USB_REQUEST_CLEAR_FEATURE               0x01
  #define USB_REQUEST_SET_FEATURE                 0x03
  #define USB_REQUEST_SET_ADDRESS                 0x05
  #define USB_REQUEST_GET_DESCRIPTOR              0x06
  #define USB_REQUEST_SET_DESCRIPTOR              0x07
  #define USB_REQUEST_GET_CONFIGURATION           0x08
  #define USB_REQUEST_SET_CONFIGURATION           0x09
  #define USB_REQUEST_GET_INTERFACE               0x0A
  #define USB_REQUEST_SET_INTERFACE               0x0B
  #define USB_REQUEST_SYNC_FRAME                  0x0C
/* Descriptor types (spec. 9.4) */
  #define USB_DESCRIPTOR_TYPE_DEVICE                    0x01
  #define USB_DESCRIPTOR_TYPE_CONFIGURATION             0x02
  #define USB_DESCRIPTOR_TYPE_STRING                    0x03
  #define USB_DESCRIPTOR_TYPE_INTERFACE                 0x04
  #define USB_DESCRIPTOR_TYPE_ENDPOINT                  0x05
  #define USB_DESCRIPTOR_TYPE_DEVICE_QUALIFIER          0x06
  #define USB_DESCRIPTOR_TYPE_OTHER_SPEED_CONFIGURATION 0x07
  #define USB_DESCRIPTOR_TYPE_POWER                     0x08

/* values for the bits returned by the USB GET_STATUS command (spec. 9.4.5) */
  #define USB_GETSTATUS_SELF_POWERED                0x01
  #define USB_GETSTATUS_REMOTE_WAKEUP_ENABLED       0x02

/* values for standard request Clear Feature */
  #define USB_FEATURE_ENDPOINT_STALL          0x0000
  #define USB_FEATURE_REMOTE_WAKEUP           0x0001


/*******************************************************/
/*** Standard USB Descriptor Definitions (spec. 9.6) ***/
/*******************************************************/

/* *** DEVICE *** (spec. 9.6.1) */
  struct _tag_usb_device_descriptor {
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint16_t bcdUSB;
    uint8_t  bDeviceClass;
    uint8_t  bDeviceSubClass;
    uint8_t  bDeviceProtocol;
    uint8_t  bMaxPacketSize0;
    uint16_t idVendor;
    uint16_t idProduct;
    uint16_t bcdDevice;
    uint8_t  iManufacturer;
    uint8_t  iProduct;
    uint8_t  iSerialNumber;
    uint8_t  bNumConfigurations;
  } PACKED;
  typedef struct _tag_usb_device_descriptor
    USB_DEVICE_DESCRIPTOR, *PUSB_DEVICE_DESCRIPTOR;

/* *** DEVICE_QUALIFIER *** (spec. 9.6.2) */
  struct _tag_usb_device_qualifier_descriptor {
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint16_t bcdUSB;
    uint8_t  bDeviceClass;
    uint8_t  bDeviceSubClass;
    uint8_t  bDeviceProtocol;
    uint8_t  bMaxPacketSize0;
    uint8_t  bNumConfigurations;
    uint8_t  bReserved;
  } PACKED;
  typedef struct _tag_usb_device_qualifier_descriptor
    USB_DEVICE_QUALIFIER_DESCRIPTOR, *PUSB_DEVICE_QUALIFIER_DESCRIPTOR;

/* *** CONFIGURATION *** (spec. 9.6.3) */
  struct _tag_usb_configuration_descriptor {
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint16_t wTotalLength;
    uint8_t  bNumInterfaces;
    uint8_t  bConfigurationValue;
    uint8_t  iConfiguration;
    uint8_t  bmAttributes;
    uint8_t  MaxPower;
  } PACKED;
  typedef struct _tag_usb_configuration_descriptor
    USB_CONFIGURATION_DESCRIPTOR, *PUSB_CONFIGURATION_DESCRIPTOR;
/* definitions for bits in the bmAttributes field of a configuration descriptor. (spec. 9.6.3) */
  #define USB_CONFIG_POWERED_MASK  0xc0
  #define USB_CONFIG_BUS_POWERED   0x80
  #define USB_CONFIG_SELF_POWERED  0x40
  #define USB_CONFIG_REMOTE_WAKEUP 0x20
  #define BUS_POWERED              0x80
  #define SELF_POWERED             0x40
  #define REMOTE_WAKEUP            0x20

/* *** OTHER_SPEED_CONFIGURATION *** (spec. 9.6.4) */
  struct _tag_usb_other_speed_configuration {
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint16_t wTotalLength;
    uint8_t  bNumInterfaces;
    uint8_t  bConfigurationValue;
    uint8_t  iConfiguration;
    uint8_t  bmAttributes;
    uint8_t  bMaxPower;
  } PACKED;
  typedef struct _tag_usb_other_speed_configuration
    USB_OTHER_SPEED_CONFIGURATION_DESCRIPTOR, *PUSB_OTHER_SPEED_CONFIGURATION_DESCRIPTOR;

/* *** INTERFACE *** (spec. 9.6.5) */
  struct _tag_usb_interface_descriptor {
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint8_t  bInterfaceNumber;
    uint8_t  bAlternateSetting;
    uint8_t  bNumEndpoints;
    uint8_t  bInterfaceClass;
    uint8_t  bInterfaceSubClass;
    uint8_t  bInterfaceProtocol;
    uint8_t  iInterface;
  } PACKED;
  typedef struct _tag_usb_interface_descriptor
    USB_INTERFACE_DESCRIPTOR, *PUSB_INTERFACE_DESCRIPTOR;

/* *** ENDPOINT *** (spec. 9.6.6) */
  struct _tag_usb_endpoint_descriptor {
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint8_t  bEndpointAddress;
    uint8_t  bmAttributes;
    uint16_t wMaxPacketSize;
    uint8_t  bInterval;
  } PACKED;
  typedef struct _tag_usb_endpoint_descriptor
    USB_ENDPOINT_DESCRIPTOR, *PUSB_ENDPOINT_DESCRIPTOR;

/* Endpoint direction bit, stored in address (spec. 9.6.6) */
  #define USB_ENDPOINT_DIRECTION_MASK               0x80
/* test direction bit in the bEndpointAddress field of an endpoint descriptor. */
  #define USB_ENDPOINT_DIRECTION_OUT(addr)          (!((addr) & USB_ENDPOINT_DIRECTION_MASK))
  #define USB_ENDPOINT_DIRECTION_IN(addr)           ((addr) & USB_ENDPOINT_DIRECTION_MASK)
/* Values for bmAttributes field of an endpoint descriptor (spec. 9.6.6) */
  #define USB_ENDPOINT_TYPE_MASK                    0x03
  #define USB_ENDPOINT_TYPE_CONTROL                 0x00
  #define USB_ENDPOINT_TYPE_ISOCHRONOUS             0x01
  #define USB_ENDPOINT_TYPE_BULK                    0x02
  #define USB_ENDPOINT_TYPE_INTERRUPT               0x03

/* *** STRING *** (spec. 9.6.7) */
  struct _tag_usb_string_descriptor {
    uint8_t  bLength;
    uint8_t  bDescriptorType;
    uint8_t  bString[];
  } PACKED;
  typedef struct _tag_usb_string_descriptor
    USB_STRING_DESCRIPTOR, *PUSB_STRING_DESCRIPTOR;


/*******************************************/
/*** USB_IF - Defined USB device classes ***/
/*******************************************/
  #define USB_DEVICE_CLASS_RESERVED             0x00
  #define USB_DEVICE_CLASS_AUDIO                0x01
  #define USB_DEVICE_CLASS_COMMUNICATIONS       0x02
  #define USB_DEVICE_CLASS_HUMAN_INTERFACE      0x03
  #define USB_DEVICE_CLASS_MONITOR              0x04
  #define USB_DEVICE_CLASS_PHYSICAL_INTERFACE   0x05
  #define USB_DEVICE_CLASS_POWER                0x06
  #define USB_DEVICE_CLASS_PRINTER              0x07
  #define USB_DEVICE_CLASS_STORAGE              0x08
  #define USB_DEVICE_CLASS_HUB                  0x09
  #define USB_DEVICE_CLASS_APPLICATION_SPECIFIC 0xFE
  #define USB_DEVICE_CLASS_VENDOR_SPECIFIC      0xFF

  /* define application specific device class subclasses */
  #define USB_APPL_SUBCLASS_FIRMWARE_UPDATE     0x01
  #define USB_APPL_SUBCLASS_IRDA_USB_BRIDGE     0x02

#endif
