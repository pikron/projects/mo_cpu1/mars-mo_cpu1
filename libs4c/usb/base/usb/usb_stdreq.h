#ifndef _USB_STDREQ_MODULE_
  #define _USB_STDREQ_MODULE_

  int usb_stdreq_get_status( usb_device_t *udev);

  int usb_stdreq_clear_feature( usb_device_t *udev);

  int usb_stdreq_set_feature( usb_device_t *udev);

  int usb_stdreq_set_address( usb_device_t *udev);

  int usb_stdreq_get_configuration( usb_device_t *udev);

  int usb_stdreq_set_configuration( usb_device_t *udev);

  int usb_stdreq_get_interface( usb_device_t *udev);

  int usb_stdreq_set_interface( usb_device_t *udev);

  int usb_stdreq_get_descriptor( usb_device_t *udev);

#endif /*_USB_STDREQ_MODULE_*/

