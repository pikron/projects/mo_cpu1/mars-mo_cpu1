/* Global debug macros, variables, functions - header file */
/* R.B. - 23.4.2003 */

#ifndef _USB_DEBUG_H_
#define _USB_DEBUG_H_

#if 1
  #include <stdio.h>
  #define usb_printf printf
#else
  int simple_printf(const char *f, ...);
  #define usb_printf simple_printf
#endif

/* Debug levels */
#define DEBUG_LEVEL_NONE    0
#define DEBUG_LEVEL_LOW     1
#define DEBUG_LEVEL_MEDIUM  2
#define DEBUG_LEVEL_HIGH    3
#define DEBUG_LEVEL_VERBOSE 4

#ifndef DEBUG_STATIC_LEVEL
/* Global static debug level */
#define DEBUG_STATIC_LEVEL     DEBUG_LEVEL_NONE
#endif

/* If it is defined, current global debug level is
   in 'global_debug_level' variable. Otherwise will
   be used 'DEBUG_STATIC_LEVEL' as current global
   debug level
*/
#define DEBUG_USE_DYNAMIC_LEVEL

#define DEBUG


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

#ifdef DEBUG

/* use static or dynamic global debug level */
#if defined(DEBUG_USE_DYNAMIC_LEVEL) /* use dynamic debug level */
  extern unsigned char usb_debug_level;
#else /* use only static debug level */
  #define usb_debug_level DEBUG_STATIC_LEVEL
#endif


/* usb_debug_print( level, printargs) print debug info in printargs if
   level is lower or equal to 'global_debug_level'/'DEBUG_STATIC_LEVEL'.
   'printargs' is with parenthesis.
   usage : usb_debug_print( DEBUG_LEVEL_MEDIUM,("i=%d",i));
*/
#define usb_debug_print( _lvl_, _prnargs_)      \
  do {                                          \
    if ( usb_debug_level >= _lvl_) {            \
      usb_printf _prnargs_;                     \
    }                                           \
  } while(0)

/* usb_debug_info( printargs) print debug info always
   usage : usb_debug_info( "debug info");
*/
#define usb_debug_info usb_printf  /* FIXME: this is not correct */

#else /* DEBUG */
  #define usb_debug_print( _lvl_, _prnargs_)
  #define usb_debug_info(...)
#endif /* DEBUG */


  void usb_debug_set_level(int level);

  char *usb_debug_get_req_recipient( char rqt);
  char *usb_debug_get_req_type( char rqt);
  char *usb_debug_get_std_request( char req);
  char *usb_debug_get_std_descriptor( unsigned char desc);


#endif /* _GLOBAL_DEBUG_H_ */
