/**************************************************************/
/***   Module : USB module - header file                    ***/
/***   Author : Roman Bartosinski (C) 28.04.2002            ***/
/***   Modify : 08.08.2002                                  ***/
/***   Rewrite: 05.09.2002                                  ***/
/**************************************************************/

#ifndef _USB_MODULE_
  #define _USB_MODULE_

  #include "usb_spec.h"

#if defined(SDCC) || defined(__KEIL__) || defined(__C51__)
  /*8051 special handling*/
  #define REENTRANT_SIGN reentrant
#else
  #define xdata           /*nothing*/
  #define REENTRANT_SIGN  /*nothing*/
#endif

#define USB_WITH_CB_FNC
#define USB_WITH_UDEV_FNC

#ifdef USB_WITH_UDEV_FNC
  #define USB_UDEV_REENTRANT_SIGN REENTRANT_SIGN
#else /*USB_WITH_UDEV_FNC*/
  #define USB_UDEV_REENTRANT_SIGN /*nothing*/
#endif /*USB_WITH_UDEV_FNC*/

/* control endpoint */
  #define MAX_CONTROL_XFER_DATA_SIZE 8

  struct usb_ep_t;

  typedef void endfnc_t( struct  usb_ep_t *ep) REENTRANT_SIGN;

  #define USB_NEXT_PKT_SEND 0
  #define USB_NEXT_PKT_REC  1

  #define USB_COMPLETE_OK   0
  #define USB_COMPLETE_FAIL -1

  typedef struct usb_ep_t {
    struct usb_device_t *udev;      /* pointer to parent device */
    unsigned short max_packet_size; /* max. size of endpoint package, e.g. PDI_EP0_PACKET_SIZE */
    unsigned char *ptr;             /* pointer to current transmitted data */
    unsigned int  size;             /* full size of current transmitted data */
    unsigned int  actual;           /* transmitted data size */
    endfnc_t *efnc;                 /* ??? */
    unsigned char flags;            /* endpoint flags & state - idle,receiving, transmitting  ??? HERE ??? */
    unsigned char epnum;            /* endpoint number (index) - endpoint0 must be set to 0 */
    unsigned short event_mask;      /* event(interrupt) mask for this endpoint, e.g. PDI_INT_EP1_IN for pdiusbd1x */
   #ifdef USB_WITH_CB_FNC
    int (*next_pkt_fnc)(struct usb_ep_t *ep, int len, int codeval) REENTRANT_SIGN;
    int (*complete_fnc)(struct usb_ep_t *ep, int status) REENTRANT_SIGN;
    long user_data;
   #endif /*USB_WITH_CB_FNC*/
  } usb_ep_t;


/* Vendor & Class functions */
/*
 #ifdef USB_WITH_CB_FNC
  typedef int usb_vendor_extension_fnc_t(usb_ep_t *ep, USB_DEVICE_REQUEST *dreq);
  extern xdata usb_vendor_extension_fnc_t USBVendorRequestCBFnc;

  typedef int usb_class_extension_fnc_t(usb_ep_t *ep, USB_DEVICE_REQUEST *dreq);
  extern xdata usb_class_extension_fnc_t USBClassRequestCBFnc;
 #else //USB_WITH_CB_FNC
  char USBVendorRequest( USB_DEVICE_REQUEST *dr);
  char USBClassRequest( USB_DEVICE_REQUEST *dr);
 #endif //USB_WITH_CB_FNC
*/


/* USB device */
  typedef struct usb_device_t {
    unsigned char id;               /* device ID ??? */
    unsigned char flags;            /* usb device flags + endpoint0 events */
    unsigned char ep_events;        /* one bit for each endpoint (without ep0) event,(bit0 for udev->ep[0], bit1 for udev->ep[1], ...)*/
    unsigned char configuration;    /* current configuration */
//    unsigned char interface;        /* current interface */
//    unsigned char altinterface;     /* current alternative interface */

    //int (stdreq[13])( struct usb_device_t *udev) REENTRANT_SIGN;    /* pointer to array of standard request processing functions */
    int (*vendor_fnc)( struct usb_device_t *udev) REENTRANT_SIGN;     /* pointer to vendor request processing function */
    int (*class_fnc)( struct usb_device_t *udev) REENTRANT_SIGN;      /* pointer to class request processing function */

   #ifdef USB_WITH_UDEV_FNC
    int (*init)( struct usb_device_t *udev) REENTRANT_SIGN;              /* function for hw specific part of initialize usb device */
    int (*connect)( struct usb_device_t *udev) REENTRANT_SIGN;           /* function for hw specific part of connecting device to usb */
    int (*disconnect)( struct usb_device_t *udev) REENTRANT_SIGN;        /* function for hw specific part of disconnecting device to usb */
    void (*ack_setup)( struct usb_device_t *udev) REENTRANT_SIGN;        /* function for hw specific part of control response acknowledge */
    void (*ack_control_setup)( struct usb_device_t *udev) REENTRANT_SIGN;        /* function for hw specific part of control response acknowledge */
    int (*check_events)( struct usb_device_t *udev) REENTRANT_SIGN;      /* hw specific part of function for checking events */
    void (*stall)( usb_ep_t *ep) REENTRANT_SIGN;                         /* hw specific function for stall endpoint */
    int (*read_endpoint)( usb_ep_t *ep, void *ptr, int size) REENTRANT_SIGN;
    int (*write_endpoint)( usb_ep_t *ep, const void *ptr, int size) REENTRANT_SIGN;
   #endif /*USB_WITH_UDEV_FNC*/

//    USB_DEVICE_REQUEST *request;   /* current usb request - only if there is a valid usb request in processing */
    USB_DEVICE_REQUEST request;    /* usb device request */

    unsigned char cntep;           /* number of device endpoints in ep array without EP0 */
    usb_ep_t ep0;                  /* endpoint 0 */
    usb_ep_t *ep;                 /* others endpoints in array */
  } usb_device_t;


/* endpoint flags */
  /* endpoint state */
  #define USB_STATE_IDLE       0x00
  #define USB_STATE_TRANSMIT   0x01
  #define USB_STATE_RECEIVE    0x02
  #define USB_STATE_MASK       0x03

/* usb_device flags */
  #define USB_FLAG_CONFIGURED   0x01
  #define USB_FLAG_BUS_RESET    0x02
  #define USB_FLAG_SUSPEND      0x04
  #define USB_FLAG_SETUP        0x08 // setup_packet
  #define USB_FLAG_REMOTE_WAKE  0x10

  #define USB_FLAG_EVENT_RX0  0x40
  #define USB_FLAG_EVENT_TX0  0x80



/* device functions - inline ??? */
  int usb_init( usb_device_t *udev);
  int usb_connect( usb_device_t *udev);
  int usb_disconnect( usb_device_t *udev);
  void usb_stall( usb_ep_t *ep);

  #define usb_stall_ep0( udev) \
    do { \
      usb_stall( &(udev->ep0)); \
    } while(0)

/* check usb events(interrupts) */
  int usb_check_events( usb_device_t *udev);
/* response to standard constrol requests */
  int usb_control_response( usb_device_t *udev);
/* send control data */
  void usb_send_control_data( usb_device_t *udev, unsigned char *pData, unsigned short len);
  void usb_set_control_endfnc( usb_device_t *udev, endfnc_t *efnc);// REENTRANT_SIGN;
  void usb_ack_setup( usb_ep_t *ep);


/* Standard requests functions */
//  typedef int (*usb_stdreq_fnc_t)( usb_device_t *udev) REENTRANT_SIGN;
//  extern xdata usb_stdreq_fnc_t usb_standard_requests[13];


#ifdef USB_WITH_UDEV_FNC

  #define usb_udev_is_fnc(_M_udev, _M_fnc) (_M_udev->_M_fnc)

  #define usb_udev_init(_M_udev) (_M_udev->init(_M_udev))
  #define usb_udev_connect(_M_udev) (_M_udev->connect(_M_udev))
  #define usb_udev_disconnect(_M_udev) (_M_udev->disconnect(_M_udev))
  #define usb_udev_ack_setup(_M_udev) (_M_udev->ack_setup(_M_udev))
  #define usb_udev_ack_control_setup(_M_udev) (_M_udev->ack_control_setup(_M_udev))
  #define usb_udev_check_events(_M_udev) (_M_udev->check_events(_M_udev))

  #define usb_udev_stall(_M_ep) ((_M_ep)->udev->stall(_M_ep))

  #define usb_udev_read_endpoint(_M_ep, _M_ptr, _M_size) \
	((_M_ep)->udev->read_endpoint(_M_ep, _M_ptr, _M_size))

  #define usb_udev_write_endpoint(_M_ep, _M_ptr, _M_size) \
	((_M_ep)->udev->write_endpoint(_M_ep, _M_ptr, _M_size))

#else /*USB_WITH_UDEV_FNC*/

  #define USB_PDI_DIRECT_FNC
  #include "pdi.h"

#endif /*USB_WITH_UDEV_FNC*/

#endif
