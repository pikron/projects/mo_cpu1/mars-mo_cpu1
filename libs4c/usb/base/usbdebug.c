/* Global debug macros, variables, functions - header file */
/* R.B. - 23.4.2003 */

#include <usb/usbdebug.h>
#include <usb/usb_spec.h>

#ifdef DEBUG

/* debug can be enabled in run-time */
#if defined(DEBUG_USE_DYNAMIC_LEVEL) /* use dynamic debug level */
  unsigned char usb_debug_level = DEBUG_LEVEL_NONE;
#endif

/*************************************************************
 *** Debug infos
 *************************************************************/

  void usb_debug_set_level(int level) {
   #if defined(DEBUG_USE_DYNAMIC_LEVEL) /* use dynamic debug level */
    usb_debug_level = level;
   #endif
  }


  char *usb_debug_get_req_recipient( char rqt) {
    switch ( rqt & USB_RECIPIENT) {
      case USB_RECIPIENT_DEVICE:    return "DEVICE";
      case USB_RECIPIENT_INTERFACE: return "INTERFACE";
      case USB_RECIPIENT_ENDPOINT:  return "ENDPOINT";
    }
    return "OTHER";
  }
  char *usb_debug_get_req_type( char rqt) {
    switch ( rqt & USB_REQUEST_TYPE_MASK) {
      case USB_STANDARD_REQUEST: return "STANDARD";
      case USB_CLASS_REQUEST:    return "CLASS";
      case USB_VENDOR_REQUEST:   return "VENDOR";
    }
    return "RESERVED";
  }
  char *usb_debug_get_std_request( char req) {
    switch ( req & USB_REQUEST_MASK) {
      case USB_REQUEST_GET_STATUS:        return "GET STATUS";
      case USB_REQUEST_CLEAR_FEATURE:     return "CLEAR FEATURE";
      case USB_REQUEST_SET_FEATURE:       return "SET FEATURE";
      case USB_REQUEST_SET_ADDRESS:       return "SET ADDRESS";

      case USB_REQUEST_GET_DESCRIPTOR:    return "GET DESCRIPTOR";
      case USB_REQUEST_SET_DESCRIPTOR:    return "SET DESCRIPTOR";
      case USB_REQUEST_GET_CONFIGURATION: return "GET CONFIGURATION";
      case USB_REQUEST_SET_CONFIGURATION: return "SET CONFIGURATION";
      case USB_REQUEST_GET_INTERFACE:     return "GET INTERFACE";
      case USB_REQUEST_SET_INTERFACE:     return "SET INTERFACE";
      case USB_REQUEST_SYNC_FRAME:        return "SYNC FRAME";
    }
    return "UNKNOWN";
  }
  char *usb_debug_get_std_descriptor( unsigned char desc) {

    switch ( desc) {
      case USB_DESCRIPTOR_TYPE_DEVICE:                    return "DEVICE";
      case USB_DESCRIPTOR_TYPE_CONFIGURATION:             return "CONFIGURATION";
      case USB_DESCRIPTOR_TYPE_STRING:                    return "STRING";
      case USB_DESCRIPTOR_TYPE_INTERFACE:                 return "INTERFACE";
      case USB_DESCRIPTOR_TYPE_ENDPOINT:                  return "ENDPOINT";
      case USB_DESCRIPTOR_TYPE_DEVICE_QUALIFIER:          return "DEVICE_QUALIFIER";
      case USB_DESCRIPTOR_TYPE_OTHER_SPEED_CONFIGURATION: return "OTHER_SPEED_CONFIG";
      case USB_DESCRIPTOR_TYPE_POWER:                     return "POWER";
    }
    return "UNKNOWN";
  }

#else /*DEBUG*/
  void usb_debug_set_level(int level) {
  }
#endif /* DEBUG */
