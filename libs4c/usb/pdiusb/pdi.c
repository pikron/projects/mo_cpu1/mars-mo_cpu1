/*****************************************************/
/***   Module : USB PDI                            ***/
/***   Author : Roman Bartosinski (C) 28.04.2002   ***/
/***   Modify : 08.08.2002, 16.04.2003             ***/
/*****************************************************/

#include <stdio.h>
#include <string.h>
#include <system_def.h>

#define USB_PDI_EXPORT_FNC

#include <usb/pdiusb.h>
#include <usb/pdi.h>
#include <usb/usbdebug.h>


/* init chip state */
  void pdiInitChipState( void) {
   #ifdef PDIUSBD11
    pdiSetHUBAddressEnable( 0, 0);
   #endif /*PDIUSBD11*/
    pdiSetAddressEnable( PDI_ENAD_ENABLE);
    pdiSetEndpointEnable( PDI_EPEN_ENABLE);
  }

#ifdef PDIUSBD11

  #define PDI_MODE_BASE_VALUE (PDI_MODE_MUSTBEONE | \
  PDI_MODE_REMOTE_WAKEUP | PDI_MODE_NO_LAZY_CLOCK | \
  PDI_MODE_CLOCK_RUNNING | PDI_CLOCK_4M)

#else /*PDIUSBD11*/

  #define PDI_MODE_BASE_VALUE (PDI_CLOCK_SET_TO_ONE | \
  PDI_MODE_NO_LAZY_CLOCK | PDI_MODE_CLOCK_RUNNING | \
  PDI_CLOCK_4M)

#endif /*PDIUSBD11*/


/* connect usb */
  int usb_pdi_connect( usb_device_t *udev) {
    pdiSetMode( PDI_MODE_BASE_VALUE | PDI_MODE_SOFT_CONNECT );
    return 0;
  }

/* disconnect usb */
  int usb_pdi_disconnect( usb_device_t *udev) {
    pdiSetMode( PDI_MODE_BASE_VALUE & ~PDI_MODE_SOFT_CONNECT );
    return 0;
  }

/* acknowledge control transfer */
  void usb_pdi_ack_setup( usb_device_t *udev) {
    pdiWriteEndpoint( PDI_EP0_TX, 0, 0);
  }


/* acknowledge control transfer */
  void usb_pdi_ack_control_setup( usb_device_t *udev) {
    pdiAckSetupControl();
  }


/**
 * usb_pdi_check events
 * function reads interrupt register and sets event flags
 * function returns 1 if there is some new event.
 * function returns 0 if there isn't new event but all is OK
 * function returns -1 if there is any error
*/
  int usb_pdi_check_events( usb_device_t *udev) {
    volatile unsigned char LastTrans = 0;
    volatile unsigned int LastInt;
    int ret = 0, i;

    LastInt = pdiGetInterrupt();
if ( LastInt) {
  usb_debug_print( DEBUG_LEVEL_LOW, ("PI=0x%04X\n", LastInt));
}
    usb_debug_print( DEBUG_LEVEL_HIGH, ("PDI Int=0x%04X\n", LastInt));

    if ( LastInt & PDI_INT_BUSRESET) {                 // D12 - Bus reset reached
      pdiInitChipState();
      udev->flags |= USB_FLAG_BUS_RESET;
      ret = 1;
    } else {
      #ifdef PDIUSBD12
      if ( LastInt & PDI_INT_SUSPEND) {                 // D12 - Suspend flag changed
        udev->flags |= USB_FLAG_SUSPEND;
        ret = 1;
      }
      #endif


      if ( LastInt & PDI_INT_EP0_OUT) {                // D12 - Ep0RxDone - some data was received in EP0
        LastTrans = pdiGetLastTransStatus( PDI_EP0_RX);
        if ( LastTrans & PDI_LTSTAT_SETUP) { // setup packet
          udev->flags |= USB_FLAG_SETUP;
        } else {
          udev->flags |= USB_FLAG_EVENT_RX0;
        }
        ret = 1;
      }
      if ( LastInt & PDI_INT_EP0_IN) {                 // D12 - Ep0TxDone - data in EP0 was sended
        LastTrans = pdiGetLastTransStatus( PDI_EP0_TX);
        udev->flags |= USB_FLAG_EVENT_TX0;
        ret = 1;
      }


      for( i=0; i<udev->cntep; i++) {
        if ( LastInt & (udev->ep+i)->event_mask) {
          LastTrans = pdiGetLastTransStatus( (udev->ep+i)->epnum);
          udev->ep_events |= 1<<i;
          LastInt &= ~((udev->ep+i)->event_mask);
          ret = 1;
        }
      }

      /* check unsupported endpoints */
      if ( LastInt & PDI_INT_EP1_OUT) {                // D12 - Ep1RxDone - some data was received in EP1
        LastTrans = pdiGetLastTransStatus( PDI_EP1_RX);
        pdiSetEpStatus( PDI_EP1_RX, PDI_SET_EP_STALLED);
      }
      if ( LastInt & PDI_INT_EP1_IN) {                 // D12 - Ep1TxDone - data in EP1 was sended
        LastTrans = pdiGetLastTransStatus( PDI_EP1_TX);
        pdiSetEpStatus( PDI_EP1_TX, PDI_SET_EP_STALLED);
      }
      if ( LastInt & PDI_INT_EP2_OUT) {                // D12 - Ep2RxDone - some data was received in EP2
        LastTrans = pdiGetLastTransStatus( PDI_EP2_RX);
        pdiSetEpStatus( PDI_EP2_RX, PDI_SET_EP_STALLED);
      }
      if ( LastInt & PDI_INT_EP2_IN) {                 // D12 - Ep2TxDone - data in EP2 was sended
        LastTrans = pdiGetLastTransStatus( PDI_EP2_TX);
        pdiSetEpStatus( PDI_EP2_TX, PDI_SET_EP_STALLED);
      }
     #if defined(PDIUSBD11) || defined(PDIUSBH11A_SINGLE)  // D11,H11_S
      if ( LastInt & PDI_INT_EP3_OUT) {                // D11 - Ep3RxDone - some data was received in EP3
        LastTrans = pdiGetLastTransStatus( PDI_EP3_RX);
        pdiSetEpStatus( PDI_EP3_RX, PDI_SET_EP_STALLED);
      }
      if ( LastInt & PDI_INT_EP3_IN) {                 // D11 - Ep3TxDone - data in EP3 was sended
        LastTrans = pdiGetLastTransStatus( PDI_EP3_TX);
        pdiSetEpStatus( PDI_EP3_TX, PDI_SET_EP_STALLED);
      }
     #endif /* D11,H11_S */
    }
    return ret;
  }


/* stall endpoint X */
  void usb_pdi_stall( usb_ep_t *ep) {
    if ( ep->epnum) {
      pdiSetEpStatus( ep->epnum, PDI_SET_EP_STALLED);
    } else { // endpoint0
      pdiSetEpStatus( PDI_EP0_TX, PDI_SET_EP_STALLED);
      pdiSetEpStatus( PDI_EP0_RX, PDI_SET_EP_STALLED);
    }
  }

  int usb_pdi_read_endpoint( usb_ep_t *ep, void *ptr, int size) USB_UDEV_REENTRANT_SIGN
  {
    if(!ep->epnum)
       return pdiReadEndpoint( PDI_EP0_RX, size, ptr);
    else
       return pdiReadEndpoint( ep->epnum, size, ptr);
  }

  int usb_pdi_write_endpoint( usb_ep_t *ep, const void *ptr, int size) USB_UDEV_REENTRANT_SIGN
  {
    if(!ep->epnum)
       pdiWriteEndpoint( PDI_EP0_TX, size, ptr);
    else
       pdiWriteEndpoint( ep->epnum, size, ptr);
    return size;
  }


/* init usb structures and chip */
  int usb_pdi_init( usb_device_t *udev) {

   #ifndef USB_PDI_DIRECT_FNC
    udev->connect = usb_pdi_connect;
    udev->disconnect = usb_pdi_disconnect;
    udev->ack_setup = usb_pdi_ack_setup;
    udev->ack_control_setup = usb_pdi_ack_control_setup;
    udev->stall = usb_pdi_stall;
    udev->check_events = usb_pdi_check_events;
    udev->read_endpoint = usb_pdi_read_endpoint;
    udev->write_endpoint = usb_pdi_write_endpoint;
   #endif /*USB_PDI_DIRECT_FNC*/

    udev->ep0.max_packet_size = PDI_EP0_PACKET_SIZE;

    pdiInitChipState();
    pdiSetMode( PDI_MODE_BASE_VALUE);
    return 0;
  }
