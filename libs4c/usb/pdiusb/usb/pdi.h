/*****************************************************/
/***   Module : USB PDI - header file              ***/
/***   Author : Roman Bartosinski (C) 28.04.2002   ***/
/***   Modify : 08.08.2002, 16.04.2003             ***/
/*****************************************************/

#ifndef _USB_PDI_SUBMODULE_HEADER_FILE_
  #define _USB_PDI_SUBMODULE_HEADER_FILE_

  #include <usb/usb.h>

//  int usb_pdi_init( usb_device_t *udev) REENTRANT_SIGN;
  int usb_pdi_init( usb_device_t *udev);

  #ifdef USB_PDI_DIRECT_FNC
    #define USB_PDI_EXPORT_FNC

    #define usb_udev_is_fnc(_M_udev, _M_fnc) (1)

    #define usb_udev_init usb_pdi_init
    #define usb_udev_connect usb_pdi_connect
    #define usb_udev_disconnect usb_pdi_disconnect
    #define usb_udev_ack_setup usb_pdi_ack_setup
    #define usb_udev_ack_control_setup usb_pdi_ack_control_setup
    #define usb_udev_check_events usb_pdi_check_events

    #define usb_udev_stall usb_pdi_stall
    #define usb_udev_read_endpoint usb_pdi_read_endpoint
    #define usb_udev_write_endpoint usb_pdi_write_endpoint

  #endif /*USB_PDI_DIRECT_FNC*/

  #ifdef USB_PDI_EXPORT_FNC

    int usb_pdi_init( usb_device_t *udev);
    int usb_pdi_connect( usb_device_t *udev);
    int usb_pdi_disconnect( usb_device_t *udev);
    void usb_pdi_ack_setup( usb_device_t *udev);
    void usb_pdi_ack_control_setup( usb_device_t *udev);
    int usb_pdi_check_events( usb_device_t *udev);

    void usb_pdi_stall( usb_ep_t *ep);
    int usb_pdi_read_endpoint( usb_ep_t *ep, void *ptr, int size) USB_UDEV_REENTRANT_SIGN;
    int usb_pdi_write_endpoint( usb_ep_t *ep, const void *ptr, int size) USB_UDEV_REENTRANT_SIGN;

  #endif /*USB_PDI_EXPORT_FNC*/

#endif /* _USB_PDI_SUBMODULE_HEADER_FILE_ */

