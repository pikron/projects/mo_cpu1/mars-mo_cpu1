/*********************************************************/
/***   Module : PDIUSB D11,H11,H11A,D12 - header file  ***/
/***   Author : Roman Bartosinski (C) 03.10.2002       ***/
/***   Description : Integrate common functions for    ***/
/***                PDIUSBD11,PDIUSBD12,PDIUSBH11(old) ***/
/***                PDIUSBH11A in Single/Multiple mode ***/
/***                 to one common file.               ***/
/***   Modify : 10.10.2002 - add H11                   ***/
/***            13.10.2002 - add spec.fnc for 'using'  ***/
/***            22.12.2002 - rebuild for smaller out   ***/
/*********************************************************/

#ifndef _PDIUSB_BASE_MODULE
 #define _PDIUSB_BASE_MODULE

/*********************************************************/
// Comments - !!! Please, read this section !!!
//
//
// If you can use included functions for read and write
//  byte from/to PDIUSB, please uncomment defined PDI_CMD_RWD_INTERNAL
//  and check defined addresses for communication with PDIUSB.
// Else you must uncomment defined PDI_CMD_RWD_EXTERNAL and declare
//  and implement next functions :
//     void pdiSendCommand( unsigned char byCmd);
//     unsigned char pdiReadData( unsigned char byCount, void *pbyData);
//     void pdiWriteData( unsigned char byCount, const void *pbyData);
//  - if you want use 'using' (see below), you must implement next function(on i51 is NEEDED).
//     unsigned short pdiIntCmdReadData( unsigned char byCmd, unsigned char byShort) _PDI_USING;
// -- all in section 'External functions'
//
//
// Next you must select type of chip.(uncomment line with defined type)
// -- in section 'Type of PDIUSB'
//
//
// If you want use functions 'pdiGetInterrupt' and 'pdiGetLastTransStatus'
// with keyword 'using' (for i51 it means using spec.reg.bank), uncomment
// define 'USING'. Only these two functions need pdiIntCmdReadData.
// -- in section 'Use using in interrupt functions'
//
// ***   External choice   ***
// Or you can make all previous choises externally before including this file
// by defining one of PDIUSBD11,PDIUSBD12,PDIUSBH11,PDIUSBH11A_SINGLE,PDIUSBH11A_MULTIPLE
//
// defining one of PDI_CMD_RWD_INTERNAL,PDI_CMD_RWD_EXTERNAL
// (you can define externally PDIUSB_COMMAND_ADDR, PDIUSB_READ_DATA_ADDR, PDIUSB_WRITE_DATA_ADDR)
//
// and defining one of PDI_USE_USING,PDI_DONTUSE_USING
// (you can define externally _PDI_USING_NUMBER)



/*********************************************************/
// Type of PDIUSB


//
//   PDIUSBD11           - USB device with serial interface
//   PDIUSBH11           - USB hub with one embedded fnc and serial interface
//   PDIUSBH11A_SINGLE   - USB hub with one embedded fnc, serial interface and additional functions
//   PDIUSBH11A_MULTIPLE - USB hub with multiple(3) embedded fncs, serial interface and add.fncs
//   - H11A_SINGLE and H11A_MULTIPLE is divided by pdiusb command SetMode (bit is set automatically)
//   PDIUSBD12           - USB device with parallel interface
//

// uncomment right chip type below

//#define PDIUSBD11
//#define PDIUSBH11
//#define PDIUSBH11A_SINGLE
//#define PDIUSBH11A_MULTIPLE
#define PDIUSBD12


/*********************************************************/
// Create common type PDIUSBH11A for SINGLE and MULTIPLE
// for mux H11A_S & H11A_M
#if defined(PDIUSBH11A_SINGLE) || defined(PDIUSBH11A_MULTIPLE)
  #define PDIUSBH11A
#else
  #undef  PDIUSBH11A
#endif



/*********************************************************/
//  Use using in interrupt functions

  // If you want use keyword 'using' in function
  // 'pdiGetInterrupt' and 'pdiGetLastTransStatus',
  // uncomment next line.(and check and correct number in _PDI_USING_NUMBER)
//#define PDI_USE_USING
  // If you don't want use keyword 'using',
  // uncomment next line.
#define PDI_DONTUSE_USING

 #if defined(PDI_USE_USING)
  #if !defined(_PDI_USING_NUMBER)
   #define _PDI_USING_NUMBER  2
  #endif
  #define _PDI_USING   using _PDI_USING_NUMBER
 #else
  #define _PDI_USING
 #endif


/*********************************************************/
//  External functions

  // If you want use internal functions, uncomment next line.
//#define PDI_CMD_RWD_INTERNAL
  // If you want use your own external functions, uncomment next line.
//#define PDI_CMD_RWD_EXTERNAL

// this is automatically selected by your choice

 #if defined(PDI_CMD_RWD_INTERNAL)
  #if !defined(PDIUSB_COMMAND_ADDR)       // if not defined address for select command
   #if defined(PDIUSBD12)
    #define PDIUSB_COMMAND_ADDR    0x7001 // sel.cmd address for D12 (this is address for my application)
   #else
    #define PDIUSB_COMMAND_ADDR    0x36   // sel.cmd address for all other
   #endif
  #endif
  #if !defined(PDIUSB_READ_DATA_ADDR)     // if not defined address for read data from chip
   #if defined(PDIUSBD12)
    #define PDIUSB_READ_DATA_ADDR  0x7000 // read data address for D12
   #else
    #define PDIUSB_READ_DATA_ADDR  0x35   // read data address for all other
   #endif
  #endif
  #if !defined(PDIUSB_WRITE_DATA_ADDR)    // if not defined address for write data to chip
   #if defined(PDIUSBD12)
    #define PDIUSB_WRITE_DATA_ADDR 0x7000 // write data address for D12
   #else
    #define PDIUSB_WRITE_DATA_ADDR 0x34   // write data address for all other
   #endif
  #endif

 #endif

/* Build internal version of functions, but do not define addresses */
#define PDI_CMD_RWD_INTERNAL

  extern void pdiSendCommand( unsigned char byCmd);
  extern unsigned char pdiReadData( unsigned char byCount, void *pbyData);
  extern void pdiWriteData( unsigned char byCount, const void *pbyData);
#if defined(PDI_USE_USING)
  extern unsigned short pdiIntCmdReadData( unsigned char byCmd, unsigned char byShort) _PDI_USING;
#endif


/*********************************************************/
// Check chip selection, int/ext rw functions, using

#if !defined(PDIUSBD11) && !defined(PDIUSBH11) && !defined(PDIUSBH11A) && !defined(PDIUSBD12)
  #define _PDI_ERROR_NO_CONTINUE
  #error "!!! Any type of PDIUSB wasn't selected !!!"
  #error "Please select one of PDIUSB in file pdiusb.h in section 'Type of PDIUSB'."
#endif
#if !defined(PDI_CMD_RWD_INTERNAL) && !defined(PDI_CMD_RWD_EXTERNAL)
  #define _PDI_ERROR_NO_CONTINUE
  #error "!!! You must select if you want use internal or external basic fncs for PDIUSB !!!"
  #error "Please select your choice in file pdiusb.h in section 'External functions'."
#endif
#if !defined(PDI_USE_USING) && !defined(PDI_DONTUSE_USING)
  #define _PDI_ERROR_NO_CONTINUE
  #error "!!! You must choose if you want keyword 'using' !!!"
  #error "Please see into file pdiusb.h in section 'Use using in interrupt functions'."
#endif

#if defined(_PDI_ERROR_NO_CONTINUE)
  #error " For help ... read in file pdiusb.h section 'Comments'."
#endif


/*********************************************************/
/*********************************************************/
#if !defined(_PDI_ERROR_NO_CONTINUE)

/*********************************************************/
// Endpoints - size and index defines

  //  PDI_CNT_EP - number of endpoints without control endpoints
#if defined(PDIUSBD11) || defined(PDIUSBH11A_SINGLE)       // D11, H11A_S
  #define PDI_CNT_EP    0x03
#elif defined(PDIUSBH11) || defined(PDIUSBH11A_MULTIPLE)  // H11, H11A_M
  #define PDI_CNT_EP    0x01
#elif defined(PDIUSBD12)                                  // D12
  #define PDI_CNT_EP    0x02
#endif

  //  PDI_EPx_yyyy_SIZE - max packet size for endpoint x
#if defined(PDIUSBH11) || defined(PDIUSBH11A)             // H11, H11A
  #define PDI_HUB_TX_SIZE      8
  #define PDI_HUB_RX_SIZE      8
  #define PDI_HUB_PACKET_SIZE  8
 #if defined(PDIUSBH11)
  #define PDI_HUB_INT_SIZE     1
 #endif
#endif


#if !defined(PDIUSBD12)                     // D11,H11,H11A
  #define PDI_EP0_TX_SIZE      8
  #define PDI_EP0_RX_SIZE      8
  #define PDI_EP0_PACKET_SIZE  8
  #define PDI_EP1_TX_SIZE      8
  #define PDI_EP1_RX_SIZE      8
  #define PDI_EP1_PACKET_SIZE  8
 #if defined(PDIUSBD11) || defined(PDIUSBH11A_SINGLE)
  #define PDI_EP2_TX_SIZE      8
  #define PDI_EP2_RX_SIZE      8
  #define PDI_EP2_PACKET_SIZE  8
  #define PDI_EP3_TX_SIZE      8
  #define PDI_EP3_RX_SIZE      8
  #define PDI_EP3_PACKET_SIZE  8
 #endif
#else                                       // D12
  #define PDI_EP0_TX_SIZE      16
  #define PDI_EP0_RX_SIZE      16
  #define PDI_EP0_PACKET_SIZE  16
  #define PDI_EP1_TX_SIZE      16
  #define PDI_EP1_RX_SIZE      16
  #define PDI_EP1_PACKET_SIZE  16
  #define PDI_EP2_TX_SIZE      64
  #define PDI_EP2_RX_SIZE      64
  #define PDI_EP2_PACKET_SIZE  64
#endif

  //  PDI_EPx_TX, PDI_EPx_RX - index for endpoint x (write/read)
#if defined(PDIUSBH11) || defined(PDIUSBH11A)                                        // H11
  #define PDI_HUB_EP0_RX  0
  #define PDI_HUB_EP0_TX  1
  // another index names
  #define PDI_HUB_EP0_OUT  PDI_HUB_EP0_RX
  #define PDI_HUB_EP0_IN   PDI_HUB_EP0_TX
#endif

#if defined(PDIUSBH11)
  #define PDI_EP0_RX  2
  #define PDI_EP0_TX  3
  #define PDI_EP1_TX  4
  // another index names
  #define PDI_EP0_OUT  PDI_EP0_RX
  #define PDI_EP0_IN   PDI_EP0_TX
  #define PDI_EP1_IN   PDI_EP1_TX
#elif defined(PDIUSBD11) || defined(PDIUSBH11A_SINGLE)          // D11,H11A_S
  #define PDI_EP0_RX  2
  #define PDI_EP0_TX  3
  #define PDI_EP1_RX  5
  #define PDI_EP1_TX  4
  #define PDI_EP2_RX  6
  #define PDI_EP2_TX  7
  #define PDI_EP3_RX  8
  #define PDI_EP3_TX  9
  // another index names
  #define PDI_EP0_OUT  PDI_EP0_RX
  #define PDI_EP0_IN   PDI_EP0_TX
  #define PDI_EP1_OUT  PDI_EP1_RX
  #define PDI_EP1_IN   PDI_EP1_TX
  #define PDI_EP2_OUT  PDI_EP2_RX
  #define PDI_EP2_IN   PDI_EP2_TX
  #define PDI_EP3_OUT  PDI_EP3_RX
  #define PDI_EP3_IN   PDI_EP3_TX
#elif defined(PDIUSBH11A_MULTIPLE)                            // H11_M
  #define PDI_F1_EP0_RX   2
  #define PDI_F1_EP0_TX   3
  #define PDI_F1_EP1_RX   5
  #define PDI_F1_EP1_TX   4
  #define PDI_F6_EP0_RX  10
  #define PDI_F6_EP0_TX  11
  #define PDI_F6_EP1_RX   6
  #define PDI_F6_EP1_TX   7
  #define PDI_F7_EP0_RX  12
  #define PDI_F7_EP0_TX  13
  #define PDI_F7_EP1_RX   8
  #define PDI_F7_EP1_TX   9
  // another index names
  #define PDI_F1_EP0_OUT  PDI_F1_EP0_RX
  #define PDI_F1_EP0_IN   PDI_F1_EP0_TX
  #define PDI_F1_EP1_OUT  PDI_F1_EP1_RX
  #define PDI_F1_EP1_IN   PDI_F1_EP1_TX
  #define PDI_F6_EP0_OUT  PDI_F6_EP0_RX
  #define PDI_F6_EP0_IN   PDI_F6_EP0_TX
  #define PDI_F6_EP1_OUT  PDI_F6_EP1_RX
  #define PDI_F6_EP1_IN   PDI_F6_EP1_TX
  #define PDI_F7_EP0_OUT  PDI_F7_EP0_RX
  #define PDI_F7_EP0_IN   PDI_F7_EP0_TX
  #define PDI_F7_EP1_OUT  PDI_F7_EP1_RX
  #define PDI_F7_EP1_IN   PDI_F7_EP1_TX
#elif defined(PDIUSBD12)                                     // D12
  #define PDI_EP0_RX  0
  #define PDI_EP0_TX  1
  #define PDI_EP1_RX  2
  #define PDI_EP1_TX  3
  #define PDI_EP2_RX  4
  #define PDI_EP2_TX  5
  // another index names
  #define PDI_EP0_OUT  PDI_EP0_RX
  #define PDI_EP0_IN   PDI_EP0_TX
  #define PDI_EP1_OUT  PDI_EP1_RX
  #define PDI_EP1_IN   PDI_EP1_TX
  #define PDI_EP2_OUT  PDI_EP2_RX
  #define PDI_EP2_IN   PDI_EP2_TX
#endif

/*********************************************************/
//  Commands - index and bits, description

  // Set Address/Enable
#if defined(PDIUSBH11) || defined(PDIUSBH11A) || defined(PDIUSBD11)         // only H11 or H11A
  #define PDI_CMD_HUB_ENB_ADDR  0x00D0
 #if defined(PDIUSBH11) || defined(PDIUSBH11A_SINGLE)  // for H11 and H11A_SINGLE
  #define PDI_CMD_FNC_ENB_ADDR  0x00D1
 #else                                                 // for H11A_MULTIPLE
  #define PDI_CMD_FNC1_ENB_ADDR 0x00D1
  #define PDI_CMD_FNC6_ENB_ADDR 0x00D2
  #define PDI_CMD_FNC7_ENB_ADDR 0x00D3
// for compatible with H11A_SINGLE and other
  #define PDI_CMD_FNC_ENB_ADDR  PDI_CMD_FNC1_ENB_ADDR
 #endif
#elif defined(PDIUSBD11)             // for D11
  #define PDI_CMD_FNC_ENB_ADDR  0x00D1
#elif defined(PDIUSBD12)             // for D12
  #define PDI_CMD_FNC_ENB_ADDR  0x00D0
#endif
    // set address/enable bits - for all
    #define PDI_ENAD_ENABLE   0x0080
    #define PDI_ENAD_ADDRMASK 0x007F

  // Set Endpoint enable
  #define PDI_CMD_EPEN          0x00D8
    // set endpoint enable bits
   #if defined(PDIUSBD12)             // D12 - generic ep
    #define PDI_EPEN_ENABLE       0x0001
   #elif defined(PDIUSBD11)           // D11 - generic ep
    #define PDI_EPEN_ENABLE       0x0002
   #elif defined(PDIUSBH11A) || defined(PDIUSBH11)   // H11,H11A
    #define PDI_EPEN_HUB_ENB      0x0001
     #if defined(PDIUSBH11A_MULTIPLE)  // H11A_M
    #define PDI_EPEN_FNC1_ENB       0x0002
    #define PDI_EPEN_FNC6_ENB       0x0004
    #define PDI_EPEN_FNC7_ENB       0x0008
     #else                            // H11,H11A_S
    #define PDI_EPEN_FNC_ENB      0x0002
     #endif
   #endif

 #if !defined(PDIUSBH11)
  // Set Mode
  #define PDI_CMD_SET_MODE      0x00F3
    // set mode bits - configuration
   #if !defined(PDIUSBD12)
    #define PDI_MODE_REMOTE_WAKEUP   0x0001  // D11,H11A
   #endif
    #define PDI_MODE_NO_LAZY_CLOCK   0x0002  // all
    #define PDI_MODE_CLOCK_RUNNING   0x0004  // all
   #if defined(PDIUSBD12)
    #define PDI_MODE_INTERRUPT_MODE  0x0008  // D12
   #else
    #define PDI_MODE_DEBUG_MODE      0x0008  // D11,H11A
   #endif
    #define PDI_MODE_SOFT_CONNECT    0x0010  // all
   #if defined(PDIUSBH11A)
    #define PDI_MODE_DWN_RESISTORS   0x0020  // H11A
    #define PDI_MODE_NONBLINK_LED    0x0040  // H11A
    #define PDI_MODE_SINGLE_FNC      0x0080  // H11A
   #elif defined(PDIUSBD11)
    #define PDI_MODE_MUSTBEONE       0x0080  // D11
   #else
    #define PDI_MODE_EP_NONISO       0x0000  // D12
    #define PDI_MODE_EP_ISOOUT       0x0040
    #define PDI_MODE_EP_ISOIN        0x0080
    #define PDI_MODE_EP_ISOIO        0x00C0

   #endif
   // set mode bits - clock div factor  [48Mhz/(N+1)]
   #if defined(PDIUSBH11A)
    #define PDI_CLOCK_CRYSTAL_12M    0x3000  // for 12Mhz crystal
    #define PDI_CLOCK_CRYSTAL_48M    0x0000  // for 48Mhz crystal
   #endif
   #if defined(PDIUSBD12)                  // D12
    #define PDI_CLOCK_48M            0x0000    // 48 Mhz
   #endif                                  // all
    #define PDI_CLOCK_24M            0x0100    // 24 Mhz
    #define PDI_CLOCK_16M            0x0200    // 16 Mhz
    #define PDI_CLOCK_12M            0x0300    // 12 Mhz
    #define PDI_CLOCK_9M6            0x0400    //  9.6 Mhz
    #define PDI_CLOCK_8M             0x0500    //  8 Mhz
    #define PDI_CLOCK_6M8            0x0600    //  6.857142857 Mhz
    #define PDI_CLOCK_6M             0x0700    //  6 Mhz
    #define PDI_CLOCK_5M3            0x0800    //  5.333333333 Mhz
    #define PDI_CLOCK_4M8            0x0900    //  4.8 Mhz
    #define PDI_CLOCK_4M3            0x0A00    //  4.363636364 Mhz
    #define PDI_CLOCK_4M             0x0B00    //  4 Mhz
   #if defined(PDIUSBD12)                  // D12
    #define PDI_CLOCK_SET_TO_ONE     0x4000
    #define PDI_CLOCK_SOF_ONLY       0x8000
   #endif
 #endif

  // Set DMA
 #if defined(PDIUSBD12)
  #define PDI_CMD_SET_DMA       0x00FB
  #define PDI_CMD_GET_DMA       0x00FB
    // set dma bits
    #define PDI_DMA_SINGLE_DMA    0x0000
    #define PDI_DMA_BURST_4       0x0001
    #define PDI_DMA_BURST_8       0x0002
    #define PDI_DMA_BURST_16      0x0003
    #define PDI_DMA_ENABLE        0x0004
    #define PDI_DMA_DIRECTION     0x0008
    #define PDI_DMA_AUTOLOAD      0x0010
    #define PDI_DMA_INT_SOF       0x0020
    #define PDI_DMA_EP4_INT       0x0040
    #define PDI_DMA_EP5_INT       0x0080
 #endif

  // Read Interrupt Register
  // be careful with PDIUSBH11 - you must read only 1 byte
  #define PDI_CMD_GET_INT_REG   0x00F4
    // read interrupt register bits
   #if defined(PDIUSBD11) || defined(PDIUSBH11) || defined(PDIUSBH11A) // D11,H11,H11A
     #if defined(PDIUSBH11) || defined(PDIUSBH11A)
    #define PDI_INT_HUB_OUT       0x0001
    #define PDI_INT_HUB_IN        0x0002
     #endif
     #if defined(PDIUSBH11)
    #define PDI_INT_EP0_OUT       0x0004
    #define PDI_INT_EP0_IN        0x0008
    #define PDI_INT_EP1_IN        0x0010
     #elif defined(PDIUSBD11) || defined(PDIUSBH11A_SINGLE)  // D11,H11_S
    #define PDI_INT_EP0_OUT       0x0004
    #define PDI_INT_EP0_IN        0x0008
    #define PDI_INT_EP1_OUT       0x0020
    #define PDI_INT_EP1_IN        0x0010
    #define PDI_INT_EP2_OUT       0x0040
    #define PDI_INT_EP2_IN        0x0080
    #define PDI_INT_EP3_OUT       0x0100
    #define PDI_INT_EP3_IN        0x0200
     #else                                                // H11_M
    #define PDI_INT_F1_EP0_OUT    0x0004
    #define PDI_INT_F1_EP0_IN     0x0008
    #define PDI_INT_F1_EP1_OUT    0x0020
    #define PDI_INT_F1_EP1_IN     0x0010
    #define PDI_INT_F6_EP0_OUT    0x0400
    #define PDI_INT_F6_EP0_IN     0x0800

    #define PDI_INT_F6_EP1_OUT    0x0040
    #define PDI_INT_F6_EP1_IN     0x0080
    #define PDI_INT_F7_EP0_OUT    0x1000
    #define PDI_INT_F7_EP0_IN     0x2000
    #define PDI_INT_F7_EP1_OUT    0x0100
    #define PDI_INT_F7_EP1_IN     0x0200
     #endif
    #define PDI_INT_BUSRESET      0x4000
   #elif defined(PDIUSBD12)
    #define PDI_INT_EP0_OUT       0x0001
    #define PDI_INT_EP0_IN        0x0002
    #define PDI_INT_EP1_OUT       0x0004
    #define PDI_INT_EP1_IN        0x0008
    #define PDI_INT_EP2_OUT       0x0010
    #define PDI_INT_EP2_IN        0x0020
    #define PDI_INT_BUSRESET      0x0040
    #define PDI_INT_SUSPEND       0x0080
    #define PDI_INT_DMA_EOT       0x0100
   #endif

  // Select Endpoint
  #define PDI_CMD_SELECT_EP     0x0000  // base index for select endpoint
    // select endpoint bits
    #define PDI_SELEP_FULL        0x0001
   #if defined(PDIUSBD12)
    #define PDI_SELEP_STALL       0x0002
   #endif

  // Read Last Transaction Status Register
  #define PDI_CMD_GET_LAST_STAT 0x0040  // base index for read last transaction
    // read last transaction bits and errors
    #define PDI_LTSTAT_RXTX_OK    0x0001
    #define PDI_LTSTAT_ERR_MASK   0x001E
    #define PDI_LTSTAT_SETUP      0x0020
    #define PDI_LTSTAT_DATA1      0x0040
    #define PDI_LTSTAT_PREV_NRD   0x0080
      // error codes
      #define PDI_ERR_NO_ERROR      0x0000
      #define PDI_ERR_PID_ENCODING  0x0001
      #define PDI_ERR_PID_UNKNOWN   0x0002
      #define PDI_ERR_UNEXPECT_PKT  0x0003

      #define PDI_ERR_TOKEN_CRC     0x0004
      #define PDI_ERR_DATA_CRC      0x0005
      #define PDI_ERR_TIME_OUT      0x0006
      #define PDI_ERR_NEVER_HAPPENS 0x0007
      #define PDI_ERR_UNEXPECT_EOP  0x0008
      #define PDI_ERR_NAK           0x0009
      #define PDI_ERR_STALL         0x000A
      #define PDI_ERR_OVERFLOW      0x000B
      #define PDI_ERR_BITSTUFF      0x000D
      #define PDI_ERR_DATA_PID      0x000F

  // Read Endpoint Status
 #if defined(PDIUSBD11) || defined(PDIUSBH11) || defined(PDIUSBH11A)
   #define PDI_CMD_GET_EP_STAT   0x0080  // base index for read ep status
    // read ep status bits
     #define PDI_EPSTAT_SETUP      0x0004
     #define PDI_EPSTAT_STALL      0x0008
     #define PDI_EPSTAT_DATA1      0x0010
     #define PDI_EPSTAT_FULL       0x0020
 #endif

  // Read Buffer
  #define PDI_CMD_READ_BUFFER   0x00F0

  // Write Buffer
  #define PDI_CMD_WRITE_BUFFER  0x00F0

  // Clear Buffer
  #define PDI_CMD_CLEAR_BUFFER  0x00F2

  // Validate Buffer
  #define PDI_CMD_VALID_BUFFER  0x00FA


  // Set Endpoint Status
  #define PDI_CMD_SET_EP_STAT   0x0040  // base index for endpoint status
    // set endpoint status bits
    #define PDI_SET_EP_STALLED    0x0001

  // Acknowledge Setup
  #define PDI_CMD_ACK_SETUP     0x00F1

  // Send Resume
  #define PDI_CMD_SEND_RESUME   0x00F6

  // Read Current Frame Number
  #define PDI_CMD_GET_FRAME     0x00F5

  // Get Chip ID
  #define PDI_CMD_GET_CHIP_ID   0x00FD

// HUB commands
#if defined(PDIUSBH11) || defined(PDIUSBH11A)
  // Clear Port Feature
  #define PDI_CMD_P2_CLR_FEATURE 0x00E0
  #define PDI_CMD_P3_CLR_FEATURE 0x00E1
  #define PDI_CMD_P4_CLR_FEATURE 0x00E2
  #define PDI_CMD_P5_CLR_FEATURE 0x00E3
  // Set Port Feature
  #define PDI_CMD_P2_SET_FEATURE 0x00E8
  #define PDI_CMD_P3_SET_FEATURE 0x00E9
  #define PDI_CMD_P4_SET_FEATURE 0x00EA
  #define PDI_CMD_P5_SET_FEATURE 0x00EB
    // Feature code
    #define PDI_F_PORT_ENABLE       0
    #define PDI_F_PORT_SUSPEND      1
    #define PDI_FC_PORT_RESET       2
    #define PDI_F_PORT_POWER        3
    #define PDI_C_PORT_CONNECTION   4
    #define PDI_C_PORT_ENABLE       5
    #define PDI_C_PORT_SUSPEND      6
    #define PDI_C_PORT_OVERCURRENT  7

  // Get Port Status
  #define PDI_CMD_P2_GET_STATUS  0x00E0
  #define PDI_CMD_P3_GET_STATUS  0x00E1
  #define PDI_CMD_P4_GET_STATUS  0x00E2
  #define PDI_CMD_P5_GET_STATUS  0x00E3
    // get port status bits - port status byte
    #define PDI_PSTAT_CONNECT        0x0001
    #define PDI_PSTAT_ENABLED        0x0002
    #define PDI_PSTAT_SUSPEND        0x0004
    #define PDI_PSTAT_OVERCUR        0x0008
    #define PDI_PSTAT_RESET          0x0010
    #define PDI_PSTAT_POWER          0x0020
    #define PDI_PSTAT_LOWSPEED       0x0040
    // get port status bits - port status change byte
    #define PDI_PSTAT_CHANGE_CONNECT 0x0100
    #define PDI_PSTAT_CHANGE_ENABLED 0x0200
    #define PDI_PSTAT_CHANGE_SUSPEND 0x0400
    #define PDI_PSTAT_CHANGE_OVERCUR 0x0800
    #define PDI_PSTAT_CHANGE_RESET   0x1000
  // Set Status Change Bits
  #define PDI_CMD_SET_CHNG_BITS  0x00F7
    // set status change bits - bits
    #define PDI_SCHB_LOCAL_POWER    0x0001
    #define PDI_SCHB_FNC1           0x0002
   #if !defined(PDIUSBH11) && !defined(PDIUSBH11A_SINGLE)
    #define PDI_SCHB_FNC6           0x0004
    #define PDI_SCHB_FNC7           0x0008
   #endif
#endif


/*********************************************************/
//  Function prototypes
//
// PDIUSB common commands

 #if defined(PDIUSBH11) || defined(PDIUSBH11A) || defined(PDIUSBD11)
  void pdiSetHUBAddressEnable( unsigned char byAddress, unsigned char byEnable);
 #endif

 #if !defined(PDIUSBH11A_MULTIPLE) // D11,D12,H11,H11A_S(emb.fnc)
  void pdiSetAddressEnable( unsigned char byAddr_Enb);
//  void pdiSetAddressEnable( unsigned char byAddress, unsigned char byEnable);
 #else
  void pdiSetEmbFncAddressEnable( unsigned char byFnc, unsigned char byAddress, unsigned char byEnable);
 #endif

  void pdiSetEndpointEnable( unsigned char byEnable);

 #if !defined(PDIUSBH11)  // H11 has not it ( !!! is great - try give single quote mark into comments ;-)
  void pdiSetMode( unsigned short wMode_Clock);
 #endif

 #if defined(PDIUSBD12)
  void pdiSetDMA( unsigned char byDma);
  unsigned char pdiGetDMA( void);
 #endif

 #if defined(PDIUSBH11)
  unsigned char pdiGetInterrupt( void) _PDI_USING;
 #else
  unsigned short pdiGetInterrupt( void) _PDI_USING;
 #endif

  unsigned char pdiSelectEp( unsigned char byEpIdx);
  unsigned char pdiGetLastTransStatus( unsigned char byEpIdx) _PDI_USING;

 #if defined(PDIUSBD11) || defined(PDIUSBH11) || defined(PDIUSBH11A)
  unsigned char pdiGetEpStatus( unsigned char byEpIdx);
 #endif

  unsigned char pdiReadFromEpBuffer( unsigned char byLength, unsigned char *pToBuff);
  void pdiWriteToEpBuffer( unsigned char byLength, const unsigned char *pFromBuff);
  void pdiSetEpStatus( unsigned char byEpIdx, unsigned char byStatus);
  void pdiAcknowledgeSetup( void);
  void pdiClearBuffer( void);
  void pdiValidateBuffer( void);

  void pdiSendResume( void);
  unsigned short pdiGetFrameNumber( void);
  unsigned short pdiGetChipID( void); // this function is undocumented

 // HUB command
 #if defined(PDIUSBH11) || defined(PDIUSBH11A)
  void pdiClearPortFeature( unsigned char byEpIdx, unsigned char byFeature);
  void pdiSetPortFeature( unsigned char byEpIdx, unsigned char byFeature);
  unsigned short pdiGetPortFeature( unsigned char byEpIdx);
  void pdiSetStatusChangeBits( unsigned char byBits);
 #endif

// PDIUSB other commands
 void pdiWriteEndpoint( unsigned char byEpIdx, unsigned char byLength, const unsigned char *pbyData);
 unsigned char pdiReadEndpoint( unsigned char byEpIdx, unsigned char byMaxLength, unsigned char *pbyData);

 unsigned char pdiCmdData( unsigned char byCmd, unsigned char *pbyData,
                           unsigned char byCount, unsigned char byRead);
 void pdiInit( void);
#if !defined(PDIUSBH11A_MULTIPLE)
 void pdiAckSetupControl( void);
#else
 void pdiAckSetupFncControl( unsigned char Fnc);
#endif

unsigned char pdiEp2Idx(unsigned char ep);

/*********************************************************/
#endif // from _PDI_ERROR_NO_CONTINUE
/*********************************************************/
#endif // from _PDI_BASE_MODULE
