/*****************************************************/
/***   Module : USB PDI                            ***/
/***   Author : Roman Bartosinski (C) 28.04.2002   ***/
/***   Modify : 08.08.2002, 16.04.2003             ***/
/*****************************************************/

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include <system_def.h>
#include <i2c_ifc.h>

#include <usb/usb.h>
#include <usb/usb_spec.h>
#include <usb/pdiusb.h>
#include <usb/pdi.h>
#include <usb/usbdebug.h>

#include <rtems.h>
#include <rtems/io.h>
#include <rtems/libio.h>
#include <rtems/error.h>

#undef PDI_I2C_DEBUG

i2c_ifc_t *usb_pdi_i2c_ifc;
int usb_pdi_i2c_error_cnt;
int usb_pdi_connected;
usb_device_t usb_pdi_device;
static rtems_id usb_pdi_task=(rtems_id)0;

void pdiSendCommand( unsigned char byCmd) {
  if(i2c_master_transfer(usb_pdi_i2c_ifc, D11_REG_CMD,
                         1, 0, &byCmd, NULL, NULL, NULL)<0) {
    usb_pdi_i2c_error_cnt++;
   #ifdef PDI_I2C_DEBUG
    usb_debug_print( DEBUG_LEVEL_HIGH, ("uECM\n"));
   #endif
    return;
  }
 #ifdef PDI_I2C_DEBUG
  usb_debug_print( DEBUG_LEVEL_HIGH, ("uCM %02x\n",byCmd));
 #endif
}

unsigned char pdiReadData( unsigned char byCount, void *pbyData) {
  int ret;

  if(i2c_master_transfer(usb_pdi_i2c_ifc, D11_REG_DATA_READ,
                         0, byCount, NULL, pbyData, NULL, &ret)<0) {
    usb_pdi_i2c_error_cnt++;
   #ifdef PDI_I2C_DEBUG
    usb_debug_print( DEBUG_LEVEL_HIGH, ("uERD\n"));
   #endif
    return -1;
  }
 #ifdef PDI_I2C_DEBUG
  usb_debug_print( DEBUG_LEVEL_HIGH, ("uRD "));
  int i;
  for(i=0;i<byCount;i++) usb_debug_print( DEBUG_LEVEL_HIGH,("%s%02X",i?",":"",((unsigned char*)pbyData)[i]));
  usb_debug_print( DEBUG_LEVEL_HIGH, ("\n"));
 #endif

  return ret;
}

void pdiWriteData( unsigned char byCount, const void *pbyData) {
  if(i2c_master_transfer(usb_pdi_i2c_ifc, D11_REG_DATA_WRITE,
                         byCount, 0, &pbyData, NULL, NULL, NULL)<0){
    usb_pdi_i2c_error_cnt++;
   #ifdef PDI_I2C_DEBUG
    usb_debug_print( DEBUG_LEVEL_HIGH, ("uEWR\n"));
   #endif
    return -1;
  }

 #ifdef PDI_I2C_DEBUG
  usb_debug_print( DEBUG_LEVEL_HIGH, ("uWR "));
  int i;
  for(i=0;i<byCount;i++) usb_debug_print( DEBUG_LEVEL_HIGH,("%s%02X",i?",":"",((unsigned char*)pbyData)[i]));
  usb_debug_print( DEBUG_LEVEL_HIGH, ("\n"));
 #endif
}

static rtems_task
usb_pdi_task_proc(rtems_task_argument argument)
{
  rtems_status_code status;
  int res;
  usb_device_t *udev=&usb_pdi_device;

  udev->id = 1;
  udev->cntep = 0;

 #ifndef USB_PDI_DIRECT_FNC
  udev->init = usb_pdi_init;
 #endif /*USB_PDI_DIRECT_FNC*/
  usb_debug_set_level(DEBUG_LEVEL_HIGH);

  do{

    while(usb_pdi_connected) {

      //cmd_usb_processor_run();

      if(1 /*check for IRQ there*/) res = usb_check_events(udev);

      res |= usb_control_response(udev);

      if(!res) {
        status = rtems_task_wake_after(10);
      }

      if(usb_pdi_i2c_error_cnt) {
        usb_debug_print( DEBUG_LEVEL_MEDIUM, ("usb_pdi: I2C errors encountered, disconnecting\n"));
        break;
      }
    }

    if(usb_pdi_connected)
      usb_disconnect(udev);

    usb_pdi_connected=0;
    usb_pdi_i2c_error_cnt=0;

    /* wait before new attempt to connect to USB bus */
    status = rtems_task_wake_after(1000);

    /* attempt to initialize PDI chip */
    pdiSendCommand(0);

    udev->vendor_fnc=NULL;

    if(!usb_pdi_i2c_error_cnt) {

      rtems_task_wake_after(100);

      /* attempt to initialize PDI chip */
      usb_pdi_init(udev);

      rtems_task_wake_after(20);

      /*connect to USB bus*/
      res = usb_connect(udev);
      if(res>=0) {
        if(!usb_pdi_i2c_error_cnt) {
          /*int chip_id;*/
	  usb_debug_print( DEBUG_LEVEL_LOW, ("usb_pdi: connect\n"));
	  usb_pdi_connected=1;
	  /*chip_id=pdiGetChipID();*/
	  /*usb_debug_print( DEBUG_LEVEL_MEDIUM, ("PDIUSB chip id is %04x\n",chip_id));*/
	}
      }
      if(!usb_pdi_connected) {
        usb_disconnect(udev);
        usb_debug_print( DEBUG_LEVEL_LOW, ("usb_pdi: connect failed => disconnect\n"));
      }
    }

  }while(1);
}

int usb_pdi4rtems_init(void)
{
  rtems_status_code status;

  if(!usb_pdi_i2c_ifc)
    usb_pdi_i2c_ifc = i2c_find_ifc(NULL, 0);

  if(!usb_pdi_i2c_ifc)
    return -1;

  status=rtems_task_create (rtems_build_name('U','S','B','P'),
          /*priority*/ USB_PDI_KWT_PRIORITY,
          /*stacksize*/ RTEMS_MINIMUM_STACK_SIZE+1024,
          RTEMS_DEFAULT_MODES /*RTEMS_PREEMPT|RTEMS_NO_TIMESLICE|RTEMS_NO_ASR|RTEMS_INTERRUPT_LEVEL(0)*/,
          RTEMS_DEFAULT_ATTRIBUTES /*RTEMS_NO_FLOATING_POINT|RTEMS_LOCAL*/,
          &usb_pdi_task);
  if (status != RTEMS_SUCCESSFUL){
    printf("usb_pdi4rtems_init: rtems_task_create %s\n",rtems_status_text(status));
    return status;
  }

  status = rtems_task_start (usb_pdi_task, usb_pdi_task_proc, (rtems_task_argument)0);
  if (status != RTEMS_SUCCESSFUL){
    printf("usb_pdi4rtems_init: rtems_task_start %s\n",rtems_status_text(status));
    return status;
  }

  return 0;
}

