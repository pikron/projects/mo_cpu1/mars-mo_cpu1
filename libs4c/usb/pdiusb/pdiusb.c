/*********************************************************/
/***   Module : PDIUSB D11,H11,H11A,D12 - implement.   ***/
/***   Author : Roman Bartosinski (C) 03.10.2002       ***/
/***   Description : Integrate common functions for    ***/
/***                PDIUSBD11,PDIUSBD12,PDIUSBH11(old) ***/
/***                PDIUSBH11A in Single/Multiple mode ***/
/***                 to one common file.               ***/
/***   Modify : 10.10.2002 - add H11                   ***/
/***            13.10.2002 - add spec.fnc for 'using'  ***/
/*********************************************************/

#include <system_def.h>
#include <endian.h>
#if __BYTE_ORDER == __BIG_ENDIAN
  #include <byteswap.h>
#endif
#include <usb/pdiusb.h>
#include <usb/usb_spec.h>

  #ifdef PDI_CMD_RWD_INTERNAL
   #ifndef PDIUSBD12
     #include <periph/i2c.h>
   #endif
  #endif

  #ifndef SDCC
   #define xdata
  #endif


/*********************************************************/
// Function for read and write from/into chip

 #if defined(PDI_CMD_RWD_INTERNAL)
  #if defined(PDIUSBD12) // parallel interface

   void pdiSendCommand( unsigned char byCmd) {
     *((volatile xdata unsigned char *) PDIUSB_COMMAND_ADDR) = byCmd;
   }
   unsigned char pdiReadData( unsigned char byCount, void *pbyData) {
     unsigned char out = byCount;
     while (byCount) {
       byCount--;
       *(unsigned char*)pbyData++ = *((volatile xdata unsigned char *) PDIUSB_READ_DATA_ADDR);
     }
     return out;
   }
   void pdiWriteData( unsigned char byCount, const void *pbyData) {
     while (byCount) {
       byCount--;
       *((volatile xdata unsigned char *) PDIUSB_WRITE_DATA_ADDR) = *(unsigned char*)pbyData++;
     }
   }

   #if defined(PDI_USE_USING)
     unsigned short pdiIntCmdReadData( unsigned char byCmd, unsigned char byShort) _PDI_USING {
       unsigned char i[2];
       *((volatile xdata unsigned char *) PDIUSB_COMMAND_ADDR) = byCmd;
       i[0] = *((volatile xdata unsigned char *) PDIUSB_READ_DATA_ADDR);
       if ( !byShort) {
        i[1] = 0;
        return i[0];
       }
       i[1] = *((volatile xdata unsigned char *) PDIUSB_READ_DATA_ADDR);
       return (((unsigned short) i[1]) << 8) + i[0];
     }
   #endif

  #else // serial interface iic
   #ifndef D11_REG_CMD
     #define D11_REG_CMD  PDIUSB_COMMAND_ADDR
   #endif
   #ifndef D11_REG_DATA_WRITE
     #define D11_REG_DATA_WRITE PDIUSB_WRITE_DATA_ADDR
   #endif
   #ifndef D11_REG_DATA_READ
     #define D11_REG_DATA_READ PDIUSB_READ_DATA_ADDR
   #endif

   void pdiSendCommand( unsigned char byCmd) {
     I2C_Write( D11_REG_CMD, &byCmd, 1);
   }
   unsigned char pdiReadData( unsigned char byCount, void *pbyData) {
     I2C_Read( D11_REG_DATA_READ, pbyData, byCount);
     return byCount;
   }
   void pdiWriteData( unsigned char byCount, const void *pbyData) {
     I2C_Write( D11_REG_DATA_WRITE, pbyData, byCount);
   }
   #if defined(PDI_USE_USING)
    unsigned short pdiIntCmdReadData( unsigned char byCmd, unsigned char byShort) _PDI_USING {
    }
   #endif

  #endif
 #endif


/*********************************************************/
/*********************************************************/
// PDIUSB common commands

#if defined(PDIUSBH11) || defined(PDIUSBH11A) || defined(PDIUSBD11)

 /*********************************************************/
 // pdiSetHUBAddressEnable
 // enable HUB function and set address (byAddress is 0-0x7F, byEnable is 0 or 1)
   void pdiSetHUBAddressEnable( unsigned char byAddress, unsigned char byEnable) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("H "));
     byAddress = (byAddress & 0x7F) | (byEnable << 7);
     pdiSendCommand( PDI_CMD_HUB_ENB_ADDR);
     pdiWriteData( 1, &byAddress);
   }

#endif
#if !defined(PDIUSBH11A_MULTPLE) // D11,D12,H11,H11A_S(emb.fnc)

 /*********************************************************/
 // pdiSetAddressEnable
 // Enable function and set address (byAddress is 0-0x7F, byEnable is 0 or 1)
   void pdiSetAddressEnable( unsigned char byAddr_Enb) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("A "));
     pdiSendCommand( PDI_CMD_FNC_ENB_ADDR);
     pdiWriteData( 1, &byAddr_Enb);
   }

#else

 /*********************************************************/
 // pdiSetEmbFncAddressEnable
 // Enable Embedded function and set address (byAddress is 0-0x7F, byEnable is 0 or 1)
 // byFnc - function index (zero based) 0-3
   void pdiSetEmbFncAddressEnable( unsigned char byFnc, unsigned char byAddress, unsigned char byEnable) {
     byAddress = (byAddress & 0x7F) | (byEnable << 7);
     pdiSendCommand( PDI_CMD_FNC1_ENB_ADDR + byFnc);
     pdiWriteData( 1, &byAddress);
   }

#endif



 /*********************************************************/
 // pdiSetEndpointEnable
 // enable/disable endpoints (PDI_EPEN_xxx)
   void pdiSetEndpointEnable( unsigned char byEnable) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("E "));
     pdiSendCommand( PDI_CMD_EPEN);
     pdiWriteData( 1, &byEnable);
   }


#if !defined(PDIUSBH11)  // H11 has not it

 /*********************************************************/
 // pdiSetMode
 // set chip mode (PDI_MODE_xxx) and clock division factor (PDI_CLOCK_xxx)
  void pdiSetMode( unsigned short wMode_Clock) {
     unsigned char sm[2];
// usb_debug_print( DEBUG_LEVEL_HIGH, ("M%04X ",wMode_Clock));
     sm[0] = (unsigned char) (wMode_Clock & 0xff);
     sm[1] = (unsigned char) (wMode_Clock >> 8);
     pdiSendCommand( PDI_CMD_SET_MODE);
     pdiWriteData( 2, sm);
   }

#endif

 #if defined(PDIUSBD12)

 /*********************************************************/
 // pdiSetDMA
 // set DMA mode (PDI_DMA_xxx)
  void pdiSetDMA( unsigned char byDma) {
    pdiSendCommand( PDI_CMD_SET_DMA);
    pdiWriteData( 1, &byDma);
  }

 /*********************************************************/
 // pdiGetDMA
 // get current DMA mode
  unsigned char pdiGetDMA( void) {
    unsigned char dma;
    pdiSendCommand( PDI_CMD_GET_DMA);
    pdiReadData( 1, &dma);
    return dma;
  }

 #endif


 #if defined(PDIUSBH11)

 /*********************************************************/

 // pdiGetInterrupt - H11 return only one byte
 // get interrupt register (PDI_INT_xxx)
  unsigned char pdiGetInterrupt( void) _PDI_USING {
    unsigned char gin;
#if defined(PDI_USE_USING)
    return pdiIntCmdReadData( PDI_CMD_GET_INT_REG, 0);
#else
    pdiSendCommand( PDI_CMD_GET_INT_REG);
    pdiReadData( 1, &gin);
    return gin;
#endif
  }

 #else

 /*********************************************************/
 // pdiGetInterrupt
 // get interrupt register (PDI_INT_xxx)
  unsigned short pdiGetInterrupt( void) _PDI_USING {
    unsigned short gin;
#if defined(PDI_USE_USING)
    return pdiIntCmdReadData( PDI_CMD_GET_INT_REG, 1);
#else
    pdiSendCommand( PDI_CMD_GET_INT_REG);
    pdiReadData( 2, &gin);
   #if __BYTE_ORDER == __BIG_ENDIAN
    gin=bswap_16(gin);
   #endif
    return gin; //pdiData[0] + (((unsigned short)pdiData[1])<<8);
#endif
  }

 #endif

 /*********************************************************/
 // pdiSelectEp
 // set internal buffer pointer to selected endpoint (zero based) (PDI_SELEP_xxx)
  unsigned char pdiSelectEp( unsigned char byEpIdx) {
    unsigned char sep;
// usb_debug_print( DEBUG_LEVEL_HIGH, ("e%1d ",byEpIdx));
    pdiSendCommand( PDI_CMD_SELECT_EP + byEpIdx);
    pdiReadData( 1, &sep);
    return sep;
  }

 /*********************************************************/
 // pdiGetLastTransStatus
 // get Last transaction status (PDI_LTSTAT_xxx and PDI_ERR_xxx)
  unsigned char pdiGetLastTransStatus( unsigned char byEpIdx) _PDI_USING {
    unsigned char lts;
// usb_debug_print( DEBUG_LEVEL_HIGH, ("L "));
#if defined(PDI_USE_USING)
    return pdiIntCmdReadData( PDI_CMD_GET_LAST_STAT + byEpIdx, 0);
#else
    pdiSendCommand( PDI_CMD_GET_LAST_STAT + byEpIdx);
    pdiReadData( 1, &lts);
    return lts;
#endif
  }


 #if defined(PDIUSBD11) || defined(PDIUSBH11) || defined(PDIUSBH11A)

 /*********************************************************/
 // pdiGetEpStatus
 // get Endpoint Status (PDI_EPSTAT_xxx)
  unsigned char pdiGetEpStatus( unsigned char byEpIdx) {
    unsigned char ges;
// usb_debug_print( DEBUG_LEVEL_HIGH, ("G "));
    pdiSendCommand( PDI_CMD_GET_EP_STAT + byEpIdx);
    pdiReadData( 1, &ges);
    return ges;
  }

 #endif

 /*********************************************************/
 // pdiReadFromEpBuffer - raw reading
 // read data from selected internal chip buffer
 // if byLength < length of buffer data, so we read only byLength data)
  unsigned char pdiReadFromEpBuffer( unsigned char byLength, unsigned char *pToBuff) {
    unsigned char rdep[2];
// usb_debug_print( DEBUG_LEVEL_HIGH, ("R "));
    pdiSendCommand( PDI_CMD_READ_BUFFER);
    pdiReadData( 2, rdep);
    if ( rdep[1]) { // there is some data
      if ( byLength < rdep[1]) // we need less data then is received
        rdep[1] = byLength;
      pdiReadData( rdep[1], pToBuff);
    }
    return rdep[1];
  }

 /*********************************************************/
 // pdiWriteToEpBuffer - raw writing
 // write data to selected internal chip buffer
  void pdiWriteToEpBuffer( unsigned char byLength, const unsigned char *pFromBuff) {
    unsigned char hd[2];
// usb_debug_print( DEBUG_LEVEL_HIGH, ("W "));
    pdiSendCommand( PDI_CMD_WRITE_BUFFER);
    hd[0] = 0; hd[1] = byLength;
    pdiWriteData( 2, hd);
    if ( byLength) {
      pdiWriteData( byLength, pFromBuff);
    }
  }

 /*********************************************************/
 // pdiSetEpStatus
 // set endpoint stall flag
  void pdiSetEpStatus( unsigned char byEpIdx, unsigned char byStatus) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("T "));
    pdiSendCommand( PDI_CMD_SET_EP_STAT + byEpIdx);
    pdiWriteData( 1, &byStatus);
  }

 /*********************************************************/
 // pdiAcknowledgeSetup
 // chip disable fncs Validate and Clear after SETUP packet,
 // this cmd re-enable these fncs
  void pdiAcknowledgeSetup( void) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("C "));
    pdiSendCommand( PDI_CMD_ACK_SETUP);
  }

 /*********************************************************/
 // pdiClearBuffer
 // set endpoint flag 'empty' and next data can be receive
  void pdiClearBuffer( void) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("B "));
    pdiSendCommand( PDI_CMD_CLEAR_BUFFER);
  }


 /*********************************************************/
 // pdiValidateBuffer
 // set endpoint flag 'full' and data can be send
  void pdiValidateBuffer( void) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("V "));
    pdiSendCommand( PDI_CMD_VALID_BUFFER);
  }

 /*********************************************************/
 // pdiSendResume
 // send an upstream resume signal for 10ms
  void pdiSendResume( void) {
// usb_debug_print( DEBUG_LEVEL_HIGH, ("M "));
    pdiSendCommand( PDI_CMD_SEND_RESUME);
  }

 /*********************************************************/
 // pdiGetFrameNumber
 // return frame number of last successfully received SOF
  unsigned short pdiGetFrameNumber( void) {
    unsigned short gfn;
    pdiSendCommand( PDI_CMD_GET_FRAME);
    pdiReadData( 2, &gfn);
   #if __BYTE_ORDER == __BIG_ENDIAN
    gfn=bswap_16(gfn);
   #endif
    return gfn; //pdiData[0] + (((unsigned short)pdiData[1])<<8);
  }

 /*********************************************************/
 // pdiGetChipID  - this function is undocumented
 // read chip ID (not documented function) ( LSB is maybe type of chip in hex (0x12,0x11))
  unsigned short pdiGetChipID( void) {
    unsigned short gid;
    pdiSendCommand( PDI_CMD_GET_CHIP_ID);
    pdiReadData( 2, &gid);
   #if __BYTE_ORDER == __BIG_ENDIAN
    gid=bswap_16(gid);
   #endif
    return gid; //pdiData[0] + (((unsigned short)pdiData[1])<<8);
  }


 /*********************************************************/
 /*********************************************************/
 // HUB command
#if defined(PDIUSBH11) || defined(PDIUSBH11A)
 /*********************************************************/
 // pdiClearPortFeature
 // clear feature 'byFeature' in downstream port 2-5 'byEpIdx' (zero based)
  void pdiClearPortFeature( unsigned char byEpIdx, unsigned char byFeature) {
    pdiSendCommand( PDI_CMD_P2_CLR_FEATURE + byEpIdx);
    pdiWriteData( 1, &byFeature);
  }

 /*********************************************************/
 // pdiSetPortFeature
 // set feature 'byFeature' in downstream port 2-5 'byEpIdx' (zero based)
  void pdiSetPortFeature( unsigned char byEpIdx, unsigned char byFeature) {
    pdiSendCommand( PDI_CMD_P2_SET_FEATURE + byEpIdx);
    pdiWriteData( 1, &byFeature);
  }

 /*********************************************************/
 // pdiGetPortFeature
 // get port status (port status byte and port status change byte)
  unsigned short pdiGetPortFeature( unsigned char byEpIdx) {
    unsigned short gpf;
    pdiSendCommand( PDI_CMD_P2_GET_STATUS + byEpIdx);
    pdiReadData( 2, &gpf);
   #if __BYTE_ORDER == __BIG_ENDIAN
    gpf=bswap_16(gpf);
   #endif
    return gpf; //pdiData[0] + (((unsigned short)pdiData[1])<<8);
  }

 /*********************************************************/
 // pdiSetStatusChangeBits
 // set local power change bits status
  void pdiSetStatusChangeBits( unsigned char byBits) {
    pdiSendCommand( PDI_CMD_SET_CHNG_BITS);
    pdiWriteData( 1, &byBits);
  }

#endif



/*********************************************************/
/*********************************************************/
// PDIUSB other commands

 // complex function for select endpoint, write data and validate data in endpoint buffer
 void pdiWriteEndpoint( unsigned char byEpIdx, unsigned char byLength, const unsigned char *pbyData) {
   pdiSelectEp( byEpIdx);
   pdiWriteToEpBuffer( byLength, pbyData);
   pdiValidateBuffer();
 }

 // complex function for select endpoint, read data and clear endpoint buffer
 // byMaxLength means how many bytes will be maximally read.
 unsigned char pdiReadEndpoint( unsigned char byEpIdx, unsigned char byMaxLength, unsigned char *pbyData) {
   unsigned char cnt = 0, sep;
   sep = pdiSelectEp( byEpIdx);
   if ( sep & PDI_SELEP_FULL) {
    cnt = pdiReadFromEpBuffer( byMaxLength, pbyData);
    pdiClearBuffer();
   }
   return cnt;
 }

 // complex universal function for select command and read/write data to/from PDIUSB
 unsigned char pdiCmdData( unsigned char byCmd, unsigned char *pbyData,
                           unsigned char byCount, unsigned char byRead) {
   pdiSendCommand( byCmd);
   if ( byCount) {
     if( byRead) byCount = pdiReadData( byCount, pbyData);  // Read Data
     else pdiWriteData( byCount, pbyData);                  // Write Data
   }
   return byCount;
 }

#if !defined(PDIUSBH11A_MULTIPLE)
 // complex function for acknowledge control endpoint
 void pdiAckSetupControl( void) {
   pdiSendCommand( PDI_CMD_SELECT_EP + PDI_EP0_RX);
   pdiSendCommand( PDI_CMD_ACK_SETUP);
   pdiSendCommand( PDI_CMD_CLEAR_BUFFER);
   pdiSendCommand( PDI_CMD_SELECT_EP + PDI_EP0_TX);
   pdiSendCommand( PDI_CMD_ACK_SETUP);
 }
#else
 // complex function for acknowledge control endpoint ( for one emb.fnc. 1,6,7) (zero-based 0,1,2)
 void pdiAckSetupFncControl( unsigned char Fnc) {
   unsigned char FncTab[3] = { PDI_F1_EP0_RX, PDI_F6_EP0_RX, PDI_F7_EP0_RX};
   pdiSendCommand( PDI_CMD_SELECT_EP + FncTab[Fnc]);
   pdiSendCommand( PDI_CMD_ACK_SETUP);
   pdiSendCommand( PDI_CMD_CLEAR_BUFFER);
   pdiSendCommand( PDI_CMD_SELECT_EP + FncTab[Fnc] + 1);
   pdiSendCommand( PDI_CMD_ACK_SETUP);
 }
#endif

#if defined(PDIUSBD12) // parallel interface
 void pdiInit( void) {
   pdiSetAddressEnable( PDI_ENAD_ENABLE);
   pdiSetEndpointEnable( PDI_EPEN_ENABLE);
   pdiSetDMA( PDI_DMA_EP4_INT | PDI_DMA_EP5_INT);
   pdiSetMode( PDI_MODE_NO_LAZY_CLOCK | PDI_MODE_CLOCK_RUNNING |
               PDI_MODE_SOFT_CONNECT | PDI_CLOCK_12M);
 }
#endif

#if defined(PDIUSBH11A_SINGLE) // serial interface
 void pdiInit( void) {
   pdiSetHUBAddressEnable( 0, 0); // disable HUB
   pdiSetAddressEnable( PDI_ENAD_ENABLE);    // enable emb.function
   pdiSetEndpointEnable( PDI_EPEN_FNC_ENB);
   pdiSetMode( PDI_MODE_REMOTE_WAKEUP | PDI_MODE_NO_LAZY_CLOCK | PDI_MODE_CLOCK_RUNNING |
               PDI_MODE_SOFT_CONNECT | PDI_MODE_SINGLE_FNC | PDI_CLOCK_12M);
 }
#endif

/*********************************************************/

 static const unsigned char epin2idx[]={
   PDI_EP0_IN,
   PDI_EP1_IN,
  #if PDI_CNT_EP > 1
   PDI_EP2_IN,
  #if PDI_CNT_EP > 2
   PDI_EP3_IN,
  #endif
  #endif
 };

 static const unsigned char epout2idx[]={
   PDI_EP0_OUT,
   PDI_EP1_OUT,
  #if PDI_CNT_EP > 1
   PDI_EP2_OUT,
  #if PDI_CNT_EP > 2
   PDI_EP3_OUT,
  #endif
  #endif
 };

 // pdiEp2Idx
 // convert endpoint number to pdi index number
 unsigned char pdiEp2Idx(unsigned char ep) {
   if(ep & USB_ENDPOINT_DIRECTION_MASK)
     return epin2idx[ep & 0xf];
   else
     return epout2idx[ep & 0xf];
 }

 /*********************************************************/
 /*********************************************************/
