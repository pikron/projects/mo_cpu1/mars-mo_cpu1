#include <types.h>
//#include <cpu_def.h>
//#include <h8s2633h.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>

#include <usb/usb.h>
//#include <usb/pdiusb.h>
//#include <msp430-lib/JTAGfunc.h>
//#include <msp430-lib/LowLevelFunc.h>
#include "usb_loader.h"


#ifdef PDI_EP0_PACKET_SIZE
  #define LOADER_BUFFER_SIZE PDI_EP0_PACKET_SIZE
#else /*PDI_EP0_PACKET_SIZE*/
  #define LOADER_BUFFER_SIZE 128
#endif /*PDI_EP0_PACKET_SIZE*/

unsigned char loader_buffer[LOADER_BUFFER_SIZE];
/*
unsigned long msp430_flash_minaddr=0x1100;
unsigned long msp430_flash_maxaddr=0xffff;

int usb_msp430_pkt_wrmsp(usb_ep_t *ep, int len, int codeval)
{
  unsigned char *ptr=ep->ptr-len;
  unsigned char c;
  int i;
  printf("usb_msp430_pkt_wrmsp bytes %d, loc %lx -> msp %lx\n", len, (long)ptr, ep->user_data);
  if(!(ep->user_data&1)) {
    i = len/2;
    while(i--){
      // swap endianning
      c = ptr[0]; ptr[0] = ptr[1]; ptr[1] = c;
      ptr += 2;
    }
    ptr -= len & ~1;

    if( (ep->user_data<msp430_flash_minaddr) ||
        (ep->user_data>msp430_flash_maxaddr) ) {
      WriteMemQuick(ep->user_data, len/2, (word*)ptr);
    } else {
      WriteFLASH(ep->user_data, len/2, (word*)ptr);
      if(VerifyMem(ep->user_data, len/2, (word*)ptr) != STATUS_OK) {
        ep->ptr=loader_buffer;
        return USB_COMPLETE_FAIL;
      }
    }
    //ptr += len & ~1;
    ep->user_data += len & ~1;
    len &= 1;
  }

  while(len--){
    //F_BYTE or F_WORD
    WriteMem(F_BYTE, ep->user_data++, *(ptr++));
  }
  ep->ptr=loader_buffer;
  return USB_COMPLETE_OK;
}

int usb_msp430_pkt_rdmsp(usb_ep_t *ep, int len, int codeval) // 'code' must not be used as identifier
{
  unsigned char *ptr;
  unsigned char c;
  int i;
  printf("usb_msp430_pkt_rdmsp bytes %d\n", len);
  ptr = ep->ptr = loader_buffer;
  if(!(ep->user_data&1)) {
    i = len/2;
    ReadMemQuick(ep->user_data, i, (word*)ptr);
    while(i--){
      // swap endianning
      c = ptr[0]; ptr[0] = ptr[1]; ptr[1] = c;
      ptr += 2;
    }
    /ptr += len & ~1;
    ep->user_data += len & ~1;
    len &= 1;
  }

  while(len--){
    //F_BYTE or F_WORD
    *(ptr++) = ReadMem(F_BYTE, ep->user_data++);
  }
  return USB_COMPLETE_OK;
}
*/

/*

ERASE_MASS or ERASE_MAIN or ERASE_SGMT
void EraseFLASH(word EraseMode, word EraseAddr)

*/

//int msp430_erase(unsigned addr, unsigned len)
int msc1210_erase(unsigned addr, unsigned len)
{
  unsigned end = addr + len - 1;
  if(addr<0x8000) //0x1000)
    return -1;

  while(addr < end) {
//    EraseFLASH( ERASE_SGMT, addr);
//    if(addr<0x1100) addr = (addr & ~0x7f) + 0x80;
//    else addr = (addr & ~0x1ff) + 0x200;
    *((xdata unsigned char *)addr) = 0;
  }
  return 0;
}
/*
int msp430_mass_erase(unsigned mode, unsigned addr)
{
  if(mode == 0) mode = ERASE_MASS;
  EraseFLASH( mode, addr);
  return 0;
}
*/
int msc1210_mass_erase(unsigned mode, unsigned addr)
{
//  unsigned addr = 0x7fff;
  while( addr != 0xffff) {
    addr++;
    if ( !(addr & 0x00ff)) printf(" erase segm. 0x%X\n", addr);
    *((xdata unsigned char *)addr) = 0;
  }
  return 0;
}


#ifdef SDCC
void msc1210_goto( unsigned addr)
{
  _asm
    pop  acc
    pop  acc
    mov  a,dpl
    push acc
    mov  a,dph
    push acc
    ret
  _endasm;
}
#else /*SDCC*/
#pragma asm
;void msc1210_goto( unsigned addr)
;{
	public _msc1210_goto
_msc1210_goto:
	pop  acc
	pop  acc
	mov  a,r7
	push acc
	mov  a,r6
	push acc
	ret
;  return 0;
;}
#pragma endasm
#endif



void msc1210_reset_device( void)
{
  printf("...reset...\n");
}


//int usb_msp430_loader(usb_control_ep_t *ep, USB_DEVICE_REQUEST *dreq)

//int usb_msp430_loader(struct usb_device_t *udev)
int usb_msc1210_loader(usb_device_t *udev)
{
  unsigned long addr;
  unsigned len;

  switch ( udev->request.bRequest & USB_VENDOR_MASK) {

    case USB_VENDOR_GET_CAPABILITIES:
      printf("GET_CAPABILITIES\n");
      loader_buffer[0] = 0xAA; // test
      usb_send_control_data(udev, loader_buffer, 1);
      return 1;

    case USB_VENDOR_RESET_DEVICE:
      printf("RESET_DEVICE\n");
//      InitController();
//      InitTarget();
//      if (!GetDevice()) {
//        return -1;
//      }
      usb_ack_setup(&udev->ep0);
      msc1210_reset_device();
      return 1;

    case USB_VENDOR_GOTO:
      //printf( "GOTO 0x%X\n", udev->request.wValue);
      usb_ack_setup(&udev->ep0);
      //msp430_goto(udev->request.wValue);
      msc1210_goto(udev->request.wValue);
      return 1;

    case USB_VENDOR_ERASE_MEMORY:
      printf( "ERASE 0x%X 0x%X\n", udev->request.wValue, udev->request.wIndex);
      usb_ack_setup(&udev->ep0);
      //msp430_erase(udev->request.wValue, udev->request.wIndex);
      msc1210_erase(udev->request.wValue, udev->request.wIndex);
      return 1;

    case USB_VENDOR_MASS_ERASE:
      printf( "MASSERASE 0x%X 0x%X\n", udev->request.wIndex, udev->request.wValue);
      usb_ack_setup(&udev->ep0);
      //msp430_mass_erase(udev->request.wIndex, udev->request.wValue);
      msc1210_mass_erase(udev->request.wIndex, udev->request.wValue);
      return 1;

    case USB_VENDOR_GET_SET_MEMORY:
      addr = (udev->request.wValue & 0xffff) | (((unsigned long)udev->request.wIndex&0xffff)<<16);
      len = udev->request.wLength;
      //printf("GET_SET_MEMORY, addr=0x%lx, len=%d\n", (long)addr, len);
      if (( udev->request.bmRequestType & USB_DATA_DIR_MASK) == USB_DATA_DIR_FROM_HOST) {
        // read from HOST ???
        switch( udev->request.bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_XDATA:
            udev->ep0.ptr = (xdata unsigned char *)addr;
            break;
          case USB_VENDOR_TARGET_DATA:
            udev->ep0.ptr = (data unsigned char *)addr;
            break;
/*
          case USB_VENDOR_TARGET_MSP430:
            if (jtag_check_init(0)<0) {
                  printf("MSP430 JTAG init ERROR\n");
                  return -1;
                }
            udev->ep0.next_pkt_fnc = usb_msp430_pkt_wrmsp;
            udev->ep0.user_data=addr;
            udev->ep0.ptr = loader_buffer; break;
*/
          default: return -1;
        }
        if ( len) usb_set_control_endfnc( udev, usb_ack_setup);
        else usb_ack_setup(&udev->ep0);
        return 1;
      } else {
        switch( udev->request.bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_XDATA:
            usb_send_control_data( udev, (xdata unsigned char *)addr, len); //(void*)addr, len);
            break;
          case USB_VENDOR_TARGET_DATA:
            usb_send_control_data( udev, (data unsigned char *)addr, len); //(void*)addr, len);
            break;
/*
          case USB_VENDOR_TARGET_MSP430:
            if (jtag_check_init(0)<0) {
                  printf("MSP430 JTAG init ERROR\n");
                  return -1;
                }
            udev->ep0.next_pkt_fnc = usb_msp430_pkt_rdmsp;
            udev->ep0.user_data=addr;
            usb_send_control_data( udev, loader_buffer, len); break;
*/
          default: return -1;
        }
        return 1;
      }
      break;
  }


  return 0;
}
