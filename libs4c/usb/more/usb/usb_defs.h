
#ifndef USB_DEFS_MODULE
  #define USB_DEFS_MODULE

  #include <usb/usb_spec.h>
  #include <usb/pdiusb.h>
//  #include <cpu_def.h>

  #include <endian.h>
  #if __BYTE_ORDER == __BIG_ENDIAN
    #include <byteswap.h>
    #define SWAP(x) ((((x) & 0xFF) << 8) | (((x) >> 8) & 0xFF))
  #else /*__LITTLE_ENDIAN*/
    #define SWAP(x) (x)
  #endif

//  #define MSB(x) ((unsigned char)(((x) >> 8)& 0xff))
//  #define LSB(x) ((unsigned char)((x) & 0xff))

  #ifndef CODE
    #define CODE
  #endif

/*****************************************************/
/*** Static data structures for device descriptors ***/
/*****************************************************/
  #define USB_VENDOR_ID      0xDEAD  /* vymyslene cislo ( snad ho nikdo nema ... ;-) */
//  #define USB_PRODUCT_ID     0x2262  /* Program Uploader(DFU) 8.8.2002 */
//  #define USB_PRODUCT_ID     0xABCD  /* Program Uploader(DFU) 8.8.2002 */
  #define USB_PRODUCT_ID     0x0101  /* testing program for m68 */
  #define USB_RELEASE_VER    0x0100

/*** Class codes for device description ***/
  #define USB_CLASS_CODE      0xFF
  #define USB_SUBCLASS_CODE   0x00
  #define USB_PROTOCOL_CODE   0x00


  #define NUM_ENDPOINTS  2
  #define CONFIG_DESCRIPTOR_LENGTH sizeof( USB_CONFIGURATION_DESCRIPTOR) \
                                     + sizeof( USB_INTERFACE_DESCRIPTOR) \
                                     + ( NUM_ENDPOINTS * sizeof( USB_ENDPOINT_DESCRIPTOR))

/*** Device descriptor ***/
  CODE const USB_DEVICE_DESCRIPTOR DeviceDescription = {
    sizeof( USB_DEVICE_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_DEVICE,
    SWAP( 0x0100),
    USB_CLASS_CODE,
    USB_SUBCLASS_CODE,
    USB_PROTOCOL_CODE,
    PDI_EP0_PACKET_SIZE,
    SWAP( USB_VENDOR_ID),
    SWAP( USB_PRODUCT_ID),
    SWAP( USB_RELEASE_VER),
    1, /* manufacturer string ID */
    2, /* product string ID */
    3, /* serial number string ID */
    1
  };

/*** All In Configuration 0 ***/
  CODE const struct {
    USB_CONFIGURATION_DESCRIPTOR configuration;
    USB_INTERFACE_DESCRIPTOR interface;
    USB_ENDPOINT_DESCRIPTOR endpoint_tx;
    USB_ENDPOINT_DESCRIPTOR endpoint_rx;
  } ConfigDescription = {
    /*** Configuration descriptor ***/
    {
      sizeof( USB_CONFIGURATION_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_CONFIGURATION,
      SWAP( CONFIG_DESCRIPTOR_LENGTH),
      1, /* cnt of interfaces */
      1, /* this configuration ID */
      4, /* config.name string ID*/
      0xa0, /* cfg, in spec is, taha bit 7 must be set to one -> 0xe0 */
      50    /* device power current from host */
    },
    /*** Interface Descriptor ***/
    {
      sizeof( USB_INTERFACE_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_INTERFACE,
      0,    /* index of this interface for SetInterface request */
      0,    /* ID alternate interface */
      NUM_ENDPOINTS,
      USB_CLASS_CODE,
      USB_SUBCLASS_CODE,
      USB_PROTOCOL_CODE,
      5
    },
    /*** Endpoint 1 - Tx,Bulk ***/
    {
      sizeof( USB_ENDPOINT_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_ENDPOINT,
      0x82,
      USB_ENDPOINT_TYPE_BULK,
      SWAP( PDI_EP2_PACKET_SIZE),
      0
    },
    /*** Endpoint 2 - Rx,Bulk ***/
    {
      sizeof( USB_ENDPOINT_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_ENDPOINT,
      0x02,
      USB_ENDPOINT_TYPE_BULK,
      SWAP( PDI_EP2_PACKET_SIZE),
      0
    }
  };
  /*** Strings - in unicode ***/
  CODE const char Str0Desc[] = {  /* supported languages of strings */
    4, 0x03,  /* 2+2*N , N is count of supported languages */
    0x09,0x04 /* english 0x0409 */
  //  0x05,0x04 /* czech 0x0405 */
  };

  CODE const char Str1Desc[] = {  /* 1 = manufacturer */
    58, 0x03,
    'R',0,
    'o',0,
    'm',0,
    'a',0,
    'n',0,
    ' ',0,
    'B',0,
    'a',0,
    'r',0,
    't',0,
    'o',0,
    's',0,
    'i',0,
    'n',0,
    's',0,
    'k',0,
    'i',0,
    ',',0,
    'C',0,
    'T',0,
    'U',0,
    ' ',0,
    'P',0,
    'r',0,
    'a',0,
    'g',0,
    'u',0,
    'e',0
  };
  CODE const char Str2Desc[] = {  /* 2 = product */
    42, 0x03,
    'U',0,
    'S',0,
    'B',0,
    ' ',0,
    'T',0,
    'e',0,
    's',0,
    't',0,
    'i',0,
    'n',0,
    'g',0,
    ' ',0,
    'P',0,
    'r',0,
    'o',0,
    'g',0,
    '.',0,
    ' ',0,
    'M',0,
    '2',0
  };
  CODE const char Str3Desc[] = {  /* 3 = version */
    26, 0x03,
    '0',0,
    '4',0,
    '.',0,
    '0',0,
    '5',0,
    '.',0,
    '1',0,
    '1',0,
    '-',0,
    '1',0,
    '.',0,
    '1',0
  };
  CODE const char Str4Desc[] = {  /* 4 = configuration */
    34, 0x03,
    'C',0,
    'o',0,
    'n',0,
    'f',0,
    'i',0,
    'g',0,
    'u',0,
    'r',0,
    'a',0,
    't',0,
    'i',0,
    'o',0,
    'n',0,
    ' ',0,
    '#',0,
    '1',0
  };
  CODE const char Str5Desc[] = {  /* 5 = interface */
    26,0x03,
    'I',0,
    'n',0,
    't',0,
    'e',0,
    'r',0,
    'f',0,
    'a',0,
    'c',0,
    'e',0,
    ' ',0,
    '#',0,
    '0',0
  };

  #define CNT_STRINGS 0x06

  /* all strings in pointers array */
  CODE const PUSB_STRING_DESCRIPTOR StringDescriptors[] = {
    (PUSB_STRING_DESCRIPTOR) Str0Desc,
    (PUSB_STRING_DESCRIPTOR) Str1Desc,
    (PUSB_STRING_DESCRIPTOR) Str2Desc,
    (PUSB_STRING_DESCRIPTOR) Str3Desc,
    (PUSB_STRING_DESCRIPTOR) Str4Desc,
    (PUSB_STRING_DESCRIPTOR) Str5Desc
  };

#endif /* USB_DEFS_MODULE */
