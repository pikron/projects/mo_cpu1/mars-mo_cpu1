/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  mo_flashprog.c - universal FLASH programming module
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <types.h>
#include <flashprog.h>


flash_alg_info_t const amd_29f400_x16={
  addr_mask:  0x7ffff,
  reg1_addr:  0x555*2,
  reg2_addr:  0x2aa*2,
  sec_size:   0x10000,
  width8:     0,
  cmd_unlock1:0xaa,	/* reg1 */
  cmd_unlock2:0x55,	/* reg2 */
  cmd_rdid:   0x90,	/* reg1 */
  cmd_prog:   0xa0,	/* reg1 */
  cmd_erase:  0x80,	/* reg1 */
  cmd_reset:  0xf0,	/* any */
  erase_all:  0x10,	/* reg1 */
  erase_sec:  0x30,	/* sector */
  fault_bit:  0x20,
  manid:      1,
  devid:      0x22ab
};

flash_alg_info_t const amd_29f800_x16={
  addr_mask:  0xfffff,
  reg1_addr:  0x555*2,
  reg2_addr:  0x2aa*2,
  sec_size:   0x10000,
  width8:     0,
  cmd_unlock1:0xaa,	/* reg1 */
  cmd_unlock2:0x55,	/* reg2 */
  cmd_rdid:   0x90,	/* reg1 */
  cmd_prog:   0xa0,	/* reg1 */
  cmd_erase:  0x80,	/* reg1 */
  cmd_reset:  0xf0,	/* any */
  erase_all:  0x10,	/* reg1 */
  erase_sec:  0x30,	/* sector */
  fault_bit:  0x20,
  manid:      1,
  devid:      0x2258
};

#define FLASH_WR16(addr,val) (*(volatile uint16_t*)(addr)=(val))
#define FLASH_RD16(addr) (*(volatile uint16_t*)(addr))

int flash_check_id(flash_alg_info_t const *alg, void *addr, 
		   uint16_t retid[2])
{
  int ret=0;
  uint16_t devid, manid;
  uint32_t a=(uint32_t)addr&~alg->addr_mask;
  /* reset */
  FLASH_WR16(a,alg->cmd_reset);
  /* security sequence */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_unlock1);
  if(alg->cmd_unlock2)
    FLASH_WR16(a+alg->reg2_addr,alg->cmd_unlock2);
  /* read manufacturer ID */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_rdid);
  manid=FLASH_RD16(a+0);
  if(manid!=alg->manid) ret=-1;
  /* reset */
  FLASH_WR16(a,alg->cmd_reset);
  /* security sequence */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_unlock1);
  if(alg->cmd_unlock2)
    FLASH_WR16(a+alg->reg2_addr,alg->cmd_unlock2);
  /* read device ID */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_rdid);
  devid=FLASH_RD16(a+2);
  if(devid!=alg->devid) ret=-1;
  /* reset */
  FLASH_WR16(a,alg->cmd_reset);
  if(retid)
    {retid[0]=manid;retid[1]=devid;};
  return ret;
}

int flash_prog(flash_alg_info_t const *alg, void *addr, uint16_t val)
{
  int ret=0;
  uint16_t old,new;
  uint32_t a=(uint32_t)addr&~alg->addr_mask;
  /* security sequence */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_unlock1);
  if(alg->cmd_unlock2)
    FLASH_WR16(a+alg->reg2_addr,alg->cmd_unlock2);
  /* program command */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_prog);
  FLASH_WR16(addr,val);
  /* wait for result */
  old=FLASH_RD16(addr);
  while((new=FLASH_RD16(addr))!=old){
    if((old&alg->fault_bit)&&(new&alg->fault_bit)){
      if((FLASH_RD16(addr))!=new) ret=-2;
      break;
    }
    old=new;
  }
  /* reset */
  FLASH_WR16(a,alg->cmd_reset);
  if(FLASH_RD16(addr)!=val) ret--;
  return ret;
}

int flash_erase(flash_alg_info_t const *alg, void *addr, long size)
{
  uint16_t old,new;
  int ret=0;
  uint32_t a=(uint32_t)addr&~alg->addr_mask;
  /* security sequence */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_unlock1);
  if(alg->cmd_unlock2)
    FLASH_WR16(a+alg->reg2_addr,alg->cmd_unlock2);
  /* erase command */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_erase);
  /* security sequence */
  FLASH_WR16(a+alg->reg1_addr,alg->cmd_unlock1);
  if(alg->cmd_unlock2)
    FLASH_WR16(a+alg->reg2_addr,alg->cmd_unlock2);
  /* select erase range */
  a=(uint32_t)addr;
  if(size==0)
    FLASH_WR16(a+alg->reg1_addr,alg->erase_all);
  else{
    FLASH_WR16(addr,alg->erase_sec);
  }  
  old=FLASH_RD16(addr);
  while((new=FLASH_RD16(addr))!=old){
    if((old&alg->fault_bit)&&(new&alg->fault_bit)){
      if((FLASH_RD16(addr))!=new) ret=-2;
      break;
    }
    old=new;
  }
  /* reset */
  FLASH_WR16(a,alg->cmd_reset);
  if(FLASH_RD16(addr)!=0xffff) ret--;    
  return ret;
}
