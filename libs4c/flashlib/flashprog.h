/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  flashprog.h - universal FLASH programming module interface 
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

typedef struct flash_alg_info {
  uint32_t addr_mask;	/* Mask to take offset inside flash */
  uint32_t reg1_addr;	/* Flash control register 1 */
  uint32_t reg2_addr;	/* Flash control register 2 */
  uint32_t sec_size;	/* block size of bigger blocks */
  uint16_t width8;	/* 0 .. 16 bit data, 1 .. 8 bit data bus, 2 ..
			   inerleaved 8 bit */
  uint16_t cmd_unlock1;	/* first byte of command sequence */
  uint16_t cmd_unlock2;	/* second byte of command sequence */
  uint16_t cmd_rdid;	/* read identifier */
  uint16_t cmd_prog;	/* program one loc */
  uint16_t cmd_erase;	/* erase command */
  uint16_t cmd_reset;	/* leave program mode */
  uint16_t erase_all;	/* erase all */
  uint16_t erase_sec;	/* erase sector */
  uint16_t fault_bit;	/* programing of location failed */
  uint16_t manid;	/* manufacturer ID */
  uint16_t devid;	/* device ID */
} flash_alg_info_t;

int flash_check_id(flash_alg_info_t const *alg, void *addr,
		   uint16_t retid[2]);

int flash_prog(flash_alg_info_t const *alg, void *addr, uint16_t val);

int flash_erase(flash_alg_info_t const *alg, void *addr, long size);

flash_alg_info_t const amd_29f400_x16;

flash_alg_info_t const amd_29f800_x16;
