/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_rs232.c - interconnection of text command processor
                with RS-232 line
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <types.h>
#include <ctype.h>
#include <string.h>
#include <utils.h>
#include <cmd_proc.h>

#ifndef ED_LINE_CHARS
#define ED_LINE_CHARS 250
#endif  /*ED_LINE_CHARS*/

cmd_io_t cmd_io_rs232_dev;

char ed_line_chars_rs232_in[ED_LINE_CHARS+1];
char ed_line_chars_rs232_out[ED_LINE_CHARS+1];

ed_line_buf_t ed_line_buf_rs232_in={
  flg:FL_ELB_ECHO,
  inbuf:0,
  alloc:sizeof(ed_line_chars_rs232_in),
  maxlen:0,
  lastch:0,
  buf:ed_line_chars_rs232_in
};

ed_line_buf_t ed_line_buf_rs232_out={
  flg:FL_ELB_NOCRLF,
  inbuf:0,
  alloc:sizeof(ed_line_chars_rs232_out),
  maxlen:0,
  lastch:0,
  buf:ed_line_chars_rs232_out
};

cmd_io_t cmd_io_rs232={
  putc:cmd_io_putc_ed_line,
  getc:NULL,
  write:cmd_io_write_bychar,
  read:NULL,
  priv:{
    ed_line:{
      in: &ed_line_buf_rs232_in,
      out:&ed_line_buf_rs232_out,
      io_stack:&cmd_io_rs232_dev
    }
  }
};

/* process pending output */
int cmd_rs232_line_out(cmd_io_t *cmd_io)
{
  cmd_io_t* io_stack;
  ed_line_buf_t* ed_line=cmd_io->priv.ed_line.out;
  
  if(!ed_line->inbuf) return 0;
  if(!(ed_line->flg&FL_ELB_INSEND)){
    ed_line->flg|=FL_ELB_INSEND;
    ed_line->lastch=0;
  }
  if(!(io_stack=cmd_io->priv.ed_line.io_stack))
    return -1;
  
  while(cmd_io_putc(io_stack,(uint8_t)ed_line_buf_rs232_out.
    	      buf[ed_line_buf_rs232_out.lastch])>=0){
    if(++ed_line_buf_rs232_out.lastch>=ed_line_buf_rs232_out.inbuf){
      ed_line->lastch=0;
      ed_line->inbuf=0;
      ed_line->flg&=~FL_ELB_INSEND;
      return 0;
    }
  }
  return 1;
}

/* process input */
int cmd_rs232_line_in(cmd_io_t *cmd_io)
{
  int ch;
  cmd_io_t* io_stack;
  
  if(!(io_stack=cmd_io->priv.ed_line.io_stack))
    return -1;

  while((ch=cmd_io_getc(io_stack))>=0){
    if(ed_line_buf_rs232_in.flg&FL_ELB_ECHO)
      while(cmd_io_putc(io_stack,ch)<0);
      
    if(cmd_ed_line_buf(&ed_line_buf_rs232_in,ch)){
      if(ed_line_buf_rs232_in.flg&FL_ELB_ECHO){
        while(cmd_io_putc(io_stack,'\r')<0);
        while(cmd_io_putc(io_stack,'\n')<0);
      }
      return 1;
    }
  }

  return 0;
}

char *cmd_rs232_rdline(cmd_io_t *cmd_io, int mode)
{
  int ret;
  while((ret=cmd_rs232_line_in(cmd_io))==0)
    if(!mode) break;
  if(ret<=0) return NULL;
  return cmd_io->priv.ed_line.in->buf;
}

#if 0
/* H8300 */
#include <periph/sci_rs232.h>

int cmd_io_putc_rs232(struct cmd_io *cmd_io,int ch)
{ 
  return sci_rs232_sendch(ch);;
}

int cmd_io_getc_rs232(struct cmd_io *cmd_io)
{ 
  return sci_rs232_recch();
}

#else

/* MC68376 */
#include <periph/qsm_rs232.h>

int cmd_io_putc_rs232(struct cmd_io *cmd_io,int ch)
{ 
  return qsm_rs232_sendch(ch);;
}

int cmd_io_getc_rs232(struct cmd_io *cmd_io)
{ 
  return qsm_rs232_recch();
}

#endif

cmd_io_t cmd_io_rs232_dev={
  putc:cmd_io_putc_rs232,
  getc:cmd_io_getc_rs232,
  write:cmd_io_write_bychar,
  read:cmd_io_read_bychar
};

