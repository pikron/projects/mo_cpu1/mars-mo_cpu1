/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  utils.h - utilities for reading numbers and parsing of text line
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _UTILS_DEF_H_
#define _UTILS_DEF_H_


/* skip space/blank characters, return 0 if no space found */
int si_skspace(char **ps);

/* skip spaces and checks for <sepchars>, */
/* if no such char return -1, else char is returned */
int si_fndsep(char **ps,char *sepchrs);

/* reads max <n> letters and digits to <pout> */
/* returns number of readed chars */
int si_alnumn(char **ps,char *pout,int n);

/* same as above, but only letters are read */
int si_alphan(char **ps,char *pout,int n);

/* reads long number, if no digit found return -1 */
int si_long(char **ps,long *val,int base);

#endif /* _UTILS_DEF_H_ */

