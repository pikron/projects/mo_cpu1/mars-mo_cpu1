#include <i2str.h>

/**
 * Converts integer to string.
 * @param s Buffer to store the result.
 * @param val Value to convert.
 * @param len Minimal width of the converted strign (padded by ' ').
 * @param form Unused.
 * @return 0
 */
int i2str(char *s,long val,int len,int form)
{
  int sig;
  int dig=1;
  int pad=0;
  unsigned base=form&0xff;
  unsigned long u;
  unsigned long mag;
  unsigned long num;
  if(!base) base=10;
  if((sig=(val<0)&&(base==10))) num=-val;
  else num=val;
 
  mag=1;
  u=base*mag;
  while(num>=u){
    dig++;
    mag=u;
    if(mag>(unsigned long)(~(unsigned long)0)/base) break;
    u*=base;
  }

  if(len){
    pad=len-dig;
    if(sig) pad--;
  }
  if(pad<0) pad=0;


  if(form&I2STR_PAD_0) {
    if(sig) *(s++)='-';
    while(pad){
      *(s++)='0';
      pad--;
    }
  }else{
    while(pad){
      *(s++)=' ';
      pad--;
    }
    if(sig) *(s++)='-';
  }

  while(dig--){
    u=num/mag;
    if(u>9) *(s++)='A'-10+u;
    else *(s++)='0'+u;
    num=num%mag;
    mag/=base;
  }
  *s=0;
  return 0;
}

