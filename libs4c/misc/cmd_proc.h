/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_proc.h - text line command processor
               designed for instruments control and setup
	       over RS-232 line
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com
            (C) 2007 by Michal Sojka <sojkam1@fel.cvut.cz>

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#ifndef _CMD_PROC_H_
#define _CMD_PROC_H_

#define FL_ELB_ECHO   0x10      /* Read characters are echoed back */
#define FL_ELB_INSEND 0x20      /* The line is currently being sent */
#define FL_ELB_NOCRLF 0x40      /* CR and/or LF will not start the new line */
#define CMD_DES_CONTINUE_AT_ID ((cmd_des_t*)1)
#define CMD_DES_INCLUDE_SUBLIST_ID ((cmd_des_t*)2)
#define CMD_DES_CONTINUE_AT(list)     CMD_DES_CONTINUE_AT_ID,((cmd_des_t*)list)
#define CMD_DES_INCLUDE_SUBLIST(list) CMD_DES_INCLUDE_SUBLIST_ID,((cmd_des_t*)list)
/* generic cmd_io tructure */

/* buffer for in/out line collection */
typedef struct{
  int  flg;                     /* FL_ELB_xxx */
  int  inbuf;                   /* Index to store new characters */
  int  alloc;                   /* Size of the buffer pointed by buf */
  int  maxlen;
  int  lastch;                  /* Last characted added to the buffer.
                                 * If FL_ELB_INSEND is set, lastch is
                                 * index of last sent char. */
  char *buf;
} ed_line_buf_t;

/* Structure for character input output. It is used either for direct
 * access to IO device or for buffered IO. In the later case,
 * priv.edline is used to store buffer information. */
typedef struct cmd_io{
  int (*putc)(struct cmd_io *cmd_io,int ch);
  int (*getc)(struct cmd_io *cmd_io);
  int (*write)(struct cmd_io *cmd_io,const void *buf,int count);
  int (*read)(struct cmd_io *cmd_io,void *buf,int count);
  union {
    struct {
      ed_line_buf_t *in;
      ed_line_buf_t *out;
      struct cmd_io *io_stack;
    } ed_line;
    struct {
      long pos;
      void *ptr;
    } device;
  } priv;
} cmd_io_t;

static inline int cmd_io_putc(struct cmd_io *cmd_io,int ch)
{ if(!cmd_io->putc) return -1;
  return (*cmd_io->putc)(cmd_io,ch);
}

static inline int cmd_io_getc(struct cmd_io *cmd_io)
{ if(!cmd_io->getc) return -1;
  return (*cmd_io->getc)(cmd_io);
}

static inline int cmd_io_write(struct cmd_io *cmd_io,const void *buf,int count)
{ if(!cmd_io->write) return -1;
  return (*cmd_io->write)(cmd_io,buf,count);
}

static inline int cmd_io_read(struct cmd_io *cmd_io,void *buf,int count)
{ if(!cmd_io->read) return -1;
  return (*cmd_io->read)(cmd_io,buf,count);
}

int cmd_ed_line_buf(ed_line_buf_t *elb, int ch);

int cmd_io_putc_ed_line(cmd_io_t *cmd_io,int ch);

int cmd_io_write_bychar(cmd_io_t *cmd_io,const void *buf,int count);

int cmd_io_read_bychar(cmd_io_t *cmd_io,void *buf,int count);

/* command descriptions */

#define CDESM_OPCHR	0x10	/* Command uses operation character */
#define CDESM_RD	0x01	/* Value read is possible */
#define CDESM_WR	0x02	/* Value write is possible */
#define CDESM_RW	(CDESM_RD|CDESM_WR) /* Both */

typedef struct cmd_des{
  int code;
  int mode;
  char *name;
  char *help;
  int (*fnc)(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
  char *info[];
} cmd_des_t;

#define CMDERR_BADCMD 2
#define CMDERR_OPCHAR 10
#define CMDERR_WRPERM 11
#define CMDERR_RDPERM 12
#define CMDERR_GARBAG 13
#define CMDERR_BADSEP 14
#define CMDERR_BADCFG 15
#define CMDERR_NOMEM  16
#define CMDERR_BADSUF 17
#define CMDERR_BADPAR 20
#define CMDERR_VALOOR 21
#define CMDERR_BADREG 40
#define CMDERR_BSYREG 41
#define CMDERR_BADDIO 50
#define CMDERR_NODEV  60
#define CMDERR_TIMEOUT 61
#define CMDERR_EIO    62

int cmd_opchar_check(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_num_suffix(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[], unsigned *val);

int proc_cmd_line(cmd_io_t *cmd_io, cmd_des_t const **des_arr, char *line);

int i2str(char *s,long val,int len,int form);

int cmd_do_stamp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_do_help(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_do_rw_short(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_do_rw_int(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_do_rw_long(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_do_rw_bitflag(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

int cmd_opchar_replong(cmd_io_t *cmd_io, char *param[], long val,int len,int form);

/* RS232 specific */

cmd_des_t const **cmd_rs232;

cmd_io_t cmd_io_rs232;

int cmd_rs232_line_out(cmd_io_t *cmd_io);

int cmd_rs232_line_in(cmd_io_t *cmd_io);

char *cmd_rs232_rdline(cmd_io_t *cmd_io, int mode);

#endif /* _CMD_PROC_H_ */
