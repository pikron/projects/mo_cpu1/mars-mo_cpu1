/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmd_proc.c - text command processor
               enables to define multilevel tables of commands
	       which can be received from more inputs and send reply
	       to respective I/O stream output
 
  Copyright (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002 by PiKRON Ltd. http://www.pikron.com
            (C) 2007 by Michal Sojka <sojkam1@fel.cvut.cz>

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators
 *******************************************************************/

#include <types.h>
#include <ctype.h>
#include <string.h>
#include <utils.h>
#include <stdio.h>
#include <stdlib.h>
#include <i2str.h>
#include <cmd_proc.h>

/* generic cmd_io tructure */

#if 0 /* declared inline */

inline int cmd_io_putc(struct cmd_io *cmd_io,int ch)
{ if(!cmd_io->putc) return -1;
  return (*cmd_io->putc)(cmd_io,ch);
}

inline int cmd_io_getc(struct cmd_io *cmd_io)
{ if(!cmd_io->getc) return -1;
  return (*cmd_io->getc)(cmd_io);
}

inline int cmd_io_write(struct cmd_io *cmd_io,const void *buf,int count)
{ if(!cmd_io->write) return -1;
  return (*cmd_io->write)(cmd_io,buf,count);
}

inline int cmd_io_read(struct cmd_io *cmd_io,void *buf,int count)
{ if(!cmd_io->read) return -1;
  return (*cmd_io->read)(cmd_io,buf,count);
}

#endif /* declared inline */

/* cmd_io lene editor */

int cmd_ed_line_buf(ed_line_buf_t *elb, int ch)
{
  int lastch=elb->lastch;
  elb->lastch=ch;
  if(!lastch){
    elb->inbuf=0;
  }
  if((!(elb->flg&FL_ELB_NOCRLF))&&((ch=='\n')||(ch=='\r'))){
   if((lastch=='\n')&&(ch=='\r'))
     return 0;
   elb->lastch=0;
   elb->buf[elb->inbuf]=0;
   return 1;
  }
  if(elb->inbuf>=elb->alloc-1){ 
    /* try to reallocate buffer len not implemented */
    return 0;
  }
  elb->buf[elb->inbuf++]=ch;  
  return 0;
}

int cmd_io_putc_ed_line(cmd_io_t *cmd_io,int ch)
{
  return cmd_ed_line_buf(cmd_io->priv.ed_line.out,ch);
}

int cmd_io_write_bychar(cmd_io_t *cmd_io,const void *buf,int count)
{
  int cn=0;
  uint8_t* p=(uint8_t*)buf;
  while(count--&&(*cmd_io->putc)(cmd_io,*p++)>=0){
    cn++;
  }
  return cn;
}

int cmd_io_read_bychar(cmd_io_t *cmd_io,void *buf,int count)
{
  int cn=0;
  int ch;
  uint8_t* p=(uint8_t*)buf;
  while(count--&&(ch=(*cmd_io->getc)(cmd_io))>=0){
    *p++=ch;
    cn++;
  }
  return cn;
}

/* cmd_line processing */

char *skip_white(char *p)
{
  while(isspace((uint8_t)*p)) p++;
  return p;
}

#define CMD_ARR_STACK_SIZE 4

/**
 *
 * @return Zero if no command was given in line, -CMDERR_BADCMD if
 * command is not known or has no function assigned to it. If a
 * command is executed, then the return value of the command function
 * is returned.
 */
int proc_cmd_line(cmd_io_t *cmd_io, cmd_des_t const **des_arr, char *line)
{
  char *p=line, *r, *var;
  const cmd_des_t *des;
  cmd_des_t const **arr_stack[CMD_ARR_STACK_SIZE];
  int arr_stack_sp=0;
  char *param[10];
  short cmd_len;
  int res, i, parcnt; 
  param[0]=p=skip_white(p);
  if(!*p) return 0;
  /* Determine the name of the command */
  if(!isalnum((uint8_t)*p)){
    cmd_len=1;
    p++;
  }else{
    while(isalnum((uint8_t)*p)) p++;
    cmd_len=p-param[0];
  }
  param[1]=param[2]=skip_white(p);
  
  /* Find the command in des_arr */
  while(1){
    des=*(des_arr++);
    if(!des){
      if(!arr_stack_sp) break;
      des_arr=arr_stack[--arr_stack_sp];
      continue;
    }
    if(des==CMD_DES_CONTINUE_AT_ID){
      /* list continues at new address */
      des_arr=(const cmd_des_t **)*des_arr;continue;
    }
    if(des==CMD_DES_INCLUDE_SUBLIST_ID){
      /* list includes commands from sublists */
      if(arr_stack_sp>=CMD_ARR_STACK_SIZE){
        des_arr++;
      }else{
        arr_stack[arr_stack_sp++]=des_arr+1;
        des_arr=(const cmd_des_t **)*des_arr;
        continue;
      }
    }
    p=param[0];
    if(!(r=des->name))continue;
    i=cmd_len;
    var=NULL;
    while(*r){
      while((*p==*r)&&i){i--;r++;p++;};
      if((i==0)&&!*r) break;    /* We've found the command */
      if((*r=='?')&&i){
        if(!var) var=p;
	p++; r++; i--;
	continue;
      }
      if((*r=='#')&&i&&isdigit((uint8_t)*p)){
	if(!var) var=p;
	p++; r++; i--;
	continue;
      }
      if(*r=='*'){
	if(!var) var=p;
	i=0;
	break;
      }
      i=1;
      break;      
    }
    if(i!=0) continue; /* Try next command */
    if(des->mode&CDESM_OPCHR){
      if(!param[2])continue;
      if(!*param[2])continue;
      param[3]=skip_white(param[2]+1);
      param[1]=var;
      parcnt=4;
    }else{
      parcnt=1;
      if(var){param[1]=var;parcnt++;}
      if(param[parcnt])
        if(*param[parcnt]) parcnt++;
    }
    param[parcnt]=0;
    if(!des->fnc) return -CMDERR_BADCMD;
    res=des->fnc(cmd_io,des,param);
    return res;
  }
  return -CMDERR_BADCMD;
}

/**
 * Checks whether the the command allows the operation specified by
 * opchar.
 *
 * @return opchar if perimssions allow this operations, -CMDERR_WRPERM
 * or -CMDERR_RDPERM if the operation is not allows, -CMDERR_OPCHAR,
 * if the opchar is not ':' or '?'.
 */
int cmd_opchar_check(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{ 
  int opchar=*param[2];
  if(opchar==':'){
    if(!(des->mode&CDESM_WR)){
      return -CMDERR_WRPERM;
    }
  }else if(opchar=='?'){
    if(!(des->mode&CDESM_RD)){
      return -CMDERR_RDPERM;
    }
  }
  else return -CMDERR_OPCHAR;
  return opchar;
}

int cmd_num_suffix(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[], unsigned *pval)
{
  unsigned val=0;
  unsigned x;
  char *p;
  p=param[1];
  if(!p) return -CMDERR_BADSUF;

  do{
    x=*p-'0';
    if((unsigned)x>9)
      return -CMDERR_BADSUF;
    val*=10;
    val+=x;
    p++;
  }while(*p && !strchr(" :?=",*p));
  *pval=val;
  return 0;  
}


int cmd_do_stamp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  cmd_io_write(cmd_io,param[3],strlen(param[3]));
  return 0; 
}

/**
 * Implementation of a command that reads or writes short pointed by des->info[0].
 */
int cmd_do_rw_short(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  short val;
  short *ptr;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  ptr=(short*)(des->info[0]);
  if(opchar==':'){
    val=atoi(param[3]);
    *ptr=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0; 
}

/**
 * Implementation of a command that reads or writes int pointed by des->info[0].
 */
int cmd_do_rw_int(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  int *ptr;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  ptr=(int*)(des->info[0]);
  if(opchar==':'){
    val=atoi(param[3]);
    *ptr=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0; 
}


/**
 * Implementation of a command that reads or writes long pointed by des->info[0].
 */
int cmd_do_rw_long(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  long *ptr;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  ptr=(long*)(des->info[0]);
  if(opchar==':'){
    val=atol(param[3]);
    *ptr=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, (long)*ptr, 0, 0);
  }
  return 0; 
}

/**
 * Prints name of the command followed by '=' and the value of val.
 */
int cmd_opchar_replong(cmd_io_t *cmd_io, char *param[], long val,int len,int form)
{
  char str[20];
  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  i2str(str,val,len,form);
  cmd_io_write(cmd_io,str,strlen(str));
  return 0;
}

#if 0

int cmd_do_rw_bitflag(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned val,mask,*pval;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  pval=(unsigned*)(des->info[0]);
  mask=(unsigned)(des->info[1]);
  if(opchar==':'){
    val=*param[3];
    if(val=='0')
      atomic_clear_mask(mask,pval);
    else if(val=='1')
      atomic_set_mask(mask,pval);
    else return -CMDERR_BADPAR;
  }else{
    cmd_io_write(cmd_io,param[0],param[2]-param[0]);
    cmd_io_putc(cmd_io,'=');
    cmd_io_putc(cmd_io,*pval&mask?'1':'0');
  }
  
  return 0;
}

#endif

/**
 * Implementation of help command
 */
int cmd_do_help(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *help;
  char *filt=param[1];
  const cmd_des_t **des_arr=*(const cmd_des_t ***)des->info[0];
  cmd_des_t const **arr_stack[CMD_ARR_STACK_SIZE];
  int arr_stack_sp=0;
  
  if(filt) {
    filt=skip_white(filt);
    if(!*filt) filt=NULL;
  }
  printf("Help for commands\n");
  while(1){
    des=*(des_arr++);
    if(!des){
      if(!arr_stack_sp) break;
      des_arr=arr_stack[--arr_stack_sp];
      continue;
    }
    if(des==(cmd_des_t*)CMD_DES_CONTINUE_AT_ID){
      /* list continues at new address */
      des_arr=(const cmd_des_t **)*des_arr;
      continue;
    }
    if(des==(cmd_des_t*)CMD_DES_INCLUDE_SUBLIST_ID){
      /* list includes commands from sublists */
      if(arr_stack_sp>=CMD_ARR_STACK_SIZE){
        des_arr++;
      }else{
        arr_stack[arr_stack_sp++]=des_arr+1;
        des_arr=(const cmd_des_t **)*des_arr;
        continue;
      }
    }
    if(des->name){
      if(!filt || !strncmp(des->name,filt,strlen(filt))) {
        help=des->help;
        if(!help) help="?";
	printf("  %s - %s\n",des->name,help);
      }
    }
  }
  return 0;
}
