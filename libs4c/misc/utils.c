/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  utils.c - utilities for reading numbers and parsing of text line
 
  Copyright (C) 2001-2003 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2003 by PiKRON Ltd. http://www.pikron.com


 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

/* string parse and input routines */

int si_skspace(char **ps)
{
  char *p=*ps;
  if (!isspace((uint8_t)*(p++))) return 0;
  while(isspace((uint8_t)*(p++)));
  *ps=p-1;
  return 1;
}

int si_fndsep(char **ps,char *sepchrs)
{
  char c;
  si_skspace(ps);
  c=**ps;
  if(!c) return 0;
  if(!strchr(sepchrs,c)) return -1;
  (*ps)++;
  return c;
}

int si_alnumn(char **ps,char *pout,int n)
{
  char *p=*ps;
  char c;
  int ret=0;
  while(isalnum((uint8_t)(c=*p))) {
    p++;
    *(pout++)=c;
    if(++ret==n) break;
  }
  *pout=0;
  *ps=p;
  return ret;
}

int si_alphan(char **ps,char *pout,int n)
{
  char *p=*ps;
  char c;
  int ret=0;
  while(isalpha((uint8_t)(c=*p))) {
    p++;
    *(pout++)=c;
    if(++ret==n) break;
  }
  *pout=0;
  *ps=p;
  return ret;
}

int si_long(char **ps,long *val,int base)
{
  char *p;
  *val=strtol(*ps,&p,base);
  if(*ps==p) return -1;
  *ps=p;
  return 1;
}

